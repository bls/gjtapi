package net.sourceforge.gjtapi.xtapi;

import java.time.Duration;

import net.xtapi.serviceProvider.IXTapiCallBack;
import net.xtapi.serviceProvider.MSTAPI;
import net.xtapi.serviceProvider.XTapiLineInfo;

public class GetLineInfoTest {
	public static void main(String[] args) throws Exception {
		
		MSTAPI mstapi = new MSTAPI();
		IXTapiCallBack callback = (dwDevice, dwMessage, dwInstance, dwParam1, dwParam2, dwParam3) -> {};
		int linesNum = mstapi.XTinit(callback, Duration.ofSeconds(10L));
		
		for (int i = 0; i < linesNum; i++) {
			XTapiLineInfo lineInfo = mstapi.XTGetLineInfo(i);
			System.out.println(lineInfo);
		}
		Thread.sleep(1000);
		mstapi.XTShutdown();
	}
}
