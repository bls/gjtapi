package net.sourceforge.gjtapi.raw.xtapi;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import net.sourceforge.gjtapi.TelephonyListener;

/**
 * @author haf
 */
@ExtendWith(MockitoExtension.class)
public class DelayedLineOfferingTest {
	
	private @Mock TelephonyListener listener;
	DelayedLineOffering lineOffering;
	
	@BeforeEach
	public void setUp() {
		lineOffering = new DelayedLineOffering();
	}
	
	@Test
	public void testIsCompleteIsTrueIfComplete() {
		initLineOffering();
		
		boolean isComplete = lineOffering.isComplete();
		
		assertTrue(isComplete, "isComplete returned false although all information is present");
	}
	
	@Test
	public void testIsCompleteIsFalseIfMissingCallId() {
		lineOffering.setCallInformation(new CallInformation("terminalName", "number"));
		boolean isComplete = lineOffering.isComplete();
		
		assertFalse(isComplete, "isComplete returned true without a CallId");
	}
	
	@Test
	public void testIsCompleteIsFalseIfMissingCallInformation() {
		lineOffering.setCallId(CallIdUtils.createCallId(42, "localAddress", "remoteAddress"));
		boolean isComplete = lineOffering.isComplete();
		
		assertFalse(isComplete, "isComplete returned true without CallInformation");
	}
	
	@Test
	public void testFireThrowsExceptionIfIncomplete() {
		Executable executable = () -> lineOffering.fire(listener);
		
		IllegalStateException exception = assertThrows(IllegalStateException.class, executable,
				"No exception thrown although an incomplete event was fired");
		assertTrue(exception.getMessage().contains("Missing information"), "Wrong exception thrown");
	}
	
	@Test
	public void testFireThrowsExceptionIfAlreadyFired() {
		initLineOffering();
		
		lineOffering.fire(listener);
		Executable executable = () -> lineOffering.fire(listener);
		
		IllegalStateException exception = assertThrows(IllegalStateException.class, executable,
				"No exception thrown although the same event was fired twice");
		assertTrue(exception.getMessage().contains("already fired"), "Wrong exception thrown");
	}
	
	@Test
	public void testFireSetsRemoteAddress() {
		CallInformation callInformation = new CallInformation("terminalName", "number");
		XtapiCallId callId = CallIdUtils.createCallId(42, "localAddress", "remoteAddress");
		
		lineOffering.setCallInformation(callInformation);
		lineOffering.setCallId(callId);
		lineOffering.fire(listener);
		
		assertEquals(callInformation.getNumber(), callId.getRemoteAddress(),
				"Remote address has not been updated when firing the event");
	}
	
	@Test
	public void testFireDoesNotFireEventsIfListenerIsNull() {
		initLineOffering();
		
		Executable executable = () -> lineOffering.fire(null);
		
		assertDoesNotThrow(executable, "Specifying a null listener should not cause an exception");
	}
	
	@Test
	public void testFireDoesNotFireConnectionEventIfRemoteNumberIsNull() {
		lineOffering.setCallInformation(new CallInformation("terminalName", null));
		lineOffering.setCallId(CallIdUtils.createCallId(42, "localAddress", "remoteAddress"));
		lineOffering.fire(listener);
		
		verify(listener, never()).connectionConnected(ArgumentMatchers.any(),
				ArgumentMatchers.any(), ArgumentMatchers.anyInt());
	}
	
	@Test
	public void testFireDoesNotFireTerminalConnectionEventIfTerminalNameIsNull() {
		lineOffering.setCallInformation(new CallInformation(null, "number"));
		lineOffering.setCallId(CallIdUtils.createCallId(42, "localAddress", "remoteAddress"));
		lineOffering.fire(listener);
		
		verify(listener, never()).terminalConnectionTalking(ArgumentMatchers.any(),
				ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.anyInt());
	}
	
	@Test
	public void testFireFiresConnectionAndTerminalConnectionEvent() {
		CallInformation callInformation = new CallInformation("terminalName", "number");
		XtapiCallId callId = CallIdUtils.createCallId(42, "localAddress", "remoteAddress");
		
		lineOffering.setCallInformation(callInformation);
		lineOffering.setCallId(callId);
		lineOffering.fire(listener);
		
		verify(listener).connectionConnected(ArgumentMatchers.same(callId),
				ArgumentMatchers.same(callInformation.getNumber()), ArgumentMatchers.anyInt());
		verify(listener).terminalConnectionTalking(ArgumentMatchers.same(callId),
				ArgumentMatchers.same(callInformation.getNumber()),
				ArgumentMatchers.same(callInformation.getTerminalName()),
				ArgumentMatchers.anyInt());
	}
	
	private void initLineOffering() {
		lineOffering.setCallInformation(new CallInformation("terminalName", "number"));
		lineOffering.setCallId(CallIdUtils.createCallId(42, "localAddress", "remoteAddress"));
	}
}
