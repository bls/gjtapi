package net.sourceforge.gjtapi.raw.invert;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;

import javax.telephony.Call;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import net.sourceforge.gjtapi.CallId;

/**
 * @author haf
 */
public class IdMapperTest {
	
	private IdMapper classUnderTest = new IdMapper();
	
	@Test
	public void testFreeIdReturnsNullIfNoCallWithTheSpecifiedIdExists() {
		CallId id = createCallId();
		
		Call call = classUnderTest.freeId(id);
		
		assertNull(call, "Returned a call not belonging to the specified call ID");
	}
	
	@Test
	public void testFreeIdReturnsAssociatedCall() {
		Call expectedCall = createCall();
		CallId id = classUnderTest.getId(expectedCall);
		
		Call call = classUnderTest.freeId(id);
		
		assertNotNull(call, "Did not return a call");
		assertSame(expectedCall, call, "Wrong call");
	}
	
	@Test
	public void testFreeIdReturnsNullIfFreeingTheSameCallMultipleTimes() {
		Call expectedCall = createCall();
		CallId id = classUnderTest.getId(expectedCall);
		
		classUnderTest.freeId(id);
		Call call = classUnderTest.freeId(id);
		
		assertNull(call, "Returned a call when freeing an already freed call");
	}
	
	@Test
	public void testGetIdReturnsUniqueForUnknownCalls() {
		Call call1 = createCall();
		Call call2 = createCall();
		Call call3 = createCall();
		
		CallId id1 = classUnderTest.getId(call1);
		CallId id2 = classUnderTest.getId(call2);
		CallId id3 = classUnderTest.getId(call3);
		
		assertNotNull(id1, "Did not return an ID for unknown call");
		assertNotNull(id2, "Did not return an ID for unknown call");
		assertNotNull(id3, "Did not return an ID for unknown call");
		assertNotEquals(id1, id2, "First and second ID are not unique");
		assertNotEquals(id1, id3, "First and third ID are not unique");
		assertNotEquals(id2, id3, "Second and third ID are not unique");
	}
	
	@Test
	public void testGetIdReturnsTheSameIdKnownCalls() {
		Call call = createCall();
		CallId expectedId = classUnderTest.getId(call);
		
		CallId id = classUnderTest.getId(call);
		
		assertNotNull(expectedId, "Returned no ID for unknown call");
		assertNotNull(id, "Returned no ID for known call");
		assertSame(expectedId, id, "First and second ID are not unique");
	}
	
	@Test
	public void testGetIdDoesNotReuseFreedIds() {
		Call call1 = createCall();
		Call call2 = createCall();
		CallId freedId = classUnderTest.getId(call1);
		classUnderTest.freeId(freedId);
		
		CallId id = classUnderTest.getId(call2);
		
		assertNotNull(id, "Returned no ID for unknown call");
		assertNotEquals(freedId, id, "Freed ID was reused");
	}
	
	@Test
	public void testJtapiCallReturnsNullForUnknownCall() {
		CallId callId = createCallId();
		
		Call call = classUnderTest.jtapiCall(callId);
		
		assertNull(call, "Returned a call not belonging to the specified call ID");
	}
	
	@Test
	public void testJtapiCallReturnsCorrectCall() {
		Call expectedCall = createCall();
		CallId id = classUnderTest.getId(expectedCall);
		
		Call call = classUnderTest.jtapiCall(id);
		
		assertNotNull(call, "Did not return a call");
		assertSame(expectedCall, call, "Wrong call");
	}
	
	private static CallId createCallId() {
		return Mockito.mock(CallId.class);
	}
	
	private static Call createCall() {
		return Mockito.mock(Call.class);
	}
}
