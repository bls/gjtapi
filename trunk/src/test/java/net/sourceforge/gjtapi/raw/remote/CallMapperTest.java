package net.sourceforge.gjtapi.raw.remote;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import net.sourceforge.gjtapi.CallId;

/**
 * @author haf
 */
public class CallMapperTest {
	
	private CallMapper classUnderTest;
	
	@BeforeEach
	private void setUp() {
		classUnderTest = new CallMapper();
	}
	
	@Test
	public void testCallToIntReturnsUniqueHandlesForUnknownIds() {
		CallId id1 = new SerializableCallId(42);
		CallId id2 = new SerializableCallId(1337);
		CallId id3 = new SerializableCallId(12345);
		
		int handle1 = classUnderTest.callToInt(id1);
		int handle2 = classUnderTest.callToInt(id2);
		int handle3 = classUnderTest.callToInt(id3);
		
		assertNotEquals(handle1, handle2, "First and second handle are not unique");
		assertNotEquals(handle1, handle3, "First and third handle are not unique");
		assertNotEquals(handle2, handle3, "Second and third handle are not unique");
	}
	
	@Test
	public void testCallToIntReturnsCorrectHandlesForKnownId() {
		CallId id = new SerializableCallId(42);
		
		int handle1 = classUnderTest.callToInt(id);
		int handle2 = classUnderTest.callToInt(id);
		
		assertEquals(handle1, handle2, "Wrong handle");
	}
	
	@Test
	public void testFreeIdReturnsNullForUnknownId() {
		SerializableCallId serializableId = new SerializableCallId(42);
		
		CallId id = classUnderTest.freeId(serializableId);
		
		assertNull(id, "Returned a call ID not belonging to the specified serializable call ID");
	}
	
	@Test
	public void testFreeIdReturnsAssociatedId() {
		CallId expectedId = new SerializableCallId(42);
		SerializableCallId serializableId = classUnderTest.swapId(expectedId);
		
		CallId id = classUnderTest.freeId(serializableId);
		
		assertNotNull(id, "Did not return a call ID");
		assertSame(expectedId, id, "Wrong call ID");
	}
	
	@Test
	public void testFreeIdReturnsNullIfFreeingTheSameCallMultipleTimes() {
		CallId expectedId = new SerializableCallId(42);
		SerializableCallId serializableId = classUnderTest.swapId(expectedId);
		
		classUnderTest.freeId(serializableId);
		CallId id = classUnderTest.freeId(serializableId);
		
		assertNull(id, "Returned a call ID when freeing an already freed call ID");
	}
	
	@Test
	public void testIntToCallReturnsNullForUnknownHandle() {
		CallId id = classUnderTest.intToCall(0);
		
		assertNull(id, "Returned a call ID not belonging to the specified handle");
	}
	
	@Test
	public void testIntToCallReturnsCorrectIdForKnownHandle() {
		CallId expectedId = new SerializableCallId(42);
		SerializableCallId serializableId = classUnderTest.swapId(expectedId);
		
		CallId id = classUnderTest.intToCall(serializableId.getId());
		
		assertNotNull(id, "Did not return a call ID");
		assertSame(expectedId, id, "Wrong call ID");
	}
	
	@Test
	public void testProviderIdReturnsNullForUnknownId() {
		SerializableCallId serializableId = new SerializableCallId(42);
		
		CallId id = classUnderTest.providerId(serializableId);
		
		assertNull(id, "Returned a call ID not belonging to the specified serializalbe call ID");
	}
	
	@Test
	public void testProviderIdReturnsCorrectIdForKnownId() {
		CallId expectedId = new SerializableCallId(42);
		SerializableCallId serializableId = classUnderTest.swapId(expectedId);
		
		CallId id = classUnderTest.providerId(serializableId);
		
		assertNotNull(id, "Did not return a call ID");
		assertSame(expectedId, id, "Wrong call ID");
	}
	
	@Test
	public void testSwapIdRegistersNewId() {
		CallId expectedId = new SerializableCallId(42);
		
		SerializableCallId serializableId = classUnderTest.swapId(expectedId);
		CallId id = classUnderTest.providerId(serializableId);
		
		assertNotNull(id, "Did not return a provider ID");
		assertSame(expectedId, id, "Wrong call ID");
	}
	
	@Test
	public void testSwapIdReturnsUniqueIdsForUnknownIds() {
		CallId id1 = new SerializableCallId(42);
		CallId id2 = new SerializableCallId(1337);
		CallId id3 = new SerializableCallId(12345);
		
		SerializableCallId serializableId1 = classUnderTest.swapId(id1);
		SerializableCallId serializableId2 = classUnderTest.swapId(id2);
		SerializableCallId serializableId3 = classUnderTest.swapId(id3);
		
		assertNotNull(serializableId1, "Did not returned a serializable call ID for unknown call ID");
		assertNotNull(serializableId2, "Did not returned a serializable call ID for unknown call ID");
		assertNotNull(serializableId3, "Did not returned a serializable call ID for unknown call ID");
		assertNotEquals(serializableId1, serializableId2, "First and second ID are not unique");
		assertNotEquals(serializableId1, serializableId3, "First and third ID are not unique");
		assertNotEquals(serializableId2, serializableId3, "Second and third ID are not unique");
	}
	
	@Test
	public void testSwapIdForKnownIdsReturnsTheSameObject() {
		CallId id = new SerializableCallId(42);
		
		SerializableCallId serializableId1 = classUnderTest.swapId(id);
		SerializableCallId serializableId2 = classUnderTest.swapId(id);
		
		assertNotNull(serializableId1, "Did not return a serializable call ID");
		assertNotNull(serializableId2, "Did not return a serializable call ID");
		assertSame(serializableId1, serializableId2,
				"Did not return the same serializable call ID on subsequent invokations");
	}
	
	@Test
	public void testSwapIdDoesNotReuseFreedIds() {
		CallId id1 = new SerializableCallId(42);
		CallId id2 = new SerializableCallId(1337);
		SerializableCallId freedSerializableId = classUnderTest.swapId(id1);
		classUnderTest.freeId(freedSerializableId);
		
		SerializableCallId serializableId = classUnderTest.swapId(id2);
		
		assertNotNull(serializableId, "Did not return a serializable call ID for unknown call ID");
		assertNotEquals(freedSerializableId, serializableId, "Freed serializable call ID was reused");
	}
}
