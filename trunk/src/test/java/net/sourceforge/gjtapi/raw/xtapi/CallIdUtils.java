package net.sourceforge.gjtapi.raw.xtapi;

/**
 * @author haf
 */
class CallIdUtils {
	
	static XtapiCallId createCallId(int callHandle, String localAddress, String remoteAddress) {
		XtapiCallId callId = new XtapiCallId();
		callId.setCallNum(callHandle);
		callId.setLocalAddress(localAddress);
		callId.setRemoteAddress(remoteAddress);
		
		return callId;
	}
	
	private CallIdUtils() {
	}
}
