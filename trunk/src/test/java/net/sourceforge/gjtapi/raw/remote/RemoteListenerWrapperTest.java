package net.sourceforge.gjtapi.raw.remote;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;

import java.rmi.RemoteException;
import java.time.Duration;
import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import net.sourceforge.gjtapi.CallId;

/**
 * @author haf
 */
@ExtendWith(MockitoExtension.class)
public class RemoteListenerWrapperTest {
	
	private @Mock RemoteListener remoteListener;
	private RemoteListenerWrapper listenerWrapper;
	private @Mock CallMapper callMapper;
	private final CallId callId = new CallId() {};
	
	@BeforeEach
	public void setUp() {
		listenerWrapper = new RemoteListenerWrapper(remoteListener, callMapper);
	}
	
	@Test
	public void testExceptionStatisticIsInitializedAsZero() {
		assertEquals(0, listenerWrapper.getUnreachableCount(),
				"The unreachable count is not initialized as 0");
		assertEquals(Duration.ZERO, listenerWrapper.getUnreachableDuration(),
				"The unreachable duration is not initialized as 0");
	}
	
	@Test
	public void testFirstRemoteExceptionStartsExceptionStatisticMeasuring() throws RemoteException {
		mockCallActiveEventToThrowExceptions();
		
		listenerWrapper.callActive(callId, 0);
		
		assertEquals(1, listenerWrapper.getUnreachableCount(),
				"The unreachable count has not been set to 1");
		assertEquals(Duration.ZERO, listenerWrapper.getUnreachableDuration(),
				"The unreachable duration has not been set to 0");
	}
	
	@ParameterizedTest
	@ValueSource(ints = {2, 3, 10, 50})
	public void testRemoteExceptionUpdatesExceptionStatistic(int numEvents) throws RemoteException {
		mockCallActiveEventToThrowExceptions();
		
		LocalDateTime earlyStartEstimate = LocalDateTime.now();
		listenerWrapper.callActive(callId, 0);
		LocalDateTime lateStartEstimate = LocalDateTime.now();
		for (int i = 2; i < numEvents; ++i) {
			listenerWrapper.callActive(callId, 0);
		}
		LocalDateTime earlyEndEstimate = LocalDateTime.now();
		listenerWrapper.callActive(callId, 0);
		LocalDateTime lateEndEstimate = LocalDateTime.now();
		
		assertEquals(numEvents, listenerWrapper.getUnreachableCount(),
				"The unreachable count has not been increased correctly");
		Duration lowerDurationLimit = Duration.between(lateStartEstimate, earlyEndEstimate);
		assertTrue(listenerWrapper.getUnreachableDuration().compareTo(lowerDurationLimit) >= 0,
				"The unreachable duration is too short");
		Duration upperDurationLimit = Duration.between(earlyStartEstimate, lateEndEstimate);
		assertTrue(listenerWrapper.getUnreachableDuration().compareTo(upperDurationLimit) <= 0,
				"The unreachable duration is too long");
	}
	
	@Test
	public void testSuccessfullyFiredRemoteEventResetsExceptionStatistic() throws RemoteException {
		initRemoteListenerWrapperWithOneException();
		
		listenerWrapper.callActive(callId, 0);
		
		assertEquals(0, listenerWrapper.getUnreachableCount(),
				"The unreachable count not been reset");
		assertEquals(Duration.ZERO, listenerWrapper.getUnreachableDuration(),
				"The unreachable duration has not been reset");
	}
	
	private void initRemoteListenerWrapperWithOneException() throws RemoteException {
		mockCallActiveEventToThrowExceptions();
		listenerWrapper.callActive(callId, 0);
		doNothing().when(remoteListener).callActive(ArgumentMatchers.any(),
				ArgumentMatchers.anyInt());
	}
	
	private void mockCallActiveEventToThrowExceptions() throws RemoteException {
		doThrow(RemoteException.class).when(remoteListener).callActive(ArgumentMatchers.any(),
				ArgumentMatchers.anyInt());
	}
}
