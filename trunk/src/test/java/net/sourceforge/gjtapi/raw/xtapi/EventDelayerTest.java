package net.sourceforge.gjtapi.raw.xtapi;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import lombok.AllArgsConstructor;
import net.sourceforge.gjtapi.TelephonyListener;

/**
 * @author haf
 */
@ExtendWith(MockitoExtension.class)
public class EventDelayerTest {
	
	private @Mock TelephonyListener listener;
	private @Spy DelayedLineOfferingFactory factory;
	private EventDelayer delayer;
	
	@BeforeEach
	public void setUp() {
		delayer = new EventDelayer(listener, factory);
	}
	
	@Test
	public void testCanSetCallInformation() {
		DelayedLineOffering lineOffering = Mockito.spy(DelayedLineOffering.class);
		when(factory.create()).thenReturn(lineOffering);
		CallInformation callInfo = new CallInformation("Terminal0", "Address0");
		int callHandle = 42;
		
		delayer.addCallInformation(callHandle, callInfo);
		
		verify(lineOffering).setCallInformation(callInfo);
	}
	
	@Test
	public void testCanSetCallId() {
		DelayedLineOffering lineOffering = Mockito.spy(DelayedLineOffering.class);
		when(factory.create()).thenReturn(lineOffering);
		int callHandle = 42;
		XtapiCallId callId = CallIdUtils.createCallId(callHandle, "localAddress0", "UNKNOWN");
		
		delayer.addCallId(callHandle, callId);
		
		verify(lineOffering).setCallId(callId);
	}
	
	@Test
	public void testCallingSetCallInformationCreatesOneEntryPerCallHandle() {
		CallHandleCallInformationPair[] pairs = {
			new CallHandleCallInformationPair(42, new CallInformation("42:1", "42:1")),
			new CallHandleCallInformationPair(42, new CallInformation("42:2", "42:2")),
			new CallHandleCallInformationPair(43, new CallInformation("43:1", "43:1")),
			new CallHandleCallInformationPair(44, new CallInformation("44:1", "44:1")),
			new CallHandleCallInformationPair(42, new CallInformation("42:3", "42:3")),
			new CallHandleCallInformationPair(44, new CallInformation("44:2", "44:2")),
			new CallHandleCallInformationPair(43, new CallInformation("43:2", "43:2"))
		};
		int numCallHandles = 3;
		
		for (CallHandleCallInformationPair pair : pairs) {
			delayer.addCallInformation(pair.callHandle, pair.info);
		}
		
		verify(factory, times(numCallHandles)).create();
	}
	
	
	@Test
	public void testCallingSetCallIdCreatesOneEntryPerCallHandle() {
		XtapiCallId[] callIds = {
			CallIdUtils.createCallId(42, "42:1", "UNKNOWN"),
			CallIdUtils.createCallId(42, "42:2", "UNKNOWN"),
			CallIdUtils.createCallId(43, "43:1", "UNKNOWN"),
			CallIdUtils.createCallId(44, "44:1", "UNKNOWN"),
			CallIdUtils.createCallId(42, "42:3", "UNKNOWN"),
			CallIdUtils.createCallId(44, "44:2", "UNKNOWN"),
			CallIdUtils.createCallId(43, "42:2", "UNKNOWN")
		};
		int numCallHandles = 3;
		
		for (XtapiCallId callId : callIds) {
			delayer.addCallId(callId.getCallNum(), callId);
		}
		
		verify(factory, times(numCallHandles)).create();
	}
	
	@ParameterizedTest
	@ValueSource(booleans = {true, false})
	public void testEventIsFiredOnCompletion(boolean isCallIdSetFirst) {
		DelayedLineOffering lineOffering = Mockito.spy(DelayedLineOffering.class);
		when(factory.create()).thenReturn(lineOffering);
		int callHandle = 42;
		XtapiCallId callId = CallIdUtils.createCallId(callHandle, "localAddress0", "UNKNOWN");
		CallInformation callInfo = new CallInformation("remoteTerminal0", "remoteAddress0");
		
		if (isCallIdSetFirst) {
			delayer.addCallId(callHandle, callId);
			delayer.addCallInformation(callHandle, callInfo);
		}
		else {
			delayer.addCallInformation(callHandle, callInfo);
			delayer.addCallId(callHandle, callId);
		}
		
		verify(lineOffering).fire(listener);
	}
	
	@Test
	public void testEventIsNotFiredWhileIncomplete() {
		DelayedLineOffering firstLineOffering = Mockito.spy(DelayedLineOffering.class);
		DelayedLineOffering lastLineOffering = Mockito.spy(DelayedLineOffering.class);
		when(factory.create()).thenReturn(firstLineOffering, lastLineOffering);
		
		delayer.addCallId(42, CallIdUtils.createCallId(42, "localAddress0", "UNKNOWN"));
		delayer.addCallInformation(43, new CallInformation("remoteTerminal0", "remoteAddress0"));
		
		verify(firstLineOffering, never()).fire(listener);
		verify(lastLineOffering, never()).fire(listener);
	}
	
	@Test
	public void testRemoveErasesEntry() {
		DelayedLineOffering firstLineOffering = new DelayedLineOffering();
		DelayedLineOffering lastLineOffering = new DelayedLineOffering();
		when(factory.create()).thenReturn(firstLineOffering, lastLineOffering);
		int callHandle = 42;
		
		delayer.addCallInformation(callHandle, new CallInformation("Terminal1", "Address1"));
		delayer.remove(callHandle);
		delayer.addCallInformation(callHandle, new CallInformation("Terminal2", "Address2"));
		
		verify(factory, times(2)).create();
	}
	
	@AllArgsConstructor
	private static class CallHandleCallInformationPair {
		
		private int callHandle;
		private CallInformation info;
	}
}
