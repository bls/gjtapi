package net.sourceforge.gjtapi.raw.xtapi;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Map;

import javax.telephony.Connection;
import javax.telephony.InvalidStateException;
import javax.telephony.MethodNotSupportedException;
import javax.telephony.ResourceUnavailableException;
import javax.telephony.TerminalConnection;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.EnumSource.Mode;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import net.sourceforge.gjtapi.LineCallInfoState;
import net.sourceforge.gjtapi.LineError;
import net.sourceforge.gjtapi.RawStateException;
import net.xtapi.serviceProvider.IXTapi;
import net.xtapi.serviceProvider.IXTapiCallBack;
import net.xtapi.serviceProvider.XTapiFactory;
import net.xtapi.serviceProvider.XTapiLineInfo;
import net.xtapi.serviceProvider.XTapiLineInfoUtils;

/**
 * @author haf
 */
@ExtendWith(MockitoExtension.class)
public class XtapiProviderTest {
	
	private XtapiProvider provider = new XtapiProvider();
	@Mock private IXTapi tapi;
	private final XTapiFactory tapiFactory = () -> tapi;
	
	@BeforeEach
	public void setUp() throws ResourceUnavailableException, InvalidStateException {
		initProviderWithLines(
				XTapiLineInfoUtils.createXTapiLineInfo(1, "localLine1", "localAddress1"),
				XTapiLineInfoUtils.createXTapiLineInfo(2, "localLine2", "localAddress2"));
	}
	
	private void initProviderWithZeroLines() {
		when(tapi.XTinit(any(), any())).thenReturn(0);
		initProvider();
	}
	
	private void initProviderWithLines(XTapiLineInfo firstLine, XTapiLineInfo... followingLines) throws ResourceUnavailableException, InvalidStateException {
		int numLines = 1 + followingLines.length;
		when(tapi.XTinit(any(), any())).thenReturn(numLines);
		when(tapi.XTGetLineInfo(anyInt())).thenReturn(firstLine, followingLines);
		when(tapi.XTOpenLine(anyInt())).then(returnsFirstArg());
		
		initProvider();
	}
	
	private void initProvider() {
		Map<String, Object> properties =
				Collections.singletonMap(XTapiProviderInitParameter.FACTORY.getKey(), tapiFactory);
		provider.initialize(properties);
	}
	
	@Test
	public void testAnswerCallRedirectsToTapi() throws ResourceUnavailableException, InvalidStateException {
		when(tapi.XTAnswerCall(anyInt())).thenReturn(0);
		XtapiCallId callId = CallIdUtils.createCallId(42, "localAddress", "remoteAddress");
		
		Executable executable = () -> provider.answerCall(callId, "localAddress", "localTerminal");
		
		assertDoesNotThrow(executable,
				"An exception was thrown although the TAPI returned no error code");
		verify(tapi).XTAnswerCall(anyInt());
	}
	
	@ParameterizedTest
	@EnumSource(LineError.class)
	public void testAnswerCallThrowsExceptionOnTapiError(LineError lineError) throws ResourceUnavailableException, InvalidStateException {
		when(tapi.XTAnswerCall(anyInt())).thenReturn(lineError.getBitValue());
		XtapiCallId callId = CallIdUtils.createCallId(42, "localAddress", "remoteAddress");
		
		Executable executable = () -> provider.answerCall(callId, "localAddress", "localTerminal");
		
		RawStateException exception = assertThrows(RawStateException.class, executable,
				"No exception was thrown when the TAPI returned an error code");
		assertSame(exception.getCall(), callId, "The wrong call was the cause for the exception");
		assertEquals(exception.getState(), TerminalConnection.UNKNOWN,
				"The exception did not have the expected state");
	}
	
	@Test
	public void testReleaseCallIdUsesTapi() throws ResourceUnavailableException, InvalidStateException {
		when(tapi.XTDropCall(anyInt())).thenReturn(0);
		XtapiCallId callId = CallIdUtils.createCallId(42, "localAddress", "remoteAddress");
		
		Executable executable = () -> provider.releaseCallId(callId);
		
		assertDoesNotThrow(executable,
				"An exception was thrown although the TAPI returned no error code");
		verify(tapi).XTDropCall(anyInt());
	}
	
	@ParameterizedTest
	@EnumSource(LineError.class)
	public void testReleaseCallIdIgnoredTapiErrors(LineError lineError) throws ResourceUnavailableException, InvalidStateException {
		when(tapi.XTDropCall(anyInt())).thenReturn(lineError.getBitValue());
		XtapiCallId callId = CallIdUtils.createCallId(42, "localAddress", "remoteAddress");
		
		Executable executable = () -> provider.releaseCallId(callId);
		
		assertDoesNotThrow(executable, "This method should never throw an error");
	}
	
	@ParameterizedTest
	@EnumSource(value = LineCallInfoState.class, mode = Mode.EXCLUDE,
			names = {"CALLED_ID", "CALLER_ID"})
	public void testCallbackDoesNotRequestCallInfoIfNoPartyIdChanges(LineCallInfoState infoState) throws MethodNotSupportedException, ResourceUnavailableException, InvalidStateException {
		int callHandle = 42;
		int lineKey = 1337;
		
		provider.callback(callHandle, IXTapiCallBack.LINE_CALLINFO, lineKey,
				infoState.getBitValue(), 0, 0);
		
		verify(tapi, never()).XTGetCallInfo(callHandle);
	}
	
	@ParameterizedTest
	@EnumSource(value = LineCallInfoState.class, names = {"CALLED_ID", "CALLER_ID"})
	public void testCallbackRequestsCallInfoIfPartyIdChanges(LineCallInfoState infoState) throws MethodNotSupportedException, ResourceUnavailableException, InvalidStateException {
		int callHandle = 42;
		int lineKey = 1337;
		
		provider.callback(callHandle, IXTapiCallBack.LINE_CALLINFO, lineKey,
				infoState.getBitValue(), 0, 0);
		
		verify(tapi).XTGetCallInfo(callHandle);
	}
	
	@Test
	public void testReleaseUsesTapi() throws ResourceUnavailableException, InvalidStateException {
		when(tapi.XTDropCall(anyInt())).thenReturn(0);
		XtapiCallId callId = CallIdUtils.createCallId(42, "localAddress1", "remoteAddress");
		
		Executable executable = () -> provider.release("localAddress1", callId);
		
		assertDoesNotThrow(executable,
				"An exception was thrown although the TAPI returned no error code");
		verify(tapi).XTDropCall(anyInt());
	}
	
	@ParameterizedTest
	@EnumSource(LineError.class)
	public void testReleaseThrowsExceptionOnTapiError(LineError lineError) throws ResourceUnavailableException, InvalidStateException {
		when(tapi.XTDropCall(anyInt())).thenReturn(lineError.getBitValue());
		XtapiCallId callId = CallIdUtils.createCallId(42, "localAddress1", "remoteAddress");
		
		Executable executable = () -> provider.release("localAddress1", callId);
		
		RawStateException exception = assertThrows(RawStateException.class, executable,
				"No exception was thrown when the TAPI returned an error code");
		assertSame(exception.getCall(), callId, "The wrong call was the cause for the exception");
		assertEquals(exception.getState(), Connection.UNKNOWN,
				"The exception did not have the expected state");
	}
	
	@Test
	public void testReleaseThrowsExceptionOnInvalidStateException() throws ResourceUnavailableException, InvalidStateException {
		int state = Connection.FAILED;
		Exception stateException = new InvalidStateException(null, 0, state);
		when(tapi.XTDropCall(anyInt())).thenThrow(stateException);
		XtapiCallId callId = CallIdUtils.createCallId(42, "localAddress1", "remoteAddress");
		
		Executable executable = () -> provider.release("localAddress1", callId);
		
		RawStateException exception = assertThrows(RawStateException.class, executable,
				"No exception was thrown when the TAPI threw an exception");
		assertSame(exception.getCall(), callId, "The wrong call was the cause for the exception");
		assertEquals(exception.getState(), state, "The exception did not have the expected state");
	}
	
	@Test
	public void testReleaseIgnoresInvalidArgumentException() throws ResourceUnavailableException, InvalidStateException {
		XtapiCallId callId = CallIdUtils.createCallId(42, "localAddress1", "remoteAddress");
		
		Executable executable = () -> provider.release("localAddress0", callId);
		
		assertDoesNotThrow(executable, "InvalidArgumentExceptions should be ignored");
		verify(tapi, never()).XTDropCall(anyInt());
	}
	
	@Test
	public void testReleaseDoesNotUseTapiWithoutOpenedLines() throws ResourceUnavailableException, InvalidStateException {
		initProviderWithZeroLines();
		XtapiCallId callId = CallIdUtils.createCallId(42, "localAddress1", "remoteAddress");
		
		provider.release("localAddress1", callId);
		
		verify(tapi, never()).XTDropCall(anyInt());
	}
}
