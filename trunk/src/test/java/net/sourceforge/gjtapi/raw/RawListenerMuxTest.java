package net.sourceforge.gjtapi.raw;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.time.Duration;
import java.util.stream.Stream;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import net.sourceforge.gjtapi.CallId;
import net.sourceforge.gjtapi.raw.remote.RemoteListenerWrapper;

/**
 * @author haf
 */
public class RawListenerMuxTest {
	
	@ParameterizedTest
	@ArgumentsSource(AllowedExceptionSettingsProvider.class)
	public void testRemoteListenersAreRemovedIfExceedingSettings(int allowedCount, long allowedSeconds) {
		RemoteListenerWrapper wrapper = Mockito.mock(RemoteListenerWrapper.class);
		when(wrapper.getUnreachableCount()).thenReturn(allowedCount + 1);
		when(wrapper.getUnreachableDuration()).thenReturn(Duration.ofSeconds(allowedSeconds + 1));
		RawListenerMux rawListenerMux = initRawListenerMus(wrapper, allowedCount, allowedSeconds);
		CallId callId = new CallId() {};
		
		rawListenerMux.callActive(callId, 0);
		rawListenerMux.callActive(callId, 0);
		
		Mockito.verify(wrapper).callActive(ArgumentMatchers.any(), ArgumentMatchers.anyInt());
	}
	
	@ParameterizedTest
	@ArgumentsSource(AllowedExceptionSettingsProvider.class)
	public void testRemoteListenersAreNotRemovedIfOnlyExceedingExceptionCount(int allowedCount, long allowedSeconds) {
		RemoteListenerWrapper wrapper = Mockito.mock(RemoteListenerWrapper.class);
		when(wrapper.getUnreachableCount()).thenReturn(allowedCount - 1);
		when(wrapper.getUnreachableDuration()).thenReturn(Duration.ofSeconds(allowedSeconds + 1));
		RawListenerMux rawListenerMux = initRawListenerMus(wrapper, allowedCount, allowedSeconds);
		CallId callId = new CallId() {};
		
		rawListenerMux.callActive(callId, 0);
		rawListenerMux.callActive(callId, 0);
		
		Mockito.verify(wrapper, times(2)).callActive(ArgumentMatchers.any(), ArgumentMatchers.anyInt());
	}
	
	@ParameterizedTest
	@ArgumentsSource(AllowedExceptionSettingsProvider.class)
	public void testRemoteListenersAreNotRemovedIfOnlyExceedingExceptionDuration(int allowedCount, long allowedSeconds) {
		RemoteListenerWrapper wrapper = Mockito.mock(RemoteListenerWrapper.class);
		when(wrapper.getUnreachableCount()).thenReturn(allowedCount + 1);
		when(wrapper.getUnreachableDuration()).thenReturn(Duration.ofSeconds(allowedSeconds - 1));
		RawListenerMux rawListenerMux = initRawListenerMus(wrapper, allowedCount, allowedSeconds);
		CallId callId = new CallId() {};
		
		rawListenerMux.callActive(callId, 0);
		rawListenerMux.callActive(callId, 0);
		
		Mockito.verify(wrapper, times(2)).callActive(ArgumentMatchers.any(), ArgumentMatchers.anyInt());
	}
	
	private static RawListenerMux initRawListenerMus(RemoteListenerWrapper wrapper, int allowedCount, long allowedSeconds) {
		RawListenerMux rawListenerMux = new RawListenerMux(allowedCount, allowedSeconds);
		rawListenerMux.addListener(wrapper);
		return rawListenerMux;
	}
	
	/**
	 * @author haf
	 */
	public static class AllowedExceptionSettingsProvider implements ArgumentsProvider {
		
		@Override
		public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
			return Stream.of(Arguments.of(0, 0), Arguments.of(1, 1), Arguments.of(20, 10));
		}
	}
}
