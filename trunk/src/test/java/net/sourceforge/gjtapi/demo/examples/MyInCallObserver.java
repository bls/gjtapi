package net.sourceforge.gjtapi.demo.examples;
import javax.telephony.*;
import javax.telephony.events.*;

import javax.telephony.*;
import javax.telephony.events.*;

/*
 * The MyInCallObserver class implements the CallObserver and
 * recieves all Call-related events.
 */
 
public class MyInCallObserver implements CallObserver {
 
  public void callChangedEvent(CallEv[] evlist) {
    TerminalConnection termconn;
    String name;
    System.out.println("MyInCallObserver.callChangedEvent");
    for (int i = 0; i < evlist.length; i++) {
 
      if (evlist[i] instanceof TermConnEv) {
        termconn = null;
        name = null;
 
        try {
          TermConnEv tcev = (TermConnEv)evlist[i];
          termconn = tcev.getTerminalConnection();
          Terminal term = termconn.getTerminal();
          name = term.getName();
        } catch (Exception excp) {
          // Handle exceptions.
            System.out.println("Exception in MyInCallObserver: " + excp.toString()) ;
        }

        String msg = "TerminalConnection to Terminal: " + name + " is ";
 
        if (evlist[i].getID() == TermConnActiveEv.ID) {
          System.out.println(msg + "ACTIVE");
        }
        else if (evlist[i].getID() == TermConnRingingEv.ID) {
          System.out.println(msg + "RINGING");
 
          /* Answer the telephone Call using "inner class" thread */
          try {
		final TerminalConnection _tc = termconn;
	     	Runnable r = new Runnable() {
		  public void run(){
		    try{
				_tc.answer();
		    } catch (Exception excp){
		      // handle answer exceptions
                        System.out.println(excp.toString());
		    }
			};
		
		};
		Thread T = new Thread(r);
		T.start();
          } catch (Exception excp) {
            // Handle Exceptions;
              System.out.println("Exception in MyInCallObserver: " + excp.toString()) ;
          }
        } else if (evlist[i].getID() == TermConnDroppedEv.ID) {
          System.out.println(msg + "DROPPED");
        }
      }  
    }
  }
}

