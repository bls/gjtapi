package net.sourceforge.gjtapi.demo.examples;

import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Map;

import javax.telephony.Address;
import javax.telephony.Call;
import javax.telephony.CallEvent;
import javax.telephony.CallListener;
import javax.telephony.CallObserver;
import javax.telephony.Connection;
import javax.telephony.ConnectionEvent;
import javax.telephony.ConnectionListener;
import javax.telephony.InvalidArgumentException;
import javax.telephony.InvalidPartyException;
import javax.telephony.InvalidStateException;
import javax.telephony.JtapiPeer;
import javax.telephony.JtapiPeerFactory;
import javax.telephony.JtapiPeerUnavailableException;
import javax.telephony.MetaEvent;
import javax.telephony.MethodNotSupportedException;
import javax.telephony.PrivilegeViolationException;
import javax.telephony.Provider;
import javax.telephony.ResourceUnavailableException;
import javax.telephony.Terminal;
import javax.telephony.TerminalEvent;
import javax.telephony.TerminalListener;
import javax.telephony.callcontrol.events.CallCtlTermConnTalkingEv;
import javax.telephony.events.CallActiveEv;
import javax.telephony.events.CallEv;
import javax.telephony.events.CallInvalidEv;
import javax.telephony.events.CallObservationEndedEv;
import javax.telephony.events.ConnAlertingEv;
import javax.telephony.events.ConnConnectedEv;
import javax.telephony.events.ConnCreatedEv;
import javax.telephony.events.ConnDisconnectedEv;
import javax.telephony.events.ConnEv;
import javax.telephony.events.ConnFailedEv;
import javax.telephony.events.ConnInProgressEv;
import javax.telephony.events.ConnUnknownEv;
import javax.telephony.events.TermConnActiveEv;
import javax.telephony.events.TermConnCreatedEv;
import javax.telephony.events.TermConnDroppedEv;
import javax.telephony.events.TermConnEv;
import javax.telephony.events.TermConnPassiveEv;
import javax.telephony.events.TermConnRingingEv;
import javax.telephony.events.TermConnUnknownEv;
import javax.telephony.media.events.MediaTermConnAvailableEv;
import javax.telephony.media.events.MediaTermConnUnavailableEv;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import net.sourceforge.gjtapi.test.TestIncomingCallListenerObserver;

/*
 * Create a provider and monitor a particular terminal for an incoming call.
 */
public class InitCallThroughRmiProvider {
	
	private static final Log log = LogFactory.getLog(InitCallThroughRmiProvider.class); 

	public static final void main(String args[]) throws JtapiPeerUnavailableException, ResourceUnavailableException,
			MethodNotSupportedException, InvalidArgumentException, PrivilegeViolationException, InvalidPartyException,
			InvalidStateException, InterruptedException {

		
		JtapiPeer peer = JtapiPeerFactory.getJtapiPeer(null);
		Provider provider = peer.getProvider("Rmi;server=d3s-bls-tpi;port=1099;name=Xtapi");

		Address myAddress = provider.getAddress("addr_4");
		Terminal myTerminal = myAddress.getTerminals()[0];
		System.out.println(myAddress.getName());
		System.out.println(myTerminal.getName());
		//myAddress.addCallListener(new MyConnectionListener());

		
//		Call call = provider.createCall();
//		call.connect(myTerminal, myAddress, "0017699771289");


		Thread.sleep(10000);
		provider.shutdown();
	}
	private static class MyConnectionListener implements ConnectionListener {

		@Override
		public void callActive(CallEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void callInvalid(CallEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void callEventTransmissionEnded(CallEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void singleCallMetaProgressStarted(MetaEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void singleCallMetaProgressEnded(MetaEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void singleCallMetaSnapshotStarted(MetaEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void singleCallMetaSnapshotEnded(MetaEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void multiCallMetaMergeStarted(MetaEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void multiCallMetaMergeEnded(MetaEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void multiCallMetaTransferStarted(MetaEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void multiCallMetaTransferEnded(MetaEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void connectionAlerting(ConnectionEvent event) {
			printConnEvent("connectionAlerting", event);
		}

		@Override
		public void connectionConnected(ConnectionEvent event) {
			printConnEvent("connectionConnected", event);			
		}

		@Override
		public void connectionCreated(ConnectionEvent event) {
			printConnEvent("connectionCreated", event);			
		}

		@Override
		public void connectionDisconnected(ConnectionEvent event) {
			printConnEvent("connectionDisconnected", event);			
			
		}

		@Override
		public void connectionFailed(ConnectionEvent event) {
			printConnEvent("connectionFailed", event);
			
		}

		@Override
		public void connectionInProgress(ConnectionEvent event) {
			printConnEvent("connectionInProgress", event);			
		}

		@Override
		public void connectionUnknown(ConnectionEvent event) {
			printConnEvent("connectionUnkown", event);			
		}
		
		private void printConnEvent(String state, ConnectionEvent event)
		{
			MetaEvent metaEvent = event.getMetaEvent();
			log.info((String.format("%s, cause=%s, conn=%s, call=%s", state, event.getCause(), event.getConnection().toString(), event.getCall())));
		}
	}

	private static class MyListener implements CallObserver {
		@Override
		public void callChangedEvent(CallEv[] eventList) {
			for (int i = 0; i < eventList.length; i++) {
				CallEv ce = eventList[i];
				Call c = ce.getCall();
				StringBuilder builder = new StringBuilder(i);
				builder.append(i).append(' ');				
				switch (ce.getID()) {
				case CallActiveEv.ID:
					builder.append("Call Active");
					break;
				case CallInvalidEv.ID:
					builder.append("Call Invalid");
					break;
				case CallObservationEndedEv.ID:
					builder.append("Call Observation Ended");
					break;
				case ConnAlertingEv.ID:
					builder.append("Connection Alerting");
					break;
				case ConnConnectedEv.ID:
					builder.append("Connection Connected");
					break;
				case ConnCreatedEv.ID:
					builder.append("Connection Created");
					break;
				case ConnDisconnectedEv.ID:
					builder.append("Connection Disconnected");
					break;
				case ConnFailedEv.ID:
					builder.append("Connection Failed");
					break;
				case ConnInProgressEv.ID:
					builder.append("Connection In Progress");
					break;
				case ConnUnknownEv.ID:
					builder.append("Connection Unknown");
					break;
				case TermConnActiveEv.ID:
					builder.append("Terminal Connection Active");
					break;
				case TermConnCreatedEv.ID:
					builder.append("Terminal Connection Created");
					break;
				case TermConnDroppedEv.ID:
					builder.append("Terminal Connection Dropped");
					break;
				case TermConnPassiveEv.ID:
					builder.append("Terminal Connection Passive");
					break;
				case TermConnRingingEv.ID:
					builder.append("Terminal Connection Ringing");
					break;
				case TermConnUnknownEv.ID:
					builder.append("Terminal Connection Unknown");
					break;
				case CallCtlTermConnTalkingEv.ID:
					builder.append("Call Control Terminal Connection Talking");
					break;
				case MediaTermConnAvailableEv.ID:
					builder.append("Media Terminal Connection Available");
					break;
				case MediaTermConnUnavailableEv.ID:
					builder.append("Media Terminal Connection Unavailable");
					break;
				default:
					builder.append(ce.toString()).append(' ').append(ce.getID());
					break;
				}
				builder.append(", call = '" + c.toString()+"'");
				
				if (ce instanceof ConnEv)
				{
					builder.append(", conn = '" + ((ConnEv)ce).getConnection());
				}

				if (ce instanceof TermConnEv)
				{
					builder.append(", terminal conn = '" + ((TermConnEv)ce).getTerminalConnection());
				}
				log.info(builder.toString());
			}
		}
	}
}
