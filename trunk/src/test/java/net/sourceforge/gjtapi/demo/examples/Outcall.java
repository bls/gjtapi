package net.sourceforge.gjtapi.demo.examples;
import javax.telephony.Address;
import javax.telephony.Call;
import javax.telephony.Connection;
import javax.telephony.JtapiPeer;
import javax.telephony.JtapiPeerFactory;
import javax.telephony.Provider;
import javax.telephony.Terminal;


/*
 * Places a telephone call from 476111 to 5551212
 */
public class Outcall {
 
  public static final void main(String args[]) {
 
    /*
     * Create a provider by first obtaining the default implementation of
     * JTAPI and then the default provider of that implementation.
     */
    Provider myprovider = null;
    try {
      //JtapiPeer peer = JtapiPeerFactory.getJtapiPeer(null);
      JtapiPeer peer = JtapiPeerFactory.getJtapiPeer(
        "net.xtapi.XJtapiPeer");
      //myprovider = peer.getProvider(null);
      myprovider = peer.getProvider("Serial");
    } catch (Exception excp) {
      System.out.println("Can't get Provider: " + excp.toString());
      System.exit(0);
    }
 
   /*
    * We need to get the appropriate objects associated with the
    * originating side of the telephone call. We ask the Address for a list
    * of Terminals on it and arbitrarily choose one.
    */

    Address origaddr = null;
    Terminal origterm = null;
    try {
      origaddr = myprovider.getAddress("0");
 
      //Just get some Terminal on this Address
      Terminal[] terminals = origaddr.getTerminals();
      if (terminals == null) {
        System.out.println("No Terminals on Address.");
        System.exit(0);
      }  
      origterm = terminals[0];
    } catch (Exception excp) {
      // Handle exceptions;
        System.err.println(excp) ;
    }
 
    // Create the telephone call object and add an observer.
     Call mycall = null;
    try {
      mycall = myprovider.createCall();
      mycall.addObserver(new MyOutCallObserver());
    } catch (Exception excp) {
      // Handle exceptions
        System.err.println(excp) ;
    }

    /*
     * Place the telephone call.
     */
    
    try {
      Connection c[] = mycall.connect(origterm, origaddr, "12");
    } catch (Exception excp) {
      System.out.println(excp.toString());
    }

  }

}
