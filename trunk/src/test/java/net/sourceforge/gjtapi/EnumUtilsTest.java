package net.sourceforge.gjtapi;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

import lombok.Getter;

/**
 * @author haf
 */
public class EnumUtilsTest {
	
	@Test
	public void testToValueCorrectlyMapsTheValuesToEnums() {
		Map<String, BitFieldEnum> bitValueMap =
				EnumUtils.toValueMap(BitFieldEnum.class, BitFieldEnum::name);
		
		HashMap<String, BitFieldEnum> expectedMap = new HashMap<>();
		expectedMap.put(BitFieldEnum.A.name(), BitFieldEnum.A);
		expectedMap.put(BitFieldEnum.B.name(), BitFieldEnum.B);
		expectedMap.put(BitFieldEnum.C.name(), BitFieldEnum.C);
		checkValueMap(expectedMap, bitValueMap);
	}
	
	@Test
	public void testToBitValueCorrectlyMapsTheBitValuesToEnums() {
		Map<Integer, BitFieldEnum> bitValueMap = EnumUtils.toBitValueMap(BitFieldEnum.class);
		
		HashMap<Integer, BitFieldEnum> expectedMap = new HashMap<>();
		expectedMap.put(BitFieldEnum.A.bitValue, BitFieldEnum.A);
		expectedMap.put(BitFieldEnum.B.bitValue, BitFieldEnum.B);
		expectedMap.put(BitFieldEnum.C.bitValue, BitFieldEnum.C);
		checkValueMap(expectedMap, bitValueMap);
	}

	private static <T> void checkValueMap(Map<T, BitFieldEnum> expectedMap, Map<T, BitFieldEnum> actualMap) {
		assertEquals(expectedMap.size(), actualMap.size(), "The created map has a wrong size");
		assertEquals(expectedMap, actualMap, "The enums are mapped to wrong bit values");
	}
	
	@ParameterizedTest
	@ArgumentsSource(EnumSetProvider.class)
	public void testFromBitFieldIdentifiesAllEnums(EnumSet<BitFieldEnum> expectedSet) {
		int bitField = calculateBitField(expectedSet);
		
		Set<BitFieldEnum> determinedSet = EnumUtils.fromBitField(bitField, BitFieldEnum.class);
		
		assertEquals(expectedSet, determinedSet, "The wrong enums have been determined");
	}
	
	private static int calculateBitField(EnumSet<BitFieldEnum> set) {
		return set.stream()
				.map(state -> state.getBitValue())
				.reduce((a, b) -> a | b)
				.orElse(0);
	}
	
	/**
	 * @author haf
	 */
	private enum BitFieldEnum implements BitField {
		A(1 << 0),
		B(1 << 1),
		C(1 << 2);
		
		private @Getter int bitValue;
		
		private BitFieldEnum(int bitValue) {
			this.bitValue = bitValue;
		}
	}
	
	/**
	 * @author haf
	 */
	public static class EnumSetProvider implements ArgumentsProvider {
		
		@Override
		public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
			return createCombinations().stream().map(Arguments::of);
		}
		
		private static ArrayList<EnumSet<BitFieldEnum>> createCombinations() {
			ArrayList<EnumSet<BitFieldEnum>> sets = new ArrayList<>();
			
			sets.add(EnumSet.noneOf(BitFieldEnum.class));
			sets.add(EnumSet.of(BitFieldEnum.A));
			sets.add(EnumSet.of(BitFieldEnum.B));
			sets.add(EnumSet.of(BitFieldEnum.A, BitFieldEnum.B));
			sets.add(EnumSet.of(BitFieldEnum.C));
			sets.add(EnumSet.of(BitFieldEnum.A, BitFieldEnum.C));
			sets.add(EnumSet.of(BitFieldEnum.B, BitFieldEnum.C));
			sets.add(EnumSet.allOf(BitFieldEnum.class));
			
			return sets;
		}
	}
}
