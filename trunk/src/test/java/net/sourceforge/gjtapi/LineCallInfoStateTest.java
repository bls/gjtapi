package net.sourceforge.gjtapi;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * @author haf
 */
public class LineCallInfoStateTest {
	
	private static Integer toBitField(List<LineCallInfoState> states) {
		return states.stream().map(LineCallInfoState::getBitValue).reduce(0, Integer::sum);
	}
	
	@ParameterizedTest
	@EnumSource(LineCallInfoState.class)
	public void testIfTheBitValueIsValidThenFromBitValueReturnsTheCorrectState(LineCallInfoState state) {
		int bitValue = state.getBitValue();
		
		Optional<LineCallInfoState> result = LineCallInfoState.fromBitValue(bitValue);
		
		assertThat(result).containsSame(state);
	}
	
	@ParameterizedTest
	@ValueSource(ints = {-1, 0, 3})
	public void testIfTheBitValueIsInvalidThenFromBitValueReturnsAnEmptyOptional(int bitValue) {
		Optional<LineCallInfoState> result = LineCallInfoState.fromBitValue(bitValue);
		
		assertThat(result).isEmpty();
	}
	
	@ParameterizedTest
	@ArgumentsSource(BitFieldProvider.class)
	public void testIfTheBitFieldIsValidThenFromBitFieldReturnsTheCorrectStates(List<LineCallInfoState> states) {
		int bitField = toBitField(states);
		
		Set<LineCallInfoState> result = LineCallInfoState.fromBitField(bitField);
		
		assertThat(result).containsExactlyInAnyOrderElementsOf(states);
	}
	
	/**
	 * @author haf
	 */
	private static class BitFieldProvider implements ArgumentsProvider {
		
		@Override
		public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
			return Stream.of(
					Arrays.asList(),
					Arrays.asList(LineCallInfoState.APP_SPECIFIC),
					Arrays.asList(LineCallInfoState.CALL_ID, LineCallInfoState.ORIGIN),
					Arrays.asList(LineCallInfoState.MEDIA_MODE, LineCallInfoState.CALLER_ID,
							LineCallInfoState.CALLED_ID))
					.map(Arguments::of);
		}
	}
}
