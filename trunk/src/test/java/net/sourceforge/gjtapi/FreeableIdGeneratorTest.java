package net.sourceforge.gjtapi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import net.sourceforge.gjtapi.raw.remote.SerializableCallId;

/**
 * @author haf
 */
public class FreeableIdGeneratorTest {
	
	private FreeableIdGenerator classUnderTest;
	
	@BeforeEach
	private void setUp() {
		classUnderTest = new FreeableIdGenerator();
	}
	
	@Test
	public void testCanGenerateId() {
		SerializableCallId id = classUnderTest.getSerializableId();
		
		assertNotNull(id, "Did not return an ID");
	}
	
	@Test
	public void testCanCreateSpecificId() {
		SerializableCallId id = classUnderTest.newSerializableId(42);
		
		assertNotNull(id, "No ID was created");
		assertEquals(42, id.getId(), "Wrong ID value");
	}
	
	@Test
	public void testIdsAreIncreasing() {
		SerializableCallId id1 = classUnderTest.getSerializableId();
		SerializableCallId id2 = classUnderTest.getSerializableId();
		SerializableCallId id3 = classUnderTest.getSerializableId();
		
		assertTrue(id1.getId() < id2.getId(), "Second ID was not bigger than the first one");
		assertTrue(id2.getId() < id3.getId(), "Third ID was not bigger than the second one");
	}
	
	@Test
	public void testFreedIdsAreReused() {
		SerializableCallId id1 = classUnderTest.getSerializableId();
		SerializableCallId id2 = classUnderTest.getSerializableId();
		
		classUnderTest.freeSerializableId(id1);
		SerializableCallId id3 = classUnderTest.getSerializableId();
		
		assertTrue(id1.getId() < id2.getId(), "Second ID was not bigger than the first one");
		assertSame(id1.getId(), id3.getId(), "Freed ID was not reused");
	}
}
