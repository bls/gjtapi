package net.sourceforge.gjtapi;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * @author haf
 */
public class LineErrorTest {
	
	@ParameterizedTest
	@EnumSource(LineError.class)
	public void testIfTheBitValueIsValidThenFromBitValueReturnsTheCorrectError(LineError error) {
		int bitValue = error.getBitValue();
		
		Optional<LineError> result = LineError.fromBitValue(bitValue);
		
		assertThat(result).containsSame(error);
	}
	
	@ParameterizedTest
	@ValueSource(ints = {-1, 0, 3})
	public void testIfTheBitValueIsInvalidThenFromBitValueReturnsAnEmptyOptional(int bitValue) {
		Optional<LineError> result = LineError.fromBitValue(bitValue);
		
		assertThat(result).isEmpty();
	}
}
