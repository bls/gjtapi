package net.sourceforge.gjtapi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.telephony.Call;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * @author haf
 */
@ExtendWith(MockitoExtension.class)
public class CallMgrTest {
	
	private @Mock GenericProvider provider;
	private CallMgr classUnderTest;
	
	@BeforeEach
	private void setUp() {
		classUnderTest = new CallMgr(provider, false);
	}
	
	@Test
	public void testRemovingIdleCallsWhenRequestingCallFromCache() {
		FreeCall idleCall = registerFreeCall(Call.IDLE);
		FreeCall activeCall = registerFreeCall(Call.ACTIVE);
		registerFreeCall(Call.INVALID);
		
		classUnderTest.getCachedCall(activeCall.getCallID());
		FreeCall[] calls = classUnderTest.toArray();
		
		assertNotNull(calls, "Did not return any calls");
		assertContains(idleCall, calls, "Missing idle call");
		assertContains(activeCall, calls, "Missing active call");
		assertEquals(2, calls.length, "Wrong amount of calls returned");
	}
	
	@Test
	public void testGetCachedCallReturnsRegisteredCall() {
		FreeCall activeCall = registerFreeCall(Call.ACTIVE);
		
		FreeCall call = classUnderTest.getCachedCall(activeCall.getCallID());
		
		assertNotNull(call, "Did not return registered call");
		assertSame(activeCall, call, "Returned wrong call");
	}
	
	@Test
	public void testGetCachedCallReturnsNullForUnregisteredCall() {
		CallId callId = createCallId();
		
		FreeCall call = classUnderTest.getCachedCall(callId);
		
		assertNull(call, "Returned a call although no fitting call was registered");
	}
	
	@Test
	public void testGetLazyCallReturnsRegisteredCall() {
		FreeCall activeCall = registerFreeCall(Call.ACTIVE);
		
		FreeCall call = classUnderTest.getLazyCall(activeCall.getCallID());
		
		assertNotNull(call, "Did not return registered call");
		assertSame(activeCall, call, "Returned wrong call");
	}
	
	@Test
	public void testGetLazyCallCreatesCallForUnregisteredCall() {
		CallId callId = createCallId();
		
		FreeCall call = classUnderTest.getLazyCall(callId);
		
		assertNotNull(call, "Did not create a new call instance");
		assertSame(callId, call.getCallID(), "Wrong call ID");
	}
	
	@Test
	public void testCanRegisterCall() {
		FreeCall call = createFreeCall(Call.ACTIVE);
		
		classUnderTest.register(call);
		FreeCall[] calls = classUnderTest.toArray();
		
		assertNotNull(calls, "No registered calls");
		assertEquals(1, calls.length, "Wrong amount of registered calls");
		assertSame(call, calls[0], "Wrong registered call");
	}
	
	@Test
	public void testCanRemoveInvalidRegisteredCall() {
		FreeCall call = registerFreeCall(Call.ACTIVE);
		
		call.toInvalid(0);
		boolean isRemoved = classUnderTest.removeCall(call);
		FreeCall[] calls = classUnderTest.toArray();
		
		assertNotNull(calls, "Returned call array should not be null");
		assertTrue(isRemoved, "Invalid call was not removed");
		assertEquals(0, calls.length, "Wrong amount of currently registered calls");
	}
	
	@Test
	public void testCannotRemoveValidRegisteredCall() {
		FreeCall idleCall = registerFreeCall(Call.IDLE);
		FreeCall activeCall = registerFreeCall(Call.ACTIVE);
		
		boolean isActiveRemoved = classUnderTest.removeCall(idleCall);
		boolean isIdleRemoved = classUnderTest.removeCall(activeCall);
		FreeCall[] calls = classUnderTest.toArray();
		
		assertNotNull(calls, "Returned call array should not be null");
		assertFalse(isIdleRemoved, "Idle call was removed");
		assertFalse(isActiveRemoved, "Active call was removed");
		assertContains(idleCall, calls, "Idle call is not registered anymore");
		assertContains(activeCall, calls, "Active call is not registered anymore");
		assertEquals(2, calls.length, "Wrong amount of currently registered calls");
	}
	
	@Test
	public void testCanPreRegisterCall() {
		FreeCall call = createFreeCall(Call.IDLE);
		
		classUnderTest.preRegister(call);
		FreeCall[] calls = classUnderTest.toArray();
		
		assertNotNull(calls, "No pre-registered calls");
		assertEquals(1, calls.length, "Wrong amount of pre-registered calls");
		assertSame(call, calls[0], "Wrong pre-registered call");
	}
	
	@Test
	public void testToArrayReturnsPreRegisteredAndRegisteredCalls() {
		FreeCall idleCall = createFreeCall(Call.IDLE);
		classUnderTest.preRegister(idleCall);
		FreeCall activeCall = registerFreeCall(Call.ACTIVE);
		
		FreeCall[] calls = classUnderTest.toArray();
		
		assertNotNull(calls, "Returned call array should not be null");
		assertContains(idleCall, calls, "Pre-registered call is not listed");
		assertContains(activeCall, calls, "Registered call is not listed");
		assertEquals(2, calls.length, "Wrong amount of returned calls");
	}
	
	@Test
	public void testToArrayDoesNotReturnRemovedCalls() {
		FreeCall activeCall = registerFreeCall(Call.INVALID);
		classUnderTest.removeCall(activeCall);
		
		FreeCall[] calls = classUnderTest.toArray();
		
		assertNotNull(calls, "Returned call array should not be null");
		assertEquals(0, calls.length, "Removed call was still listed");
	}
	
	private FreeCall registerFreeCall(int state) {
		FreeCall call = createFreeCall(state);
		classUnderTest.register(call);
		return call;
	}
	
	private static CallId createCallId() {
		return Mockito.mock(CallId.class);
	}
	
	private FreeCall createFreeCall(int state) {
		CallId id = createCallId();
		CallData callData = new CallData(id, state, new ConnectionData[0]);
		return new FreeCall(callData, provider);
	}
	
	private static <T> void assertContains(T expectedElement, T[] actualElements, String message) {
		T actualElement = null;
		for (T element : actualElements) {
			if (element.equals(expectedElement)) {
				actualElement = element;
			}
		}
		assertEquals(expectedElement, actualElement, message);
	}
}
