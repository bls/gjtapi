package net.xtapi.serviceProvider;

/**
 * @author haf
 */
public class XTapiLineInfoUtils {
	
	public static XTapiLineInfo createXTapiLineInfo(int lineKey, String lineName, String address) {
		return new XTapiLineInfo(lineName, lineKey, address);
	}
	
	private XTapiLineInfoUtils() {
	}
}
