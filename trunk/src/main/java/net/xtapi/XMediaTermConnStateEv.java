/*
 * XMediaTermConnStateEv.java
 *
 * Created on March 2, 2002, 4:05 PM
 */

package net.xtapi;

import javax.telephony.* ;
import javax.telephony.events.TermConnEv;
import javax.telephony.events.*;
import javax.telephony.media.events.*;
import javax.telephony.media.*;

/*
*  XTAPI JTapi implementation
*  Copyright (C) 2002 Steven A. Frare
* 
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*  
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
 * @author  Steven A. Frare
 * @version .01

The original files were lastly changed in April 2021 by Fabian Hall - f.hall (at) bls-integration.de
 */

public class XMediaTermConnStateEv extends XMediaTermConnEv implements 
    MediaTermConnStateEv {

    private int m_state = MediaTerminalConnection.NOACTIVITY;
    
    /** Creates new XMediaTermConnStateEv */
    protected XMediaTermConnStateEv(Call call, int cause, int metaCode, 
        boolean isNewMetaEvent, TerminalConnection tc, int state)
    {
        super(call, MediaTermConnStateEv.ID, cause, metaCode, 
                isNewMetaEvent, tc) ;
    }
    
  /**
   * Returns the current state of playing/recording on the TerminalConnection
   * in the form of a bit mask.
   * <p>
   * @return The current playing/recording state.
   */
  public int getMediaState()    
  {
      return m_state;
  }
}
