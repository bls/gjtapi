/*
 * XConnEv.java
 *
 * Created on February 23, 2002, 3:07 PM
 */

package net.xtapi;

import javax.telephony.*;
import javax.telephony.events.*;
import  javax.telephony.Connection;

/*
*  XTAPI JTapi implementation
*  Copyright (C) 2002 Steven A. Frare
* 
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*  
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
 * @author  Steven A. Frare
 * @version .01

The original files were lastly changed in April 2021 by Fabian Hall - f.hall (at) bls-integration.de
 */

public class XConnEv extends XCallEv implements ConnEv {

    private Connection m_Connection ;
    
    /** Creates new XConnEv */
    protected XConnEv(Connection connection, int id, int cause, int metaCode, 
        boolean isNewMetaEvent)
    {
        super(connection.getCall(), id, cause, metaCode, isNewMetaEvent) ;
        m_Connection = connection;
    }
    
  /**
   * Returns the Connection associated with this Connection event.
   * <p>
   * @return The Connection associated with this event.
   */
  public Connection getConnection()
  {
      return m_Connection;
  }
}
