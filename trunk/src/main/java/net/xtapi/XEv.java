/*
 * XEv.java
 *
 * Created on February 23, 2002, 2:20 PM
 */

package net.xtapi;

import javax.telephony.events.*;

/*
*  XTAPI JTapi implementation
*  Copyright (C) 2002 Steven A. Frare
* 
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*  
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
 * @author  Steven A. Frare
 * @version .01

The original files were lastly changed in April 2021 by Fabian Hall - f.hall (at) bls-integration.de
 */

public class XEv extends java.lang.Object implements Ev {

    private int m_id ;
    private int m_cause ;
    private int m_metaCode ;
    private boolean m_isNewMetaEvent ;
    
    /** Creates new XEv */
    protected XEv(int id, int cause, int metaCode, boolean isNewMetaEvent) {
        super() ;
        m_id = id ;
        m_cause = cause ;
        m_metaCode = metaCode ;
        m_isNewMetaEvent = isNewMetaEvent ;
    }

    /**
     * Returns the cause associated with this event. Every event has a cause.
     * The various cause values are defined as public static final variables
     * in this interface.
     * <p>
     * @return The cause of the event.
 */
    public int getCause() {
        return m_cause;
    }
    
    /**
     * Returns the meta code associated with this event. The meta code provides
     * a higher-level description of the event.
     * <p>
     * @return The meta code for this event.
 */
    public int getMetaCode() {
        return m_metaCode;
    }
    
    /**
     * Returns true when this event is the start of a meta code group. This
     * method is used to distinguish two contiguous groups of events bearing
     * the same meta code.
     * <p>
     * @return True if this event represents a new meta code grouping, false
     * otherwise.
 */
    public boolean isNewMetaEvent() {
        return m_isNewMetaEvent;
    }
    
    /**
     * Returns the id of event. Every event has an id. The defined id of each
     * event matches the object type of each event. The defined id allows
     * applications to switch on event id rather than having to use multiple "if
     * instanceof" statements.
     * <p>
     * @return The id of the event.
 */
    public int getID() {
        return m_id;
    }
    
    /**
     * Returns the object that is being observed.
     * <p>
     * <STRONG>Note:</STRONG>Implementation need no longer supply this
     * information. The <CODE>CallObsevationEndedEv.getObservedObject()</CODE>
     * method has been added which returns related information. This method may
     * return null in JTAPI v1.2 and later.
     * <p>
     * @deprecated Since JTAPI v1.2 This interface no longer needs to supply this
     * information and may return null.
     * @return The object that is being observed.
 */
    public Object getObserved() {
        return null;
    }
    
}
