/*
 * XProvEv.java
 *
 * Created on April 6, 2002, 12:47 PM
 */

package net.xtapi;
import javax.telephony.events.*;
import javax.telephony.Provider.*;
import javax.telephony.* ;

/**
*  XTAPI JTapi implementation
*  Copyright (C) 2002 Steven A. Frare
* 
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*  
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
 * @author  Steven A. Frare
 * @version .01

The original files were lastly changed in April 2021 by Fabian Hall - f.hall (at) bls-integration.de
 */

public class XProvEv extends XEv implements ProvEv {

    private Provider m_Provider ;

    /** Creates new XProvEv */
    public XProvEv(int id, int cause, int metaCode,
        boolean isNewMetaEvent, Provider p)
    {
        super(id, cause, metaCode, isNewMetaEvent) ;
        m_Provider = p ;
    }
    
  /**
   * Returns the Provider associated with this Provider event.
   */
  public Provider getProvider()
  {
      return m_Provider ;
  }

}
