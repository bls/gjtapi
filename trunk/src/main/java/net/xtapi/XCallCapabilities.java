/*
 * XCallCapabilities.java
 *
 * Created on April 6, 2002, 11:55 AM
 */

package net.xtapi;
import javax.telephony.capabilities.*;
/**
*  XTAPI JTapi implementation
*  Copyright (C) 2002 Steven A. Frare
* 
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*  
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
 * @author  Steven A. Frare
 * @version .01

The original files were lastly changed in April 2021 by Fabian Hall - f.hall (at) bls-integration.de
 */

public class XCallCapabilities extends java.lang.Object implements CallCapabilities {

    private boolean bCanConnect ;

    /** Creates new XCallCapabilities */
    protected XCallCapabilities(boolean cnct) {
        bCanConnect = cnct ;
    }

    /**
     * Returns true if the application can invoke <CODE>Call.connect()</CODE>,
     * false otherwise.
     * <p>
     * @return True if the application can perform a connect, false otherwise.
 */
    public boolean canConnect() {
        return bCanConnect ;
    }
    
    /**
     * Returns true if this Call can be observed, false otherwise.
     * <p>
     * @return True if this Call can be observed, false otherwise.
 */
    public boolean isObservable() {
         return true ;
    }
    
}
