/*
 * XTermConnEv.java
 *
 * Created on February 23, 2002, 5:31 PM
 */

package net.xtapi;

import javax.telephony.* ;
import javax.telephony.events.*;
import javax.telephony.TerminalConnection;

/*
*  XTAPI JTapi implementation
*  Copyright (C) 2002 Steven A. Frare
* 
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*  
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
 * @author  Steven A. Frare
 * @version .01

The original files were lastly changed in April 2021 by Fabian Hall - f.hall (at) bls-integration.de
 */

public class XTermConnEv extends XCallEv implements TermConnEv{

    private TerminalConnection  m_terminalConnection;
    
    /** Creates new XTermConnEv */
    protected XTermConnEv(Call call, int id, int cause, int metaCode, 
        boolean isNewMetaEvent, TerminalConnection tc)
    {
        super(call, id, cause, metaCode, isNewMetaEvent) ;
        //Connection[] c = call.getConnections() ;
        //TerminalConnection[] x = c[0].getTerminalConnections() ;
        m_terminalConnection = tc ;
    }


    /**
     * Returns the TerminalConnection associated with this event.
     * <p>
     * @return The TerminalConnection associated with this event.
 */
    public TerminalConnection getTerminalConnection() {
        return m_terminalConnection ;
    }
    
}
