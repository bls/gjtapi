// Revision 8.01 2002-07-18 09:04:28 reggiehg
// Added "if (state == m_state) return;" at start of setState().

/*
 * XConnection.java
 *
 * Created on February 23, 2002, 1:22 PM
 */

package net.xtapi;

import javax.telephony.*;
import javax.telephony.capabilities.ConnectionCapabilities;
import javax.telephony.events.*;
import java.util.* ;

/*
*  XTAPI JTapi implementation
*  Copyright (C) 2002 Steven A. Frare
* 
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*  
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
 * @author  Steven A. Frare
 * @version .01

The original files were lastly changed in April 2021 by Fabian Hall - f.hall (at) bls-integration.de
 */

public class XConnection extends java.lang.Object implements javax.telephony.Connection {
    
    private int         m_state;
    private Call        m_call;
    private Address     m_address;
    private Provider    m_provider;
    private Vector      m_terminalConnections;

    /** Creates new XConnection */
    protected XConnection(Call call, Address address, Provider p) {
        m_address = address;
        m_call = call;
        m_provider = p;
        m_state = ConnCreatedEv.ID;
        m_terminalConnections = new Vector();

        CallEv[] oEvt = new CallEv[1];
        oEvt[0] = new XConnCreatedEv(this,Ev.CAUSE_NEW_CALL,0,false) ;
        publishEvents(oEvt);
    }

    /**
     * Returns the current state of the Connection. The return value will
     * be one of states defined above.
     * <p>
     * @return The current state of the Connection.
 */
    public int getState() {
        return m_state;
    }
    
    
    protected void addTerminalConnection(TerminalConnection tc)
    {
        m_terminalConnections.remove(tc);
        m_terminalConnections.add(tc);
    }
    
    protected void setState(int state){
	if (state == m_state) return;
        m_state = state;
        CallEv[] oEvt = new CallEv[1];
        switch(state)
        {
            case ConnConnectedEv.ID:
                oEvt[0] = new XConnConnectedEv(this,Ev.CAUSE_NEW_CALL,0,false) ;
                publishEvents(oEvt);
                
                XAddress xa = (XAddress)m_address;
                if(xa.m_local)
                {
                    // TODO:
                    // Need to do call.getConnections() and enumerate them!
                    // This does not work!
                    
                    // Media ready on local terminal
                    Enumeration e = m_terminalConnections.elements();
                    while(e.hasMoreElements())
                    {
                        XTerminalConnection xtc = (XTerminalConnection)e.nextElement();
                        xtc.setState(XProvider.MEDIA_CONNECTED);
                    }                    
                }
                break;
               
            case ConnInProgressEv.ID:
                oEvt[0] = new XConnInProgressEv(this,Ev.CAUSE_NEW_CALL,0,false) ;
                publishEvents(oEvt);
                break;
                
            case ConnAlertingEv.ID:
                oEvt[0] = new XConnAlertingEv(this,Ev.CAUSE_NEW_CALL,0,false) ;
                publishEvents(oEvt);
                break;                

            case ConnDisconnectedEv.ID:
                
                xa = (XAddress)m_address;
                if(!xa.m_local)
                {
                    // Media not ready on remote terminal
                    Connection[] ct = m_call.getConnections();
                    
                    for(int loop = 0; loop < ct.length; loop++)
                    {
                        TerminalConnection[] tc = ct[loop].getTerminalConnections();
                        
                        for(int iLoop = 0; iLoop < tc.length; iLoop++)
                        {
                            XTerminalConnection xtc = (XTerminalConnection)tc[iLoop];
                            xtc.setState(XProvider.MEDIA_DISCONNECTED);
                        }
                    }
                }                
                
                oEvt[0] = new XConnDisconnectedEv(this,Ev.CAUSE_NEW_CALL,0,false) ;
                publishEvents(oEvt);
                
                break;
                
            case ConnUnknownEv.ID:
                oEvt[0] = new XConnUnknownEv(this,Ev.CAUSE_NEW_CALL,0,false) ;
                publishEvents(oEvt);
                // Since we don't know our own state our Terminal Connections
                // state is unknown as well.
                Enumeration e = m_terminalConnections.elements();
                while(e.hasMoreElements())
                {
                    XTerminalConnection xtc = (XTerminalConnection)e.nextElement();
                    xtc.setState(TermConnUnknownEv.ID);
                }
                break;                
                
            default:
                break;
        }
        
    }
    
    /**
     * Returns the Call object associated with this Connection. This Call
     * reference remains valid throughout the lifetime of the Connection object,
     * despite the state of the Connection object. This Call reference does not
     * change once the Connection object has been created.
     * <p>
     * @return The call object associated with this Connection.
 */
    public Call getCall() {
        return m_call;
    }
    
    /**
     * Returns the Address object associated with this Connection. This Address
     * object reference remains valid throughout the lifetime of the Connection
     * object despite the state of the Connection object. This Address reference
     * does not change once the Connection object has been created.
     * <p>
     * @return The Address associated with this Connection.
 */
    public Address getAddress() {
        return m_address;
    }
    
    /**
     * Returns an array of TerminalConnection objects associated with this
     * Connection. TerminalConnection objects represent the relationship between
     * a Connection and a specific Terminal endpoint. There may be zero
     * TerminalConnections associated with this Connection. In that case, this
     * method returns null. Connection objects lose their reference to a
     * TerminalConnection once the TerminalConnection moves into the
     * <CODE>TerminalConnection.DROPPED</CODE> state.
     * <p>
     * <B>Post-conditions:</B>
     * <OL>
     * <LI>Let TerminalConnection tc[] = this.getTerminalConnections()
     * <LI>tc == null or tc.length >= 1
     * <LI>For all i, tc[i].getState() != TerminalConnection.DROPPED
     * </OL>
     * @return An array of TerminalConnection objects associated with this
     * Connection, null if there are no TerminalConnections.
 */
    public TerminalConnection[] getTerminalConnections() {
        
        if(m_terminalConnections.size() == 0)
            return null;
        
        TerminalConnection[] tc = new TerminalConnection[m_terminalConnections.size()];
        return (TerminalConnection[]) m_terminalConnections.toArray(tc);
    }
    
    /**
     * Drops a Connection from an active telephone call. The Connection's Address
     * is no longer associated with the telephone call. This method does not
     * necessarily drop the entire telephone call, only the particular
     * Connection on the telephone call. This method provides the ability to
     * disconnect a specific party from a telephone call, which is especially
     * useful in telephone calls consisting of three or more parties. Invoking
     * this method may result in the entire telephone call being dropped, which
     * is a permitted outcome of this method. In that case, the appropriate
     * events are delivered to the application, indicating that more than just
     * a single Connection has been dropped from the telephone call.
     *
     * <H5>Allowable Connection States</H5>
     *
     * The Connection object must be in one of several states in order for
     * this method to be successfully invoked. These allowable states are:
     * <CODE>Connection.CONNECTED</CODE>, <CODE>Connection.ALERTING</CODE>,
     * <CODE>Connection.INPROGRESS</CODE>, or <CODE>Connection.FAILED</CODE>. If
     * the Connection is not in one of these allowable states when this method is
     * invoked, this method throws InvalidStateException. Having the Connection
     * in one of the allowable states does not guarantee a successful invocation
     * of this method.
     *
     * <H5>Method Return Conditions</H5>
     *
     * This method returns successfully only after the Connection has been
     * disconnected from the telephone call and has transitioned into the
     * <CODE>Connection.DISCONNECTED</CODE>. This method may return
     * unsuccessfully by throwing one of the exceptions listed below. Note that
     * this method waits (i.e. the invocating thread blocks) until either the
     * Connection is actually disconnected from the telephone call or an error
     * is detected and an exception thrown. Also, all of the TerminalConnections
     * associated with this Connection are moved into the
     * <CODE>TerminalConnection.DROPPED</CODE> state. As a result, they are no
     * longer reported via the Connection by the
     * <CODE>Connection.getTerminalConnections()</CODE> method.
     * <p>
     * As a result of this method returning successfully, one or more events
     * are delivered to the application. These events are listed below:
     * <p>
     * <OL>
     * <LI>A ConnDisconnectedEv event for this Connection.
     * <LI>A TermConnDroppedEv event for all TerminalConnections associated with
     * this Connection.
     * </OL>
     *
     * <H5>Dropping Additional Connections</H5>
     *
     * Additional Connections may be dropped indirectly as a result of this
     * method. For example, dropping the destination Connection of a two-party
     * Call may result in the entire telephone call being dropped. It is up to
     * the implementation to determine which Connections are dropped as a result
     * of this method. Implementations should not, however, drop additional
     * Connections if it does not reflect the natural response of the underlying
     * telephone hardware.
     * <p>
     * Dropping additional Connections implies that their TerminalConnections are
     * dropped as well. Also, if all of the Connections on a telephone call are
     * dropped as a result of this method, the Call will move into the
     * <CODE>Call.INVALID</CODE> state. The following lists additional events
     * which may be delivered to the application.
     * <p>
     * <OL>
     * <LI>ConnDisconnectedEv/TermConnDroppedEv are delivered for all other
     * Connections and TerminalConnections dropped indirectly as a result of
     * this method.
     * <LI>CallInvalidEv if all of the Connections are dropped indirectly as a
     * result of this method.
     * </OL>
     * <p>
     * <B>Pre-conditions:</B>
     * <OL>
     * <LI>((this.getCall()).getProvider()).getState() == Provider.IN_SERVICE
     * <LI>this.getState() == Connection.CONNECTED or Connection.ALERTING
     * or Connection.INPROGRESS or Connection.FAILED
     * <LI>Let TerminalConnection tc[] = this.getTerminalConnections (see post-
     * conditions)
     * </OL>
     * <B>Post-conditions:</B>
     * <OL>
     * <LI>((this.getCall()).getProvider()).getState() == Provider.IN_SERVICE
     * <LI>this.getState() == Connection.DISCONNECTED
     * <LI>For all i, tc[i].getState() == TerminalConnection.DROPPED
     * <LI>this.getTerminalConnections() == null.
     * <LI>this is not an element of (this.getCall()).getConnections()
     * <LI>ConnDisconnectedEv is delivered for this Connection.
     * <LI>TermConnDroppedEv is delivered for all TerminalConnections associated
     * with this Connection.
     * <LI>ConnDisconnectedEv/TermConnDroppedEv are delivered for all other
     * Connections and TerminalConnections dropped indirectly as a result of
     * this method.
     * <LI>CallInvalidEv if all of the Connections are dropped indirectly as a
     * result of this method.
     * </OL>
     * <p>
     * @see javax.telephony.events.ConnDisconnectedEv
     * @see javax.telephony.events.TermConnDroppedEv
     * @see javax.telephony.events.CallInvalidEv
     * @exception PrivilegeViolationException The application does not have
     * the authority or permission to disconnected the Connection. For example,
     * the Address associated with this Connection may not be controllable in
     * the Provider's domain.
     * @exception ResourceUnavailableException An internal resource required
     * to drop a connection is not available.
     * @exception MethodNotSupportedException This method is not supported by
     * the implementation.
     * @exception InvalidStateException Some object required for the successful
     * invocation of this method is not in the proper state as given by this
     * method's pre-conditions. For example, the Provider may not be in the
     * Provider.IN_SERVICE state or the Connection may not be in one of the
     * allowable states.
 */
    public void disconnect() throws PrivilegeViolationException, ResourceUnavailableException, MethodNotSupportedException, InvalidStateException {
        XProvider p = (XProvider)m_provider;
        XCall c = (XCall)m_call;
        p.dropCall(c,m_address);

        try{
            Enumeration e = m_terminalConnections.elements();

            while(e.hasMoreElements())
            {
                XTerminalConnection xtc = (XTerminalConnection)e.nextElement();
                xtc.setState(TermConnDroppedEv.ID);
            }
            
            // We hungup, if we originated the call we need to set the remote
            // connections state to unknown only if the other connections state
            // != disconnected
            Connection ct[] = m_call.getConnections();
            
            for(int loop = 0; loop < ct.length; loop++)
            {
                if(ct[loop] != this)
                {
                    if(ct[loop].getState() != ConnDisconnectedEv.ID)
                    {
                        XConnection xct = (XConnection)ct[loop];
                        xct.setState(ConnUnknownEv.ID);
                    }
                }
            }
        }catch(Exception e){} // Do nothing on exception
        
        setState(ConnDisconnectedEv.ID);
    }
    
    /**
     * Returns the dynamic capabilities for the instance of the Connection
     * object. Dynamic capabilities tell the application which actions are
     * possible at the time this method is invoked based upon the implementations
     * knowledge of its ability to successfully perform the action. This
     * determination may be based upon the current state of the call model
     * or some implementation-specific knowledge. These indications do not 
     * guarantee that a particular method can be successfully invoked, however.
     * <p>
     * The dynamic Connection capabilities require no additional arguments.
     * <p>
     * @return The dynamic Connection capabilities.
 */
    public ConnectionCapabilities getCapabilities() {
        if(m_state == ConnConnectedEv.ID)
            return new XConnectionCapabilities(true);
        else
            return new XConnectionCapabilities(false);
    }
    
    /**
     * Gets the ConnectionCapabilities object with respect to a Terminal and
     * an Address. If null is passed as a Terminal parameter, the general/
     * provider-wide Connection capabilities are returned.
     * <p>
     * <STRONG>Note:</STRONG> This method has been replaced in JTAPI v1.2. The
     * <CODE>Connection.getCapabilities()</CODE> method returns the dynamic
     * Connection capabilities. This method now should simply invoke the
     * <CODE>Connection.getCapabilities()</CODE> method.
     * <p>
     * @deprecated Since JTAPI v1.2. This method has been replaced by the
     * Connection.getCapabilities() method.
     * @param terminal This argument is ignored in JTAPI v1.2 and later.
     * @param address This argument is ignored in JTAPI v1.2 and later.
     * @exception InvalidArgumentException This exception is never thrown in
     * JTAPI v1.2 and later.
     * @exception PlatformException A platform-specific exception occurred.
 */
    public ConnectionCapabilities getConnectionCapabilities(Terminal terminal,Address address) throws InvalidArgumentException, PlatformException {
        return getCapabilities();
    }

    private void publishEvents(CallEv[] oEvt)
    {
        CallObserver cs[] = m_call.getObservers();
        
        if(null == cs)
            return;
        
        for(int loop = 0; loop < cs.length; loop++)
        {
              CallObserver co = cs[loop];
              final CallObserver o = co;
              final CallEv[] fOEvt = oEvt;
              try {
                   Runnable r = new Runnable() {
                       public void run(){
                           try{
                               o.callChangedEvent(fOEvt);
                           } catch (Exception excp){
                           // handle publish exceptions
                           }
                       };
                    };
                    Thread T = new Thread(r);
                    T.start();
              } catch (Exception excp) {
                // Handle Exceptions;
              }        
       }
    }    
}
