/*
 * Copyright (C) 2002 Steven A. Frare
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package net.xtapi.serviceProvider;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;

import lombok.SneakyThrows;
import lombok.extern.apachecommons.CommonsLog;
import net.jsign.pe.PEFile;
import net.sourceforge.gjtapi.LineError;

/**
 * XTAPI JTapi implementation
 * 
 * @author  Steven A. Frare
 * @version .01

The original files were lastly changed in April 2021 by Fabian Hall - f.hall (at) bls-integration.de
 */
@CommonsLog
public class MSTAPI implements IXTapi {
	
	private static final String DLL_NAME = "mstapi";
	
	private RuntimeException initializationException;
	private int m_numLines = -1; // Used in native code. Do not rename!
	private IXTapiCallBack m_provider = null;
	
	static {
		File dll = prepareMstapiDll();
		System.load(dll.getAbsolutePath());
	}
	
	// Core Package
	private native int answerCall(int callHandle);
	private native int connectCall(int lineNumber, String address, int lineHandle);
	private native void debug(int level);
	private native int dropCall(int callHandle);
	private native String[] getCallInfo(int callHandle);
	private native XTapiLineInfo getLineInfo(int lineNumber);
	private native int initTapi();
	private native int openLine(int lineNumber);
	private native int shutDown();
	
	// Media Package
	private native int monitorDigits(int handle, boolean enable);
	private native int playSound(String file, int lineHandle);
	private native int recordSound(String file, int lineHandle);
	private native int sendDigits(int handle, String digits);
	private native int stopRecording(int lineHandle);
	private native int stopPlaying(int lineHandle);
	
	@SneakyThrows
	private static File prepareMstapiDll() {
		File availableDll = new File(DLL_NAME + ".dll");
		File deliveredDll = unpackDeliveredDll(availableDll.toPath().toAbsolutePath().getParent());
		try {
			if (isAvailableDllOutOfDate(deliveredDll, availableDll)) {
				moveMstapiDll(deliveredDll, availableDll);
			}
			else {
				deleteFile(deliveredDll);
			}
		}
		catch (IOException e) {
			deleteFile(deliveredDll);
			throw e;
		}
		return availableDll;
	}
	
	private static void deleteFile(File file) throws IOException {
		Files.delete(file.toPath());
	}
	
	private static void moveMstapiDll(File source, File target) throws IOException {
		Files.move(source.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);
	}
	
	private static File unpackDeliveredDll(Path directory) throws IOException {
		String fittingDllName = getFittingDllName();
		try (InputStream is = MSTAPI.class.getClassLoader().getResourceAsStream(fittingDllName)) {
			Path tempPath = Files.createTempFile(directory, "mstapi-", null);
			Files.copy(is, tempPath, StandardCopyOption.REPLACE_EXISTING);
			return new File(tempPath.toUri());
		}
	}
	
	@SneakyThrows
	private static boolean isAvailableDllOutOfDate(File deliveredDll, File availableDll) {
		if (availableDll.exists()) {
			Date deliveredDllTimestamp = getFileTimestamp(deliveredDll);
			Date availableDllTimestamp = getFileTimestamp(availableDll);
			
			return !deliveredDllTimestamp.equals(availableDllTimestamp);
		}
		
		return true;
	}
	
	private static Date getFileTimestamp(File file) throws IOException {
		try (PEFile availablePeFile = new PEFile(file)) {
			return availablePeFile.getTimeDateStamp();
		}
	}
	
	private static String getFittingDllName() {
		String architecture = System.getProperty("sun.arch.data.model");
		if (architecture.equals("32")) {
			return "mstapi-x32.dll";
		}
		else if (architecture.equals("64")) {
			return "mstapi-x64.dll";
		}
		else {
			throw new IllegalStateException("The current architecture is not supported");
		}
	}
	
	public void callback(int dwDevice, int dwMessage, int dwInstance, int dwParam1, int dwParam2, int dwParam3) {
		m_provider.callback(dwDevice, dwMessage, dwInstance, dwParam1, dwParam2, dwParam3);
	}
	
	@Override
	public int XTinit(IXTapiCallBack provider, Duration initTimeout) {
		m_provider = provider;
		Thread initializationThread = new Thread(this::runNativeInit);
		initializationThread.start();
		waitForLineNumberResult(initTimeout);
		
		return m_numLines;
	}
	
	/**
	 * Calls the native TAPI initialization which, on success, sets {@link #m_numLines} to the
	 * number of available lines. If the initialization fails, {@link #initializationException} is
	 * set to the reason of failure because this method is supposed to be run from a different
	 * thread and Java ignores exceptions thrown in other threads.
	 * <p>
	 * <strong>Note:</strong> On success, the native initialization will only return once the
	 * application quits. This is because the same native method which initializes the TAPI has to
	 * work the message queue afterwards. Thus, this method should be run in a separate thread.
	 */
	private void runNativeInit() {
		try {
			int errorCode = initTapi();
			if (errorCode < 0) {
				String message = LineError.fromBitValue(errorCode)
						.map(error -> "Initializing the TAPI failed due to " + error)
						.orElseGet(() -> "Initializing the TAPI failed with the unknown error code " + errorCode);
				initializationException = new RuntimeException(message);
			}
		}
		catch (RuntimeException e) {
			initializationException = e;
		}
	}
	
	/**
	 * Waits for another thread to change {@link #m_numLines}.
	 * 
	 * @param initTimeout the maximum duration to wait for the result
	 * @throws RuntimeException if {@link #m_numLines} has not been initialized or if
	 * {@link #initializationException} becomes non-null
	 */
	private void waitForLineNumberResult(Duration initTimeout) {
		log.info(String.format("Waiting for TAPI initialization (timeout %s)", initTimeout));
		for (Instant start = Instant.now(); m_numLines == -1;) {
			try {
				Thread.sleep(100L);
				
				if (initializationException != null) {
					RuntimeException e = initializationException;
					initializationException = null;
					throw e;
				}
				Duration duration = Duration.between(start, Instant.now());
				log.debug(String.format("Waiting for the TAPI initialization since %d ms", duration.toMillis()));
				if (duration.compareTo(initTimeout) > 0) {
					throw new RuntimeException(String.format(
							"TAPI initialization timed out after %d ms", duration.toMillis()));
				}
			}
			catch (InterruptedException e) {
				log.warn("Waiting for the TAPI initialization has been interrupted", e);
			}
		}
		log.info(String.format("Initialized the TAPI and found %d lines", m_numLines));
	}
	
	@Override
	public int XTOpenLine(int line) {
		return openLine(line);
	}
	
	@Override
	public XTapiLineInfo XTGetLineInfo(int line) {
		return getLineInfo(line);
	}
	
	@Override
	public int XTConnectCall(int line, String address, int handle) {
		return connectCall(line, address, handle);
	}
	
	@Override
	public int XTAnswerCall(int callHandle) {
		return answerCall(callHandle);
	}
	
	@Override
	public int XTDropCall(int handle) {
		return dropCall(handle);
	}
	
	@Override
	public String[] XTGetCallInfo(int handle) {
		return getCallInfo(handle);
	}
	
	@Override
	public int XTShutdown() {
		return shutDown();
	}
	
	@Override
	public int XTMonitorDigits(int handle, boolean enable) {
		return monitorDigits(handle, enable);
	}
	
	@Override
	public int XTPlaySound(String file, int lineHandle) {
		return playSound(file, lineHandle);
	}
	
	@Override
	public int XTRecordSound(String file, int lineHandle) {
		return recordSound(file, lineHandle);
	}
	
	@Override
	public int XTStopRecording(int lineHandle) {
		return stopRecording(lineHandle);
	}
	
	@Override
	public int XTSendDigits(int handle, String digits) {
		return sendDigits(handle, digits);
	}
	
	@Override
	public int XTStopPlaying(int lineHandle) {
		return stopPlaying(lineHandle);
	}
	
	@Override
	public void XTDebug(int level) {
		log.info("Set minimum log level to value " + level);
		debug(level);
	}
}
