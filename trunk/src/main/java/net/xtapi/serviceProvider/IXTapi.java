/*
 * XTAPI JTapi implementation
 * Copyright (C) 2002 Steven A. Frare
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * @author  Steven A. Frare
 * @version .02

The original files were lastly changed in April 2021 by Fabian Hall - f.hall (at) bls-integration.de
 */
package net.xtapi.serviceProvider;

import java.time.Duration;

import javax.telephony.InvalidStateException;
import javax.telephony.MethodNotSupportedException;
import javax.telephony.ResourceUnavailableException;

public interface IXTapi {
	
	// core methods
	/**
	 * Initializes the TAPI and uses the current thread for working the message queue. This means
	 * that this method does not exist until the TAPI is shut down.
	 * 
	 * @param provider containing the callback method for TAPI events
	 * @param initTimeout the maximum duration for waiting for the initialization
	 * @return the number of available lines
	 * @see #XTShutdown()
	 */
	public int XTinit(IXTapiCallBack provider, Duration initTimeout);
	
	/**
	 * Opens a single line.
	 * 
	 * @param lineId the ID of the line to be opened
	 * @return the opened line's handle if successful, otherwise a negative integer which might be
	 *         an error code
	 * @throws ResourceUnavailableException
	 * @throws InvalidStateException
	 */
	public int XTOpenLine(int lineId) throws ResourceUnavailableException, InvalidStateException;
	
	/**
	 * Retrieves information about a single line.
	 * 
	 * @param lineId the ID of the line to be queried
	 * @return information about the queried line or {@code null} if an error occurred
	 * @throws ResourceUnavailableException
	 * @throws InvalidStateException
	 */
	public XTapiLineInfo XTGetLineInfo(int lineId) throws ResourceUnavailableException, InvalidStateException;
	
	/**
	 * Establishes a call to a destination address from an open TAPI line.
	 * 
	 * @param lineId the line which starts the call
	 * @param address the destination address
	 * @param lineHandle the line's handle
	 * @return the started call's handle if successful, otherwise a negative integer which might be
	 *         an error code
	 * @throws ResourceUnavailableException
	 * @throws InvalidStateException
	 * @see #XTAnswerCall(int)
	 * @see #XTDropCall(int)
	 */
	public int XTConnectCall(int line, String address, int lineHandle)
			throws ResourceUnavailableException, InvalidStateException;
	
	/**
	 * Accepts an incoming TAPI call.
	 * 
	 * @param callHandle the handle of the call to be accepted
	 * @return a positive call identifier if successful, otherwise a negative error code
	 * @throws ResourceUnavailableException
	 * @throws InvalidStateException
	 * @see #XTDropCall(int)
	 */
	public int XTAnswerCall(int callHandle) throws ResourceUnavailableException, InvalidStateException;
	
	/**
	 * Drops a call.
	 * <p>
	 * This may affect related calls, e. g. if it is a conference call, all other participating calls
	 * might be dropped as well.
	 * 
	 * @param callHandle the handle of the call to be dropped
	 * @return 0 if successful, otherwise a negative error code 
	 * @throws ResourceUnavailableException
	 * @throws InvalidStateException
	 * @see #XTAnswerCall(int)
	 */
	public int XTDropCall(int callHandle) throws ResourceUnavailableException, InvalidStateException;
	
	/**
	 * Retrieves call information
	 * 
	 * @param callHandle the ID of the call to be queried
	 * @return a string array with length 2 and format {@code [<caller name>, <caller address>]} if
	 *         successful, otherwise {@code null}
	 * @throws MethodNotSupportedException
	 * @throws ResourceUnavailableException
	 * @throws InvalidStateException
	 */
	public String[] XTGetCallInfo(int callHandle)
			throws MethodNotSupportedException, ResourceUnavailableException, InvalidStateException;
	
	/**
	 * Shuts down the thread working the message queue.
	 * 
	 * @return a non-zero integer if successful, otherwise 0.
	 * @throws InvalidStateException
	 * @see {@link #XTinit(IXTapiCallBack)}
	 */
	public int XTShutdown() throws InvalidStateException;
	
	/**
	 * Sets the debug level for an external component like a DLL
	 * 
	 * @param level the value of the debug level
	 */
	public void XTDebug(int level);
	
	// media methods
	/**
	 * Sets whether or not to send digit events.
	 * 
	 * @param callHandle the handle of the call which is affected by this method
	 * @param enable {@code true} if digit events are to be enabled, {@code false} otherwise
	 * @return 0 if successful, otherwise a negative error code
	 * @throws MethodNotSupportedException
	 * @throws ResourceUnavailableException
	 * @throws InvalidStateException
	 */
	public int XTMonitorDigits(int callHandle, boolean enable)
			throws MethodNotSupportedException, ResourceUnavailableException, InvalidStateException;
	
	/**
	 * Asynchronously sends a string of digits as DTMF tones.
	 * 
	 * @param callHandle the handle of the call to which to send the digits
	 * @param digits the digits to send to the call as DTMF tones
	 * @return 0 if successful, otherwise a negative error code
	 * @throws MethodNotSupportedException
	 * @throws ResourceUnavailableException
	 * @throws InvalidStateException
	 */
	public int XTSendDigits(int callHandle, String digits)
			throws MethodNotSupportedException, ResourceUnavailableException, InvalidStateException;
	
	/**
	 * Play sound from a file or use the default microphone as the source.
	 * <p>
	 * If sound is rendered from the microphone, then {@code file} has to be {@code null}. In that
	 * case, the sound from the microphone is streamed through the TAPI.
	 * 
	 * @param file the file from which to play sound
	 * @param lineHandle the line handle of which the default microphone is used as the sound source
	 * @return 0 if successful, otherwise an unspecified integer which might be 0
	 * @throws MethodNotSupportedException
	 * @throws ResourceUnavailableException
	 * @throws InvalidStateException
	 * @see {@link #XTStopPlaying(int)}
	 */
	public int XTPlaySound(String file, int lineHandle)
			throws MethodNotSupportedException, ResourceUnavailableException, InvalidStateException;
	
	/**
	 * Record sound to a file or render the sound to the speakers.
	 * <p>
	 * If {@code file} is {@code null}, the sound is rendered straight to the speakers and not
	 * recorded. There is no facility for doing both at the same time.
	 * 
	 * @param file the file to which to save the sound
	 * @param lineHandle the handle of the line to which to send the sound
	 * @return 0 if successfully rendering to the speakers, 1 if unsuccessful or if rendering to a
	 *         file
	 * @throws MethodNotSupportedException
	 * @throws ResourceUnavailableException
	 * @throws InvalidStateException
	 * @see {@link #XTStopRecording(int)}
	 */
	public int XTRecordSound(String file, int lineHandle)
			throws MethodNotSupportedException, ResourceUnavailableException, InvalidStateException;
	
	/**
	 * Stops recording sound to a device's speakers
	 * 
	 * @param lineHandle the handle of the line for which to stop recording
	 * @return always returns 0
	 * @see #XTRecordSound(String, int)
	 */
	public int XTStopRecording(int lineHandle);
	
	/**
	 * Stops playing sound by stopping to play a file or streaming to the speakers.
	 * 
	 * @param lineHandle the handle of the line for which to stop streaming
	 * @return always returns 0
	 * @see #XTPlaySound(String, int)
	 */
	public int XTStopPlaying(int lineHandle);
}
