package net.xtapi.serviceProvider;

/*
 * Copyright (C) 2021 Fabian Hall, BLS Integration - f.hall (at) bls-integration.de
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
public final class XTapiLineInfo {
	private final String lineName;
	private final int linePermanentId;
	private final String firstAddress;
	
	XTapiLineInfo(String lineName, int linePermanentId, String firstAddress) {
		this.lineName = lineName;
		this.linePermanentId = linePermanentId;
		this.firstAddress = firstAddress;
	}
	public String getLineName() {
		return lineName;
	}
	public int getLinePermanentId() {
		return linePermanentId;
	}
	public String getFirstAddress() {
		return firstAddress;
	}
	@Override
	public String toString() {
		return String.format("XTapiLineInfo: line = %s, lineId= %08X, address=%s", lineName, linePermanentId, firstAddress);
	}
}
