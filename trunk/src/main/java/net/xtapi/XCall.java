// Revision 8.01 2002-07-18 09:04:28 reggiehg
// Added "if (state == m_state) return;" at start of setState().

/*
 * XCall.java
 *
 * Created on February 23, 2002, 10:37 AM
 */

package net.xtapi;

import javax.telephony.*;
import javax.telephony.events.*;
import java.util.* ;
import javax.telephony.capabilities.CallCapabilities;

/*
*  XTAPI JTapi implementation
*  Copyright (C) 2002 Steven A. Frare
* 
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*  
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
 * @author  Steven A. Frare
 * @version .01

The original files were lastly changed in April 2021 by Fabian Hall - f.hall (at) bls-integration.de
 */


public class XCall extends java.lang.Object implements javax.telephony.Call {

    private Vector      m_observers = null ;
    private Provider    m_provider = null;
    private int         m_state = IDLE;
    private Vector      m_connections;
    
    protected int       m_handle = 0;
    
    /** Creates new XCall */
    protected XCall(Provider p) {
        m_observers = new Vector();
        m_provider = p;
        m_connections = new Vector();
    }
    
    protected void addConnection(Connection c)
    {
        m_connections.add(c);
    }
    
    /**
     * Set the MS TAPI handle to the call
     * @param An opaque integer used as in identifier
     */
    protected void setHandle(int handle)
    {
        m_handle = handle;
    }
    /**
     * Returns an array of Connections associated with this call. Note that none
     * of the Connections returned will be in the
     * <CODE>Connection.DISCONNECTED</CODE> state. Also, if the Call is in the
     * <CODE>Call.IDLE</CODE> state or the <CODE>Call.INVALID</CODE> state, this
     * method returns null. Otherwise, it returns one or more Connection objects.
     * <p>
     * <B>Post-conditions:</B>
     * <OL>
     * <LI>Let Connection[] conn = Call.getConnections()
     * <LI>if this.getState() == Call.IDLE then conn = null
     * <LI>if this.getState() == Call.INVALID then conn = null
     * <LI>if this.getState() == Call.ACTIVE then conn.length >= 1
     * <LI>For all i, conn[i].getState() != Connection.DISCONNECTED
     * </OL>
     * @return An array of the Connections associated with the call.
 */
    public Connection[] getConnections() {
        Connection[] c = new Connection[m_connections.size()];
        return (Connection[])m_connections.toArray(c);
    }
    
    /**
     * Returns the Provider associated with the Call. This Provider reference
     * remains valid throughout the lifetime of the Call object, despite the
     * state of the Call object. This Provider reference does not change once the
     * Call object has been created.
     * <p>
     * @return The Provider associated with the call.
 */
    public Provider getProvider() {
        return m_provider;
    }
    
    /**
     * Returns the current state of the telephone call. The state will be
     * either <CODE>Call.IDLE</CODE>, <CODE>Call.ACTIVE</CODE>, or
     * <CODE>Call.INVALID</CODE>.
     * <p>
     * @return The current state of the call.
 */
    public int getState() {
        return m_state;
    }
    
    protected void setState(int state)//CallActiveEv.ID
    {
	if (state == m_state) return;
        CallEv[] oEvt = new CallEv[1];
        switch(state)
        {
            case Call.ACTIVE:
                oEvt[0] = new XCallActiveEv(this,Ev.CAUSE_NEW_CALL,0,false) ;
                m_state = Call.ACTIVE;
                publishEvents(oEvt);
                break;
                
            default:
                break;
        }
    }
    
    /**
     * Places a telephone call from an originating endpoint to a destination
     * address string.
     * <p>
     * The Call must be in the <CODE>Call.IDLE</CODE> state (and therefore have
     * no existing associated Connections and the Provider must be in the
     * <CODE>Provider.IN_SERVICE</CODE> state. The successful effect of this
     * method is to place the telephone call and create and return two
     * Connections associated with this Call.
     *
     * <H5>Method Arguments</H5>
     *
     * This method has three arguments. The first argument is the originating
     * Terminal for the telephone call. The second argument is the originating
     * Address for the telephone Call. This Terminal/Address pair must reference
     * one another. That is, the originating Address must appear on the Terminal
     * (via <CODE>Address.getTerminals()</CODE) and the originating Terminal
     * must appear on the Address (via <CODE>Terminal.getAddress()</CODE>). If
     * not, an InvalidArgumentException is thrown.
     * <p>
     * The third argument is a destination string whose value represents the
     * address to which the telephone call is placed. This destination address
     * must be valid and complete.
     *
     * <H5>Method Post-conditions</H5>
     *
     * This method returns successfully when the Provider can successfully
     * initiate the placing of the telephone call. As a result, when the
     * <CODE>Call.connect()</CODE> method returns, the Call will be in the
     * <CODE>Call.ACTIVE</CODE> state and exactly two Connections will be
     * created and returned. The Connection associated with the originating
     * endpoint is the first Connection in the returned array, and the Connection
     * associated with the destination endpoint is the second Connection in the
     * returned array. These two Connections must at least be in the
     * <CODE>Connection.IDLE</CODE> state. That is, if one of the Connections
     * progresses beyond the <CODE>Connection.IDLE</CODE> state while this
     * method is completing, this Connection may be in a state other than the
     * <CODE>Connection.IDLE</CODE>. This state must be reflected by an event
     * sent to the application.
     *
     * <H5>Call Scenarios or Flows</H5>
     *
     * As a telephone call progresses during its lifetime, the various objects
     * associated with the Call may change state and new objects may be created
     * an associated with the Call. These changes typically occur after this
     * method has successfully returned.
     * <p>
     * How the Call and its associated objects progress depends upon real-world
     * conditions. There is a large but finite number of ways in which the state
     * of a Call may progress. It is difficult to enumerate each possible way
     * in which the state of a Call may progress. Instead, several illustrative
     * <EM>scenarios</EM> (also called <EM>flows</EM>) are presented for common
     * real-world conditions. These scenarios obey the valid state transitions
     * as defined by the Call, Connection, and TerminalConnection objects.
     * <p>
     * Two common scenarios are presented below. Note that there may exist
     * additional scenarios very similar to these which may only differ in a
     * single step or state change. Any implementation which adheres to the
     * rules outlined by the Call, Connection, and TerminalConnection objects
     * is considered a proper implementations with respect to call flows.
     *
     * <H5>Normal <CODE>Call.connect()</CODE> Scenario</H5>
     *
     * This this scenario, a telephone call is placed to a telephone number
     * address. This address is outside the Provider's domain. The destination
     * party answers the telephone call.
     *
     * <OL>
     * <p>
     * <LI>The <CODE>Call.connect()</CODE> method is invoked with the given
     * arguments. Two Connection objects are created and returned, each in the
     * <CODE>Connection.IDLE</CODE> state.
     * <p>
     * <B>Events delivered to the application:</B> a CallActivEv and two
     * ConnCreatedEv, one for each Connection.
     * <p>
     * <LI>Once the Provider begins to place the telephone call, the originating
     * Connection moves from the <CODE>Connection.IDLE</CODE> state into 
     * the <CODE>Connection.CONNECTED</CODE> state. A TerminalConnection is
     * created in the <CODE>TerminalConnection.IDLE</CODE> state and moves
     * into the to model the <CODE>TerminalConnection.ACTIVE</CODE>
     * relationship between the originating Terminal and the Connection.
     * <p>
     * <B>Events delivered to the application:</B> a ConnConnectedEv for the
     * originating Connection, a TermConnCreatedEv and a TermConnActiveEv for
     * the new TerminalConnection.
     * <p>
     * <B>Note:</B> Depending upon the configuration of the switch, additional
     * TerminalConnection objects associated with the originating Connection may
     * be created. If the originating Address has more than on Terminal, these
     * additional Terminals may be involved in the telephone call. For each
     * TerminalConnection created a TermConnCreatedEv is delivered. Typically,
     * these TerminalConnections will be in the
     * <CODE>TerminalConnection.PASSIVE</CODE> state and a TermConnPassiveEv
     * is delivered for each.
     * <p>
     * <LI>The destination Connection into the
     * <CODE>Connection.INPROGRESS</CODE> state as the Call proceeds.
     * <p>
     * <B>Events delivered to the application:</B> a ConnInProgressEv for the
     * destination Connection.
     * <p>
     * <LI>The destination Connection moves into the
     * <CODE>Connection.ALERTING</CODE> state as the destination is alerted to
     * the telephone call. TerminalConnection object may be created to model
     * the relationship between any known destination Terminals associated with
     * the Call, each in the <CODE>TerminalConnection.RINGING</CODE> state. If
     * the destination Terminals are unknown, then no TerminalConnections are
     * created.
     * <p>
     * <B>Events delivered to the application:</B> a TermConnAlerting for the
     * destination Connection, a TermConnCreatedEv and TermConnRingingEv for
     * any destination TerminalConnections created.
     * <p>
     * <LI>The destination Connection moves into the
     * <CODE>Connection.CONNECTED</CODE> state when the called party answers the
     * telephone call. If the destination Terminals are known, the answering
     * TerminalConnection moves into the <CODE>TerminalConnection.ACTIVE</CODE>
     * state.
     * <p>
     * <B>Events delivered to the application:</B> a ConnConnectedEv for the
     * destination Connection and a TermConnActiveEv for the answering
     * TerminalConnection, if known.
     * <p>
     * <B>Note:</B>
     * For all other non-answering destination TerminalConnections known, either
     * one of two state changes will occur depending upon the configuration of
     * the switch. These TerminalConnections will either move into the
     * <CODE>TerminalConnection.PASSIVE</CODE> state or the
     * <CODE>TerminalConnection.DROPPED</CODE> state, depending upon whether or
     * not these Terminals continue as part of the telephone call. For each,
     * either a TermConnPassiveEv or TermConnDroppedEv is delivered.
     * </OL>
     * <p>
     * At the conclusion of this scenario, the Call will be in the
     * <CODE>Call.ACTIVE</CODE> state, both Connections will be in the
     * <CODE>Connection.CONNECTED</CODE> state, and the originating
     * TerminalConnection will be in the <CODE>TerminalConnection.ACTIVE</CODE>
     * state.
     *
     * <H5>Failure <CODE>Call.connect()</CODE> Scenario</H5>
     *
     * In this scenario, a telephone call is placed to a destination address.
     * The destination cannot be reached, perhaps because the destination address
     * is busy.
     * <p>
     * The first three steps of the first scenario are the same as in this
     * scenario. They are not repeated here for brevity. The fourth and final
     * step of this scenario is:
     * <p>
     * <OL>
     * <LI>The destination Connection moves into the
     * <CODE>Connection.FAILED</CODE> because the destination party could not
     * be reached. (e.g. busy)
     * <p>
     * <B>Events delivered to the application:</B> a ConnFailedEv is delivered
     * for the destination Connection.
     * </OL>
     * <p>
     * At the conclusion of this scenario, the Call will be in the
     * <CODE>Call.ACTIVE</CODE> state, the originating Connection will be in the
     * <CODE>Connection.CONNECTED</CODE> state, the destination Connection will
     * be in the <CODE>Connection.FAILED</CODE> state, and the originating
     * TerminalConnection will be in the <CODE>TerminalConnection.ACTIVE</CODE>
     * state.
     * <p>
     * <B>Pre-conditions:</B>
     * <OL>
     * <LI>(this.getProvider()).getState() == Provider.IN_SERVICE
     * <LI>this.getState() == Call.IDLE
     * </OL>
     * <B>Post-conditions:</B>
     * <OL>
     * <LI>(this.getProvider()).getState() == Provider.IN_SERVICE
     * <LI>this.getState() == Call.ACTIVE
     * <LI>Let Connection c[] = this.getConnections()
     * <LI>c.length == 2
     * <LI>c[0].getState() == Connection.IDLE (at least)
     * <LI>c[1].getState() == Connection.IDLE (at least)
     * </OL>
     * @see javax.telephony.events.CallActiveEv
     * @see javax.telephony.events.ConnAlertingEv
     * @see javax.telephony.events.ConnConnectedEv
     * @see javax.telephony.events.ConnCreatedEv
     * @see javax.telephony.events.ConnInProgressEv
     * @see javax.telephony.events.TermConnActiveEv
     * @see javax.telephony.events.TermConnCreatedEv
     * @see javax.telephony.events.TermConnDroppedEv
     * @see javax.telephony.events.TermConnPassiveEv
     * @see javax.telephony.events.TermConnRingingEv
     * @param origterm The originating Terminal for this telephone call.
     * @param origaddr The originating Address for this telephone call.
     * @param dialedDigits The destination address string for this telephone call.
     * @exception ResourceUnavailableException An internal resource necessary
     * for placing the phone call is unavailable.
     * @exception PrivilegeViolationException The application does not have
     * the proper authority to place a telephone call.
     * @exception InvalidPartyException Either the originator or the destination
     * does not represent a valid party required to place a telephone call.
     * @exception InvalidArgumentException An argument provided is not valid
     * either by not providing enough information for this method or is
     * inconsistent with another argument.
     * @exception InvalidStateException Some object required by this method is
     * not in a valid state as designated by the pre-conditions for this method.
     * @exception MethodNotSupportedException The implementation does not
     * support this method.
 */
    public Connection[] connect(Terminal origterm,Address origaddr,String dialedDigits) throws ResourceUnavailableException, PrivilegeViolationException, InvalidPartyException, InvalidArgumentException, InvalidStateException, MethodNotSupportedException {

        //According to the 1.2 Spec:
        /*
        ...exactly two Connections will be created and returned. The Connection 
        associated with the originating endpoint is the first Connection in the 
        returned array, and the Connection associated with the destination 
        endpoint is the second Connection in the returned array. 
        */
        
        // Origination
        XConnection c;//m_connections = new XConnection[2] ;
        c = new XConnection(this, origaddr, m_provider) ;
        //TerminalConnection tc = new XTerminalConnection(origterm, c, this, m_provider);
        TerminalConnection tc = new XMediaTerminalConnection(origterm, c, this, m_provider);
        //c.addTerminalConnection(tc);
        m_connections.add(c);
        
        // Destination
        Address dest = new XAddress(m_provider,false,dialedDigits,0) ;
        Terminal t = new XTerminal("UNKNOWN",m_provider,dest);
        XConnection c2 = new XConnection(this,dest, m_provider);
        TerminalConnection tc2 = new XTerminalConnection(t,c2,this,m_provider);
        //c2.addTerminalConnection(tc2);
        m_connections.add(c2);
        
        // TODO:
        // We don't know the state of the destination terminal so set it to UNKNOWN.

        XProvider x = (XProvider)m_provider;        
        x.connect(Integer.parseInt(origaddr.getName()),dialedDigits, this, origaddr);
        
        /*  
         From the JTAPI 1.2 docs:
         The Call.connect() method is invoked with the given arguments. 
         Two Connection objects are created and returned, each in the 
         Connection.IDLE state. 

         Events delivered to the application: 
         a CallActivEv and two ConnCreatedEv, one for each Connection. 
         the ConnCreatedEv were thrown when the connection were created
        */

        setState(Call.ACTIVE);
        
        return (Connection[])m_connections.toArray(new Connection[2]);
    }
    
    /**
     * Adds an observer to the Call. The <CODE>CallObserver</CODE> reports all
     * Call-related events. This includes changes in the state of the Call and
     * all Connection-related and TerminalConnection-related events. The observer
     * added with this method will report events on the call for as long as the
     * implementation can observer the Call. In the case that the implementation
     * can no longer observe the Call, the applications receives a
     * CallObservationEndedEv. The observer receives no more events after it
     * receives the CallObservationEndedEv and is no longer reported via the
     * <CODE>Call.getObservers()</CODE> method.
     *
     * <H5>Observer Lifetime</H5>
     *
     * The <CODE>CallObserver</CODE> will receive events until one of the
     * following occurs, whereupon the observer receives a CallObservationEndedEv
     * and the observer is no longer reported via the 
     * <CODE>Call.getObservers()</CODE> method.
     * <p>
     * <OL>
     * <LI>The observer is removed by the application with 
     * <CODE>Call.removeObserver()</CODE>.
     * <LI>The implementation can no longer monitor the call.
     * <LI>The Call has completed and moved into the 
     * <CODE>Call.INVALID</CODE> state.
     * </OL>
     *
     * <H5>Event Snapshots</H5>
     *
     * By default, when an observer is added to a telephone call, the first
     * batch of events may be a "snapshot". That is, if the observer was added
     * after state changes in the Call, the first batch of events will inform
     * the application of the current state of the Call. Note that these
     * snapshot events do NOT provide a history of all events on the Call, rather
     * they provide the minimum necessary information to bring the application
     * up-to-date with the current state of the Call. The meta code for all of
     * these events will be <CODE>Ev.META_SNAPSHOT</CODE>.
     *
     * <H5>CallObservers from Addresses and Terminals</H5>
     *
     * There may be additional call observers on the call which were not added
     * by this method. These observers may have become part of the call via
     * the <CODE>Address.addCallObserver()</CODE> and 
     * <CODE>Terminal.addCallObserver()</CODE> methods. See the specifications
     * for these methods for more information.
     *
     * <H5>Multiple Invocations</H5>
     *
     * If an application attempts to add an instance of an observer already
     * present on this Call, there are two possible outcomes:
     * <p>
     * <OL>
     * <LI>If the observer was added by the application using this method,
     * then a repeated invocation will silently fail, i.e. multiple instances
     * of an observer are not added and no exception will be thrown.
     * <LI>If the observer is part of the call because an application invoked
     * <CODE>Address.addCallObserver()</CODE> or 
     * <CODE>Terminal.addCallObserver()</CODE>, either of these methods
     * modifies the behavior of that observer as if it were added via this
     * method instead. That is, the observer is no longer removed when the call
     * leaves the Address or Terminal. The observer now receives events until
     * one of the conditions in  "Observer Lifetime" is met.
     * </OL>
     * <p>
     * <B>Post-Conditions:</B>
     * <OL>
     * <LI>observer is an element of <CODE>this.getObservers()</CODE>
     * <LI>A snapshot of events is delivered to the observer, if appropriate.
     * </OL>
     * @param observer The observer being added.
     * @exception MethodNotSupportedException The observer cannot be added at this time
     * @exception ResourceUnavailableException The resource limit for the
     * numbers of observers has been exceeded.
 */
    public void addObserver(CallObserver observer) throws ResourceUnavailableException, MethodNotSupportedException {
        m_observers.remove(observer);
        m_observers.add(observer);
    }
    
    /**
     * Returns an array of all <CODE>CallObservers</CODE> on this Call. If no
     * observers are on this Call object, then this method returns null. This
     * method returns all observers on this call no matter how they were added
     * to the Call. Call observers may be added to this call in one of three
     * ways:
     * <OL>
     * <LI>The application added the observer via
     * <CODE>Call.addObserver()</CODE>.
     * <LI>The application added the observer via
     * <CODE>Address.addCallObserver()</CODE> and the call came to that Address.
     * <LI>The application added the observer via
     * <CODE>Terminal.addCallObserver()</CODE> and the call came to that
     * Terminal.
     * </OL>
     * <p>
     * An instance of a CallObserver object will only appear once in this list.
     * <p>
     * <B>Post-Conditions:</B>
     * <OL>
     * <LI>Let CallObserver[] obs = this.getObservers()
     * <LI>obs == null or obs.length >= 1
     * </OL>
     * @return An array of CallObserver objects associated with this Call.
 */
    public CallObserver[] getObservers() {
        if(m_observers.size() == 0)
            return null;
        
        CallObserver[] o = new CallObserver[m_observers.size()];
        return (CallObserver[])m_observers.toArray(o);
    }
    
    /**
     * Removes the given observer from the Call. If successful, the observer will
     * receive a CallObservationEndedEv as the last event it receives and will
     * no longer be reported via the <CODE>Call.getObservers()</CODE> method.
     * <p>
     * This method has different effects depending upon how the observer was
     * added to the Call, as follows:
     * <p>
     * <OL>
     * <LI>If the observer was added via <CODE>Call.addObserver()</CODE>, this
     * method removes the observer until it is re-applied by the application.
     * <LI>If the observer was added via <CODE>Address.addCallObserver()</CODE>
     * or <CODE>Terminal.addCallObserver()</CODE>, this method removes the
     * observer for this call only. It does not affect whether this observer
     * will be added to future calls which come to that Address. See
     * <CODE>Address.addCallObserver()</CODE> and
     * <CODE>Terminal.addCallObserver()</CODE> for more details.
     * </OL>
     * <p>
     * If an observer is not part of the Call, then this method fails silently,
     * i.e. no observer is removed and no exception is thrown.
     * <p>
     * <B>Post-Conditions:</B>
     * <OL>
     * <LI>observer is not an element of this.getObservers()
     * <LI>CallObservationEndedEv is delivered to the application
     * </OL>
     * @param observer The observer which is being removed.
 */
    public void removeObserver(CallObserver observer) {
        m_observers.remove(observer);
    }
    
    /**
     * Returns the dynamic capabilities for the instance of the Call object.
     * Dynamic capabilities tell the application which actions are possible at
     * the time this method is invoked based upon the implementations knowledge
     * of its ability to successfully perform the action. This determination may
     * be based upon argument passed to this method, the current state of the
     * call model, or some implementation-specific knowledge. These indications
     * do not guarantee that a particular method can be successfully invoked,
     * however.
     * <p>
     * The dynamic call capabilities are based upon a Terminal/Address pair as
     * well as the instance of the Call object. These parameters are used to
     * determine whether certain call actions are possible at the present. For
     * example, the <CODE>CallCapabilities.canConnect()</CODE> method will
     * indicate whether a telephone call can be placed using the Terminal/Address
     * pair as the originating endpoint.
     * <p>
     * @param terminal Dynamic capabilities are with respect to this Terminal.
     * @param address Dynamic capabilities are with respect to this Address.
     * @return The dynamic Call capabilities.
     * @exception InvalidArgumentException Indicates the Terminal and/or Address
     * argument provided was not valid.
 */
    public CallCapabilities getCapabilities(Terminal terminal,Address address) throws InvalidArgumentException {
        if(terminal != null && address != null)
            return new XCallCapabilities(true);
        else
            throw new InvalidArgumentException();
    }
    
    /**
     * Gets the CallCapabilities object with respect to a Terminal and an Address.
     * If null is passed as a Terminal parameter, the general/provider-wide Call 
     * capabilities are returned.
     * <p>
     * <STRONG>Note:</STRONG> This method has been replaced in JTAPI v1.2. The
     * <CODE>Call.getCapabilities()</CODE> method returns the dynamic Call
     * capabilities. This method now should simply invoke the
     * <CODE>Call.getCapabilities()</CODE> method with the given Terminal and
     * Address arguments.
     * <p>
     * @deprecated Since JTAPI v1.2. This method has been replaced by the
     * Call.getCapabilities() method.
     * @param term Dynamic Call capabilities in JTAPI v1.2 are with respect to
     * this Terminal.
     * @param addr Dynamic Call capabilities in JTAPI v1.2 are with respect to
     * this Address.
     * @exception InvalidArgumentException Indicates the Terminal and/or Address
     * argument provided was not valid.
     * @exception PlatformException A platform-specific exception occurred.
 */
    public CallCapabilities getCallCapabilities(Terminal term,Address addr) throws InvalidArgumentException, PlatformException {
        return getCapabilities(term,addr);
    }
    
    private void publishEvents(CallEv[] oEvt)
    {
        for(int loop = 0; loop < m_observers.size(); loop++)
        {
              CallObserver co = (CallObserver)m_observers.elementAt(loop);
              final CallObserver o = co;
              final CallEv[] fOEvt = oEvt;
              try {
                   Runnable r = new Runnable() {
                       public void run(){
                           try{
                               o.callChangedEvent(fOEvt);
                           } catch (Exception excp){
                           // handle publish exceptions
                           }
                       };
                    };
                    Thread T = new Thread(r);
                    T.start();
              } catch (Exception excp) {
                // Handle Exceptions;
              }        
       }
    }

	@Override
	public void addCallListener(CallListener listener)
			throws ResourceUnavailableException, MethodNotSupportedException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public CallListener[] getCallListeners() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeCallListener(CallListener listener) {
		// TODO Auto-generated method stub
		
	}
    
}
