/*
 * XTermEv.java
 *
 * Created on April 6, 2002, 2:16 PM
 */

package net.xtapi;
import javax.telephony.*;
import javax.telephony.Terminal.*;
import javax.telephony.events.*;

/**
*  XTAPI JTapi implementation
*  Copyright (C) 2002 Steven A. Frare
* 
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*  
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
 * @author  Steven A. Frare
 * @version .02

The original files were lastly changed in April 2021 by Fabian Hall - f.hall (at) bls-integration.de
 */

public class XTermEv extends XEv implements TermEv {
    
    Terminal m_terminal = null;

    /** Creates new XTermEv */
    protected XTermEv(int id, int cause, int metaCode, 
        boolean isNewMetaEvent, Terminal t)
    {
        super(id, cause, metaCode, isNewMetaEvent) ;
        m_terminal = t;
    }

    /**
     * Returns the Terminal associated with this Terminal event.
     * <p>
     * @return The Terminal associated with this event.
 */
    public Terminal getTerminal() {
        return m_terminal;
    }
    
}
