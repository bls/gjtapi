/*
 * XAddrEv.java
 *
 * Created on April 6, 2002, 1:11 PM
 */

package net.xtapi;
import javax.telephony.events.*;
import javax.telephony.Address.*;
import javax.telephony.* ;

/**
*  XTAPI JTapi implementation
*  Copyright (C) 2002 Steven A. Frare
* 
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*  
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
 * @author  Steven A. Frare
 * @version .02

The original files were lastly changed in April 2021 by Fabian Hall - f.hall (at) bls-integration.de
 */

public class XAddrEv extends XEv implements AddrEv {
    
    Address m_address;

    /** Creates new XAddrEv */
    public XAddrEv(int id, int cause, int metaCode,
        boolean isNewMetaEvent, Address a)
    {
        super(id, cause, metaCode, isNewMetaEvent);
        m_address = a;
    }

    /**
     * Returns the Address associated with this Address event.
     * <p>
     * @return The Address associated with this event.
 */
    public Address getAddress() {
        return m_address;
    }
    
}
