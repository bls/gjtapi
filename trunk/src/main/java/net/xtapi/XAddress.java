/*
 * XTerminal.java
 *
 * Created on February 23, 2002, 9:14 AM
 */

package net.xtapi;

import javax.telephony.*;
import javax.telephony.events.*;
import javax.telephony.capabilities.AddressCapabilities;
import java.util.* ;

/*
*  XTAPI JTapi implementation
*  Copyright (C) 2002 Steven A. Frare
* 
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*  
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
 * @author  Steven A. Frare
 * @version .01

The original files were lastly changed in April 2021 by Fabian Hall - f.hall (at) bls-integration.de
 */

public class XAddress extends java.lang.Object implements javax.telephony.Address {

    protected     String m_name ;
    protected   int    m_id ;
    private     Provider m_provider ;
    private     Vector m_terminals ;
    private     Vector m_connections ;
    private     Vector m_callObservers ;    
    private     Vector m_addressObservers ;
    protected   boolean m_local ;
    protected   Terminal m_terminal;
    protected   int     m_lineHandle;
    
    /** Creates new XAddress */
    protected XAddress(Provider p, boolean isLocal, String number, int id ) 
        throws InvalidArgumentException
    {
        super();
        m_provider = p;
        m_local = isLocal;
        m_name = number;
        m_terminals = new Vector();
        m_connections = new Vector();
        m_callObservers = new Vector();
        m_addressObservers = new Vector();
        m_id = id;
        
        
        if(isLocal)
        {
            XProvider x = (XProvider)m_provider;
            
            try{
            
                m_lineHandle = x.open(id);

                // Throw on bad handle.
                if(m_lineHandle < 0)
                {
                    throw new InvalidArgumentException(id + "");
                }
            }
            catch(Exception e){
                throw new InvalidArgumentException(id + "");
            }
        }
        
        m_terminal = new XTerminal("UNKNOWN", m_provider, this);

    }
    
    protected int getID()
    {
        return m_id;
    }

    /**
     * Returns the name of the Address. Each Address possesses a unique
     * name. This name is a way of referencing an endpoint in a telephone call.
     * <p>
     * @return The name of this Address.
 */
    public String getName() {
        return m_name;
    }
    
    /**
     * Returns the Provider associated with this Address. This Provider object
     * is valid throughout the lifetime of the Address and does not change
     * once the Address is created.
     * <p>
     * @return The Provider associated with this Address.
 */
    public Provider getProvider() {
        return m_provider;
    }
    
    /**
     * Returns an array of Terminals associated with this Address object.
     * If no Terminals are associated with this Address, this method returns
     * null. This list does not change throughout the lifetime of the Address
     * object.
     * <p>
     * @return An array of Terminal objects associated with this Address.
 */
    public Terminal[] getTerminals() {
        Terminal[] t = new Terminal[1];
        //t[0] = new XTerminal(m_name,m_provider,this);
        t[0] = m_terminal;
        return t;
    }
    
    /**
     * Returns an array of Connection objects currently associated with this
     * Address object at the instant the <CODE>getConnections()</CODE> method 
     * is called. When a Connection moves into the
     * <CODE>Connection.DISCONNECTED</CODE> state, the Address object loses the
     * reference to the Connection and the Connection no longer returned by this
     * method. Therefore, all Connections returned by this method will never be
     * in the <CODE>Connection.DISCONNECTED</CODE> state. If the Address has no
     * Connections associated with it, this method returns null.
     * <p>
     * <B>Post-conditions:</B>
     * <OL>
     * <LI>Let Connection c[] = this.getConnections()
     * <LI>c == null or c.length >= 1
     * <LI>For all i, c[i].getState() != Connection.DISCONNECTED
     * </OL>
     * @return An array of Connection objects associated with this Address.
 */
    public Connection[] getConnections() {
        if(m_connections.size() == 0)
            return null;
        
        Connection[] c = new Connection[m_connections.size()];
        return (Connection[]) m_connections.toArray(c);        
    }
    
    /**
     * Adds an observer to the Address. The AddressObserver reports all Address-
     * related state changes as events. The Address object will report events to
     * this AddressObserver object for the lifetime of the Address object or
     * until the observer is removed with the
     * <CODE>Address.removeObserver()</CODE> method or until the Address is no
     * longer observable. In these instances, a AddrObservationEndedEv is
     * delivered to the observer as its final event. The observer will receive
     * no events after AddrObservationEndedEv unless the observer is explicitly
     * re-added via the <CODE>Address.addObserver()</CODE> method. Also, once
     * an observer receives an AddrObservationEndedEv, it will no longer be
     * reported via the <CODE>Address.getObservers()</CODE>.
     * <p>
     * If an application attempts to add an instance of an observer already
     * present on this Address, this attempt will silently fail, i.e. multiple
     * instances of an observer are not added and no exception will be thrown.
     * <p>
     * Currently, only the AddrObservationEndedEv event is defined by the core
     * package and delivered to the AddressObserver.
     * <p>
     * <B>Post-conditions:</B>
     * <OL>
     * <LI>observer is an element of this.getObservers()
     * </OL>
     * @param observer The observer being added.
     * @exception MethodNotSupportedException The observer cannot be added at this time
     * @exception ResourceUnavailableException The resource limit for the
     * number of observers has been exceeded.
 */
    public void addObserver(AddressObserver observer) throws ResourceUnavailableException, MethodNotSupportedException {
        m_addressObservers.remove(observer);
        m_addressObservers.add(observer);
    }
    
    /**
     * Returns a list of all AddressObservers associated with this Address
     * object. If there are no observers associated with this Address object,
     * this method returns null.
     * <p>
     * <B>Post-conditions:</B>
     * <OL>
     * <LI>Let AddressObserver[] obs = this.getObservers()
     * <LI>obs == null or obs.length >= 1
     * </OL>
     * @return An array of AddressObserver objects associated with this
     * Address.
 */
    public AddressObserver[] getObservers() {
        if(m_addressObservers.size() == 0)
            return null;
        
        AddressObserver[] a = new AddressObserver[m_addressObservers.size()];
        return (AddressObserver[]) m_addressObservers.toArray(a); 
    }
    
    /**
     * Removes the given observer from the Address. If successful, the observer
     * will no longer receive events generated by this Address object. As its
     * final event, the AddressObserver receives is an AddrObservationEndedEv
     * event.
     * <p>
     * If an observer is not part of the Address, then this method fails
     * silently, i.e. no observer is removed and no exception is thrown.
     * <p>
     * <B>Post-conditions:</B>
     * <OL>
     * <LI>An AddrObservationEndedEv event is reported to the observer as its
     * final event.
     * <LI>observer is not an element of this.getObservers()
     * </OL>
     * @param observer The observer which is being removed.
 */
    public void removeObserver(AddressObserver observer) {
        m_addressObservers.remove(observer);
        XAddrObservationEndedEv ev = new XAddrObservationEndedEv(
                                                AddrObservationEndedEv.ID,
                                                Ev.CAUSE_NORMAL,
                                                0,
                                                false,
                                                this);
        AddrObservationEndedEv[] aEv = new XAddrObservationEndedEv[1];
        aEv[0] = ev;
        observer.addressChangedEvent(aEv);
    }
    
    /**
     * Adds an observer to a Call object when this Address object first becomes
     * part of that Call. This method permits applications to select an Address
     * object in which they are interested and automatically have the
     * implementation attach an observer to all present and future Calls which
     * come to this Address.
     *
     * <H5>JTAPI v1.0 Semantics</H5>
     *
     * In version 1.0 of the Java Telephony API specification, the application
     * monitored the Address object for the AddrCallAtAddrEv event. This event
     * indicated that a Call has come to this Address. Then, applications would
     * manually add an observer to the Call. With this architecture, potentially
     * dangerous race conditions existed while an application was adding an
     * observer to the Call. As a result, this mechanism has been replaced in
     * version 1.1.
     *
     * <H5>JTAPI v1.1 Semantics</H5>
     *
     * In version 1.1 of the specification, the AddrCallAtAddrEv event does not
     * exist and this method replaces the functionality described above. Instead
     * of monitoring for a AddrCallAtAddrEv event, this application simply uses
     * the <CODE>Address.addCallObserver()</CODE> method, and observer will be
     * added to new telephone calls at this Address automatically.
     * <p>
     * If an application attempts to add an instance of a call observer already
     * present on this Address, these repeated attempts will silently fail, i.e.
     * multiple instances of a call observer are not added and no exception will
     * be thrown.
     * <p>
     * When a call observer is added to an Address with this method, the
     * following behavior is exhibited by the implementation.
     * <p>
     * <OL>
     * <LI>It is immediately associated with any existing calls at the Address
     * and a snapshot of those calls are reported to the call observer. For
     * each of these calls, the observer is reported via
     * <CODE>Call.getObservers()</CODE>.
     * <LI>It is associated with all future calls which comes to this Address.
     * For each new call, the observer is reported via
     * <CODE>Call.getObservers().</CODE>
     * </OL>
     * <p>
     * Note that the definition of the term ".. when a call is at an Address"
     * means that the Call contains one Connection which has this Address as
     * its Address.
     *
     * <H5>Call Observer Lifetime</H5>
     *
     * For all call observers which are present on Calls because of this method,
     * the following behavior is exhibited with respect to the lifetime of the
     * call.
     * <p>
     * <OL>
     * <LI>The call observer receives events until the Call is no longer at this
     * Address. In this case, the call observer will be re-applied to the Call if
     * the Call returns to this Address at some point in the future.
     * <LI>The call observer is removed with <CODE>Call.removeObserver()</CODE>.
     * Note that this only affects the instance of the call observer for that
     * particular call. If the Call subsequently leaves and returns to the
     * Address, the observer will be re-applied.
     * <LI>The Call can no longer be monitored by the implementation.
     * <LI>The Call moves into the <CODE>Call.INVALID</CODE> state.
     * </OL>
     * <p>
     * When the CallObserver leaves the Call because of one of the reasons above,
     * it receives a CallObservationEndedEv as its final event.
     *
     * <H5>Call Observer on Multiple Addresses and Terminals</H5>
     *
     * It is possible for an application to add CallObservers at more than one
     * Address and Terminal (using <CODE>Address.addCallObserver()</CODE> and
     * <CODE>Terminal.addCallObserver()</CODE>, respectively). The rules outlined
     * above still apply, with the following additions:
     * <p>
     * <OL>
     * <LI>A CallObserver is not added to a Call more than once, even if it
     * has been added to more than one Address/Terminal which are present on the
     * Call.
     * <LI>The CallObserver leaves the call only if <EM>all</EM> of the
     * Addresses and Terminals on which the Call Observer was added leave the
     * Call. If one of those Addresses/Terminals becomes part of the Call again,
     * the call observer is re-applied to the Call.
     * </OL>
     * <p>
     * <B>Post-Conditions:</B>
     * <OL>
     * <LI>observer is an element of this.getCallObservers()
     * <LI>observer is an element of Call.getObservers() for each Call associated
     * with the Connections from this.getConnections().
     * <LI>An array of snapshot events are reported to the observer for existing
     * calls associated with this Address.
     * </OL>
     * @see javax.telephony.Call
     * @param observer The observer being added.
     * @exception MethodNotSupportedException The observer cannot be added at this time.
     * @exception ResourceUnavailableException The resource limit for the
     * numbers of observers has been exceeded.
 */
    public void addCallObserver(CallObserver observer) throws ResourceUnavailableException, MethodNotSupportedException {
        try{
            
            Enumeration e = m_connections.elements();
            
            while(e.hasMoreElements())
            {
                Connection cn = (Connection)e.nextElement();
                Call c = cn.getCall();
                c.addObserver(observer);
            }
            
        }catch(Exception e){}
    }
    
    /**
     * Returns a list of all CallObservers associated with this Address
     * object. That is, it returns a list of CallObserver object which have
     * been added via the <CODE>Address.addCallObserver()</CODE> method. If
     * there are no CallObservers associated with this Address object, this
     * method returns null.
     * <p>
     * <B>Post-conditions:</B>
     * <OL>
     * <LI>Let CallObserver[] obs = this.getCallObservers()
     * <LI>obs == null or obs.length >= 1
     * </OL>
     * @return An array of CallObserver objects associated with this
     * Address.
 */
    public CallObserver[] getCallObservers() {
        CallObserver[] cos = null;
        try{
            
            Enumeration e = m_connections.elements();
            
            Vector v = new Vector();
            
            while(e.hasMoreElements())
            {
                Connection cn = (Connection)e.nextElement();
                Call c = cn.getCall();
                CallObserver[] co = c.getObservers();
                
                for(int loop = 0; loop < co.length; loop++)
                {
                    v.add(co[loop]);
                }
                
            }
            
            if(v.size() > 0)
            {
                CallObserver[] t = new CallObserver[v.size()];
                cos = (CallObserver[]) v.toArray(t);                 
            }
            
        }catch(Exception e){}
        
        return cos;
    }
    
    /**
     * Removes the given CallObserver from the Address. In other words, it
     * removes a CallObserver which was added via the
     * <CODE>Address.addCallObserver()</CODE> method. If successful, the
     * observer will no longer be added to new Calls which are presented to this
     * Address, however it does not affect CallObservers which have already been
     * added at a Call.
     * <p>
     * Also, if the CallObserver is not part of the Address, then this method
     * fails silently, i.e. no observer is removed and no exception is thrown.
     * <p>
     * <B>Post-conditions:</B>
     * <OL>
     * <LI>observer is not an element of this.getCallObservers()
     * </OL>
     * @param observer The CallObserver which is being removed.
 */
    public void removeCallObserver(CallObserver observer) {
        try{
            
            Enumeration e = m_connections.elements();
            
            while(e.hasMoreElements())
            {
                Connection cn = (Connection)e.nextElement();
                Call c = cn.getCall();
                c.removeObserver(observer);
            }
            
        }catch(Exception e){}        
    }
    
    /**
     * Returns the dynamic capabilities for this instance of the Address object.
     * Dynamic capabilities tell the application which actions are possible at
     * the time this method is invoked based upon the implementations knowledge
     * of its ability to successfully perform the action. This determination may
     * be based upon argument passed to this method, the current state of the
     * call model, or some implementation-specific knowledge. These indications
     * do not guarantee that a particular method will succeed when invoked,
     * however.
     * <p>
     * The dynamic Address capabilities require no additional arguments.
     * <p>
     * @return The dynamic Address capabilities.
 */
    public AddressCapabilities getCapabilities() {
        return new XAddressCapabilities();
    }
    
    /**
     * Gets the AddressCapabilities object with respect to a Terminal 
     * If null is passed as a Terminal parameter, the general/provider-wide
     * Address capabilities are returned.
     * <p>
     * <STRONG>Note:</STRONG> This method has been replaced in JTAPI v1.2. The
     * <CODE>Address.getCapabilities()</CODE> method returns the dynamic Address
     * capabilities. This method now should simply invoke the
     * <CODE>Address.getCapabilities()</CODE> method.
     * <p>
     * @deprecated Since JTAPI v1.2. This method has been replaced by the
     * Address.getCapabilities() method.
     * @param terminal This argument is ignored in JTAPI v1.2 and later.
     * @exception InvalidArgumentException This exception is never thrown in
     * JTAPI v1.2 and later.
     * @exception PlatformException A platform-specific exception occurred.
 */
    public AddressCapabilities getAddressCapabilities(Terminal terminal) throws InvalidArgumentException, PlatformException {
        return getCapabilities();
    }

	@Override
	public void addAddressListener(AddressListener listener)
			throws ResourceUnavailableException, MethodNotSupportedException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public AddressListener[] getAddressListeners() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeAddressListener(AddressListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addCallListener(CallListener listener)
			throws ResourceUnavailableException, MethodNotSupportedException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public CallListener[] getCallListeners() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeCallListener(CallListener listener) {
		// TODO Auto-generated method stub
		
	}
    
}
