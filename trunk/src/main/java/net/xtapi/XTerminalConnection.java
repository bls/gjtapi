/*
 * XTerminalConnection.java
 *
 * Created on February 23, 2002, 5:22 PM
 */

package net.xtapi;

import javax.telephony.*;
import  javax.telephony.capabilities.TerminalConnectionCapabilities;
import javax.telephony.events.*;

/*
*  XTAPI JTapi implementation
*  Copyright (C) 2002 Steven A. Frare
* 
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*  
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
 * @author  Steven A. Frare
 * @version .01

The original files were lastly changed in April 2021 by Fabian Hall - f.hall (at) bls-integration.de
 *
 */

public class XTerminalConnection extends java.lang.Object implements TerminalConnection {

    XTerminal   m_terminal = null ;
    XConnection m_connection = null ;
    XCall       m_call  = null;
    XProvider   m_provider = null;
    int m_state = TermConnCreatedEv.ID;
    
    
    /** Creates new XTerminalConnection */
    protected XTerminalConnection(Terminal t, Connection c, Call call, Provider p) {
        
        m_terminal = (XTerminal)t ;
        m_connection = (XConnection)c ;
        m_state = TermConnCreatedEv.ID;
        m_call = (XCall)call;
        m_provider = (XProvider)p;
        
        m_connection.addTerminalConnection(this) ;
        m_terminal.addTerminalConnection(this) ; 
        
        CallEv[] oEvt = new CallEv[1];
        oEvt[0] = new XTermConnCreatedEv(m_call,Ev.CAUSE_NEW_CALL,0,false, this) ;
        publishEvents(oEvt);        
    }

    /**
     * Returns the state of the TerminalConnection object.
     * <p>
     * @return The current state of the TerminalConnection object.
 */
    public int getState() {
        return m_state;
    }
    
    protected void setState(int state){
        
        // Needs to be removed!  Not to spec!
        // for now let multiple ringing events through.  
        if(state != TermConnRingingEv.ID)
        {
            if(state == m_state)
                return;
        }
        
        if(state !=    XProvider.MEDIA_CONNECTED)
            if(state !=    XProvider.MEDIA_DISCONNECTED)
                m_state = state;

        CallEv[] oEvt = new CallEv[1];
        XAddress xa;
        
        switch(state)
        {
            case TermConnRingingEv.ID:
                oEvt[0] = new XTermConnRingingEv(m_call,Ev.CAUSE_NEW_CALL,0,
                                false, this) ;
                publishEvents(oEvt);
                break;
                
            case TermConnDroppedEv.ID:
                xa = (XAddress)m_connection.getAddress();
                if(xa.m_local)
                {
                    oEvt[0] = new XMediaTermConnUnavailableEv(m_call,
                                    Ev.CAUSE_NEW_CALL,0,false, this) ;
                    publishEvents(oEvt);
                }                
                
                oEvt[0] = new XTermConnDroppedEv(m_call,Ev.CAUSE_NEW_CALL,0,
                                false, this) ;
                publishEvents(oEvt);                
                break;
                
            case TermConnActiveEv.ID:
                oEvt[0] = new XTermConnActiveEv(m_call,Ev.CAUSE_NEW_CALL,0,
                                false, this) ;
                publishEvents(oEvt);
                break;
                
            case XProvider.MEDIA_CONNECTED:
                xa = (XAddress)m_connection.getAddress();
                if(xa.m_local)
                {
                    oEvt[0] = new XMediaTermConnAvailableEv(m_call,
                                    Ev.CAUSE_NEW_CALL,0,false, this) ;
                    publishEvents(oEvt);
                }
                break;
                
            case XProvider.MEDIA_DISCONNECTED:
                xa = (XAddress)m_connection.getAddress();
                if(xa.m_local)
                {
                    oEvt[0] = new XMediaTermConnUnavailableEv(m_call,
                                    Ev.CAUSE_NEW_CALL,0,false, this) ;
                    publishEvents(oEvt);
                }
                break;                
                
            default:
                break;
        }
        
    }
    
    /**
     * Returns the Terminal associated with this TerminalConnection object.
     * A TerminalConnection's reference to its Terminal remains valid for
     * the lifetime of the object, even if the Terminal loses its references
     * to this TerminalConnection object. Also, this reference does not
     * change once the TerminalConnection object has been created.
     * <p>
     * @return The Terminal object associated with this TerminalConnection.
 */
    public Terminal getTerminal() {
        return m_terminal;
    }
    
    /**
     * Returns the Connection object associated with this TerminalConnection.
     * A TerminalConnection's reference to the Connection remains valid
     * throughout the lifetime of the TerminalConnection. Also, this reference
     * does not change once the TerminalConnection object has been created.
     * <p>
     * @return The Connections associated with this TerminalConnection.
 */
    public Connection getConnection() {
        return m_connection;
    }
    
    /**
     * Answers an incoming telephone call on this TerminalConnection. This
     * method waits (i.e. the invoking thread blocks) until the telephone call
     * has been answered at the endpoint before returning. When this method
     * returns successfully, the state of this TerminalConnection object is
     * <CODE>TerminalConnection.ACTIVE</CODE>.
     *
     * <H4>Allowable TerminalConnection States</H4>
     *
     * The TerminalConnection must be in the
     * <CODE>TerminalConnection.RINGING</CODE> state when this method is invoked.
     * According to the specification of the TerminalConnection object, this
     * state implies the associated Connection object is also in the
     * <CODE>Connection.ALERTING</CODE> state. There may be more than one
     * TerminalConnection on the Connection which are in the
     * <CODE>TerminalConnection.RINGING</CODE> state. In fact, if the Connection
     * is in the <CODE>Connection.ALERTING</CODE> state, all of these
     * TerminalConnections must be in the <CODE>TerminalConnection.RINGING</CODE>
     * state. Any of these TerminalConnections may invoke this method to answer
     * the telephone call.
     *
     * <H4>Multiple TerminalConnections</H4>
     *
     * The underlying telephone hardware determines the resulting state of other
     * TerminalConnection objects after the telephone call has been answered by
     * one of the TerminalConnections. The other TerminalConnection object may
     * either move into the <CODE>TerminalConnection.PASSIVE</CODE> state or the
     * <CODE>TerminalConnection.DROPPED</CODE> state. If a TerminalConnection
     * moves into the <CODE>TerminalConnection.PASSIVE</CODE> state, it remains
     * part of the telephone call, but not actively so. It may have the ability
     * to join the call in the future. If a TerminalConnection moves into the
     * <CODE>TerminalConnection.DROPPED</CODE> state, it is removed from the
     * telephone call and will never have the ability to join the call in the
     * future. The appropriate events are delivered to the application indicates
     * into which of these two states the other TerminalConnection objects have
     * moved.
     *
     * <H4>Events</H4>
     *
     * The following events are reported to applications via the CallObserver
     * interface as a result of the successful outcome of this method:
     * <p>
     * <OL>
     * <LI>TermConnActiveEv for the TerminalConnection which invoked this method.
     * <LI>ConnConnectedEv for the Connection associated with the
     * TerminalConnection.
     * <LI>TermConnPassiveEv or TermConnActiveEv for other TerminalConnections
     * associated with the Connection.
     * </OL>
     * <p>
     * <B>Pre-conditions:</B>
     * <OL>
     * <LI>((this.getTerminal()).getProvider()).getState() == Provider.IN_SERVICE
     * <LI>this.getState() == TerminalConnection.RINGING
     * <LI>(this.getConnection()).getState() == Connection.ALERTING
     * </OL>
     * <B>Post-conditions:</B>
     * <OL>
     * <LI>((this.getTerminal()).getProvider()).getState() == Provider.IN_SERVICE
     * <LI>this.getState() == TerminalConnection.ACTIVE
     * <LI>(this.getConnection()).getState() == Connection.CONNECTED
     * <LI>TermConnActiveEv for the TerminalConnection which invoked this method.
     * <LI>ConnConnectedEv for the Connection associated with the
     * TerminalConnection.
     * <LI>TermConnPassiveEv or TermConnActiveEv for other TerminalConnections
     * associated with the Connection.
     * </OL>
     * @see javax.telephony.events.TermConnActiveEv
     * @see javax.telephony.events.TermConnPassiveEv
     * @see javax.telephony.events.TermConnDroppedEv
     * @see javax.telephony.events.ConnConnectedEv
     * @exception PrivilegeViolationException The application did not have
     * proper authority to answer the telephone call. For example, the
     * Terminal associated with the TerminalConnection may not be in the
     * Provider's local domain.
     * @exception ResourceUnavailableException The necessary resources to
     * answer the telephone call were not available when the method was invoked.
     * @exception MethodNotSupportedException This method is currently not
     * supported by this implementation.
     * @exception InvalidStateException An object was not in the proper state,
     * violating the pre-conditions of this method. For example, the Provider
     * was not in the Provider.IN_SERVICE state or the TerminalConnection was
     * not in the TerminalConnection.RINGING state.
 */
    public void answer() throws PrivilegeViolationException, ResourceUnavailableException, MethodNotSupportedException, InvalidStateException {
        
        if(TermConnRingingEv.ID != m_state)
        {
            //System.out.println("TC State is " + m_state);
            
            throw new InvalidStateException(this,
            javax.telephony.InvalidStateException.TERMINAL_CONNECTION_OBJECT,
            m_state);
           
        }
        
        int ret = m_provider.answer(m_call.m_handle);
        
        if(ret < 0)
        {
            throw new ResourceUnavailableException(
                    javax.telephony.ResourceUnavailableException.UNKNOWN);
        }
    }
    
    /**
     * Returns the dynamic capabilities for the instance of the
     * TerminalConnection object. Dynamic capabilities tell the application which
     * actions are possible at the time this method is invoked based upon the
     * implementations knowledge of its ability to successfully perform the
     * action. This determination may be based upon argument passed to this
     * method, the current state of the call model, or some implementation-
     * specific knowledge. These indications do not guarantee that a particular
     * method will be successful when invoked, however.
     * <p>
     * The dynamic TerminalConnection capabilities require no additional
     * arguments.
     * <p>
     * @return The dynamic TerminalConnection capabilities.
 */
    public TerminalConnectionCapabilities getCapabilities() {
        if(m_state == TermConnRingingEv.ID)
            return new XTerminalConnectionCapabilities(true);
        else
            return new XTerminalConnectionCapabilities(false);
    }
    
    /**
     * Gets the TerminalConnectionCapabilities object with respect to a Terminal
     * and an Address. If null is passed as a Terminal parameter, the general/
     * provider-wide Terminal Connection capabilities are returned.
     * <p>
     * <STRONG>Note:</STRONG> This method has been replaced in JTAPI v1.2. The
     * <CODE>TerminalConnection.getCapabilities()</CODE> method returns the
     * dynamic TerminalConnection capabilities. This method now should simply
     * invoke the <CODE>TerminalConnection.getCapabilities()</CODE> method.
     * <p>
     * @deprecated Since JTAPI v1.2. This method has been replaced by the
     * TerminalConnection.getCapabilities() method.
     * @param address This argument is ignored in JTAPI v1.2 and later.
     * @param terminal This argument is ignored in JTAPI v1.2 and later.
     * @exception InvalidArgumentException This exception is never thrown in
     * JTAPI v1.2 and later.
     * @exception PlatformException A platform-specific exception occurred.
 */
    public TerminalConnectionCapabilities getTerminalConnectionCapabilities(Terminal terminal,Address address) throws InvalidArgumentException, PlatformException {
        return getCapabilities();
    }

    private void publishEvents(CallEv[] oEvt)
    {
        
        CallObserver cs[] = m_terminal.getCallObservers();
        
        for(int loop = 0; loop < cs.length; loop++)
        {
              CallObserver co = cs[loop];
              final CallObserver o = co;
              final CallEv[] fOEvt = oEvt;
              try {
                   Runnable r = new Runnable() {
                       public void run(){
                           try{
                               o.callChangedEvent(fOEvt);
                           } catch (Exception excp){
                           // handle publish exceptions
                           }
                       };
                    };
                    Thread T = new Thread(r);
                    T.start();
              } catch (Exception excp) {
                // Handle Exceptions;
              }        
       }
    }    
    
}
