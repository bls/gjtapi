/*
 * XTerminal.java
 *
 * Created on February 23, 2002, 9:19 AM
 */

package net.xtapi;

import javax.telephony.*;
import javax.telephony.events.*;
import javax.telephony.capabilities.TerminalCapabilities;
import java.util.*; 

/**
*  XTAPI JTapi implementation
*  Copyright (C) 2002 Steven A. Frare
* 
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*  
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
 * @author  Steven A. Frare
 * @version .02

The original files were lastly changed in April 2021 by Fabian Hall - f.hall (at) bls-integration.de
 */

public class XTerminal extends java.lang.Object implements javax.telephony.Terminal {
    
    private String      m_name;
    private Provider    m_provider;
    private Address     m_address;
    private Vector      m_observers = null ;
    private Vector      m_terminalConnections;

    /** Creates new XTerminal.  Only providers can create Terminals. */
    protected XTerminal(String name, Provider p, Address a) {
        super();
        m_name      = name;
        m_provider  = p;
        m_address   = a;
        m_observers = new Vector();
        m_terminalConnections = new Vector();
    }
    
    protected void addTerminalConnection(TerminalConnection tc)
    {
        m_terminalConnections.add(tc);
    }
    
    protected void setName(String name)
    {
        m_name = name;
    }

    /**
     * Returns the name of the Terminal. Each Terminal possesses a unique
     * name. This name is assigned by the implementation and may or may not
     * carry a real-world interpretation.
     * <p>
     * @return The name of the Terminal.
 */
    public String getName() {
        return m_name;
    }
    
    /**
     * Returns the Provider associated with this Terminal. This Provider object
     * is valid throughout the lifetime of the Terminal and does not change
     * once the Terminal is created.
     * <p>
     * @return The Provider associated with this Terminal.
 */
    public Provider getProvider() {
        return m_provider;
    }
    
    /**
     * Returns an array of Address objects associated with this Terminal object.
     * The Terminal object must have at least one Address object associated
     * with it. This list does not change throughout the lifetime of the
     * Terminal object.
     * <p>
     * <B>Post-conditions:</B>
     * <OL>
     * <LI>Let Address[] addrs = this.getAddresses()
     * <LI>addrs.length >= 1
     * </OL>
     * @return An array of Address objects associated with this Terminal.
 */
    public Address[] getAddresses() {
        Address[] a = new Address[1];
        a[0] = m_address;
        return a;
    }
    
    /**
     * Returns an array of TerminalConnection objects associated with this
     * Terminal. Once a TerminalConnection is added to a Terminal, the Terminal
     * maintains a reference until the TerminalConnection moves into the
     * <CODE>TerminalConnection.DROPPED</CODE> state. Therefore, all
     * TerminalConnections returned by this method will never be in the
     * <CODE>TerminalConnection.DROPPED</CODE> state. If there are no
     * TerminalConnections associated with this Terminal, this method returns
     * null.
     * <p>
     * <B>Post-conditions:</B>
     * <OL>
     * <LI>Let TerminalConnection tc[] = this.getTerminalConnections()
     * <LI>tc == null or tc.length >= 1
     * <LI>For all i, tc[i].getState() != TerminalConnection.DROPPED
     * </OL>
     * @return An array of TerminalConnection objects associated with this
     * Terminal.
 */
    public TerminalConnection[] getTerminalConnections() {
        if(m_terminalConnections.size() == 0)
            return null;
        
        TerminalConnection[] tc = new TerminalConnection[m_terminalConnections.size()];
        return (TerminalConnection[]) m_terminalConnections.toArray(tc);
    }
    
    /**
     * Adds an observer to the Terminal. The TerminalObserver reports all
     * Terminal-related state changes as events. The Terminal object will report
     * events to this TerminalObserver object for the lifetime of the Terminal
     * object or until the observer is removed with the
     * <CODE>Terminal.removeObserver()</CODE> or until the Terminal is no
     * longer observable. In these instances, a TermObservationEndedEv is
     * delivered to the observer as its final event. The observer will receive
     * no events after TermObservationEndedEv unless the observer is explicitly
     * re-added via the <CODE>Terminal.addObserver()</CODE> method. Also, once
     * an observer receives an TermObservationEndedEv, it will no longer be
     * reported via the <CODE>Terminal.getObservers()</CODE>.
     * <p>
     * If an application attempts to add an instance of an observer already
     * present on this Terminal, this attempt will silently fail, i.e. multiple
     * instances of an observer are not added and no exception will be thrown.
     * <p>
     * Currently, only the TermObservationEndedEv event is defined by the core
     * package and delivered to the TerminalObserver.
     * <p>
     * <B>Post-conditions:</B>
     * <OL>
     * <LI>observer is an element of this.getObservers()
     * </OL>
     * @param observer The observer being added.
     * @exception MethodNotSupportedException The observer cannot be added at this time
     * @exception ResourceUnavailableException The resource limit for the
     * numbers of observers has been exceeded.
 */
    public void addObserver(TerminalObserver observer) throws ResourceUnavailableException, MethodNotSupportedException {
        m_observers.remove(observer);
        m_observers.add(observer);
    }
    
    /**
     * Returns a list of all TerminalObservers associated with this Terminal
     * object. If there are no observers associated with this Terminal, this
     * method returns null.
     * <p>
     * <B>Post-conditions:</B>
     * <OL>
     * <LI>Let TerminalObserver[] obs = this.getObservers()
     * <LI>obs == null or obs.length >= 1
     * </OL>
     * @return An array of TerminalObserver objects associated with this
     * Terminal.
 */
    public TerminalObserver[] getObservers() {
        if(m_observers.size() == 0)
            return null;
        
        TerminalObserver[] to = new TerminalObserver[m_observers.size()];
        return (TerminalObserver[]) m_observers.toArray(to);
    }
    
    /**
     * Removes the given observer from the Terminal. If successful, the observer
     * will no longer receive events generated by this Terminal object. As its
     * final event, the TerminalObserver receives a TermObservationEndedEv.
     * <p>
     * If an observer is not part of the Terminal, then this method
     * fails silently, i.e. no observer is removed an no exception is thrown.
     * <p>
     * <B>Post-conditions:</B>
     * <OL>
     * <LI>A TermObservationEndedEv event is reported to the observer as its
     * final event.
     * <LI>observer is not an element of this.getObservers()
     * </OL>
     * @param observer The observer which is being removed.
 */
    public void removeObserver(TerminalObserver observer) {
        m_observers.remove(observer);
        
        XTermObservationEndedEv ev = new XTermObservationEndedEv(
                                                TermObservationEndedEv.ID,
                                                Ev.CAUSE_NORMAL,
                                                0,
                                                false,
                                                this);
        XTermObservationEndedEv[] aEv = new XTermObservationEndedEv[1];
        aEv[0] = ev;
        observer.terminalChangedEvent(aEv);        
    }
    
    /**
     * Adds an observer to a Call object when this Terminal object first becomes
     * part of that Call. This method permits applications to select a Terminal
     * object in which they are interested and automatically have the
     * implementation attach an observer to all present and future Calls which
     * come to this Terminal.
     *
     * <H5>JTAPI v1.0 Semantics</H5>
     *
     * In version 1.0 of the Java Telephony API specification, the application
     * monitored the Terminal object for the TermCallAtTermEv event. This event
     * indicated that a Call has come to this Terminal. Then, applications would
     * manually add an observer to the Call. With this architecture, potentially
     * dangerous race conditions existed while an application was adding an
     * observer to the Call. As a result, this mechanism has been replaced in
     * version 1.1.
     *
     * <H5>JTAPI v1.1 Semantics</H5>
     *
     * In version 1.1 of the specification, the TermCallAtTermEv event does not
     * exist and this method replaces the functionality described above. Instead
     * of monitoring for a TermCallAtTermEv event, the application simply uses
     * the <CODE>Terminal.addCallObserver()</CODE> method, and observer will be 
     * added to new telephone calls at this Terminal automatically.
     * <p>
     * If an application attempts to add an instance of a call observer already
     * present on this Terminal, these repeated attempts will silently fail, i.e.
     * multiple instances of a call observer are not added and no exception will
     * be thrown.
     * <p>
     * When a call observer is added to an Terminal with this method, the
     * following behavior is exhibited by the implementation.
     * <p>
     * <OL>
     * <LI>It is immediately associated with any existing calls at the Terminal
     * and a snapshot of those calls are reported to the call observer. For
     * each of these calls, the observer is reported via
     * <CODE>Call.getObservers()</CODE>.
     * <LI>It is associated with all future calls which come to this Terminal.
     * For each new call, the observer is reported via
     * <CODE>Call.getObservers().</CODE>
     * </OL>
     * <p>
     * Note that the definition of the term ".. when a call is at a Terminal"
     * means that the Call contains a Connection which contains a
     * TerminalConnection with this Terminal as its Terminal.
     *
     * <H5>Call Observer Lifetime</H5>
     *
     * For all call observers which are present on Calls because of this method,
     * the following behavior is exhibited with respect to the lifetime of the
     * call.
     * <p>
     * <OL>
     * <LI>The call observer receives events until the Call is no longer at this
     * Terminal. In this case, the call observer will be re-applied to the Call
     * if the Call returns to this Terminal at some point in the future.
     * <LI>The call observer is removed with <CODE>Call.removeObserver()</CODE>.
     * Note that this only affects the instance of the call observer for that
     * particular call. If the Call subsequently leaves and returns to the
     * Terminal, the observer will be re-applied.
     * <LI>The Call can no longer be monitored by the implementation.
     * <LI>The Call moves into the <CODE>Call.INVALID</CODE> state.
     * </OL>
     * <p>
     * When the CallObserver leaves the Call because of one of the reasons above,
     * it receives a CallObservationEndedEv as its final event.
     * <p>
     * <H5>Call Observer on Multiple Addresses and Terminals</H5>
     * It is possible for an application to add CallObservers to more than one
     * Address and Terminal (using <CODE>Address.addCallObserver()</CODE> and
     * <CODE>Terminal.addCallObserver()</CODE>, respectively). The rules outlined
     * above still apply, with the following additions:
     * <p>
     * <OL>
     * <LI>A CallObserver is not added to a Call more than once, even if it
     * has been added to more than one Address/Terminal which are present on the
     * Call.
     * <LI>The CallObserver leaves the call only if ALL of the Addresses and
     * Terminals on which the Call Observer was added leave the Call. If one
     * of those Addresses/Terminals becomes part of the Call again, the call
     * observer is re-applied to the Call.
     * </OL>
     * <p>
     * <B>Post-Conditions:</B>
     * <OL>
     * <LI>observer is an element of this.getCallObservers()
     * <LI>observer is an element of Call.getObservers() for each Call associated
     * with the Connections from this.getConnections().
     * <LI>An array of snapshot events are reported to the observer for existing
     * calls associated with this Terminal.
     * </OL>
     * @see javax.telephony.Call
     * @param observer The observer being added.
     * @exception MethodNotSupportedException The observer cannot be added at this time
     * @exception ResourceUnavailableException The resource limit for the
     * numbers of observers has been exceeded.
 */
    public void addCallObserver(CallObserver observer) throws ResourceUnavailableException, MethodNotSupportedException {
        m_observers.remove(observer);
        m_observers.add(observer);
    }
    
    /**
     * Returns a list of all CallObservers associated with this Terminal
     * object. That is, it returns a list of CallObserver objects which have
     * been added via the <CODE>Terminal.addCallObserver()</CODE> method. If
     * there are no Call observers associated with this Terminal object, this
     * method returns null.
     * <p>
     * <B>Post-conditions:</B>
     * <OL>
     * <LI>Let CallObserver[] obs = this.getCallObservers()
     * <LI>obs == null or obs.length >= 1
     * </OL>
     * @return An array of CallObserver objects associated with this
     * Address.
 */
    public CallObserver[] getCallObservers() {
        CallObserver[] co = new CallObserver[m_observers.size()];
        return (CallObserver[])m_observers.toArray(co);
    }
    
    /**
     * Removes the given CallObserver from the Terminal. In other words, it
     * removes a CallObserver which was added via the
     * <CODE>Terminal.addCallObserver()</CODE> method. If successful, the
     * observer will no longer be added to new Calls which are presented to this
     * Terminal, however it does not affect CallObservers which have already been
     * added at a Call.
     * <p>
     * Also, if the CallObserver is not part of the Terminal, then this method
     * fails silently, i.e. no observer is removed and no exception is thrown.
     * <p>
     * <B>Post-conditions:</B>
     * <OL>
     * <LI>observer is not an element of this.getCallObservers()
     * </OL>
     * @param observer The CallObserver which is being removed.
 */
    public void removeCallObserver(CallObserver observer) {
        m_observers.remove(observer);
    }
    
    /**
     * Returns the dynamic capabilities for the instance of the Terminal object.
     * Dynamic capabilities tell the application which actions are possible at
     * the time this method is invoked based upon the implementation's knowledge
     * of its ability to successfully perform the action. This determination may
     * be based upon argument passed to this method, the current state of the
     * call model, or some implementation-specific knowledge. These indications
     * do not guarantee that a particular method will be successful when invoked,
     * however.
     * <p>
     * The dynamic Terminal capabilities require no additional arguments.
     * <p>
     * @return The dynamic Terminal capabilities.
 */
    public TerminalCapabilities getCapabilities() {
        return new XTerminalCapabilities();
    }
    
    /**
     * Gets the TerminalCapabilities object with respect to a Terminal and an
     * Address. If null is passed as a Terminal parameter, the general/provider-
     * wide Terminal capabilities are returned.
     * <p>
     * <STRONG>Note:</STRONG> This method has been replaced in JTAPI v1.2. The
     * <CODE>Terminal.getCapabilities()</CODE> method returns the dynamic
     * Terminal capabilities. This method now should simply invoke the
     * <CODE>Terminal.getCapabilities()</CODE> method.
     * <p>
     * @deprecated Since JTAPI v1.2. This method has been replaced by the
     * Terminal.getCapabilities() method.
     * @param address This argument is ignored in JTAPI v1.2 and later.
     * @param terminal This argument is ignored in JTAPI v1.2 and later.
     * @exception InvalidArgumentException This exception is never thrown in
     * JTAPI v1.2 and later.
     * @exception PlatformException A platform-specific exception occurred.
 */
    public TerminalCapabilities getTerminalCapabilities(Terminal terminal,Address address) throws InvalidArgumentException, PlatformException {
        return getCapabilities();
    }
    
    public String toString()
    {
        return m_name;
    }

	@Override
	public void addTerminalListener(TerminalListener listener)
			throws ResourceUnavailableException, MethodNotSupportedException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public TerminalListener[] getTerminalListeners() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeTerminalListener(TerminalListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addCallListener(CallListener listener)
			throws ResourceUnavailableException, MethodNotSupportedException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public CallListener[] getCallListeners() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeCallListener(CallListener listener) {
		// TODO Auto-generated method stub
		
	}
    
}
