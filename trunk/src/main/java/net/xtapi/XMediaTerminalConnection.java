/*
 * XMediaTerminalConnection.java
 *
 * Created on March 2, 2002, 4:45 PM
 */

package net.xtapi;

import javax.telephony.*;
import javax.telephony.capabilities.TerminalConnectionCapabilities;
import javax.telephony.events.*;
import javax.telephony.media.*;
import javax.telephony.media.events.*;
import java.net.*;
import java.io.*;
//import javax.sound.sampled.*;

/*
*  XTAPI JTapi implementation
*  Copyright (C) 2002 Steven A. Frare
* 
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*  
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
 * @author  Steven A. Frare
 * @version .01

The original files were lastly changed in April 2021 by Fabian Hall - f.hall (at) bls-integration.de
 */

public class XMediaTerminalConnection extends XTerminalConnection implements 
    MediaTerminalConnection{

    private XTerminal       m_terminal;
    private XConnection     m_connection;
    private int             m_state;
    private XCall           m_call;
    private XProvider       m_provider;
    
    private URL             m_playURL;
    private URL             m_recURL;
    private boolean         m_bPlayURL;
    private boolean         m_bRecURL;
    
    /** Creates new XMediaTerminalConnection */
    public XMediaTerminalConnection(Terminal t, Connection c, Call call, Provider p) {

        super(t, c,call, p);
        m_terminal = (XTerminal)t ;
        m_connection = (XConnection)c ;
        m_state = TermConnCreatedEv.ID;
        m_call = (XCall)call;
        m_provider = (XProvider)p;
        
    }
    
    protected void setDigit(char digit)
    {
        CallEv[] oEvt = new CallEv[1];
        
        oEvt[0] = new XMediaTermConnDtmfEv(m_call,Ev.CAUSE_NEW_CALL,0,false, this, digit) ;
        publishEvents(oEvt);
        
    }


    /**
     * Returns the current media availability state, either AVAILABLE or
     * UNAVAILABLE.
     * <p>
     * @return The current availability of the media channel.
 */
    public int getMediaAvailability() {
        System.out.println("getMediaAvailability");
        return 0;
    }
    
    /**
     * Returns the current state of the terminal connection as a bit mask
     * of PLAYING and RECORDING. If there is not activity, then this method
     * returns <CODE>MediaTerminalConnection.NOACTIVITY</CODE>.
     * <p>
     * @return The current state of playing or recording.
 */
    public int getMediaState() {
        return m_state;
    }
    
    protected void setMediaState(int state)
    {
        m_state = state;
        
        CallEv[] oEvt = new CallEv[1];
        XAddress xa;
        
        switch(state)
        {
            case MediaTerminalConnection.NOACTIVITY:
                oEvt[0] = new XMediaTermConnStateEv(m_call,Ev.CAUSE_NEW_CALL,0,
                                false, this,MediaTerminalConnection.NOACTIVITY) ;
                publishEvents(oEvt);                
                break;
                
            case MediaTerminalConnection.AVAILABLE:
                oEvt[0] = new XMediaTermConnStateEv(m_call,Ev.CAUSE_NEW_CALL,0,
                                false, this,MediaTerminalConnection.AVAILABLE) ;
                publishEvents(oEvt);                
                break;

            case MediaTerminalConnection.UNAVAILABLE:
                oEvt[0] = new XMediaTermConnStateEv(m_call,Ev.CAUSE_NEW_CALL,0,
                                false, this,MediaTerminalConnection.UNAVAILABLE) ;
                publishEvents(oEvt);                
                break;

            case MediaTerminalConnection.PLAYING:
                oEvt[0] = new XMediaTermConnStateEv(m_call,Ev.CAUSE_NEW_CALL,0,
                                false, this,MediaTerminalConnection.PLAYING) ;
                publishEvents(oEvt);                
                break;


            case MediaTerminalConnection.RECORDING:
                oEvt[0] = new XMediaTermConnStateEv(m_call,Ev.CAUSE_NEW_CALL,0,
                                false, this,MediaTerminalConnection.RECORDING) ;
                publishEvents(oEvt);    
                break;
        }
    }
    
    /**
     * Instructs the terminal connection to use the default speaker for
     * recording from the telephone line.
     * <p>
     * @exception PrivilegeViolationException Indicates the application is
     * not permitted to direct voice media to the default speaker.
     * @exception ResourceUnavailableException Indicates that the speaker is
     * not currently available for use.
     * @exception MethodNotSupportedException This method is not supported by
     * the implementation.
 */
    public void useDefaultSpeaker() throws PrivilegeViolationException, ResourceUnavailableException, MethodNotSupportedException {
        m_bRecURL = false;
    }
    
    /**
     * Instructs the terminal connection to use a file for recording from
     * the telephone line.
     * <p>
     * @param url The URL-destination for the voice data for recording.
     * @exception PrivilegeViolationException Indicates the application is not
     * permitted to use the give URL for recording.
     * @exception ResourceUnavailableException Indicates the URL given is not
     * available, either because the URL was invalid or a network problem
     * occurred.
     * @exception MethodNotSupportedException This method is not supported by
     * the implementation.
     *
 */
    public void useRecordURL(URL url) throws PrivilegeViolationException, ResourceUnavailableException, MethodNotSupportedException {
        try{
            m_recURL = url;
            m_bRecURL = true;
            
        }catch(Exception e){
            throw new ResourceUnavailableException(
                    javax.telephony.ResourceUnavailableException.UNKNOWN);
        }        

    }
    
    /**
     * Instructs the terminal connection to use the default microphone for
     * playing to the telephone line.
     * <p>
     * @exception PrivilegeViolationException Indicates the application is
     * not permitted to direct voice media from the default microphone.
     * @exception ResourceUnavailableException Indicates that the microphone is
     * not currently available for use.
     * @exception MethodNotSupportedException This method is not supported by
     * the implementation.
     *
 */
    public void useDefaultMicrophone() throws PrivilegeViolationException, ResourceUnavailableException, MethodNotSupportedException {
        m_bPlayURL = false;
    }
    
    /**
     * Instructs the terminal connection to use a file for playing to
     * the telephone line.
     * <p>
     * @param url The URL-source of the voice data to play.
     * valid or available source of voice data.
     * @exception PrivilegeViolationException Indicates the application is not
     * permitted to use the give URL for playing.
     * @exception ResourceUnavailableException Indicates the URL given is not
     * available, either because the URL was invalid or a network problem
     * occurred.
     * @exception MethodNotSupportedException This method is not supported by
     * the implementation.
     *
 */
    public void usePlayURL(URL url) throws PrivilegeViolationException, ResourceUnavailableException, MethodNotSupportedException {

        try{
            InputStream s = url.openStream();
            
            m_playURL = url;
            m_bPlayURL = true;
            
        }catch(Exception e){
            throw new ResourceUnavailableException(
                    javax.telephony.ResourceUnavailableException.UNKNOWN);
        }        
    }
    
    /**
     * Start the playing. This method returns once playing has begun, that is,
     * when getMediaState() & PLAYING == PLAYING.
     * <p>
     * @exception MethodNotSupportedException The implementation does not
     * support playing to the telephone line.
     * @exception ResourceUnavailableException Indicates playing is not able
     * to be started because some resource is unavailable.
     * @exception InvalidStateException Indicates the TerminalConnection is not
     * in the media channel available state.
 */
    public void startPlaying() throws MethodNotSupportedException, ResourceUnavailableException, InvalidStateException {
        
        Address[] a = m_terminal.getAddresses();
        int iHandle;
        //
       iHandle = ((XAddress)a[0]).m_lineHandle;
        
       String media = null;

       if(m_bPlayURL == true)
       {
           media = m_playURL.toExternalForm();
       }
        
        final int handle =  iHandle;
        final String fMedia = media;        
        try{
              try {
                   Runnable r = new Runnable() {
                       public void run(){
                           try{
                                setMediaState(MediaTerminalConnection.PLAYING);
                                m_provider.playFile(fMedia,handle);
                                setMediaState(MediaTerminalConnection.AVAILABLE);
                           } catch (Exception excp){
                           // handle publish exceptions
                           }
                       };
                    };
                    Thread T = new Thread(r);
                    T.start();
              } catch (Exception excp) {
                // Handle Exceptions;
              }                 

        }catch(Exception e){
            System.out.println("XMediaTerminalConnection should throw.");
        }
    }
    
    /**
     * Stop the playing. This method returns once the playing has stopped, that
     * is, when getMediaState() & PLAYING == 0. If playing is not currently
     * taking place, this method has no effect.
 */
    public void stopPlaying() {
        Address[] a = m_terminal.getAddresses();
        int iHandle;
        if(m_bPlayURL = true)
            iHandle = ((XAddress)a[0]).m_lineHandle;
        else
            iHandle = -1;
        
       final int handle = iHandle;
       m_provider.stopPlaying(((XAddress)a[0]).m_lineHandle);

    }
    
    /**
     * Start the recording. This method returns once the recording has started,
     * that is, when getMediaState() & RECORDING == RECORDING.
     * <p>
     * @exception MethodNotSupportedException The implementation does not
     * support recording from the telephone line.
     * @exception ResourceUnavailableException Indicates recording is not able
     * to be started because some resource is unavailable.
     * @exception InvalidStateException Indicates the TerminalConnection is not
     * in the media channel available state.
 */
    public void startRecording() throws MethodNotSupportedException, ResourceUnavailableException, InvalidStateException {

        Address[] a = m_terminal.getAddresses();
        int iHandle;
        String media = null;
        
        if(m_bRecURL == true)
            media = m_recURL.toExternalForm();
        
       iHandle = ((XAddress)a[0]).m_lineHandle;
            
       final int handle =  iHandle;
        
       final String fMedia = media;
       final int lineHandle = handle;
       
        try{
           Runnable r = new Runnable() {
               public void run(){
                   try{
                        
                        if(m_provider.recordFile(fMedia,lineHandle) < 1)
                        {
                            setMediaState(MediaTerminalConnection.UNAVAILABLE);
                            throw new ResourceUnavailableException(
                            javax.telephony.ResourceUnavailableException.UNKNOWN);
                        }
                        else
                        {
                            if(getState() != XProvider.MEDIA_DISCONNECTED)
                            {
                                //setMediaState(MediaTerminalConnection.AVAILABLE);
                            }
                        }
                   } catch (Exception excp){
                       System.out.println("startRecording Caught Exception....");
                       m_provider.debugString(1,"Exception: " + excp.toString());

                   }
               };
            };
            Thread T = new Thread(r);
            T.start();

            m_state = m_state & MediaTerminalConnection.RECORDING;
        
        }catch(Exception e){
            System.out.println("XMediaTerminalConnection.startRecording should throw.");
            throw new ResourceUnavailableException(
            javax.telephony.ResourceUnavailableException.UNKNOWN);            
        }

    }
    
    /**
     * Stop the recording. This method returns once the recording has stopped,
     * that is, when getMediaState() & RECORDING == 0. If recording is not
     * currently taking place, this method has no effect.
 */
    public void stopRecording() {
        
        Address[] a = m_terminal.getAddresses();
        int iHandle;
        if(m_bRecURL = true)
            iHandle = ((XAddress)a[0]).m_lineHandle;
        else
            iHandle = -1;
        
       //final int handle = iHandle;        
       m_provider.stopRecording(((XAddress)a[0]).m_lineHandle);
       
    }
    
    public void setDtmfDetection(boolean enable) throws MethodNotSupportedException, ResourceUnavailableException, InvalidStateException {
        int result = m_provider.monitorDigits(m_call.m_handle, enable);
    }
    
    public void generateDtmf(String digits) throws MethodNotSupportedException, ResourceUnavailableException, InvalidStateException {
        int result = m_provider.sendDigits(m_call.m_handle, digits);
    }
    
    private void publishEvents(CallEv[] oEvt)
    {
        
        CallObserver cs[] = m_terminal.getCallObservers();
        
        for(int loop = 0; loop < cs.length; loop++)
        {
              CallObserver co = cs[loop];
              if( co instanceof MediaCallObserver)
              {
              final CallObserver o = co;
              final CallEv[] fOEvt = oEvt;
              try {
                   Runnable r = new Runnable() {
                       public void run(){
                           try{
                               o.callChangedEvent(fOEvt);
                           } catch (Exception excp){
                           // handle publish exceptions
                           }
                       };
                    };
                    Thread T = new Thread(r);
                    T.start();
              } catch (Exception excp) {
                // Handle Exceptions;
              }        
              }
       }
    }    
}
