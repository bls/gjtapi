/*
 * XMediaTermConnDtmfEv.java
 *
 * Created on March 2, 2002, 4:12 PM
 */

package net.xtapi;

import javax.telephony.* ;
import javax.telephony.events.TermConnEv;
import javax.telephony.events.*;
import javax.telephony.media.events.*;

/*
*  XTAPI JTapi implementation
*  Copyright (C) 2002 Steven A. Frare
* 
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*  
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
 * @author  Steven A. Frare
 * @version .01

The original files were lastly changed in April 2021 by Fabian Hall - f.hall (at) bls-integration.de
 */

public class XMediaTermConnDtmfEv extends XMediaTermConnEv implements MediaTermConnDtmfEv {

    private char m_digit;
    
    /** Creates new XMediaTermConnDtmfEv */
    protected XMediaTermConnDtmfEv(Call call, int cause, int metaCode, 
        boolean isNewMetaEvent, TerminalConnection tc, char digit)
    {
        super(call, MediaTermConnDtmfEv.ID, cause, metaCode, 
                isNewMetaEvent, tc) ;
        
        m_digit = digit;
    }
    
  /**
   * Returns the DTMF-digit which has been recognized. This digit may either
   * be the numbers zero through nine (0-9), the asterisk (*), or the pound
   * (#).
   * <p>
   * @return The DTMF-digit which has been detected.
   */
  public char getDtmfDigit()
  {
      return m_digit;
  }
}
