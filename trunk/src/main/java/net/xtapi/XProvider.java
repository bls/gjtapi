// Revision 8.01 2002-07-15 20:46:49 reggiehg
// Added "m_xtapi.XTDropCall(dwDevice);" in LINECALLSTATE_IDLE case of lineCallState.
// 	Should be updated when unanswered incoming calls are properly handled in the 
//	dropCall native method.
// 07-18-2002 sfrare    removed XTDropCall from LINECALLSTATE_IDLE and put logic
//                      into service provider to clean up.
// 07-19-2002 reggiehg	added "if (state == m_state) return;" at start of "setState()"
/*
 * XProvider.java
 *
 * Created on February 23, 2002, 9:02 AM
 */

package net.xtapi;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Duration;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.telephony.Address;
import javax.telephony.Call;
import javax.telephony.Connection;
import javax.telephony.InvalidArgumentException;
import javax.telephony.InvalidStateException;
import javax.telephony.MethodNotSupportedException;
import javax.telephony.PlatformException;
import javax.telephony.Provider;
import javax.telephony.ProviderListener;
import javax.telephony.ProviderObserver;
import javax.telephony.ProviderUnavailableException;
import javax.telephony.ResourceUnavailableException;
import javax.telephony.Terminal;
import javax.telephony.TerminalConnection;
import javax.telephony.capabilities.AddressCapabilities;
import javax.telephony.capabilities.CallCapabilities;
import javax.telephony.capabilities.ConnectionCapabilities;
import javax.telephony.capabilities.ProviderCapabilities;
import javax.telephony.capabilities.TerminalCapabilities;
import javax.telephony.capabilities.TerminalConnectionCapabilities;
import javax.telephony.events.ConnAlertingEv;
import javax.telephony.events.ConnConnectedEv;
import javax.telephony.events.ConnDisconnectedEv;
import javax.telephony.events.ConnInProgressEv;
import javax.telephony.events.Ev;
import javax.telephony.events.ProvEv;
import javax.telephony.events.TermConnActiveEv;
import javax.telephony.events.TermConnDroppedEv;
import javax.telephony.events.TermConnRingingEv;

import net.xtapi.serviceProvider.IXTapi;
import net.xtapi.serviceProvider.IXTapiCallBack;

/*
*  XTAPI JTapi implementation
*  Copyright (C) 2002 Steven A. Frare
* 
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*  
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
 * @author  Steven A. Frare
 * @version .01

The original files were lastly changed in April 2021 by Fabian Hall - f.hall (at) bls-integration.de
 */

public class XProvider implements Provider, IXTapiCallBack {

    /** The media connection is connected. */
    protected static final int MEDIA_CONNECTED      = 10001;
    /** The media connection is disconnected. */
    protected static final int MEDIA_DISCONNECTED   = 10002;
    
    /** MS TAPI Provider */
    protected static final int MSTAPI               = 11001;
    
    /** Serial Modem Provider */
    protected static final int SERIAL               = 11002;

    /** Debug Constants */
    protected static final int FATL = 0;
    protected static final int ERR  = 1;
    protected static final int WARN = 2;
    protected static final int INFO = 4;
    protected static final int DEBG = 6;
    protected static final int SPAM = 8;
    
    private static final String m_name = "net.xtapi.XProvider";
    private final Hashtable<String, XAddress> m_addresses;
    private final Hashtable<String, Terminal> m_terminals;
    private final Hashtable<String, Call> m_calls;
    private final Vector<ProviderObserver> m_providerObservers;
    private final Vector<ProviderListener> m_providerListeners;
    private int m_state = javax.telephony.Provider.OUT_OF_SERVICE;
    private int m_numLines = -1;
    
    // For logging
    private BufferedOutputStream logFile;
    
    private IXTapi m_xtapi = null;
    
    /** Creates new XProvider */
    protected XProvider(String provider, Duration initTimeout) throws ProviderUnavailableException {

        try{
            Class<?> XTAPIProvider = Class.forName(provider);
            m_xtapi = (IXTapi) XTAPIProvider.newInstance();
        }catch(Exception e){
            System.out.println("Failed to load class: " + e.toString());
            throw new ProviderUnavailableException();
        }
            
        m_calls     = new Hashtable<>();
        m_terminals = new Hashtable<>();
        m_addresses = new Hashtable<>();
        m_providerObservers = new Vector<>();
        m_providerListeners = new Vector<>();
        
        try{
            FileOutputStream tfile = new FileOutputStream("XTAPI.log");
                logFile = new BufferedOutputStream(tfile);
           }catch(Exception e){
                System.out.println("Will not log: " + e.toString());
           }
        
        m_numLines = m_xtapi.XTinit(this, initTimeout);
        
        if(m_numLines > 0)
        {
            setState(Provider.IN_SERVICE);
            debugString(5,"Initialized with " + m_numLines + " lines.");
            for(int loop = 0; loop < m_numLines; loop++)
            {
                try{
                    XAddress a = new XAddress(this, true, loop + "", loop);
                    m_addresses.put(loop + "",a);
                    m_terminals.put(loop + "",a.m_terminal);
                }catch(Exception e){ // could not create the address
                    debugString(4,"Could not create address: " + loop);
                }
            }
        }
        else
            debugString(0,"Failed to initialize the IXTapi object.  No Lines!");
    }
    
    private void setState(int state)
    {
	if (state == m_state) return;
        m_state = state;
        publishEvents(state);
    }
    
    protected int monitorDigits(int handle, boolean enable)
        throws MethodNotSupportedException, ResourceUnavailableException, InvalidStateException
    {
        return m_xtapi.XTMonitorDigits(handle, enable);
    }
    
    protected int sendDigits(int handle, String digits)
        throws MethodNotSupportedException, ResourceUnavailableException, InvalidStateException
    {
        return m_xtapi.XTSendDigits(handle,digits);
    }

    protected int stopPlaying(int handle)
    {
        return m_xtapi.XTStopPlaying(handle);
    }
    
    protected int stopRecording(int handle)
    {
        return m_xtapi.XTStopRecording(handle);
    }
    
    protected void connect(int line, String dest, Call call, Address a)
        throws ResourceUnavailableException, InvalidStateException
    {
        m_calls.put(line + "",call);
        XAddress xa = (XAddress) a;
        XCall c = (XCall)call;
        
        int iHandle = m_xtapi.XTConnectCall(line,dest,xa.m_lineHandle);
        c.setHandle(iHandle);
        
         debugString(8," " + iHandle);
    }
    
    protected void dropCall(XCall c, Address a)
        throws ResourceUnavailableException, InvalidStateException
    {
        m_xtapi.XTDropCall(c.m_handle);

        XAddress xa = (XAddress) a;
        Object o = m_calls.remove(xa.m_id + "");
        
        if(null == o)
            debugString(1,"Failed to remove call: " + xa.m_id);
    }
    
    protected int open(int line)
        throws ResourceUnavailableException, InvalidStateException
    {
        return m_xtapi.XTOpenLine(line);
    }
    
    protected int answer(int handle)
        throws ResourceUnavailableException, InvalidStateException
    {
        int ret = m_xtapi.XTAnswerCall(handle);
        return ret;
    }
    
    protected int playFile(String file, int line)
        throws MethodNotSupportedException, ResourceUnavailableException, InvalidStateException
    {
        if(null != file)
        {
            if(file.startsWith("file:"))
            {
                file = file.substring(5);
            }
            debugString(6,"playFile " + file + " on line " + line);
        }
        else
            debugString(6,"useDefaultMicrophone.");
        
        m_xtapi.XTPlaySound(file,line);
        
        return 0;
    }
    
    protected int recordFile(String file, int line)
        throws MethodNotSupportedException, ResourceUnavailableException, InvalidStateException
    {
        if(null != file)
        {
            if(file.startsWith("file:"))
            {
                file = file.substring(5);
            }
            debugString(6,"recordFile " + file + " on line " +
                        line);
        }
        else
            debugString(6,"useDefaultSpeaker to render line " + 
                        line);
        
        return m_xtapi.XTRecordSound(file,line);
    }    
    
    @Override
    public void callback(int dwDevice,int dwMessage, int dwInstance,
                          int dwParam1,int dwParam2,int dwParam3)
    {
        try{
            
        switch (dwMessage)
        {
            case LINE_ADDRESSSTATE:
                debugString(5,"LINE_ADDRESSSTATE");
                break;
                
            case LINE_CALLINFO:
                debugString(5,"LINE_CALLINFO");
                String[] s = m_xtapi.XTGetCallInfo(dwDevice);
                
                if(null != s)
                {
                    // We got caller id info
                    // An array of two strings 1. Name 2. Number
                    // Set the remote terminal name == name
                    // Set the remote address name == number
                    try{
                        Call c = m_calls.get(dwInstance + "");
                        if(c != null)
                        {
                            Connection[] ct = c.getConnections();

                            for(int loop = 0; loop < ct.length; loop++)
                            {
                                XAddress xa = (XAddress)ct[loop].getAddress();

                                // verify we have the remote address
                                if(!xa.m_local)
                                {
                                    // Its name is the remote phone number
                                    xa.m_name = s[1];

                                    // now find its terminal
                                    Terminal[] t = xa.getTerminals();
                                    // Its name is the remote name
                                    ((XTerminal)t[0]).setName(s[0]);
                                }
                            }
                        }
                    }catch(Exception e){
                        // do nothing hardly fatal to miss caller id
                        debugString(8,"LINE_CALLINFO: " + e.toString());
                    }
                }
                break;
                
            case LINE_CALLSTATE:
                debugString(8,"LINE_CALLSTATE");
                lineCallState(dwDevice,dwInstance,dwParam1,dwParam2,dwParam3);
                break;
                
            case LINE_CLOSE:
                debugString(5,"LINE_CLOSE");
                break;
                
            case LINE_DEVSPECIFIC:
                debugString(5,"LINE_DEVSPECIFIC");
                break;
                
            case LINE_DEVSPECIFICFEATURE:
                debugString(5,"LINE_DEVSPECIFICFEATURE");
                break;
                
            case LINE_GATHERDIGITS:
                debugString(5,"LINE_GATHERDIGITS");
                break;
                
            case LINE_MONITORDIGITS:
                debugString(5,"LINE_MONITORDIGITS");
/*                
                debugString(5,"dwDevice -> " + dwDevice + " dwInstance -> " + dwInstance +
                " dwParam1 -> "  + dwParam1 + " dwParam2 -> " + dwParam2 + 
                " dwParam3 -> " + dwParam3);
*/                
                Call c = m_calls.get(dwInstance + "");
                if(c != null)
                {
                    Connection[] ct = c.getConnections();
                    
                    for(int loop = 0; loop < ct.length; loop++)
                    {
                        XAddress xa = (XAddress)ct[loop].getAddress();
                        
                        if(xa.m_local)
                        {
                            TerminalConnection[] tc = 
                                ct[loop].getTerminalConnections();

                            for(int iLoop = 0; iLoop < tc.length; iLoop++)
                            { 
                                if(tc[iLoop] instanceof XMediaTerminalConnection)
                                {
                                    XMediaTerminalConnection xtc = 
                                        (XMediaTerminalConnection)tc[iLoop];

                                    xtc.setDigit((char)dwParam1);
                                }
                            }
                        }
                    }
                }
                break;                
                
            case LINE_LINEDEVSTATE:
                debugString(8,"LINE_LINEDEVSTATE");
                lineDevState(dwDevice,dwInstance,dwParam1,dwParam2,dwParam3);
                break;
                
            case LINE_MONITORMEDIA:
                debugString(5,"LINE_MONITORMEDIA");
                break;
                
            case LINE_MONITORTONE:
                debugString(5,"LINE_MONITORTONE");
                break;
                
            case LINE_REPLY:
                debugString(5,"LINE_REPLY -> dwInstance: " + dwInstance +
                    " -> dwParam2: " + dwParam2);
                /*
                TODO:   Check LINE_REPLY Data for success
                        Switch on current state, maybe connecting or
                        disconnecting.
                 */
                break;
                
            case LINE_REQUEST:
                debugString(5,"LINE_REQUEST");
                break;
                
            case PHONE_BUTTON:
                debugString(5,"PHONE_BUTTON");
                break;
                
            case PHONE_CLOSE:
                debugString(5,"PHONE_CLOSE");
                break;
                
            case PHONE_DEVSPECIFIC:
                debugString(5,"PHONE_DEVSPECIFIC");
                break;
                
            case PHONE_REPLY:
                debugString(5,"PHONE_REPLY");
                break;
                
            case PHONE_STATE:
                debugString(5,"PHONE_STATE");
                break;
                
            case LINE_CREATE:
                debugString(5,"LINE_CREATE");
                break;
                
            case PHONE_CREATE:
                debugString(5,"PHONE_CREATE");
                break;
                
            default:
                debugString(5,"UNKNOWN TAPI EVENT: " + dwMessage);
                break;
        }
        
        }catch(Exception e){
            System.out.println("Exception " + e.toString() + " in callback()");
        }
    }
    
    @Override
    public int getState() {
        return m_state;
    }
    
    @Override
    public String getName() {
        return m_name;
    }
    
    @Override
    public Call[] getCalls() {
        Call[] c = new XCall[m_calls.size()];
        Enumeration<Call> e = m_calls.elements();
        int i = 0;
        
        while(e.hasMoreElements())
        {
            c[i] = e.nextElement();
            i++;
        }
        return c;
    }
    
    @Override
    public Address getAddress(String number) throws InvalidArgumentException {
        try {
            XAddress a = m_addresses.get(number);
            if (null == a) {
                throw new InvalidArgumentException();
            }
            return a;
        }
        catch (Exception e) {
            throw new InvalidArgumentException();
        }
    }
    
    @Override
    public Address[] getAddresses() {
       
        Address[] a = new Address[m_addresses.size()];
        Enumeration<XAddress> e = m_addresses.elements();

        int loop = 0;
        
        while(e.hasMoreElements())
        {
            a[loop] = e.nextElement();
            loop++;
        }
        return (a.length > 0) ? a : null;
    }
    
    @Override
    public Terminal[] getTerminals() {
        Terminal[] t = new Terminal[m_terminals.size()];
        Enumeration<Terminal> e = m_terminals.elements();
        
        int loop = 0;
        
        while(e.hasMoreElements())
        {
            t[loop] = e.nextElement();
            loop++;
        }
        return (t.length > 0) ? t : null;
    }
    
    @Override
    public Terminal getTerminal(String name) throws InvalidArgumentException {
        try {
            XTerminal t = (XTerminal)m_terminals.get(name);
            if (null == t) {
                throw new InvalidArgumentException();
            }
            return t;
        }
        catch(Exception e) {
            throw new InvalidArgumentException();
        }
    }
    
    @Override
    public void shutdown() {
        if(m_state != Provider.SHUTDOWN) {
            try {
                logFile.flush();
                logFile.close();
                m_xtapi.XTShutdown();
            }
            catch(Exception e){
                
            }
            setState(Provider.SHUTDOWN);
        }
    }
    
    @Override
    public Call createCall() {
        return new XCall(this);
    }
    
    @Override
    public void addObserver(ProviderObserver observer) {
        m_providerObservers.remove(observer);
        m_providerObservers.add(observer);
    }
    
    @Override
    public ProviderObserver[] getObservers() {
        ProviderObserver[] p = new ProviderObserver[m_providerObservers.size()];
        return m_providerObservers.toArray(p);
    }
    
    @Override
    public void removeObserver(ProviderObserver observer) {
        m_providerObservers.remove(observer);
    }
    
    @Override
    public ProviderCapabilities getProviderCapabilities() {
        return new XProviderCapabilities(true) ;
    }
    
    @Override
	public CallCapabilities getCallCapabilities() {
        return new XCallCapabilities(true);
    }
    
    @Override
    public AddressCapabilities getAddressCapabilities() {
        return new XAddressCapabilities();
    }
    
    @Override
    public TerminalCapabilities getTerminalCapabilities() {
        return new XTerminalCapabilities();
    }
    
    @Override
    public ConnectionCapabilities getConnectionCapabilities() {
        return new XConnectionCapabilities(true);
    }
    
    @Override
    public TerminalConnectionCapabilities getTerminalConnectionCapabilities() {
        return new XTerminalConnectionCapabilities(true);
    }
    
    @Override
    public ProviderCapabilities getCapabilities() {
        return new XProviderCapabilities(true);
    }
    
    @Deprecated
    @Override
    public ProviderCapabilities getProviderCapabilities(Terminal terminal) throws PlatformException {
        return getProviderCapabilities();
    }
    
    @Deprecated
    @Override
    public CallCapabilities getCallCapabilities(Terminal terminal, Address address) throws PlatformException {
        return getCallCapabilities();
    }
    
    @Deprecated
    @Override
    public ConnectionCapabilities getConnectionCapabilities(Terminal terminal, Address address) throws PlatformException {
        return getConnectionCapabilities();
    }
    
    @Deprecated
    @Override
    public AddressCapabilities getAddressCapabilities(Terminal terminal) throws PlatformException {
        return getAddressCapabilities();
    }
    
    @Deprecated
    @Override
    public TerminalConnectionCapabilities getTerminalConnectionCapabilities(Terminal terminal) throws PlatformException {
        return getTerminalConnectionCapabilities();
    }
    
    @Deprecated
    @Override
    public TerminalCapabilities getTerminalCapabilities(Terminal terminal) throws PlatformException {
        return getTerminalCapabilities();
    }
    
    //callback received a LINE_DEVSTATE event:
    private void lineDevState(int dwDevice, int dwInstance, int dwParam1, int dwParam2, int dwParam3)
    {
        switch(dwParam1)
        {
            case LINEDEVSTATE_OTHER:
                debugString(5,"LINEDEVSTATE_OTHER");
                break;
                
            case LINEDEVSTATE_RINGING:
               //dwParam3 contains the ring count.
                debugString(5,"LINEDEVSTATE_RINGING -> " + dwParam3);
                debugString(8,"dwInstance = " + dwInstance);
                
                Call c = m_calls.get(dwInstance + "");
                if(c != null)
                {
                    Connection[] ct = c.getConnections();
                    TerminalConnection[] tc = ct[1].getTerminalConnections();
                    
                    for(int loop = 0; loop < tc.length; loop++)
                    {
                        XTerminalConnection xtc = (XTerminalConnection)tc[loop];
                        xtc.setState(TermConnRingingEv.ID);
                    }
                }
                break;
                
            case LINEDEVSTATE_CONNECTED:
                debugString(5,"LINEDEVSTATE_CONNECTED");
                break;
                
            case LINEDEVSTATE_DISCONNECTED:
                debugString(5,"LINEDEVSTATE_DISCONNECTED");
                break;
                
            case LINEDEVSTATE_MSGWAITON:
                debugString(5,"LINEDEVSTATE_MSGWAITON");
                break;
                
            case LINEDEVSTATE_MSGWAITOFF:
                debugString(5,"LINEDEVSTATE_MSGWAITOFF");
                break;
                
            case LINEDEVSTATE_INSERVICE:
                debugString(5,"LINEDEVSTATE_INSERVICE");
                break;
                
            case LINEDEVSTATE_OUTOFSERVICE:
                debugString(5,"LINEDEVSTATE_OUTOFSERVICE");
                break;
                
            case LINEDEVSTATE_MAINTENANCE:
                debugString(5,"LINEDEVSTATE_MAINTENANCE");
                break;
                
            case LINEDEVSTATE_OPEN:
                debugString(5,"LINEDEVSTATE_OPEN");
                break;
                
            case LINEDEVSTATE_CLOSE:
                debugString(5,"LINEDEVSTATE_CLOSE");
                break;
                
            case LINEDEVSTATE_NUMCALLS:
                debugString(5,"LINEDEVSTATE_NUMCALLS");
                break;
                
            case LINEDEVSTATE_NUMCOMPLETIONS:
                debugString(5,"LINEDEVSTATE_NUMCOMPLETIONS");
                break;
                
            case LINEDEVSTATE_TERMINALS:
                debugString(5,"LINEDEVSTATE_TERMINALS");
                break;

            case LINEDEVSTATE_ROAMMODE:
                debugString(5,"LINEDEVSTATE_ROAMMODE");
                break;

            case LINEDEVSTATE_BATTERY:
                debugString(5,"LINEDEVSTATE_BATTERY");
                break;

            case LINEDEVSTATE_SIGNAL:
                debugString(5,"LINEDEVSTATE_SIGNAL");
                break;

            case LINEDEVSTATE_DEVSPECIFIC:
                debugString(5,"LINEDEVSTATE_DEVSPECIFIC");
                break;

            case LINEDEVSTATE_REINIT:
                debugString(5,"LINEDEVSTATE_REINIT");
                break;

            case LINEDEVSTATE_LOCK:
                debugString(5,"LINEDEVSTATE_LOCK");
                break;

            case LINEDEVSTATE_CAPSCHANGE:
                debugString(5,"LINEDEVSTATE_CAPSCHANGE");
                break;

            case LINEDEVSTATE_CONFIGCHANGE:
                debugString(5,"LINEDEVSTATE_CONFIGCHANGE");
                break;

            case LINEDEVSTATE_TRANSLATECHANGE:
                debugString(5,"LINEDEVSTATE_TRANSLATECHANGE");
                break;

            case LINEDEVSTATE_COMPLCANCEL:
                debugString(5,"LINEDEVSTATE_COMPLCANCEL");
                break;

            case LINEDEVSTATE_REMOVED:
                debugString(5,"LINEDEVSTATE_REMOVED");
                break;

            default:
                debugString(5,"LINEDEVSTATE_UNKNOWN");
                break; 
        }
    }
    
    //callback received a LINE_CALLSTATE event:
    private void lineCallState(int dwDevice, int dwInstance, int dwParam1, int dwParam2, int dwParam3)
    {
        switch (dwParam1)
        {
            case LINECALLSTATE_IDLE:
                debugString(5,"LINECALLSTATE_IDLE");
                try{
                    Call c = m_calls.get(dwInstance + "");
                    if(c != null)
                    {
				// Revision 8.01: should be updated when unanswered incoming 
				// calls are properly handled in the dropCall native method
				//m_xtapi.XTDropCall(dwDevice); 

                        Connection[] ct = c.getConnections();

                        for(int loop = 0; loop < ct.length; loop++)
                        {
                            XConnection xc = (XConnection)ct[loop];
                            xc.setState(ConnDisconnectedEv.ID);
                            
                            TerminalConnection[] tc = xc.getTerminalConnections();
                            
                            for(int iLoop = 0; iLoop < tc.length; iLoop++)
                            {
                                XTerminalConnection xtc = 
                                    (XTerminalConnection)tc[iLoop];
                                xtc.setState(TermConnDroppedEv.ID);
                            }
                        }
                    }
                }catch(Exception e){
                    debugString(0,"LINECALLSTATE_IDLE exception: " + e.toString());
                }
                
                //Connection.DISCONNECTED
                break;

            case LINECALLSTATE_OFFERING:
                debugString(5,"LINECALLSTATE_OFFERING");
                debugString(8,"dwInstance = " + dwInstance);
                
                Terminal t = m_terminals.get(dwInstance + "");
                
                if(null != t){
                    try{
                    XCall c = new XCall(this);
                    m_calls.remove(dwInstance + "");
                    m_calls.put(dwInstance + "",c);
                    
                    debugString(5,"Creating call with handle: " + dwDevice);
                    c.setHandle(dwDevice);
                    
                    //According to the 1.2 Spec:
                    /*
                    ...exactly two Connections will be created and returned. The Connection 
                    associated with the originating endpoint is the first Connection in the 
                    returned array, and the Connection associated with the destination 
                    endpoint is the second Connection in the returned array. 
                    */
                    // Create objects representing the remote party first.
                    // Don't know the address unless we get callerid later..
                    
                    Address a = new XAddress(this, false, "UNKNOWN", -1 );
                    //Terminal remote = new XTerminal("UNKNOWN",this,a);
                    
                    Terminal[] r = a.getTerminals();
                    Terminal remote = r[0];

                    XConnection ct = new XConnection(c, a, this);
                    
                    c.addConnection(ct);
                    
                    XTerminalConnection tc = new XTerminalConnection(remote,ct,c,this);
                    
                    // The originating connection must be active and inprogress.  connected
                    tc.setState(TermConnActiveEv.ID);
                    ct.setState(ConnInProgressEv.ID);
                    
                    // Now the destination end.
                    Address[] b = t.getAddresses();
                    XConnection local = new XConnection(c,b[0],this);
                    c.addConnection(local);
                    XMediaTerminalConnection lTC = new XMediaTerminalConnection(t,local,c,this);
                    local.setState(ConnAlertingEv.ID);

                    //Send a ringing event here, H.323 only sends offering
                    //so if this is a VOIP call this is our only chance to
                    //pick it up.                    
                    lTC.setState(TermConnRingingEv.ID);
                    }catch(Exception e){
                        debugString(1,"Exception in LINECALLSTATE_OFFERING: " +
                            e.toString());
                    }
                }
                break;

            case LINECALLSTATE_ACCEPTED:
                debugString(5,"LINECALLSTATE_ACCEPTED");
                break;

            case LINECALLSTATE_DIALTONE:
                debugString(5,"LINECALLSTATE_DIALTONE");
                break;

            case LINECALLSTATE_DIALING:
                debugString(5,"LINECALLSTATE_DIALING");
                break;

            case LINECALLSTATE_RINGBACK:
                debugString(5,"LINECALLSTATE_RINGBACK");
                break;

            case LINECALLSTATE_BUSY:
                debugString(5,"LINECALLSTATE_BUSY");
                break;

            case LINECALLSTATE_SPECIALINFO:
                debugString(5,"LINECALLSTATE_SPECIALINFO");
                break;

            case LINECALLSTATE_CONNECTED:
                debugString(5,"LINECALLSTATE_CONNECTED");
                try{
                    Call c = m_calls.get(dwInstance + "");
                    if(c != null)
                    {
                        Connection[] ct = c.getConnections();
                        
                        for(int loop = 0; loop < ct.length; loop++)
                        {
                            XConnection xc = (XConnection)ct[loop];
                            xc.setState(ConnConnectedEv.ID);
                            
                            TerminalConnection[] tc = xc.getTerminalConnections();
                            for(int iLoop = 0; iLoop < tc.length; iLoop++)
                            {
                                XTerminalConnection xtc = (XTerminalConnection)tc[iLoop];
                                xtc.setState(TermConnActiveEv.ID);
                            }                              
                        }
                    }
                    else
                        debugString(0,"Unknown device for LINECALLSTATE_CONNECTED");
                }catch(Exception e){
                    debugString(0,"Exception in LINECALLSTATE_CONNECTED");
                }                
                
                break;

            case LINECALLSTATE_PROCEEDING:
                // Outgoing call invokes two JTAPI Connection events.
                debugString(5,"LINECALLSTATE_PROCEEDING");
                debugString(8,"dwInstance = " + dwInstance);
                
                try{
                    Call c = m_calls.get(dwInstance + "");
                    if(c != null)
                    {
                        Connection[] ct = c.getConnections();
                        
                        XConnection xc = (XConnection)ct[0];
                        xc.setState(ConnInProgressEv.ID);
                        
                        TerminalConnection[] tc = xc.getTerminalConnections();
                        for(int iLoop = 0; iLoop < tc.length; iLoop++)
                        {
                            XTerminalConnection xtc = (XTerminalConnection)tc[iLoop];
                            xtc.setState(TermConnActiveEv.ID);
                        }                           
                        
                        xc = (XConnection)ct[1];
                        xc.setState(ConnAlertingEv.ID);
                    }
                    else
                        debugString(0,"Unknown device for LINECALLSTATE_PROCEEDING");
                }catch(Exception e){
                    debugString(0,"Exception in LINECALLSTATE_PROCEEDING");
                }
                
                break;

            case LINECALLSTATE_ONHOLD:
                debugString(5,"LINECALLSTATE_ONHOLD");
                break;

            case LINECALLSTATE_CONFERENCED:
                debugString(5,"LINECALLSTATE_CONFERENCED");
                break;

            case LINECALLSTATE_ONHOLDPENDCONF:
                debugString(5,"LINECALLSTATE_ONHOLDPENDCONF");
                break;

            case LINECALLSTATE_ONHOLDPENDTRANSFER:
                debugString(5,"LINECALLSTATE_ONHOLDPENDTRANSFER");
                break;

            case LINECALLSTATE_DISCONNECTED:
                debugString(5,"LINECALLSTATE_DISCONNECTED");
                debugString(5,"dwInstance = " + dwInstance);
                try{
                    Call c = m_calls.get(dwInstance + "");
                    if(c != null)
                    {
                        Connection[] ct = c.getConnections();
                        
                        for(int loop = 0; loop < ct.length; loop++)
                        {
                            XConnection xc = (XConnection)ct[loop];
                            XAddress a = (XAddress)xc.getAddress();
                            
                            if(!a.m_local)
                            {
                                xc.setState(ConnDisconnectedEv.ID);
                                TerminalConnection[] tc = xc.getTerminalConnections();

                                for(int iLoop = 0; iLoop < tc.length; iLoop++)
                                {
                                    try{
                                        XTerminalConnection xtc = (XTerminalConnection)tc[iLoop];
                                        xtc.setState(TermConnDroppedEv.ID);
                                    }catch(Exception e){
                                        System.out.println("Exception setting TermConnDroppedEv");
                                    }
                                }
                                //break;
                            }
                            else
                            {

                                //System.out.println("would set remote to unknown");
                            }
                        }
                    }
                    else
                    {
                        debugString(0,"Unknown device for LINECALLSTATE_DISCONNECTED");
                        m_xtapi.XTDropCall(dwDevice);
                    }
                }catch(Exception e){
                    debugString(0,"Exception in LINECALLSTATE_DISCONNECTED");
                }       
                break;

            case LINECALLSTATE_UNKNOWN:
                debugString(5,"LINECALLSTATE_UNKNOWN");
                break;
                
            default:
                break; 
        }
    }

    private void publishEvents(int evt) {
        Enumeration<ProviderObserver> e = m_providerObservers.elements();
        
        while(e.hasMoreElements()) {
            final int fEvt = evt;
            final Provider fp = this;
            final ProviderObserver p = e.nextElement();
            try {
                Runnable r = () -> {
                    try {
                        ProvEv oEvt = new XProvEv(fEvt, Ev.CAUSE_NORMAL, 0, false, fp);
                        ProvEv[] pe = new ProvEv[1];
                        pe[0] = oEvt;
                        p.providerChangedEvent(pe);
                    }
                    catch (Exception excp) {
                        // handle publish exceptions
                    }
                };
                Thread T = new Thread(r);
                T.start();
            }
            catch (Exception excp) {
                // Handle Exceptions;
            }
        }
    }
    
    //////////////////////////////////////////////////////////////////
    // Begin the Logging Methods
    //
    //
    /////////////////////////////////////////////////////////////////
    
    private void debugString(int sev, String method, String message, String source)
    {
        Calendar c = Calendar.getInstance();
        
        // Fifty char string to pad message.
        String pad = "                                                  ";
        
        String now = c.get(Calendar.HOUR_OF_DAY) + ":" + 
                     c.get(Calendar.MINUTE)  + ":" + 
                     c.get(Calendar.SECOND)  + ":" + 
                     c.get(Calendar.MILLISECOND);
        
        if(message.length() < 50)
            message = message + pad.substring(0,50 - message.length());
        
        String log = now + "\t" + sev + "\t" + message + "\t" + method 
                + " @ " + source + "\r\n";
        try{
            logFile.write(log.getBytes());
            if(sev < 2)
                logFile.flush();            
        }catch(IOException e){
            System.out.println("IOException writing log file: " + e.toString());
        }
 
        //System.out.println(method + "\t" + message + "\t" + source);
    }
    
    /**
     * Logs a message to the file.
     */
    public void debugString(int sev, String message)
    {
        String s = getStackTraceAsString();
        int i = s.indexOf('(');
        debugString(sev,
                    s.substring(0,i),
                    message,
                    s.substring(i + 1,s.length()-1));
    }
    /**
     * Logs the message from Exception.getMessage()
     */
    protected void debugString(Exception e)
    {
        try{
        String s = getStackTraceAsString();
        int i = s.indexOf('(');
        debugString(1,
                    s.substring(0,i),
                    "Exception: " + e.getMessage(),
                    s.substring(i + 1,s.length()-1));
        }catch(Exception ex){
            System.out.println("debugString: Exception from exception.");
        }
    }
    
    /**
     * Sets the level of logging.  Used only for local logging.  It should
     * be one of {@link #DEBUG_ERR} or {@link #DEBUG_WARN} or 
     * {@link #DEBUG_INFO} or {@link #DEBUG_DEV} if it is not it will be 
     * set to {@link #DEBUG_ERR} which is the lowest level.
     */
    protected void setDebugLevel(int level)
    {
        if(level < 1 || level > 4)
            level = 1;
        //debugLevel(level);
    }
    
    /**
     * A much cleaner way to do this was introduced in Java 1.4
     */
    private static String getStackTraceAsString() 
    {
        //
        // StringWriter will contain text of stack trace
        //
        StringWriter stringWriter = new StringWriter();

        //
        // Need to encapsulate the StringWriter into a Printwriter object
        // to fill up with stack trace
        //
        PrintWriter printWriter = new PrintWriter(stringWriter);

        //
        // Get the stack trace and fill the PrintWriter
        //
        Throwable t = new Throwable();
        t.printStackTrace(printWriter);
        
        //
        // StringBuffer to hold stack trace
        //
        StringBuffer error = stringWriter.getBuffer();
    
        //
        // Get String at calling method
        //
        String s = error.toString();
        
        // In the stack trace we are three calls in:
        //  1. Who ever called debugString
        //  2. this.debugString
        //  3. This function
        // so drill down the stack trace to the third line + 1 for the
        // Exception message.
        int i = s.indexOf('\n');
        i = s.indexOf('\n',i + 1);
        i = s.indexOf('\n',i + 1);
        
        // Our target, now chop it off at the end of the line
        int x = s.indexOf('\n',i + 1);
        s = s.substring(i + 1, x -1);
        
        // Go backwards and trim off the package name so:
        // net.xtapi.MyClass.myMethod(MySource.java:123) is
        // MyClass.myMethod(MySource.java:123)
        
        i = s.lastIndexOf('.');
        i = s.lastIndexOf('.',i -1);
        i = s.lastIndexOf('.',i - 1);
        s = s.substring(i + 1);
        
        // Slightly off if it wasn't one of our packages that blew up.
        //"    	at dlgExtract.<init> @ (dlgExtract.java:47)"

        s = s.trim();
        int idx = s.indexOf("at");
        if(0 == idx)
        {
            // As it stands we munge any class name that begins with 'at'
            // so to fix that check for a space.
            if(s.charAt(2) == ' ')
                s = s.substring(3);
        }
        
        return s;
    }

	@Override
	public void addProviderListener(ProviderListener listener)
			throws ResourceUnavailableException, MethodNotSupportedException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ProviderListener[] getProviderListeners() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeProviderListener(ProviderListener listener) {
		// TODO Auto-generated method stub
		
	}
    
    
}
