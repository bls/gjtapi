package net.sourceforge.gjtapi.util;

import lombok.extern.apachecommons.CommonsLog;

/**
 * Handler for thread running exceptions which simply logs any exceptions.
 * Creation date: (2000-05-10 7:15:02)
 * 
 * @author Richard Deadman
 */
@CommonsLog
public class NullExceptionHandler extends ExceptionHandler {
	
	/**
	 * Log the exception.
	 * Creation date: (2000-05-10 11:04:10)
	 * 
	 * @author Richard Deadman
	 * @param eh The EventHandler being invoked when the exception occurred.
	 * @param ex The exception that occurred.
	 * @param other The parameter passed to the EventHandler
	 */
	@Override
	public void handleException(EventHandler eh, RuntimeException ex, Object other) {
		log.error("An exception occurred", ex);
	}
}
