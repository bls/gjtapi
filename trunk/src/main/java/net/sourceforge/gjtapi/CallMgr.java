/*
	Copyright (c) 2002 8x8 Inc. (www.8x8.com) 

	All rights reserved. 

	Permission is hereby granted, free of charge, to any person obtaining a 
	copy of this software and associated documentation files (the 
	"Software"), to deal in the Software without restriction, including 
	without limitation the rights to use, copy, modify, merge, publish, 
	distribute, and/or sell copies of the Software, and to permit persons 
	to whom the Software is furnished to do so, provided that the above 
	copyright notice(s) and this permission notice appear in all copies of 
	the Software and that both the above copyright notice(s) and this 
	permission notice appear in supporting documentation. 

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT 
	OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
	HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL 
	INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING 
	FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
	NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION 
	WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. 

	Except as contained in this notice, the name of a copyright holder 
	shall not be used in advertising or otherwise to promote the sale, use 
	or other dealings in this Software without prior written authorization 
	of the copyright holder.
*/

package net.sourceforge.gjtapi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import javax.telephony.Call;

import lombok.extern.apachecommons.CommonsLog;
import net.sourceforge.gjtapi.jcc.Provider;
import net.sourceforge.gjtapi.util.WeakMap;

/**
 * This is the Call Lookup manager that brokers call lookup requests for the Provider as well as
 * Addresses and Terminals.
 * <P>There are three type of smart accessors:
 * <OL>
 *  <LI><B>getCachedXXX</B> - this means that the item will be looked for in the call cache only.
 *  <LI><B>getFaultedXXX</B> - this means that the item will be looked for in the call cache and,
 * if it isn't found, requested (flushed in) from the raw TelephonyProvider.
 *  <LI><B>getLazyXXX</B> - this means that the item will be looked for in the call cache and, if
 * if it isn't found, a new empty one will be created.
 * </ol>
 * Creation date: (2000-06-15 14:03:03)
 * 
 * @author Richard Deadman
 */
@CommonsLog
class CallMgr {
	
	/**
	 * Whether this call map is dynamic, in that calls are only reported once the TelephonyProvider
	 * has requested them or asked for reporting of call that visit an Address or Terminal.
	 */
	private final boolean dynamic;
	/**
	 * Weak referents if throttling is supported
	 */
	private final Map<CallId, FreeCall> callSet;
	/**
	 * Hard holder for Calls to make sure observed Calls are not garbage collected
	 */
	private Set<FreeCall> observedCalls = null;
	/**
	 * Weak map of Calls to themselves. This is a staging area for new Calls that have not yet been
	 * registered with a CallId (i.e. {@link GenericProvider#createCall()})
	 */
	private final WeakHashMap<FreeCall, Object> idleCalls = new WeakHashMap<>();
	private final GenericProvider provider;
	/**
	 * Cached shortcut for {@code provider.getRaw()}
	 */
	private final TelephonyProvider raw;
	
	/**
	 * Constructor that determines which kind of map to keep and how to resolve Call requests.
	 * Creation date: (2000-06-15 14:33:03)
	 * 
	 * @author Richard Deadman
	 * @param provider The GenericProvider that is needed to create calls.
	 * @param isDynamic true if not all calls are reported from the TelephonyProvider.
	 */
	CallMgr(GenericProvider provider, boolean isDynamic) {
		this.provider = provider;
		this.raw = provider.getRaw(); // cache raw handle
		this.dynamic = isDynamic;
		if (isDynamic) {
			callSet = new WeakMap<>();
			setObserved();
		} else {
			callSet = new HashMap<>();
		}
	}
	
	/**
	 * Removes any lingering calls of state {@link Call#INVALID} because they are responsible for
	 * faulty call mappings.
	 */
	private void removeOrphanedCalls() {
		for (FreeCall call : toArray()) {
			if (call.getState() == Call.INVALID) {
				removeCall(call);
			}
		}
	}
	
	/**
	 * Find a call for the raw call handle.
	 * Do not create a new call object, since we may just be looking to kill it anyway.
	 * Creation date: (2000-02-14 10:18:50)
	 * 
	 * @author Richard Deadman
	 * @param id The raw wrapper call object
	 * @return The existing found Jtapi Call object or null.
	 */
	synchronized FreeCall getCachedCall(CallId id) {
		removeOrphanedCalls(); // XXX: Ideally, this should not be necessary but, alas, it is.
		FreeCall call = callSet.get(id);
		if (call != null) {
			log.debug(String.format("Found requested call (ID: %s) in the cache", id));
		}
		else {
			log.debug(String.format("Did not find requested call (ID: %s) in the cache", id));
		}
		return call;
	}
	
	/**
	 * Find a cached connection for the raw call handle and address.
	 * Creation date: (2000-02-14 10:18:50)
	 * 
	 * @author Richard Deadman
	 * @param id The raw wrapper call object
	 * @param address An address the call is connected to.
	 * @return The found Jtapi Call object, or null if none is cached.
	 */
	FreeConnection getCachedConnection(CallId id, String address) {
		FreeCall call = getCachedCall(id);
	
		if (call != null) {
			return call.getCachedConnection(address);
		}
		return null;
	}
	
	/**
	 * Find a cached terminal connection for the raw call handle, address name and terminal id.
	 * Creation date: (2000-02-14 10:18:50)
	 * 
	 * @author Richard Deadman
	 * @param id The raw wrapper call object
	 * @param address An address the call is connected to.
	 * @param terminal The name of the Terminal the connection is attached to.
	 * @return The found Jtapi TerminalConnection object, or null
	 */
	FreeTerminalConnection getCachedTermConn(CallId id, String address, String terminal) {
		FreeConnection connection = getCachedConnection(id, address);
		
		if (connection != null) {
			// Create of insert the terminal connection
			return connection.getCachedTermConn(terminal);
		}
		// indicate no cached TerminalConnection since the Connection doesn't exist
		return null;
	}
	
	/**
	 * Return a Call for a given CallId.
	 * If the Call is not in the map, then we assume that the call must be fetched from the
	 * raw provider and reported on.
	 * Creation date: (2000-06-15 14:51:37)
	 * 
	 * @author Richard Deadman
	 * @param id The logical low-level handle for the Call.
	 * @return A tracked Call object, or null if the CallId is invalid
	 */
	private FreeCall getFaultedCall(CallId id) {
		FreeCall call = getCachedCall(id);
		if (call == null && dynamic) {
			// ask raw provider for CallData
			CallData cd = raw.getCall(id);
			if (cd != null) {
				log.debug(String.format(
						"Created a new call (ID: %s) from the provider's data because it was not found in the cache",
						id));
				call = new FreeCall(cd, provider);
				register(call);
			}
		}
		return call;
	}
	
	/**
	 * Find or fault from the TelephonyProvider a connection for the raw call handle and address.
	 * Creation date: (2000-02-14 10:18:50)
	 * 
	 * @author Richard Deadman
	 * @param id The raw wrapper call object
	 * @param address An address the call is connected to.
	 * @return The new or found Jtapi Call object
	 */
	FreeConnection getFaultedConnection(CallId id, String address) {
		// fist ensure the call is faulted in
		getFaultedCall(id);
		
		// now return the cached connection
		return getCachedConnection(id, address);
	}
	
	/**
	 * Find or fault in a terminal connection for the raw call handle, address name and terminal id.
	 * We have evidence that this connection used to exist.
	 * Creation date: (2000-02-14 10:18:50)
	 * 
	 * @author Richard Deadman
	 * @param id The raw wrapper call object
	 * @param address An address the call is connected to.
	 * @param terminal The name of the Terminal the connection is attached to.
	 * @return The new or found Jtapi TerminalConnection object
	 */
	FreeTerminalConnection getFaultedTermConn(CallId id, String address, String terminal) {
		// first ensure the call is faulted in
		getFaultedCall(id);
		
		// now look for the cached TerminalConnection
		return getCachedTermConn(id, address, terminal);
	}
	
	/**
	 * Find or create a call for the raw call handle.
	 * This should be used to lazily create an empty call object for new call event.
	 * Creation date: (2000-02-14 10:18:50)
	 * 
	 * @author Richard Deadman
	 * @param id The raw wrapper call object
	 * @return The new or found Jtapi Call object
	 */
	FreeCall getLazyCall(CallId id) {
		FreeCall call = getCachedCall(id);
		if (call == null) {
			log.debug(String.format(
					"Created a new call (ID: %s) because it was not found in the cache", id));
			call = new FreeCall();
			call.setProvider(provider);
			call.setCallID(id);
			register(call);
		}
		return call;
	}
	
	/**
	 * Find or create a connection for the raw call handle and address.
	 * Creation date: (2000-02-14 10:18:50)
	 * 
	 * @author Richard Deadman
	 * @param id The raw wrapper call object
	 * @param address An address the call is connected to.
	 * @return The new or found Jtapi Call object
	 */
	FreeConnection getLazyConnection(CallId id, String address) {
		FreeConnection conn = getCachedConnection(id, address);
		if (conn == null) {
			FreeCall c = getLazyCall(id);
			log.debug(String.format(
					"Created a new connection for call %s because it was not found in the cache", id));
			conn = new FreeConnection(c, address);
		}
		return conn;
	}
	
	/**
	 * Find or create a terminal connection for the raw call handle, address name and terminal id.
	 * Creation date: (2000-02-14 10:18:50)
	 * 
	 * @author Richard Deadman
	 * @param id The raw wrapper call object
	 * @param address An address the call is connected to.
	 * @param terminal The name of the Terminal the connection is attached to.
	 * @return The new or found Jtapi TerminalConnection object
	 */
	FreeTerminalConnection getLazyTermConn(CallId id, String address, String terminal) {
		FreeTerminalConnection tc = getCachedTermConn(id, address, terminal);
		if (tc == null) { // create a new terminal connection
			FreeConnection conn = getLazyConnection(id, address);
			tc = conn.getLazyTermConn(terminal);
		}
		return tc;
	}
	
	/**
	 * Is this call map dynamic, in that calls are only reported once the TelephonyProvider has
	 * requested them or asked for reporting of call that visit an Address or Terminal.
	 * Creation date: (2000-06-15 14:25:04)
	 * 
	 * @author Richard Deadman
	 * @return true if calls are not automatically reported by the TelephonyProvider.
	 */
	boolean isDynamic() {
		return dynamic;
	}
	
	/**
	 * Ask the raw provider for all call data associated with an Address.  For any Call not yet tracked, we
	 * add it to the managed set.
	 * Creation date: (2000-06-22 14:48:07)
	 * 
	 * @author Richard Deadman
	 */
	void loadCalls(FreeAddress address) {
		log.debug("Load calls for address " + address);
		CallData[] data = raw.getCallsOnAddress(address.getName());
		registerCalls(data);
	}
	
	/**
	 * Ask the raw provider for all call data associated with a Terminal.  For any Call not yet tracked, we
	 * add it to the managed set.
	 * Creation date: (2000-06-22 14:48:07)
	 * 
	 * @author Richard Deadman
	 */
	void loadCalls(FreeTerminal terminal) {
		log.debug("Load calls for terminal " + terminal);
		CallData[] data = raw.getCallsOnTerminal(terminal.getName());
		registerCalls(data);
	}
	
	private void registerCalls(CallData[] data) {
		for (CallData element : data) {
			// check if the call is already being tracked
			if (getCachedCall(element.id) == null) {
				register(new FreeCall(element, provider));
			}
		}
	}
	
	/**
	 * Pre-register the given call with the Idle call set
	 * Creation date: (2000-06-19 12:48:59)
	 * 
	 * @author Richard Deadman
	 * @param call The Idle call to register until it is activated.
	 */
	synchronized void preRegister(FreeCall call) {
		idleCalls.put(call, null);
	}
	
	/**
	 * This is called by a Call that has been observed or Listened to so that it will be protected from
	 * any garbage collection if dynamic tracking is used.
	 * Creation date: (2000-06-23 10:59:02)
	 * 
	 * @author Richard Deadman
	 * @param call The call to protect from potential cache clearing.
	 */
	synchronized void protect(FreeCall call) {
		// check that we are using a WeakMap
		if (observedCalls != null) {
			log.debug("Mark call as observed: " + call);
			observedCalls.add(call);
		}
	}
	
	/**
	 * Register the given call with the Call set.
	 * Creation date: (2000-06-19 12:48:59)
	 * 
	 * @author Richard Deadman
	 * @param call The transient call we should track
	 */
	synchronized void register(FreeCall call) {
		// remove from idle set if it is there
		idleCalls.remove(call);
	
		log.debug("Register call listeners for call " + call);
		callSet.put(call.getCallID(), call);
		
		// tell the Jain Provider it should register any of its CallListeners
		Provider prov = provider.getJainProvider();
		if (prov != null) {
			prov.registerCallListeners(call);
		}
	}
	
	/**
	 * Remove a call from my list.
	 * This is used to clean up Invalid calls.
	 * Creation date: (2000-05-05 23:54:45)
	 * 
	 * @author Richard Deadman
	 * @param call An Invalid Call to remove
	 * @return {@code true} if the call has been removed, {@code false} otherwise
	 */
	synchronized boolean removeCall(FreeCall call) {
		if (call.getState() == Call.INVALID) {
			log.debug("Removing ended call " + call);
			callSet.remove(call.getCallID());
			return true;
		}
		log.debug("Ignore request to remove non-ended call " + call);
		return false;
	}
	
	/**
	 * Lazy creator that creates the holder for calls to protect them from garbage collection.
	 * The existence of this set turns on "holding".
	 * Creation date: (2000-06-23 11:44:43)
	 * 
	 * @author Richard Deadman
	 */
	private synchronized void setObserved() {
		if (observedCalls == null) {
			observedCalls = new HashSet<>();
		}
	}
	
	/**
	 * Return an array of the currently known Calls.
	 * Creation date: (2000-06-15 14:51:37)
	 * 
	 * @author Richard Deadman
	 * @return An array of currently known Call objects.
	 */
	FreeCall[] toArray() {
		List<FreeCall> calls = new ArrayList<>();
		calls.addAll(callSet.values()); // the active calls
		calls.addAll(idleCalls.keySet()); // the idle calls
		return calls.toArray(new FreeCall[0]);
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("A call manager with ");
		if (dynamic) {
			sb.append("dynamcally queried");
		}
		else {
			sb.append("event reported");
		}
		return sb.append(" calls: ").append(callSet.values().toString())
				.append(" and idle calls: ").append(idleCalls.keySet().toString())
				.toString();
	}
	
	/**
	 * This is called by a Call that is no longer observed or Listened to so that it will no longer
	 * be protected from any garbage collection if dynamic tracking is used.
	 * Creation date: (2000-06-23 10:59:02)
	 * 
	 * @author Richard Deadman
	 * @param call The call to free up for potential cache clearing.
	 */
	synchronized void unProtect(FreeCall call) {
		// check that we are using a WeakMap
		if (observedCalls != null) {
			log.debug("Mark call as not being observed anymore: " + call);
			observedCalls.remove(call);
		}
	}
}
