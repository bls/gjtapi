package net.sourceforge.gjtapi;

import net.sourceforge.gjtapi.raw.remote.SerializableCallId;

/**
 * Generator for serializable call IDs
 * 
 * @author haf
 */
public interface IdGenerator {
	
	/**
	 * Returns the next serializable call ID to use
	 * 
	 * @return a serializable call ID
	 */
	SerializableCallId getSerializableId();
	
	/**
	 * Marks an ID as being ready to be reused
	 * 
	 * @param id the CallID which can be reused
	 */
	void freeSerializableId(CallId id);
	
	/**
	 * Creates a new instance of a SerializableCallId
	 * 
	 * @param id the internal ID of the serializable call ID
	 * @return the new object instance
	 */
	default SerializableCallId newSerializableId(int id) {
		return new SerializableCallId(id);
	}
}
