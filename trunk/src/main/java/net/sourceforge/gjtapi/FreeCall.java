package net.sourceforge.gjtapi;

/*
	Copyright (c) 1999,2002 Westhawk Ltd (www.westhawk.co.uk) 
	Copyright (c) 2002 8x8 Inc. (www.8x8.com) 

	All rights reserved. 

	Permission is hereby granted, free of charge, to any person obtaining a 
	copy of this software and associated documentation files (the 
	"Software"), to deal in the Software without restriction, including 
	without limitation the rights to use, copy, modify, merge, publish, 
	distribute, and/or sell copies of the Software, and to permit persons 
	to whom the Software is furnished to do so, provided that the above 
	copyright notice(s) and this permission notice appear in all copies of 
	the Software and that both the above copyright notice(s) and this 
	permission notice appear in supporting documentation. 

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT 
	OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
	HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL 
	INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING 
	FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
	NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION 
	WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. 

	Except as contained in this notice, the name of a copyright holder 
	shall not be used in advertising or otherwise to promote the sale, use 
	or other dealings in this Software without prior written authorization 
	of the copyright holder.
*/
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.telephony.Address;
import javax.telephony.Call;
import javax.telephony.CallListener;
import javax.telephony.CallObserver;
import javax.telephony.Connection;
import javax.telephony.ConnectionListener;
import javax.telephony.Event;
import javax.telephony.InvalidArgumentException;
import javax.telephony.InvalidPartyException;
import javax.telephony.InvalidStateException;
import javax.telephony.MethodNotSupportedException;
import javax.telephony.PlatformException;
import javax.telephony.PrivilegeViolationException;
import javax.telephony.Provider;
import javax.telephony.ResourceUnavailableException;
import javax.telephony.Terminal;
import javax.telephony.TerminalConnection;
import javax.telephony.TerminalConnectionListener;
import javax.telephony.callcontrol.CallControlCall;
import javax.telephony.callcontrol.CallControlTerminalConnection;
import javax.telephony.capabilities.CallCapabilities;
import javax.telephony.events.CallEv;
import javax.telephony.events.CallObservationEndedEv;
import javax.telephony.events.Ev;
import javax.telephony.privatedata.PrivateData;

import lombok.extern.apachecommons.CommonsLog;
import net.sourceforge.gjtapi.capabilities.GenCallCapabilities;
import net.sourceforge.gjtapi.events.FreeCallActiveEv;
import net.sourceforge.gjtapi.events.FreeCallEvent;
import net.sourceforge.gjtapi.events.FreeCallInvalidEv;
import net.sourceforge.gjtapi.events.FreeCallObservationEndedEv;
import net.sourceforge.gjtapi.events.FreeConnAlertingEv;
import net.sourceforge.gjtapi.events.FreeConnConnectedEv;
import net.sourceforge.gjtapi.events.FreeConnCreatedEv;
import net.sourceforge.gjtapi.events.FreeConnDisconnectedEv;
import net.sourceforge.gjtapi.events.FreeConnFailedEv;
import net.sourceforge.gjtapi.events.FreeConnInProgressEv;
import net.sourceforge.gjtapi.events.FreeConnUnknownEv;
import net.sourceforge.gjtapi.events.FreeTermConnActiveEv;
import net.sourceforge.gjtapi.events.FreeTermConnCreatedEv;
import net.sourceforge.gjtapi.events.FreeTermConnDroppedEv;
import net.sourceforge.gjtapi.events.FreeTermConnHeldEv;
import net.sourceforge.gjtapi.events.FreeTermConnRingingEv;
import net.sourceforge.gjtapi.events.FreeTermConnTalkingEv;
import net.sourceforge.gjtapi.events.FreeTermConnUnknownEv;

@CommonsLog
public class FreeCall implements CallControlCall, PrivateData {
	
	private int state = Call.IDLE;
	private GenericProvider provider = null;
	private final HashMap<String, FreeConnection> connections = new HashMap<>();	// Address name -> connections
	private final ListenerManager listMgr;
	private CallId callID;
	private boolean stoppedReporting = false;		// ensure we do it only once.

	private FreeTerminalConnection confController = null;
	private boolean confEnabled = true;
	private FreeTerminalConnection transController = null;
	private boolean transEnabled = true;
	private Address calledAddress = null;
	private Address callingAddress = null;
	private Terminal callingTerminal = null;
	
	/**
	 * Protected constructor for a Call.
	 * Initially the call starts in the Idle state.
	 * 
	 * @author Richard Deadman
	 **/
	protected FreeCall() {
		listMgr = new ListenerManager(this);
	}
	
	/**
	 * Internal CallCreator that uses a CallData object to flesh out the call.
	 * Initially the call starts in the Idle state.
	 * 
	 * @author Richard Deadman
	 **/
	FreeCall(CallData callData, GenericProvider prov) {
		this();
		
		setProvider(prov);
		
		this.state = callData.callState;
		setCallID(callData.id);
		for (ConnectionData data : callData.connections) {
			new FreeConnection(this, data, provider.getDomainMgr());
		}
	}
	
	/**
	 * Add the Listener to the set of CallListeners.
	 * This should only work if the CallListener isn't already registered for
	 * Call, Connection or TerminalConnection events.
	 **/
	@Override
	public void addCallListener(CallListener l) {
		listMgr.add(l);
	}
	
	/**
	 * Add the Listener to the set of CallListeners for an Address.
	 * Later when the Call leaves the Address, any CallListeners dependent only on this Address may
	 * be removed.
	 **/
	void addCallListener(CallListener l, Address addr) {
		listMgr.add(l, addr);
	}
	
	/**
	 * Add the Listener to the set of CallListeners for a Terminal.
	 * Later when the Call leaves the Terminal, any CallListeners dependent only on this Terminal
	 * may be removed.
	 **/
	void addCallListener(CallListener l, Terminal term) {
		listMgr.add(l, term);
	}
	
	/**
	 * Add the Observer to the set of CallObserver for an Address.
	 * Later when the Call leaves the Address, any CallObservers dependent only on this Address may be removed.
	 **/
	void addCallObserver(CallObserver observer, Address addr) {
		listMgr.add(observer, addr);
	}
	
	/**
	 * Add the Observer to the set of CallObserver for a Terminal.
	 * Later when the Call leaves the Terminal, any CallObservers dependent only on this Terminal may be removed.
	 **/
	void addCallObserver(CallObserver observer, Terminal term) {
		listMgr.add(observer, term);
	}
	
	/**
	 * Store the Connection by its Address name
	 **/
	void addConnection(FreeConnection c) {
		int oldSize = connections.size();
		connections.put(c.getAddressName(), c);
		
		// now update the call's state and cause the proper events to be published
		if (oldSize == 0) {
			toActive(Event.CAUSE_NEW_CALL);
		}
	}
	
	@Override
	public void addObserver(CallObserver observer) {
		listMgr.add(observer);
	}
	
	/**
	 * Adds an additional party to an existing Call. This is sometimes called a
	 * "single-step conference" because a party is conferenced into a Call
	 * directly. The telephone address string provided as the argument must be
	 * complete and valid.
	 *
	 * <H5>States of the Existing Connections</H5>
	 *
	 * The Call must  have at least two Connections in the
	 * <CODE>CallControlConnection.ESTABLISHED</CODE> state. An additional
	 * restriction requires that at most one other Connection may be in either
	 * the <CODE>CallControlConnection.QUEUED</CODE>,
	 * <CODE>CallControlConnection.OFFERED</CODE>, or
	 * <CODE>CallControlConnection.ALERTING</CODE> state.
	 * <p>
	 * Some telephony platforms impose restrictions on the number of Connections
	 * in a particular state. For instance, it is common to restrict the number
	 * of "alerting" Connections to at most one. As a result, this method
	 * requires that at most one other Connections is in the "queued",
	 * "offering", or "alerting" state. (Note that the first two states
	 * correspond to the core Connection "in progress" state). Although some
	 * systems may not enforce this requirement, for consistency, JTAPI specifies
	 * implementations must uphold the conservative requirement.
	 *
	 * <H5>The New Connection</H5>
	 *
	 * This method creates and returns a new Connection representing the new
	 * party. This Connection must at least be in the
	 * <CODE>CallControlConnection.IDLE</CODE> state. Its state may have
	 * progressed beyond "idle" before this method returns, and should be
	 * reflected by an event. This new Connection will progress as any normal
	 * destination Connection on a Call. Typical scenarios for this Connection
	 * are described by the <CODE>Call.connect()</CODE> method.
	 * <p>
	 * <B>Pre-conditions:</B>
	 * <OL>
	 * <LI>(this.getProvider()).getState() == Provider.IN_SERVICE
	 * <LI>this.getState() == Call.ACTIVE
	 * <LI>Let c[] = call.getConnections() where c.length >= 2
	 * <LI>c[i].getCallControlState() == CallControlConnection.ESTABLISHED for
	 * at least two i
	 * <LI>c[j].getCallControlState() == CallControlConnection.QUEUED,
	 * CallControlConnection.OFFERED, or CallControlConnection.ALERTING for at
	 * most one c[j]
	 * </OL>
	 * <B>Post-conditions:</B>
	 * <OL>
	 * <LI>Let connection be the Connection created and returned
	 * <LI>(this.getProvider()).getState() == Provider.IN_SERVICE
	 * <LI>this.getState() == Call.ACTIVE
	 * <LI>connection.getCallControlState() at least CallControlConnection.IDLE
	 * <LI>ConnCreatedEv is delivered for connection
	 * </OL>
	 * 
	 * @param newParty The telephone address of the party to be added.
	 * @return The new Connection associated with the added party.
	 * @exception InvalidStateException Either the Provider is not "in service",
	 * the Call is not "active" or the proper conditions on the Connections does
	 * not exist.
	 * as designated by the pre-conditions for this method.
	 * @exception InvalidPartyException The destination address string is not
	 * valid and/or complete.
	 * @exception MethodNotSupportedException This method is not supported by
	 * the implementation.
	 * @exception PrivilegeViolationException The application does not have
	 * the proper authority to invoke this method.
	 * @exception ResourceUnavailableException An internal resource necessary
	 * for the successful invocation of this method is not available.
	 * @see javax.telephony.events.ConnCreatedEv
	 */
	@Override
	public Connection addParty(String newParty) throws InvalidStateException, MethodNotSupportedException, PrivilegeViolationException, ResourceUnavailableException, InvalidPartyException {
		// test for transfer TerminalController
		TerminalConnection conf = getConferenceController();
		boolean controllerSet = true;
		if (conf == null) {	// look for the first available TC
			controllerSet = false;
			Connection[] cs = getConnections();
			for (int i = 0; i < cs.length; i++) {
				TerminalConnection[] tcs = cs[i].getTerminalConnections();
				if (tcs != null && tcs.length > 0) {
					conf = tcs[0];
					break;
				}
			}
		}
		if (conf == null)
			throw new ResourceUnavailableException(ResourceUnavailableException.ORIGINATOR_UNAVAILABLE,
						"Conference TerminalConnection not set and cannot be found");
	
		try {	
			// Create a consultation call
			CallControlCall consult = (CallControlCall) provider.createCall();
			consult.consult(conf, newParty);
	
			// conference the call
			setConferenceController(conf);
			privateConference(consult);
	
			// reset conference controller to null if not previously set
			if (!controllerSet)
				clearConferenceController();
		} catch (InvalidArgumentException iae) {
			throw new ResourceUnavailableException(ResourceUnavailableException.UNKNOWN,
					"Error conferencing the call");
		}
	
		// Find the remote connection
		Connection conn[] = getConnections();
		Connection rem = null;
		for (int i = 0; i < conn.length; i++) {
			if (conn[i].getAddress().getName().equals(newParty)) {
				rem = conn[i];
				break;
			}
		}
		return rem;
	}
	
	/**
	 * Merges two Calls together, resulting in the union of the
	 * participants of both Calls being placed on a single Call.
	 * This method takes a Call as an argument, referred to
	 * hereafter as the "second" Call. All of the participants
	 * from the second call are moved to the Call on which this
	 * method is invoked.
	 * 
	 * See CallControlCall for pre and post conditions.
	 */
	@Override
	public void conference(Call otherCall) throws InvalidStateException, MethodNotSupportedException, PrivilegeViolationException, ResourceUnavailableException, InvalidArgumentException {
		// first check if conference is enabled
		if ((!getConferenceEnable()) || !(((CallControlCall)otherCall).getConferenceEnable())) {
			throw new InvalidStateException(this, InvalidStateException.CALL_OBJECT, getState(), "ConferenceEnabled is set to false");
		}
		
		privateConference(otherCall);
	}

	/**
	 * Private version of conference, omitting the conferenceEnabled check.
	 * 
	 * @param otherCall
	 * @throws InvalidStateException
	 * @throws MethodNotSupportedException
	 * @throws PrivilegeViolationException
	 * @throws ResourceUnavailableException
	 * @throws InvalidArgumentException
	 */
	private void privateConference(Call otherCall) throws InvalidStateException, MethodNotSupportedException, PrivilegeViolationException, ResourceUnavailableException, InvalidArgumentException {
		TerminalConnection tc = getConferenceController();
		tc = getOrVerifyController(tc, otherCall);
		provider.getRaw().join(getCallID(), ((FreeCall)otherCall).getCallID(),
				tc.getConnection().getAddress().getName(), tc.getTerminal().getName());
	}
	
	private TerminalConnection getOrVerifyController(TerminalConnection tc, Call otherCall) throws InvalidArgumentException {
		if (tc == null) {
			tc = findCommonTC(otherCall);
			if (tc == null) {
				throw new InvalidArgumentException("No Conference controller set of available");
			}
		}
		else {
			// check that the conference controller is on my call
			boolean tcValid = false;
			for (Connection c : getConnections()) {
				for (TerminalConnection callTc : c.getTerminalConnections()) {
					if (callTc.equals(tc)) {
						tcValid = true;
						break;
					}
				}
				if (tcValid) {
					break;
				}
			}
			if (!tcValid) {
				throw new InvalidArgumentException("Conference controller not part of call");
			}
			
			// now see if it shares a terminal with the other call
			Terminal term = tc.getTerminal();
			tcValid = false;
			for (Connection c : otherCall.getConnections()) {
				for (TerminalConnection callTc : c.getTerminalConnections()) {
					if (term.equals(callTc.getTerminal())) {
						tcValid = true;
						break;
					}
				}
				if (tcValid) {
					break;
				}
			}
			if (!tcValid) {
				throw new InvalidArgumentException(
						"Conference controller terminal not part of other call");
			}
		}
		return tc;
	}
	
	/**
	 * connect an idle call from a terminal/address pair to another address
	 *
	 * @author Richard Deadman
	 * @param origterm The outgoing terminal
	 * @param origaddr The outgoing address on the terminal
	 * @param dialedDigits A string representation of the remote address.
	 * @return The new connections of the call.
	 **/
	@Override
	public Connection[] connect(Terminal origterm, Address origaddr, String dialedDigits) throws ResourceUnavailableException, PrivilegeViolationException, InvalidPartyException, InvalidArgumentException, InvalidStateException, MethodNotSupportedException {
		GenericProvider gp = provider;
		
		// check if the terminal has the address
		Address[] ta = origterm.getAddresses();
		int size = ta.length;
		boolean found = false;
		for (int i = 0; i < size; i++) {
			if (ta[i].equals(origaddr)) {
				found = true;
				break;
			}
		}
		if (!found) {
			throw new InvalidArgumentException();
		}
		
		// check the states
		if (getState() != Call.IDLE) {
			throw new InvalidStateException(this, InvalidStateException.CALL_OBJECT, getState());
		}
		if (gp.getState() != Provider.IN_SERVICE) {
			throw new InvalidStateException(gp, InvalidStateException.PROVIDER_OBJECT, gp.getState());
		}
		
		// create a call id
		setCallID(gp.getRaw().reserveCallId(origterm.getName()));
		
		// register the call with its new id.
		provider.getCallMgr().register(this);
		
		// create two connections - they add themselves back to me
		Connection[] connSet = new Connection[2];
		FreeConnection origConn = new FreeConnection(this, (FreeAddress) origaddr);
		FreeConnection destConn = new FreeConnection(this, dialedDigits);
		connSet[0] = origConn;
		connSet[1] = destConn;
		
		// tell the service provider to hook up the connections
		try {
			provider.getRaw().createCall(getCallID(), origaddr.getName(), origterm.getName(),
					dialedDigits);
		}
		catch (RawStateException rse) {
			throw rse.morph(provider);
		}
		
		// check if the call has been disconnected during set up,
		// such as by an asynchronous hang-up
		if (getCallID() == null) {
			// leave the call dead
			return null;
		}
		
		// change the call state - even though an event will do this
		toActive(Event.CAUSE_NEW_CALL);
		
		// ensure the origination Connection is in the right initial state
		origConn.toConnected(Event.CAUSE_NEW_CALL);
		// see if the provider has already created the connection
		// and set it to talking
		origConn.getLazyTermConn((FreeTerminal)origterm).toTalking(Event.CAUSE_NEW_CALL);
		
		// Now ensure the destination Connection is in the INPROGRESS state (no TerminalConnections yet)
		destConn.toInProgress(Event.CAUSE_NEW_CALL);
		
		// set the called and calling address
		log.debug(String.format("Initialized calling address as %s", origaddr));
		callingAddress = origaddr;
		callingTerminal = origterm;
		calledAddress = destConn.getAddress();
		
		return connSet;
	}
	
	/**
	 * This creates a connection on a consultation call in an initiated state.  To use this, the
	 * Connection must implement CallControlConnection.addToAddress(String newDigits).  We don't currently
	 * implement this.
	 */
	@Override
	public Connection consult(TerminalConnection tc) throws MethodNotSupportedException {
		throw new MethodNotSupportedException();
	}
	
	/**
	 * Create a consultation call on the same terminal as an existing call.  Once this is done, the existing
	 * call moves into the held state and the consulation call becomes active.  The terminal connection is set as the transfer
	 * and conference terminal connection for the old call, and the corresponding terminal connection for the
	 * consultation call becomes its transfer and conference terminal connection.
	 */
	@Override
	public Connection[] consult(TerminalConnection tc, String dialedDigits) throws MethodNotSupportedException, ResourceUnavailableException, InvalidArgumentException, InvalidPartyException, InvalidStateException, PrivilegeViolationException {
		// find the other call and hold it
		CallControlCall mainCall = (CallControlCall)tc.getConnection().getCall();
		((CallControlTerminalConnection)tc).hold();
	
		// dial this call
		Terminal origTerm = tc.getTerminal();
		Connection newConn[] = connect(origTerm, tc.getConnection().getAddress(), dialedDigits);
	
		// find which TC is to the same terminal
		TerminalConnection newTC = null;
		for (int i = 0; i < newConn.length; i++) {
			TerminalConnection termConn[] = newConn[i].getTerminalConnections();
			if (termConn != null) {
				for (int j = 0; j < termConn.length; j++) {
					if (termConn[j].getTerminal().equals(origTerm)) {
						newTC = termConn[j];
						break;
					}
				}
			}
		}
	
		// set the controllers -- let it throw a NullPointerException since newTC == null is a logical error
		setTransferController(newTC);
		setConferenceController(newTC);
	
		mainCall.setTransferController(tc);
		mainCall.setConferenceController(tc);
	
		return newConn;
	}
	
	/**
	 * drop a call by dropping all its connections.
	 */
	@Override
	public void drop() throws InvalidStateException, MethodNotSupportedException, PrivilegeViolationException, ResourceUnavailableException {
		if (getState() != ACTIVE) {
			throw new InvalidStateException(this,
						InvalidStateException.CALL_OBJECT,
						getState(),
						"Drop from non-active call");
		}
		
		Connection[] cons = getConnections();
	
		for (int i = 0; i < cons.length; i++) {
			try {
				cons[i].disconnect();
			} catch (InvalidStateException ise) {
				// eat it -- the call is now dropped
				break;
			}
		}
	}
	
	/**
	 * Clean up, in case we have not unregistered yet.
	 * Creation date: (2000-06-22 13:38:56)
	 * 
	 * @author Richard Deadman
	 */
	@Override
	public void finalize() {
		// tell the raw TelephonyProvider to stop reporting my calls
		stopReporting();
		// Remove me from the Call cache
		provider.getCallMgr().removeCall(this);
	}
	
	/**
	 * Find a common TerminalConnection between two calls, if one exists.  Otherwise return null.
	 */
	private FreeTerminalConnection findCommonTC(Call otherCall) {
		Connection[] cs = getConnections();
		for (int i = 0; i < cs.length; i++) {
			TerminalConnection[] tcs = cs[i].getTerminalConnections();
			if (tcs != null) {
				for (int j = 0; j < tcs.length; j++) {
					Terminal t = null;
					try {
						t = tcs[j].getTerminal();
					}
					catch (PlatformException e){ }
					// now check the other call for the same Terminal
					Connection[] cs2 = otherCall.getConnections();
					for (int k = 0; k < cs2.length; k++) {
						TerminalConnection[] tcs2 = cs2[k].getTerminalConnections();
						if(tcs2 != null)
						for (int l = 0; l < tcs2.length; l++) {
							try {
								if (t.equals(tcs2[l].getTerminal())) {
									return (FreeTerminalConnection) tcs[j];
								}
							}
							catch (PlatformException e){ }
						}
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * Get a Connection that connects this call to a given Address.
	 * Only return a cached value -- do not go to the TelephonyProvider to fault it in.
	 * 
	 * @param addrName The name of the Connection's Address
	 * @return The currently cached Connection, or null.
	 */
	public FreeConnection getCachedConnection(String addrName) {
		return connections.get(addrName);
	}
	
	@Override
	public CallCapabilities getCallCapabilities(Terminal term, Address addr) throws PlatformException {
		return getCapabilities(term, addr);
	}
	
	@Override
	public Address getCalledAddress() {
		return calledAddress;
	}
	
	/**
	 * Set the CalledAddress for the Call. If knownAddress is
	 * false, then we are guessing that this is the CalledAddress. This differentiates
	 * assumptions from call events from directions from the provider.
	 * 
	 * @param theCalledAddress
	 * @param knowAddress
	 */
	protected void setCalledAddress(Address theCalledAddress, boolean knownAddress) {
		if (calledAddress == null || knownAddress) {
			calledAddress = theCalledAddress;
		}
	}
	
	/**
	 * Returns the GJTAPI call handle provided by the Provider
	 * that uniquely identifies the call. This is used in calls
	 * into the provider.
	 * 
	 * @return
	 */
	public CallId getCallID() {
		return callID;
	}
	
	@Override
	public Address getCallingAddress() {
		return callingAddress;
	}

	/**
	 * Set the CallingAddress for the Call. This differentiates assumptions from call events from
	 * directions from the provider.
	 * 
	 * @param theCallingAddress the calling address
	 * @param replaceExisting specifies if an already set address should be overwritten
	 */
	protected void setCallingAddress(Address theCallingAddress, boolean replaceExisting) {
		if (callingAddress == null || replaceExisting) {
			log.debug(String.format("Updated the calling address from %s to %s", callingAddress,
					theCallingAddress));
			callingAddress = theCallingAddress;
		}
		else {
			log.debug(String.format("Calling address was not updated from %s to %s", callingAddress,
					theCallingAddress));
		}
	}
	
	@Override
	public Terminal getCallingTerminal() {
		return callingTerminal;
	}
	
/**
 * Set the CallingTerminal for the Call. If knownTerminal is
 * false, then we are guessing that this is the CallingTerminal. This differentiates
 * assumptions from call events from directions from the provider.
 * @param theCallingTerminal
 * @param knowAddress
 */
protected void setCallingTerminal(Terminal theCallingTerminal, boolean knownTerminal)
{
	if ((this.callingTerminal == null) || knownTerminal)
	{
		this.callingTerminal = theCallingTerminal;
	}
}
/**
 * Return a copy of the set of currently registered CallListeners.
 * @return An array of the registered CallListeners for this call.
 **/
@Override
public CallListener[] getCallListeners() {
	Set<CallListener> cls = listMgr.getCallListeners();
	if((cls == null) || (cls.size() == 0)) {
		return null;
	}
	return cls.toArray(new CallListener[cls.size()]);
}
	/**
	 * Returns the dynamic capabilities for the instance of the Call object. Dynamic capabilities
	 * tell the application which actions are possible at the time this method is invoked based upon
	 * the implementations knowledge of its ability to successfully perform the action. This
	 * determination may be based upon argument passed to this method, the current state of the call
	 * model, or some implementation-specific knowledge. These indications do not guarantee that a
	 * particular method can be successfully invoked, however.
	 * <P>
	 * The dynamic call capabilities are based upon a Terminal/Address pair as well as the instance
	 * of the Call object. These parameters are used to determine whether certain call actions are
	 * possible at the present. For example, the CallCapabilities.canConnect() method will indicate
	 * whether a telephone call can be placed using the Terminal/Address pair as the originating
	 * endpoint.
	 */
	@Override
	public CallCapabilities getCapabilities(Terminal terminal, Address address) {
		// must merge in terminal and address capabilities
		return ((GenCallCapabilities) provider.getCallCapabilities()).getDynamic(this, terminal, address);
	}
	
	@Override
	public TerminalConnection getConferenceController() {
		return confController;
	}
	
	@Override
	public boolean getConferenceEnable() {
		return confEnabled;
	}
	
	/**
	 * Get the set of Connections associated with the call.
	 */
	@Override
	public Connection[] getConnections() {
		Connection[] ret = null;
		synchronized (connections) {
			if (!connections.isEmpty()) {
				ret = new Connection[connections.size()];
				connections.values().toArray(ret);
			}
		}
		return ret;
	}
	
	@Override
	public Address getLastRedirectedAddress() {
		return null;
	}
	
	/**
	 * Return the proxy to the sets of Listeners for a call.
	 * 
	 * @return A TerminalConnection listener proxy.
	 **/
	public TerminalConnectionListener getListener() {
		return listMgr;
	}
	
	@Override
	public CallObserver[] getObservers() {
		Set<CallObserver> observers = listMgr.getCallObservers();
		if((observers == null) || (observers.size() == 0)) {
			return null;
		}
		return observers.toArray(new CallObserver[0]);
	}
	
/**
 * Get any PrivateData associated with my low-level object.
 */
@Override
public Object getPrivateData() {
	return provider.getRaw().getPrivateData(getCallID(), null, null);
}
@Override
public Provider getProvider() {
	return provider;
}

@Override
public int getState() {
	return state;
}

@Override
public TerminalConnection getTransferController() {
	return transController;
}
/**
 * We may want to hook this into the capabilities?
 */
@Override
public boolean getTransferEnable() {
	return this.transEnabled;
}
/**
 * We don't currently support the offHook() command.
 */
@Override
public Connection offHook(Address origaddress, Terminal origterminal) throws MethodNotSupportedException {
	throw new MethodNotSupportedException();
}
/**
 * Remove an Address's CallListeners and CallObservers if the Address was the only registrar for the listeners.
 **/
 public void removeCallListAndObs(Address addr) {
	listMgr.remove(addr);
  }              
/**
 * Remove a Terminal's CallListeners and CallObservers if the Terminal was the only registrar for the listeners.
 **/
 public void removeCallListAndObs(Terminal term) {
	listMgr.remove(term);
  }                
  @Override
public void removeCallListener(CallListener l) {
	listMgr.remove(l);
  }          
  void removeConnection(Connection c) {
		Object conn = connections.remove(c.getAddress().getName());
		
		// now change state if necessary
		if ((conn != null) && (connections.size() == 0))
			this.toInvalid(Event.CAUSE_NORMAL);
  }          
  @Override
public void removeObserver(CallObserver observer) {
	listMgr.remove(observer);
  }          
  /* send an ev to the now removed observer */
  void sendObservationEndedEv(CallObserver observer){
	CallObservationEndedEv[] ev = new CallObservationEndedEv[1];
	ev[0] = new FreeCallObservationEndedEv(this);
	observer.callChangedEvent(ev);
  }
	
	/**
	 * Send PrivateData to my low-level object for processing.
	 */
	@Override
	public Object sendPrivateData(Object data) {
		return provider.getRaw().sendPrivateData(callID, null, null, data);
	}
	
	/**
	 * Send a snapshot describing the current state of the call.
	 */
	protected void sendSnapShot(CallListener listener) {
		sendSnapshotCallEvent(listener);
		if (listener instanceof ConnectionListener) {
			ConnectionListener connectionListener = (ConnectionListener) listener;
			sendConnectionSnapshot(connectionListener);
		}
	}
	
	private void sendConnectionSnapshot(ConnectionListener listener) {
		Connection[] connections = getConnections();
		if (connections != null) {
			for (int i = connections.length - 1; i > -1; i--) {
				FreeConnection connection = (FreeConnection) connections[i];
				sendSnapshotConnectionEvent(listener, connection);
				if (listener instanceof TerminalConnectionListener) {
					TerminalConnectionListener terminalConnectionListener =
							(TerminalConnectionListener) listener;
					sendTerminalConnectionSnapshot(terminalConnectionListener, connection);
				}
			}
		}
	}
	
	private static void sendTerminalConnectionSnapshot(TerminalConnectionListener listener, Connection connection) {
		TerminalConnection[] terminalConnections = connection.getTerminalConnections();
		if (terminalConnections != null) {
			for (int i = terminalConnections.length - 1; i > -1; i--) {
				FreeTerminalConnection terminalConnection =
						(FreeTerminalConnection) terminalConnections[i];
				sendSnapshotTerminalConnectionEvent(listener, terminalConnection);
			}
		}
	}
	
	/**
	 * Send a snapshot describing the current state of the call.
	 */
	protected void sendSnapShot(CallObserver observer){
		Set<FreeCallEvent> events = new HashSet<>();
		addSnapshotCallEvent(events);
		Connection[] connections = getConnections();
		if (connections != null) {
			for (int i = connections.length - 1; i > -1; i--) {
				FreeConnection connection = (FreeConnection) connections[i];
				addSnapshotConnectionEvent(events, connection);
				TerminalConnection[] terminalConnections = connection.getTerminalConnections();
				if (terminalConnections != null) {
					for (int j = terminalConnections.length - 1; j > -1; j--) {
						FreeTerminalConnection terminalConnection =
								(FreeTerminalConnection) terminalConnections[j];
						addSnapshotTerminalConnectionEvents(events, terminalConnection);
					}
				}
			}
		}
		observer.callChangedEvent(events.toArray(new CallEv[0]));
	}
	
	private void sendSnapshotCallEvent(CallListener listener) {
		int callState = state;
		switch (callState) {
			case Call.ACTIVE:
				listener.callActive(new FreeCallActiveEv(Event.CAUSE_SNAPSHOT, Ev.META_SNAPSHOT, true, this));
				break;
			case Call.INVALID:
				listener.callInvalid(new FreeCallInvalidEv(Event.CAUSE_SNAPSHOT, Ev.META_SNAPSHOT, true, this));
				break;
			default:
				log.warn(String.format("Unexpected call state %d while sending a snapshot of call %s",
						callState, this));
				break;
		}
	}
	
	private void addSnapshotCallEvent(Set<FreeCallEvent> events) {
		int callState = state;
		switch (callState) {
			case Call.ACTIVE:
				events.add(new FreeCallActiveEv(Event.CAUSE_SNAPSHOT, Ev.META_SNAPSHOT, true, this));
				break;
			case Call.INVALID:
				events.add(new FreeCallInvalidEv(Event.CAUSE_SNAPSHOT, Ev.META_SNAPSHOT, true, this));
				break;
			default:
				log.warn(String.format("Unexpected call state %d while sending a snapshot of call %s",
						callState, this));
				break;
		}
	}
	
	private static void sendSnapshotConnectionEvent(ConnectionListener listener, FreeConnection connection) {
		int connectionState = connection.getState();
		switch (connectionState) {
			case Connection.IDLE:
				listener.connectionCreated(new FreeConnCreatedEv(Event.CAUSE_SNAPSHOT,
						Ev.META_SNAPSHOT, true, connection));
				break;
			case Connection.INPROGRESS:
				listener.connectionInProgress(new FreeConnInProgressEv(Event.CAUSE_SNAPSHOT,
						Ev.META_SNAPSHOT, true, connection));
				break;
			case Connection.ALERTING:
				listener.connectionAlerting(new FreeConnAlertingEv(Event.CAUSE_SNAPSHOT,
						Ev.META_SNAPSHOT, true, connection));
				break;
			case Connection.CONNECTED:
				listener.connectionConnected(new FreeConnConnectedEv(Event.CAUSE_SNAPSHOT,
						Ev.META_SNAPSHOT, true, connection));
				break;
			case Connection.DISCONNECTED:
				listener.connectionDisconnected(new FreeConnDisconnectedEv(Event.CAUSE_SNAPSHOT,
						Ev.META_SNAPSHOT, true, connection));
				break;
			case Connection.UNKNOWN:
				listener.connectionUnknown(new FreeConnUnknownEv(Event.CAUSE_SNAPSHOT,
						Ev.META_SNAPSHOT, true, connection));
				break;
			case Connection.FAILED:
				listener.connectionFailed(new FreeConnFailedEv(Event.CAUSE_SNAPSHOT,
						Ev.META_SNAPSHOT, true, connection));
				break;
			default:
				log.warn(String.format("Unexpected connection state %d while sending a snapshot of connection %s",
						connectionState, connection));
				break;
		}
	}
	
	private static void addSnapshotConnectionEvent(Set<FreeCallEvent> events, FreeConnection connection) {
		int connectionState = connection.getState();
		switch (connectionState) {
			case Connection.IDLE:
				events.add(new FreeConnCreatedEv(Event.CAUSE_SNAPSHOT, Ev.META_SNAPSHOT, false, connection));
				break;
			case Connection.INPROGRESS:
				events.add(new FreeConnInProgressEv(Event.CAUSE_SNAPSHOT, Ev.META_SNAPSHOT, false, connection));
				break;
			case Connection.ALERTING:
				events.add(new FreeConnAlertingEv(Event.CAUSE_SNAPSHOT, Ev.META_SNAPSHOT, false, connection));
				break;
			case Connection.CONNECTED:
				events.add(new FreeConnConnectedEv(Event.CAUSE_SNAPSHOT, Ev.META_SNAPSHOT, false, connection));
				break;
			case Connection.DISCONNECTED:
				events.add(new FreeConnDisconnectedEv(Event.CAUSE_SNAPSHOT, Ev.META_SNAPSHOT, false, connection));
				break;
			case Connection.UNKNOWN:
				events.add(new FreeConnUnknownEv(Event.CAUSE_SNAPSHOT, Ev.META_SNAPSHOT, false, connection));
				break;
			case Connection.FAILED:
				events.add(new FreeConnFailedEv(Event.CAUSE_SNAPSHOT, Ev.META_SNAPSHOT, false, connection));
				break;
			default:
				log.warn(String.format("Unexpected connection state %d while sending a snapshot of connection %s",
						connectionState, connection));
				break;
		}
	}
	
	private static void sendSnapshotTerminalConnectionEvent(TerminalConnectionListener listener, FreeTerminalConnection terminalConnection) {
		int terminalConnectionState = terminalConnection.getState();
		switch (terminalConnectionState) {
			case TerminalConnection.IDLE:
				listener.terminalConnectionCreated(new FreeTermConnCreatedEv(Event.CAUSE_SNAPSHOT,
						Ev.META_SNAPSHOT, true, terminalConnection));
				break;
			case TerminalConnection.RINGING:
				listener.terminalConnectionRinging(new FreeTermConnRingingEv(Event.CAUSE_SNAPSHOT,
						Ev.META_SNAPSHOT, true, terminalConnection));
				break;
			case TerminalConnection.ACTIVE:
				boolean talking = (terminalConnection.getCallControlState() == CallControlTerminalConnection.TALKING);
				listener.terminalConnectionActive(new FreeTermConnActiveEv(Event.CAUSE_SNAPSHOT,
						Ev.META_SNAPSHOT, true, terminalConnection, talking));
				break;
			case TerminalConnection.DROPPED:
				listener.terminalConnectionDropped(new FreeTermConnDroppedEv(Event.CAUSE_SNAPSHOT,
						Ev.META_SNAPSHOT, true, terminalConnection));
				break;
			case TerminalConnection.UNKNOWN:
				listener.terminalConnectionUnknown(new FreeTermConnUnknownEv(Event.CAUSE_SNAPSHOT,
						Ev.META_SNAPSHOT, true, terminalConnection));
				break;
			default:
				log.warn(String.format(
						"Unexpected terminal connection state %d while sending a snapshot of terminal connection %s",
						terminalConnectionState, terminalConnection));
				break;
		}
	}
	
	private static void addSnapshotTerminalConnectionEvents(Set<FreeCallEvent> events, FreeTerminalConnection terminalConnection) {
		boolean active = false;
		boolean talking = false;
		int callControlState = terminalConnection.getCallControlState();
		switch (callControlState) {
			case CallControlTerminalConnection.IDLE:
				events.add(new FreeTermConnCreatedEv(Event.CAUSE_SNAPSHOT, Ev.META_SNAPSHOT, false, terminalConnection));
				break;
			case CallControlTerminalConnection.RINGING:
				events.add(new FreeTermConnRingingEv(Event.CAUSE_SNAPSHOT, Ev.META_SNAPSHOT, false, terminalConnection));
				break;
			case CallControlTerminalConnection.TALKING:
				events.add(new FreeTermConnTalkingEv(Event.CAUSE_SNAPSHOT, Ev.META_SNAPSHOT, false, terminalConnection));
				active = true;
				talking = true;
				break;
			case CallControlTerminalConnection.HELD:
				events.add(new FreeTermConnHeldEv(Event.CAUSE_SNAPSHOT, Ev.META_SNAPSHOT, false, terminalConnection));
				active = true;
				talking = false;
				break;
			case CallControlTerminalConnection.DROPPED:
				events.add(new FreeTermConnDroppedEv(Event.CAUSE_SNAPSHOT, Ev.META_SNAPSHOT, false, terminalConnection));
				break;
			case CallControlTerminalConnection.UNKNOWN:
				events.add(new FreeTermConnUnknownEv(Event.CAUSE_SNAPSHOT, Ev.META_SNAPSHOT, false, terminalConnection));
				break;
			default:
				log.warn(String.format(
						"Unexpected call control state %d while sending a snapshot of terminal connection %s",
						callControlState, terminalConnection));
				break;
		}
		if (active) {
			events.add(new FreeTermConnActiveEv(Event.CAUSE_SNAPSHOT, Ev.META_SNAPSHOT, false, terminalConnection, talking));
		}
	}
	
	/**
	 * Forward the event off to all observers
	 * Creation date: (2000-02-14 15:06:59)
	 * 
	 * @author Richard Deadman
	 * @param ev The event to forward
	 */
	public void sendToObservers(FreeCallEvent ev) {
		// send to observers
		FreeCallEvent[] evs = {ev};
		listMgr.sendEvents(evs);
	}
	
	public void setCallID(CallId newCallID) {
		callID = newCallID;
	}
	
	/**
	 * Sets the TerminalConnection which acts as the conference
	 * controller for the Call. The conference controller
	 * represents the participant in the Call around which a
	 * conference takes place.
	 * 
	 * Typically, when two Calls are conferenced together, a
	 * single participant is part of both Calls. This participant
	 * is represented by a TerminalConnection on each Call, each
	 * of which shares the same Terminal.
	 * 
	 * If the designated TerminalConnection is not part of this
	 * Call, an exception is thrown. If the TerminalConnection
	 * leaves the Call in the future, the implementation resets
	 * the conference controller to null.
	 * 
	 * See CallControlCalll.setConferenceController() for pre and
	 * post conditions.
	 */
	@Override
	public void setConferenceController(TerminalConnection tc) throws InvalidStateException, InvalidArgumentException {
		if (this.getState() != Call.ACTIVE) {
			throw new InvalidStateException(this, InvalidStateException.CALL_OBJECT, getState(), "Call must be active to set the ConferenceController");
		}
		if (tc instanceof FreeTerminalConnection) {
			// check if the ConferenceController is part of the call
			boolean member = false;
			Connection conns[] = getConnections();
			int connSize = conns.length;
			for (int i = 0; (i < connSize) && (!member); i++) {
				TerminalConnection tcs[] = conns[i].getTerminalConnections();
				if (tcs != null) {
					for (int j = 0, tcSize = tcs.length; j < tcSize; j++) {
						if (tcs[j].equals(tc)) {
							member = true;
							break;
						}
					}
				}
			}
			if (member) {
				confController = (FreeTerminalConnection)tc;
				return;
			}
		}
		throw new InvalidArgumentException("TerminalConnection is not part of the call");
	}
	
	/**
	 * Clears the conference controller. Used internally by addParty()
	 * since sending null to setConferenceController causes a
	 * InvalidArgumentException to be thrown.
	 * 
	 * @author Richard Deadman
	 * @author Doug Currie
	 *
	 */
	private void clearConferenceController() {
		confController = null;
	}
/**
 * Not currently supported. In future we should keep track of 
 */
@Override
public void setConferenceEnable(boolean enable) throws InvalidStateException, MethodNotSupportedException, PrivilegeViolationException, InvalidArgumentException {
	if (this.getState() != IDLE) {
		throw new InvalidStateException(this, InvalidStateException.CALL_OBJECT, this.getState(), "Call must be in IDLE state to set ConferenceEnable");
	}
	this.confEnabled = enable;
}

	/**
	 * Set PrivateData to be used in the next low-level command.
	 */
	@Override
	public void setPrivateData(Object data) {
		provider.getRaw().setPrivateData(getCallID(), null, null, data);
	}
	
	void setProvider(GenericProvider newProvider) {
		provider = newProvider;
	}
	
	@Override
	public void setTransferController(TerminalConnection tc) throws InvalidStateException, MethodNotSupportedException, ResourceUnavailableException, InvalidArgumentException {
		if (tc instanceof FreeTerminalConnection) {
			transController = (FreeTerminalConnection)tc;
		}
		else {
			throw new InvalidArgumentException();
		}
	}
	
	/**
	 * Turn on or off transfer -- must be done while call is IDLE.
	 */
	@Override
	public void setTransferEnable(boolean enable) throws InvalidStateException, MethodNotSupportedException, PrivilegeViolationException, InvalidArgumentException {
		if (getState() != IDLE) {
			throw new InvalidStateException(this, InvalidStateException.CALL_OBJECT, getState(), "Call must be in IDLE state to set TransferEnable");
		}
		transEnabled = enable;
	}
	
	/**
	 * Tell the raw TelephonyProvider to stop reporting events on me.
	 * <P>Don't bother synchronizing -- its unlikely that the garbage collector and an invalid
	 * signal will collide, and if they do all we lose is an extra TelephonyProvider message.
	 * Creation date: (2000-06-22 13:38:56)
	 * 
	 * @author Richard Deadman
	 */
	private void stopReporting() {
		GenericProvider prov = provider;
		if (!stoppedReporting && prov.getCallMgr().isDynamic()) {
			// tell the raw TelephonyProvider to stop reporting my calls
			prov.getRaw().stopReportingCall(getCallID());
			stoppedReporting = true;
		}
	}
	
	/**
	 * Defines the actions to be taken when the Call moves to the Active state.
	 * <P>We don't use a normal State Pattern here since our actions are state transition actions which
	 * do not change the resulting state depending on the starting state.
	 * Creation date: (2000-05-04 23:58:34)
	 * 
	 * @author Richard
	 */
	void toActive(int cause) {
		// only process once
		if (getState() == Call.IDLE) {
			state = Call.ACTIVE;
			// notify any listeners
			provider.dispatch(new FreeCallActiveEv(cause, this));
		}
	}
	
	/**
	 * Defines the actions to be taken when the Call moves to the Invalid state.
	 * <P>We don't use a normal State Pattern here since our actions are state transition actions which
	 * do not change the resulting state depending on the starting state.
	 * Creation date: (2000-05-04 23:58:34)
	 * 
	 * @author Richard Deadman
	 */
	@SuppressWarnings("unchecked")
	void toInvalid(int cause) {
		// unHook any connections
		for (FreeConnection connection : ((HashMap<String, FreeConnection>) connections.clone()).values()) {
			connection.toDisconnected(cause);
		}
		
		state = Call.INVALID;
		
		// notify any listeners, postponing the cleanup until the event is processed
		provider.dispatch(new FreeCallInvalidEv(cause, this));
	}

	/**
	 * Cleanup the call.
	 * <P>This is called by the FreeCallInvalidEv.dispatch() method when the event
	 * has been sent to everyone. Since they are in different packages, this method
	 * must be public. It should not be called by any other objects.
	 */
	public void cleanup() {
		CallId callId = getCallID();
		// check to make sure we haven't cleaned up already.
		if (callId != null) {
			// unregister any remaining listeners
			listMgr.removeAll();
		
			// tell the raw TelephonyProvider that it may now recycle the CallId
			GenericProvider prov = provider;
			prov.getRaw().releaseCallId(callId);
		
			// remove from framework
			if(prov.getCallMgr().removeCall(this)) {
				// ensure we don't cleanup twice
				setCallID(null);
			}
		}
	}
	
	/**
	 * This overloaded version of this method transfers all participants
	 * currently on this Call, with the exception of the transfer controller
	 * participant, to another telephone address. This is often called a
	 * "single-step transfer" because the transfer feature places another
	 * telephone call and performs the transfer at one time. The telephone
	 * address string given as the argument to this method must be valid and
	 * complete.
	 * <P>Note that if a transfer controller is not specified, one will be
	 * choosen which may lead to an inappropriate call participant being
	 * removed from the call.
	 */
	@Override
	public Connection transfer(String address) throws MethodNotSupportedException, ResourceUnavailableException, InvalidArgumentException, InvalidPartyException, InvalidStateException, PrivilegeViolationException {
	
		// test for transfer TerminalController
		TerminalConnection trans = getTransferController();
		if (trans == null) {	// look for the first available TC
			Connection[] cs = getConnections();
			if (cs != null) {
				for (int i = 0; i < cs.length; i++) {
					TerminalConnection[] tcs = cs[i].getTerminalConnections();
					if (tcs != null && tcs.length > 0) {
						trans = tcs[0];
						break;
					}
				}
			}
		}
		if (trans == null)
			throw new InvalidArgumentException("Transfer TerminalConnection not set and cannot be found");
	
		// Create a consultation call
		CallControlCall consult = (CallControlCall) provider.createCall();
		consult.consult(trans, address);
	
		// transfer the call
		transfer(trans, consult);
	
		// Find the remote connection
		Connection conn[] = getConnections();
		Connection rem = null;
		for (int i = 0; i < conn.length; i++) {
			if (conn[i].getAddress().getName().equals(address)) {
				rem = conn[i];
				break;
			}
		}
		return rem;
	}
	
	/**
	 * transfer by conferencing and then dropping off the call
	 */
	@Override
	public void transfer(Call otherCall) throws MethodNotSupportedException, ResourceUnavailableException, InvalidArgumentException, InvalidPartyException, InvalidStateException, PrivilegeViolationException {
		// see if we can find a common terminal
		TerminalConnection tc = getTransferController();
		tc = getOrVerifyController(tc, otherCall);
	
		transfer(tc, otherCall);
	}
	
	/**
	 * transfer by conferencing and then dropping off the call.
	 * Note that this will drop the whole Connection off the call, and not just the TerminalConnection.
	 */
	private void transfer(TerminalConnection tc, Call otherCall) throws MethodNotSupportedException, ResourceUnavailableException, InvalidArgumentException, InvalidStateException, PrivilegeViolationException {
		// first check if transfer is enabled
		if ((!getTransferEnable()) || (!((CallControlCall)otherCall).getTransferEnable())) {
			throw new InvalidStateException(this, InvalidStateException.CALL_OBJECT, getState(), "TransferEnabled is set to false");
		}
		
		// join the calls - don't use my conference method, since it uses the conference controller
		TelephonyProvider rp = provider.getRaw();
		CallId id = getCallID();
		FreeConnection tcConn = (FreeConnection)tc.getConnection();
		String tcAddress = tcConn.getAddress().getName();
		String tcTerminal = tc.getTerminal().getName();
		
		rp.join(id, ((FreeCall)otherCall).getCallID(), tcAddress, tcTerminal);
	
		// now drop the connection off the call
		rp.release(tcAddress, id);
		// and note that the old Connection is disconnected
		tcConn.toDisconnected(Event.CAUSE_NORMAL);
	}
	
	@Override
	public String toString() {
		String state;
		switch (getState()) {
			case Call.IDLE:
				state = "idle";
				break;
			case Call.ACTIVE:
				state = "active";
				break;
			default:
				state = "invalid";
		}
		Connection[] conns = getConnections();
		StringBuilder connections = new StringBuilder();
		if (conns == null || conns.length == 0) {
			connections.append("without connections");
		}
		else {
			connections.append("with ")
					.append(conns.length)
					.append(" connection(s): ")
					.append("<")
					.append(conns[0])
					.append(">");
			for (int i = 1; i < conns.length; i++) {
				connections.append(", <")
						.append(conns[i])
						.append(">");
			}
		}
		return String.format("FreeCall (id='%s', state='%s') %s", callID, state, connections);
	}
}
