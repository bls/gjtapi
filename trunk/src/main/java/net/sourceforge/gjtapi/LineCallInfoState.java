package net.sourceforge.gjtapi;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

import lombok.Getter;

/**
 * @author haf
 */
public enum LineCallInfoState implements BitField {
	
	OTHER(0x1),
	DEVICE_SPECIFIC(0x2),
	BEARER_MODE(0x4),
	RATE(0x8),
	MEDIA_MODE(0x10),
	APP_SPECIFIC(0x20),
	CALL_ID(0x40),
	RELATED_CALL_ID(0x80),
	ORIGIN(0x100),
	REASON(0x200),
	COMPLETION_ID(0x400),
	OWNER_COUNT_INCREASED(0x800),
	OWNER_COUNT_DECREASED(0x1000),
	MONITORS_COUNT(0x2000),
	TRUNK(0x4000),
	CALLER_ID(0x8000),
	CALLED_ID(0x10000),
	CONNECTED_ID(0x20000),
	REDIRECTION_ID(0x40000),
	REDIRECTING_ID(0x80000),
	DISPLAY(0x100000),
	USER_USER_INFO(0x200000),
	HIGH_LEVEL_COMPATIBILITY(0x400000),
	LOW_LEVEL_COMPATIBILITY(0x800000),
	CHARGING_INFO(0x1000000),
	TERMINAL(0x2000000),
	DIAL_PARAMETERS(0x4000000),
	MONITOR_MODES(0x8000000),
	TREATMENT(0x10000000),
	QOS(0x20000000),
	CALL_DATA(0x40000000);
	
	private static final Map<Integer, LineCallInfoState> BIT_VALUE_MAP =
			EnumUtils.toBitValueMap(LineCallInfoState.class);
	
	@Getter private final int bitValue;
	
	private LineCallInfoState(int bitValue) {
		this.bitValue = bitValue;
	}
	
	public static Optional<LineCallInfoState> fromBitValue(int value) {
		return Optional.ofNullable(BIT_VALUE_MAP.get(value));
	}
	
	public static Set<LineCallInfoState> fromBitField(int bitField) {
		return EnumUtils.fromBitField(bitField, LineCallInfoState.class);
	}
}
