package net.sourceforge.gjtapi;

/*
 Copyright (c) 2002 8x8 Inc. (www.8x8.com) 

 All rights reserved. 

 Permission is hereby granted, free of charge, to any person obtaining a 
 copy of this software and associated documentation files (the 
 "Software"), to deal in the Software without restriction, including 
 without limitation the rights to use, copy, modify, merge, publish, 
 distribute, and/or sell copies of the Software, and to permit persons 
 to whom the Software is furnished to do so, provided that the above 
 copyright notice(s) and this permission notice appear in all copies of 
 the Software and that both the above copyright notice(s) and this 
 permission notice appear in supporting documentation. 

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT 
 OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
 HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL 
 INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING 
 FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
 NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION 
 WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. 

 Except as contained in this notice, the name of a copyright holder 
 shall not be used in advertising or otherwise to promote the sale, use 
 or other dealings in this Software without prior written authorization 
 of the copyright holder.
 */
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.telephony.Address;
import javax.telephony.CallEvent;
import javax.telephony.CallListener;
import javax.telephony.CallObserver;
import javax.telephony.ConnectionEvent;
import javax.telephony.ConnectionListener;
import javax.telephony.MetaEvent;
import javax.telephony.Terminal;
import javax.telephony.TerminalConnectionEvent;
import javax.telephony.TerminalConnectionListener;
import javax.telephony.callcontrol.CallControlCallObserver;

import net.sourceforge.gjtapi.events.CCCallEv;
import net.sourceforge.gjtapi.events.FreeCallEvent;
import net.sourceforge.gjtapi.events.FreeCallObservationEndedEv;

/**
 * A manager object that a Call can use to track its set of Observers and Listeners. Listeners are
 * added and removed from here for each call. Note that when a listener is removed,
 * removing from an address or terminal will decrement the usage count, whereas
 * a full remove will remove from all lists.
 * <P>
 * The add and remove methods are synchronized to control orderly access, but
 * the event triggering is not. It is placed in a separate class for clarity.
 * Creation date: (2000-05-01 10:57:44)
 * 
 * @author: Richard Deadman
 */
class ListenerManager implements TerminalConnectionListener {
	
	/**
	 * Internal listener holder for a Listener. All its registered Listeners are tagged by the
	 * Terminals or Addresses that applied them.
	 */
	private static class ListenerStatus {
		
		/**
		 * If {@code true}, a listener has been directly applied and the Address and Terminal
		 * connectivity no longer applies.
		 */
		private boolean explicit;
		private final Set<Terminal> terminals = new HashSet<>();
		private final Set<Address> addresses = new HashSet<>();
		
		/**
		 * Constructor for an explicit Listener
		 */
		ListenerStatus() {
			this.explicit = true;
		}
		
		/**
		 * Constructor for a Listener Status triggered by a visitation to an Address with registered
		 * CallListeners
		 */
		ListenerStatus(Address addr) {
			this.explicit = false;
			this.addresses.add(addr);
		}
		
		/**
		 * Constructor for a Listener Status triggered by a visitation to an Terminal with
		 * registered CallListeners
		 */
		ListenerStatus(Terminal term) {
			this.explicit = false;
			this.terminals.add(term);
		}
		
		void addApplier(Address addr) {
			if (!explicit) {
				addresses.add(addr);
			}
		}
		
		void addApplier(Terminal term) {
			if (!explicit) {
				terminals.add(term);
			}
		}
		
		void setExplicit() {
			explicit = true;
			addresses.clear();
			terminals.clear();
		}
		
		/**
		 * Remove a Address
		 * 
		 * @param address the Address to remove
		 * @return {@code true} if the listener should now be removed.
		 */
		boolean remove(Address address) {
			addresses.remove(address);
			return shouldRemove();
		}
		
		/**
		 * Remove a Terminal
		 * 
		 * @param terminal the Terminal to remove
		 * @return {@code true} if the held listener should now be removed.
		 */
		boolean remove(Terminal terminal) {
			terminals.remove(terminal);
			return shouldRemove();
		}
		
		/**
		 * Determine if the listener is not longer applicable to the call
		 */
		boolean shouldRemove() {
			return !explicit && terminals.isEmpty() && addresses.isEmpty();
		}
	}
	
	/**
	 * Internal HashMap wrapper that catches put and remove calls to turn on throttling
	 */
	private static class ListenerMap extends ConcurrentHashMap<CallListener, ListenerStatus> {
		
		private static final long serialVersionUID = 0L; // never serialized
		
		private final FreeCall call;
		
		public ListenerMap(FreeCall call) {
			this.call = call;
		}
		
		@Override
		public ListenerStatus put(CallListener list, ListenerStatus status) {
			ListenerStatus res = super.put(list, status);
			
			// send a snapshot back to the key
			call.sendSnapShot(list);
			
			return res;
		}
	}
	
	private static class ObserverMap extends ConcurrentHashMap<CallObserver, ListenerStatus> {
		
		private static final long serialVersionUID = 0L; // never serialized
		
		private final FreeCall call;
		
		public ObserverMap(FreeCall call) {
			this.call = call;
		}
		
		@Override
		public ListenerStatus put(CallObserver obs, ListenerStatus status) {
			ListenerStatus res = super.put(obs, status);
			
			// send a snapshot back to the key
			call.sendSnapShot(obs);
			
			return res;
		}
	}
	
	/**
	 * A map of CallObservers to their ListenerStatus object that track who applied the observer to the call.
	 */
	private final ObserverMap obsMap;
	/**
	 * The subset of observers that listen for callcontrol events
	 */
	private final Set<CallObserver> ccObservers = ConcurrentHashMap.newKeySet();
	/**
	 * A map of Listeners to their ListenerStatus objects that track who applied the Listener
	 * All listeners, with their status holder
	 */
	private final ListenerMap listMap;
	/**
	 * Subset of {@link #listMap}'s keys that also receives Connection events
	 */
	private final Set<CallListener> connLists = ConcurrentHashMap.newKeySet();
	/**
	 * Subset of {@link #connLists} that also receives TerminalConnection events
	 */
	private final Set<CallListener> tcLists = ConcurrentHashMap.newKeySet();
	/**
	 * The call that these Listeners are registered against
	 */
	private final FreeCall call;

	/**
	 * Constructor for a Set of CallListeners for a Call
	 */
	ListenerManager(FreeCall c) {
		this.call = c;
		this.obsMap = new ObserverMap(c);
		this.listMap = new ListenerMap(c);
	}
	
	/**
	 * Add a CallListener to the managed set as an explicitly set CallListener.
	 * These are only removed through {@link FreeCall#removeCallListener(CallListener)}.
	 * Creation date: (2000-05-01 11:28:09)
	 * 
	 * @author Richard Deadman
	 * @param l the new or updated listener
	 */
	synchronized void add(CallListener l) {
		ListenerStatus status = listMap.get(l);
		if (status == null) {
			status = new ListenerStatus();
			listMap.put(l, status);
			protect(); // mark as needing gc protection
		} else {
			status.setExplicit();
		}
		
		// see if we need to add to subtype lists
		addSubTypes(l);
	}
	
	/**
	 * Add a CallListener to the managed set and associate it with an Address that triggered the
	 * addition.
	 * Creation date: (2000-05-01 11:28:09)
	 * 
	 * @author Richard Deadman
	 * @param l the new or updated listener
	 * @param addr an Address visited by a call that held this listener as one of its CallListeners.
	 */
	synchronized void add(CallListener l, Address addr) {
		ListenerStatus status = listMap.get(l);
		if (status == null) {
			status = new ListenerStatus(addr);
			listMap.put(l, status);
			protect(); // mark as needing gc protection
		} else {
			status.addApplier(addr);
		}
		
		// see if we need to register it for extra events
		addSubTypes(l);
	}
	
	/**
	 * Add a CallListener to the managed set and associate it with a Terminal that triggered the
	 * addition.
	 * Creation date: (2000-05-01 11:28:09)
	 * 
	 * @author Richard Deadman
	 * @param l the new or updated listener
	 * @param term a Terminal visited by a call that held this listener as one of its CallListeners.
	 */
	synchronized void add(CallListener l, Terminal term) {
		ListenerStatus status = listMap.get(l);
		if (status == null) {
			status = new ListenerStatus(term);
			listMap.put(l, status);
			protect(); // mark as needing gc protection
		} else {
			status.addApplier(term);
		}
		
		// see if we need to register it for extra events
		addSubTypes(l);
	}
	
	/**
	 * Add a CallObserver to the managed set as an explicitly set CallObserver. These are only
	 * removed through {@link FreeCall#removeObserver(CallObserver)}.
	 * Creation date: (2000-05-01 11:28:09)
	 * 
	 * @author Richard Deadman
	 * @param o the new or updated observer
	 */
	synchronized void add(CallObserver o) {
		ObserverMap obs = obsMap;
		synchronized (obs) {
			ListenerStatus status = obs.get(o);
			if (status == null) {
				status = new ListenerStatus();
				obs.put(o, status);
				protect(); // mark as needing gc protection
			} else {
				status.setExplicit();
			}
			
			// see if we need to add to subtype lists
			addSubTypes(o);
		}
	}
	
	/**
	 * Add a CallObserver to the managed set and associate it with an Address that triggered the
	 * addition.
	 * Creation date: (2000-05-01 11:28:09)
	 * 
	 * @author Richard Deadman
	 * @param o the new or updated observer
	 * @param addr an Address visited by a call that held this observer as one of its CallObservers.
	 */
	synchronized void add(CallObserver o, Address addr) {
		ObserverMap map = obsMap;
		synchronized (map) {
			ListenerStatus status = map.get(o);
			if (status == null) {
				status = new ListenerStatus(addr);
				map.put(o, status);
				protect(); // mark as needing gc protection
			} else {
				status.addApplier(addr);
			}
			
			// see if we need to register it for extra events
			addSubTypes(o);
		}
	}
	
	/**
	 * Add a CallObserver to the managed set and associate it with a Terminal that triggered the
	 * addition.
	 * Creation date: (2000-05-01 11:28:09)
	 * 
	 * @author Richard Deadman
	 * @param o the new or updated observer
	 * @param term a Terminal visited by a call that held this observer as one of its CallObservers.
	 */
	synchronized void add(CallObserver o, Terminal term) {
		ObserverMap map = obsMap;
		synchronized (map) {
			ListenerStatus status = map.get(o);
			if (status == null) {
				status = new ListenerStatus(term);
				map.put(o, status);
				protect(); // mark as needing gc protection
			} else {
				status.addApplier(term);
			}
			
			// see if we need to register it for extra events
			addSubTypes(o);
		}
	}
	
	/**
	 * Adds a CallListener to the Connection set and TerminalConnection set if it has the right
	 * signature
	 */
	private void addSubTypes(CallListener cl) {
		if (cl instanceof ConnectionListener) {
			connLists.add(cl);
			if (cl instanceof TerminalConnectionListener) {
				tcLists.add(cl);
			}
		}
	}
	
	/**
	 * Adds a CallObserver to the CallControl set if it is of the right type
	 */
	private void addSubTypes(CallObserver co) {
		if (co instanceof CallControlCallObserver) {
			ccObservers.add(co);
		}
	}
	
	@Override
	public void callActive(CallEvent event) {
		for (CallListener listener : getCallListeners()) {
			listener.callActive(event);
		}
	}
	
	@Override
	public void callEventTransmissionEnded(CallEvent event) {
		for (CallListener listener : getCallListeners()) {
			listener.callEventTransmissionEnded(event);
		}
	}
	
	@Override
	public void callInvalid(CallEvent event) {
		for (CallListener listener : getCallListeners()) {
			listener.callInvalid(event);
		}
	}
	
	@Override
	public void connectionAlerting(ConnectionEvent event) {
		for (CallListener listener : connLists) {
			if (listener instanceof ConnectionListener) {
				((ConnectionListener) listener).connectionAlerting(event);
			}
		}
	}
	
	@Override
	public void connectionConnected(ConnectionEvent event) {
		for (CallListener listener : connLists) {
			if (listener instanceof ConnectionListener) {
				((ConnectionListener) listener).connectionConnected(event);
			}
		}
	}
	
	@Override
	public void connectionCreated(ConnectionEvent event) {
		for (CallListener listener : connLists) {
			if (listener instanceof ConnectionListener) {
				((ConnectionListener) listener).connectionCreated(event);
			}
		}
	}
	
	@Override
	public void connectionDisconnected(ConnectionEvent event) {
		for (CallListener listener : connLists) {
			if (listener instanceof ConnectionListener) {
				((ConnectionListener) listener).connectionDisconnected(event);
			}
		}
	}
	
	@Override
	public void connectionFailed(ConnectionEvent event) {
		for (CallListener listener : connLists) {
			if (listener instanceof ConnectionListener) {
				((ConnectionListener) listener).connectionFailed(event);
			}
		}
	}
	
	@Override
	public void connectionInProgress(ConnectionEvent event) {
		for (CallListener listener : connLists) {
			if (listener instanceof ConnectionListener) {
				((ConnectionListener) listener).connectionInProgress(event);
			}
		}
	}
	
	@Override
	public void connectionUnknown(ConnectionEvent event) {
		for (CallListener listener : connLists) {
			if (listener instanceof ConnectionListener) {
				((ConnectionListener) listener).connectionUnknown(event);
			}
		}
	}
	
	/**
	 * Base Call Listener Set accessor
	 * Creation date: (2000-05-01 11:35:21)
	 * 
	 * @author Richard Deadman
	 * @return The set of managed call listeners.
	 */
	Set<CallListener> getCallListeners() {
		return listMap.keySet();
	}
	
	/**
	 * Base Call Observer Set accessor
	 * Creation date: (2000-05-01 11:35:21)
	 * 
	 * @author Richard Deadman
	 * @return The set of managed call observers.
	 */
	Set<CallObserver> getCallObservers() {
		return obsMap.keySet();
	}
	
	/**
	 * Note if their are no registered listeners or observers.
	 * Creation date: (2000-06-23 11:58:01)
	 * 
	 * @author Richard Deadman
	 * @return A boolean boolean indicating whether there are no registered listeners or observers
	 */
	boolean isEmpty() {
		return (listMap.isEmpty() && obsMap.isEmpty());
	}
	
	@Override
	public void multiCallMetaMergeEnded(MetaEvent event) {
		for (CallListener listener : getCallListeners()) {
			listener.multiCallMetaMergeEnded(event);
		}
	}
	
	@Override
	public void multiCallMetaMergeStarted(MetaEvent event) {
		for (CallListener listener : getCallListeners()) {
			listener.multiCallMetaMergeStarted(event);
		}
	}
	
	@Override
	public void multiCallMetaTransferEnded(MetaEvent event) {
		for (CallListener listener : getCallListeners()) {
			listener.multiCallMetaTransferEnded(event);
		}
	}
	
	@Override
	public void multiCallMetaTransferStarted(MetaEvent event) {
		for (CallListener listener : getCallListeners()) {
			listener.multiCallMetaTransferStarted(event);
		}
	}
	
	/**
	 * On adding the first observer or listener, mark the call as needing garbage collection
	 * protection.
	 * Creation date: (2000-06-23 12:06:47)
	 * 
	 * @author Richard Deadman
	 */
	private void protect() {
		// check if we should protect the call
		if (isEmpty()) {
			((GenericProvider) call.getProvider()).getCallMgr().protect(call);
		}
	}
	
	/**
	 * Remove a CallListener applier from the managed set and the CallListener itself if necessary.
	 * Creation date: (2000-05-01 11:28:09)
	 * 
	 * @author Richard Deadman
	 * @param addr an Address visited by a call that held this listener as one of its CallListeners.
	 */
	synchronized void remove(Address addr) {
		LinkedList<CallListener> itemsToRemove = new LinkedList<>();
		Iterator<CallListener> it = listMap.keySet().iterator();
		while (it.hasNext()) {
			CallListener cl = it.next();
			ListenerStatus status = listMap.get(cl);
			if (status != null) {
				if (status.remove(addr)) {
					// the address was the last listener handle -- remove the
					// listener, subtypes and notify
					
					// we hold it and remove it outside the loop to avoid a ConcurrentModificationException
					// we can't just use it.remove() since we have to do some extra logic
					itemsToRemove.add(cl);
				}
			}
		}
		
		// Now remove any found items
		for (CallListener cl : itemsToRemove) {
			remove(cl);
		}
		
		// now remove any registered observers
		ObserverMap obs = obsMap;
		LinkedList<CallObserver> observersToRemove = new LinkedList<>();
		Iterator<CallObserver> observerIterator = obs.keySet().iterator();
		while (observerIterator.hasNext()) {
			CallObserver co = observerIterator.next();
			ListenerStatus status = obs.get(co);
			if (status != null) {
				if (status.remove(addr)) {
					// the address was the last listener handle -- remove the
					// observer, subtypes and notify
					
					// we hold it and remove it outside the loop to avoid a ConcurrentModificationException
					// we can't just use it.remove() since we have to do some extra logic
					observersToRemove.add(co);
				}
			}
		}
		
		// Now remove any found items
		for (CallObserver co : observersToRemove) {
			remove(co);
		}
	}
	
	/**
	 * Remove a CallListener from the managed set. To be registered as a higher level listener, it
	 * must be registered as a base CallListener.
	 * Creation date: (2000-05-01 11:28:09)
	 * 
	 * @author Richard Deadman
	 */
	synchronized void remove(CallListener cl) {
		boolean removed = (listMap.remove(cl) != null);
		if (removed) {
			// attempt to remove from sub-type sets
			removeSubTypes(cl);
			
			// trigger the callListener that it is no longer being triggered
			// Observer code may throw an exception
			try {
				cl.callEventTransmissionEnded(new FreeCallObservationEndedEv(call));
			} catch (Exception ex) {
				// No-op code -- should we log it?
			}
			
			// check if we should unprotect the call
			unProtect();
		}
	}
	
	/**
	 * Remove a CallObserver from the managed set. To be registered as a higher level observer, it
	 * must be registered as a base CallObserver.
	 * Creation date: (2000-05-01 11:28:09)
	 * 
	 * @author Richard Deadman
	 */
	synchronized void remove(CallObserver co) {
		boolean removed = (obsMap.remove(co) != null);
		if (removed) {
			// attempt to remove from sub-type sets
			removeSubTypes(co);
			
			// trigger the callObserver that it is no longer being triggered
			FreeCallObservationEndedEv[] evs = new FreeCallObservationEndedEv[1];
			evs[0] = new FreeCallObservationEndedEv(call);
			// Observer code may throw exception
			try {
				co.callChangedEvent(evs);
			} catch (Exception ex) {
				// No-op -- we could log it?
			}
			
			// check if we should unprotect the call
			unProtect();
		}
	}
	
	/**
	 * Remove a CallListener applier from the managed set and the CallListener itself if necessary.
	 * Creation date: (2000-05-01 11:28:09)
	 * 
	 * @author Richard Deadman
	 * @param term A Terminal visited by a call that held this listener as one of its CallListeners.
	 */
	synchronized void remove(Terminal term) {
		ListenerMap lists = listMap;
		LinkedList<CallListener> itemsToRemove = new LinkedList<>();
		Iterator<CallListener> it = lists.keySet().iterator();
		while (it.hasNext()) {
			CallListener cl = it.next();
			ListenerStatus status = lists.get(cl);
			if (status != null) {
				if (status.remove(term)) {
					// the address was the last listener handle -- remove the
					// listener, subtypes and notify
					
					// we hold it and remove it outside the loop to avoid a ConcurrentModificationException
					// we can't just use it.remove() since we have to do some extra logic
					itemsToRemove.add(cl);
				}
			}
		}
		
		// Now remove any found items
		for (CallListener cl : itemsToRemove) {
			remove(cl);
		}
		
		// now remove any registered observers
		ObserverMap obs = obsMap;
		LinkedList<CallObserver> observersToRemove = new LinkedList<>();
		Iterator<CallObserver> observerIterator = obs.keySet().iterator();
		while (observerIterator.hasNext()) {
			CallObserver co = observerIterator.next();
			ListenerStatus status = obs.get(co);
			if (status != null) {
				if (status.remove(term)) {
					// the address was the last listener handle -- remove the
					// observer, subtypes and notify
					
					// we hold it and remove it outside the loop to avoid a ConcurrentModificationException
					// we can't just use it.remove() since we have to do some extra logic
					observersToRemove.add(co);
				}
			}
		}
		
		// Now remove any found items
		for (CallObserver co : observersToRemove) {
			remove(co);
		}
	}
	
	/**
	 * Remove all listeners from the call's listener manager.
	 * Creation date: (2000-05-01 11:28:09)
	 * 
	 * @author Richard Deadman
	 */
	synchronized void removeAll() {
		Iterator<CallListener> it = getCallListeners().iterator();
		while (it.hasNext()) {
			remove(it.next());
		}
		
		Iterator<CallObserver> observerIterator = obsMap.keySet().iterator();
		while (observerIterator.hasNext()) {
			remove(observerIterator.next());
		}
	}
	
	/**
	 * This removes a Call Listener from the Connection set and TerminalConnection set if it has the
	 * right type
	 */
	private void removeSubTypes(CallListener cl) {
		if (cl instanceof ConnectionListener) {
			connLists.remove(cl);
			if (cl instanceof TerminalConnectionListener) {
				tcLists.remove(cl);
			}
		}
	}
	
	/**
	 * This removes a Call Observer from the CallControl set if it has the right type
	 */
	private void removeSubTypes(CallObserver co) {
		if (co instanceof CallControlCallObserver) {
			ccObservers.remove(co);
		}
	}

	/**
	 * Forward the event set to all registered CallObservers.
	 * Creation date: (2000-05-02 14:16:38)
	 * 
	 * @author Richard Deadman
	 * @param evs CallEv array of events.
	 */
	void sendEvents(FreeCallEvent[] evs) {
		for (CallObserver observer : getCallObservers()) {
			observer.callChangedEvent(evs);
		}
		
		// now see if we have any CallControlObservers
		if (ccObservers.size() > 0) {
			// Morph the CallEvs to CallCtlEvs
			CCCallEv[] ccevs = CCCallEv.toCcEvents(evs);
			if (ccevs.length > 0) {
				// delegate to CallCtlObservers
				for (CallObserver observer : ccObservers) {
					((CallControlCallObserver) observer).callChangedEvent(ccevs);
				}
			}
		}
	}
	
	@Override
	public void singleCallMetaProgressEnded(MetaEvent event) {
		for (CallListener listener : getCallListeners()) {
			listener.singleCallMetaProgressEnded(event);
		}
	}
	
	@Override
	public void singleCallMetaProgressStarted(MetaEvent event) {
		for (CallListener listener : getCallListeners()) {
			listener.singleCallMetaProgressStarted(event);
		}
	}
	
	@Override
	public void singleCallMetaSnapshotEnded(MetaEvent event) {
		for (CallListener listener : getCallListeners()) {
			listener.singleCallMetaSnapshotEnded(event);
		}
	}
	
	@Override
	public void singleCallMetaSnapshotStarted(MetaEvent event) {
		for (CallListener listener : getCallListeners()) {
			listener.singleCallMetaSnapshotStarted(event);
		}
	}
	
	@Override
	public void terminalConnectionActive(TerminalConnectionEvent event) {
		for (CallListener listener : tcLists) {
			if (listener instanceof TerminalConnectionListener) {
				((TerminalConnectionListener) listener).terminalConnectionActive(event);
			}
		}
	}
	
	@Override
	public void terminalConnectionCreated(TerminalConnectionEvent event) {
		for (CallListener listener : tcLists) {
			if (listener instanceof TerminalConnectionListener) {
				((TerminalConnectionListener) listener).terminalConnectionCreated(event);
			}
		}
	}
	
	@Override
	public void terminalConnectionDropped(TerminalConnectionEvent event) {
		for (CallListener listener : tcLists) {
			if (listener instanceof TerminalConnectionListener) {
				((TerminalConnectionListener) listener).terminalConnectionDropped(event);
			}
		}
	}
	
	@Override
	public void terminalConnectionPassive(TerminalConnectionEvent event) {
		for (CallListener listener : tcLists) {
			if (listener instanceof TerminalConnectionListener) {
				((TerminalConnectionListener) listener).terminalConnectionPassive(event);
			}
		}
	}
	
	@Override
	public void terminalConnectionRinging(TerminalConnectionEvent event) {
		for (CallListener listener : tcLists) {
			if (listener instanceof TerminalConnectionListener) {
				((TerminalConnectionListener) listener).terminalConnectionRinging(event);
			}
		}
	}
	
	@Override
	public void terminalConnectionUnknown(TerminalConnectionEvent event) {
		for (CallListener listener : tcLists) {
			if (listener instanceof TerminalConnectionListener) {
				((TerminalConnectionListener) listener).terminalConnectionUnknown(event);
			}
		}
	}
	
	/**
	 * On removing the last observer or listener, mark the call as being available for garbage
	 * collection.
	 * Creation date: (2000-06-23 12:06:47)
	 * 
	 * @author Richard Deadman
	 */
	private void unProtect() {
		// check if we should unprotect the call
		if (isEmpty()) {
			((GenericProvider) call.getProvider()).getCallMgr().unProtect(call);
		}
	}
}
