package net.sourceforge.gjtapi;

import net.sourceforge.gjtapi.raw.remote.SerializableCallId;

/**
 * ID generator which always creates a new ID of increased value.
 * 
 * @author haf
 */
public class SequentialIdGenerator implements IdGenerator {
	
	private int nextId = 0;
	
	private synchronized int updateNextId() {
		return nextId++;
	}
	
	@Override
	public SerializableCallId getSerializableId() {
		return newSerializableId(updateNextId());
	}
	
	/**
	 * No-op. IDs are never reused.
	 * 
	 * @param id the CallId which is not used anymore
	 */
	@Override
	public void freeSerializableId(CallId id) {
	}
}
