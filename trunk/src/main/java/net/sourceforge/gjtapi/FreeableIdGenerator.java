package net.sourceforge.gjtapi;

import java.util.LinkedList;

import net.sourceforge.gjtapi.raw.remote.SerializableCallId;

/**
 * ID generator which stores freed IDs and reuses them before using new IDs.
 * 
 * @author haf
 */
public class FreeableIdGenerator implements IdGenerator {
	
	private LinkedList<SerializableCallId> freeCallIds = new LinkedList<>();
	private int nextId = 0;
	
	private synchronized int updateNextId() {
		return nextId++;
	}
	
	@Override
	public synchronized SerializableCallId getSerializableId() {
		if (!freeCallIds.isEmpty()) {
			return freeCallIds.removeFirst();
		}
		return newSerializableId(updateNextId());
	}
	
	@Override
	public synchronized void freeSerializableId(CallId id) {
		if (id instanceof SerializableCallId) {
			freeCallIds.add((SerializableCallId) id);
		}
	}
}
