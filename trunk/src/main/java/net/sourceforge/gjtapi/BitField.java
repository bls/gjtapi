package net.sourceforge.gjtapi;

/**
 * @author haf
 */
public interface BitField {
	
	public int getBitValue();
}
