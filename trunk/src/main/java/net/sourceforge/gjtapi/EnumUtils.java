package net.sourceforge.gjtapi;

import java.util.EnumSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author haf
 */
class EnumUtils {
	
	static <K, E extends Enum<E>> Map<K, E> toValueMap(Class<E> enumType, Function<E, K> keyMapper) {
		return Stream.of(enumType.getEnumConstants())
				.collect(Collectors.toMap(keyMapper::apply, element -> element));
	}
	
	static <E extends Enum<E> & BitField> Map<Integer, E> toBitValueMap(Class<E> enumType) {
		return toValueMap(enumType, e -> e.getBitValue());
	}
	
	static <E extends Enum<E> & BitField> Set<E> fromBitField(int bitField, Class<E> enumType) {
		EnumSet<E> states = EnumSet.allOf(enumType);
		states.removeIf(element -> (element.getBitValue() & bitField) == 0);
		
		return states;
	}
	
	private EnumUtils() {
	}
}
