package net.sourceforge.gjtapi.raw.xtapi;

import java.util.Objects;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.sourceforge.gjtapi.CallId;

/**
 * This is identifier for calls known by GJTAPI. As such, it has existence
 * before an XTAPI ID is created for it.
 * 
 * @author Richard Deadman
 */
@Setter
@Getter
@ToString
class XtapiCallId implements CallId {
	
	private int callNum = -1; // initially not set
	private String localAddress = null; // the local address of the call
	private String remoteAddress = null; // the remote address of the call, if known
	
	/**
	 * Ensure equality works.
	 * Note that if the callNum's are not set, we default to identity
	 */
	@Override
	public boolean equals(Object other) {
		if (isUninitialized()) {
			return super.equals(other);
		}
		
		return other instanceof XtapiCallId && callNum == ((XtapiCallId) other).callNum;
	}
	
	@Override
	public int hashCode() {
		if (isUninitialized()) {
			return super.hashCode();
		}
		
		return Objects.hashCode(callNum);
	}
	
	private boolean isUninitialized() {
		return callNum == -1;
	}
}
