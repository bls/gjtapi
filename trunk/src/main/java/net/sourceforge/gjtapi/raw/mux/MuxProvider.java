/*
	Copyright (c) 2002 8x8 Inc. (www.8x8.com) 

	All rights reserved. 

	Permission is hereby granted, free of charge, to any person obtaining a 
	copy of this software and associated documentation files (the 
	"Software"), to deal in the Software without restriction, including 
	without limitation the rights to use, copy, modify, merge, publish, 
	distribute, and/or sell copies of the Software, and to permit persons 
	to whom the Software is furnished to do so, provided that the above 
	copyright notice(s) and this permission notice appear in all copies of 
	the Software and that both the above copyright notice(s) and this 
	permission notice appear in supporting documentation. 

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT 
	OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
	HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL 
	INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING 
	FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
	NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION 
	WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. 

	Except as contained in this notice, the name of a copyright holder 
	shall not be used in advertising or otherwise to promote the sale, use 
	or other dealings in this Software without prior written authorization 
	of the copyright holder.
*/
package net.sourceforge.gjtapi.raw.mux;

import java.io.IOException;
import java.io.InputStream;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.telephony.Call;
import javax.telephony.InvalidArgumentException;
import javax.telephony.InvalidPartyException;
import javax.telephony.MethodNotSupportedException;
import javax.telephony.PrivilegeViolationException;
import javax.telephony.ProviderUnavailableException;
import javax.telephony.ResourceUnavailableException;
import javax.telephony.media.MediaResourceException;
import javax.telephony.media.RTC;
import javax.telephony.media.Symbol;

import net.sourceforge.gjtapi.CallData;
import net.sourceforge.gjtapi.CallId;
import net.sourceforge.gjtapi.ConnectionData;
import net.sourceforge.gjtapi.RawSigDetectEvent;
import net.sourceforge.gjtapi.RawStateException;
import net.sourceforge.gjtapi.TCData;
import net.sourceforge.gjtapi.TelephonyListener;
import net.sourceforge.gjtapi.TelephonyProvider;
import net.sourceforge.gjtapi.TermData;
import net.sourceforge.gjtapi.capabilities.Capabilities;
import net.sourceforge.gjtapi.capabilities.RawCapabilities;
import net.sourceforge.gjtapi.raw.CoreTpi;
import net.sourceforge.gjtapi.raw.ProviderFactory;

/**
 * This is a pluggable provider that multiplexes several other providers together.
 * <P>Note that this decides which subprovider to delegate calls off to using Call, Address and
 * Terminal to subprovider maps.  In the case where a sub-provider does not return a full set of Address
 * and Terminal information (throws ResourceUnavailableException for getAddresses() or allows dynamic
 * addresses by returning "dynamicAddresses = t" in its capabilities Properties object), we may not have
 * full delegation information.  We have two options:
 * <ol>
 *  <li>Broadcast all requests where the receiver isn't known
 *  <li>Catch all address and terminal mappings in Listener events.
 * </ol>
 * Currently we do both, with no tuning to try to determine if the extra cost of Listener interception
 * is worth it.  The three methods that provide broadcast are:
 * <ul>
 *  <li>reportCallsOnAddress(String address, boolean flag)
 *  <li>reportCallsOnTerminal(String terminal, boolean flag)
 *  <li>reserveCallId(String address)
 * </ul>
 * 
 * Creation date: (2000-02-22 10:53:56)
 * @author: Richard Deadman
 */
public class MuxProvider implements TelephonyProvider {
	private final static String RESOURCE_NAME = "Mux.props";
	private final static String PROVIDER_PREFIX = "Provider";
	private final static String CLASS_PREFIX = "Class_";
	private final static String PROPS_PREFIX = "Props_";

	private final static int UNFLUSHED = 0;
	private final static int FLUSHED = 1;
	private final static int TOOBIG = 2;
	
	/**
	 * Address to Subprovider map
	 */
	private Map<String, TelephonyProvider> addToSub = new HashMap<>();
	private int addrFlag = UNFLUSHED;
	private int termFlag = UNFLUSHED;
	private HashSet<TermData> termData = new HashSet<>();
	/**
	 * Mostly maps sub-providers to RawCapabilities but sometimes to Properties
	 * FIXME: This seems to be a bug, actually
	 */
	private Map<TelephonyProvider, Object> subToCaps = new HashMap<>();
	/**
	 * Set of MuxCallIds that hold 1 or more CallHolders.  Each MuxCallId is a logical
	 * call possibly bridged across multiple low-level sub-providers.
	 **/
	private Set<MuxCallId> calls = new HashSet<>();
	private Map<CallHolder, MuxCallId> lowToLogicalMap = new HashMap<>();
	private Map<String, TelephonyProvider> termToSub = new HashMap<>();
	
	/**
	 * Add a MuxCallId and its backwards lookup holder to my call set.
	 * 
	 * @param ch CallHolder
	 * @param mci MuxCallId
	 */
	private void addCall(CallHolder ch, MuxCallId mci) {
		calls.add(mci);
		lowToLogicalMap.put(ch, mci);
	}
	
	/**
	 * Forward to remote provider
	 */
	@Override
	public void addListener(TelephonyListener rl) {
		for (TelephonyProvider rp : getSubProviders()) {
			rp.addListener(new MuxListener(rl, this, rp));
		}
	}
	
	/**
	  * Allocate a media type resource to a sub-provider's terminal.
	  * 
	  * @param terminal The terminal to be attached to media resources
	  * @param type A flag telling the type of media to attach.  See RawProvider for static types.
	  * @param params Control parameters for the resource.
	  * @return true if the media was allocated.
	  */
	@Override
	public boolean allocateMedia(String terminal, int type, Dictionary params) {
		TelephonyProvider rp = getTerminalSub(terminal);
		if (((RawCapabilities) subToCaps.get(rp)).allocateMedia) {
			return rp.allocateMedia(terminal, type, params);
		}
		return true; // The rawProvider guaranteed that it does not need this call.
	}
	
	/**
	 * Forward answerCall to remote provider
	 */
	@Override
	public void answerCall(CallId call, String address, String terminal)
			throws PrivilegeViolationException, ResourceUnavailableException,
			MethodNotSupportedException, RawStateException {
		CallHolder ch = ((MuxCallId)call).getLeg(address);
		if (ch != null) {
			ch.getTpi().answerCall(ch.getCall(), address, terminal);
		}
	}
	
	@Override
	public boolean attachMedia(net.sourceforge.gjtapi.CallId call, java.lang.String address, boolean onFlag) {
		CallHolder ch = ((MuxCallId)call).getLeg(address);
		if (ch != null) {
			return ch.getTpi().attachMedia(ch.getCall(), address, onFlag);
		}
		return false;
	}
	
	@Override
	public void beep(net.sourceforge.gjtapi.CallId call) {
		for (Iterator<?> it = ((MuxCallId)call).getCallHolders(); it.hasNext();) {
			CallHolder ch = (CallHolder)it.next();
			ch.getTpi().beep(ch.getCall());
		}
	}
	
	/**
	 * Create a call from the given address and terminal to the remote address
	 */
	@Override
	public CallId createCall(CallId id, String address, String term, String dest)
			throws ResourceUnavailableException, PrivilegeViolationException, InvalidPartyException,
			InvalidArgumentException, RawStateException, MethodNotSupportedException {
		MuxCallId logicalCall = (MuxCallId)id;
		CallHolder ch = logicalCall.getLeg(address);
		if (ch == null) {
			TelephonyProvider sub = this.getAddressSub(address);
			if (sub == null) {
				throw new InvalidPartyException(InvalidPartyException.ORIGINATING_PARTY);
			}
			ch = new CallHolder(sub.reserveCallId(address), sub);
			logicalCall.addCall(ch);
		}
			
		return ch.getTpi().createCall(ch.getCall(), address, term, dest);
	}
	
	/**
	 * find a logical CallId for the given low-level call.
	 * 
	 * @param subCall Low-level CallId
	 * @param subTpi Low-level TelephonyProvider that holds the call, or null.
	 */
	MuxCallId findCall(CallId subCall, TelephonyProvider subTpi) {
		// Check for the logical CallId in the backward lookup table
		return this.findCall(new CallHolder(subCall, subTpi));
	}
	
	/**
	 * find a logical CallId for the given low-level call.
	 * 
	 * @param ch The CallHolder that represents the sub-call
	 */
	MuxCallId findCall(CallHolder ch) {
		// Check for the logical CallId in the backward lookup table
		return lowToLogicalMap.get(ch);
	}
	
	/**
	 * Free a media type resource from a sub-provider's terminal.
	 * 
	 * @param terminal The terminal to be freed from media resources
	 * @param type A flag telling the type of media to free.  See RawProvider for static types.
	 * @return true if the media was freed.
	 */
	@Override
	public boolean freeMedia(String terminal, int type) {
		TelephonyProvider rp = this.getTerminalSub(terminal);
		if (((RawCapabilities) subToCaps.get(rp)).allocateMedia) {
			return rp.freeMedia(terminal, type);
		}
		return true;	// The rawProvider guaranteed that it does not need this call.
	}
	
	/**
	 * Take the values pointed to by the existing and new and return the greatest common denominator in String
	 * format.
	 * 
	 * @param existing An existing value, a String of form "txxx" or "fxxx".
	 * @param update A new String representation of a boolean value.
	 */
	private static Object gcd(Object existing, Object update) {
		if ((existing instanceof String && ((String)existing).length() > 0
				&& Character.toLowerCase(((String)existing).charAt(0)) == 't')
				|| (existing instanceof Boolean && ((Boolean)existing).booleanValue())) {
			// can stop now - already true
			return existing;
		}
		
		// test if existing false and update exists -- replace with update
		if ((update instanceof String && ((String)update).length() > 0) || (update instanceof Boolean)) {
			return update;
		}
		
		// existing was false and update not valid
		return existing;
	}
	
	/**
	 * Return the static set of addresses I manage, using lazy instantiation.
	 * 
	 * @return The set of know addresses I manage
	 */
	@Override
	public String[] getAddresses() throws ResourceUnavailableException {
		Map<String, TelephonyProvider> addMap = addToSub;
	
			// test if we need to flush the addresses in
		if (addrFlag == MuxProvider.UNFLUSHED) {
			synchronized (this) {
				if (addrFlag == MuxProvider.UNFLUSHED) {	// double check
						// ask each sub-provider
					for (TelephonyProvider sub : getSubProviders()) {
						String[] subAddrs = {};
						try {
							subAddrs = sub.getAddresses();
						} catch (ResourceUnavailableException rue) {
							this.addrFlag = MuxProvider.TOOBIG;	// mark as not all available
							break;
						}
						int size = subAddrs.length;
						for (int i = 0; i < size; i++) {
							addMap.put(subAddrs[i], sub);
						}
					}
					if (addrFlag == MuxProvider.UNFLUSHED)
						addrFlag = FLUSHED;
				}
			}
		}
		// now test for too big
		if (addrFlag == MuxProvider.TOOBIG) {
			throw new ResourceUnavailableException(ResourceUnavailableException.UNKNOWN,
				"Some Sub-TelephonyProviders cannot return all addresses");
		}
	
			// must now be set to FLUSHED
		return addMap.keySet().toArray(new String[0]);
	}
	
	/**
	 * Return from the remote provider a set of address names associated with a terminal.
	 */
	@Override
	public String[] getAddresses(String terminal) throws InvalidArgumentException {
		TelephonyProvider sub = this.getTerminalSub(terminal);
		String[] addrs = null;
		if (sub != null) {
			addrs = sub.getAddresses(terminal);
		} else {	// broadcast
			boolean found = false;
			for (Iterator<TelephonyProvider> it = getSubProviders().iterator(); it.hasNext() && !found;) {
				try {
					sub = it.next();
					addrs = sub.getAddresses(terminal);
						// didn't throw exception -- success
					found = true;
					termToSub.put(terminal, sub);
				} catch (InvalidArgumentException iae) {
					// eat and move on to next provider
				}
			}
			if (!found)
				throw new InvalidArgumentException("No muxed subproviders know this Terminal: " + terminal);
		}
	
		// now map these to the sub-provider
		if (addrs != null) {
			Map<String, TelephonyProvider> addrMap = addToSub;
			int size = addrs.length;
			for (int i = 0; i < size; i++)
				addrMap.put(addrs[i], sub);
		}
	
		// now return the set
		return addrs;
	}
	
	/**
	 * Get the sub-provider mapped to by a certain Address
	 * 
	 * @param address A address name to find a subprovider for
	 * @return The subprovider that handles the address
	 */
	private TelephonyProvider getAddressSub(String address) {
		return addToSub.get(address);
	}
	
	@Override
	public int getAddressType(java.lang.String name) {
		TelephonyProvider sub = this.getAddressSub(name);
		return sub.getAddressType(name);
	}
	
	/**
	 * Find the call snapshot for a given call
	 */
	@Override
	public CallData getCall(CallId id) {
		Map<String, ConnectionData> connections = new HashMap<>();// map of address to best ConnectionData hold -- local if found
		int state = Call.IDLE; // best known state.
	
		// now collect all connections and not most active call
		for (Iterator<?> it = ((MuxCallId)id).getCallHolders(); it.hasNext();) {
			CallHolder ch = (CallHolder) it.next();
			TelephonyProvider sub = ch.getTpi();
			CallData call = sub.getCall(ch.getCall());
	
			// test if any branches are ACTIVE
			if (call.callState == Call.ACTIVE)
				state = call.callState;
	
			// now map these to the sub-provider
			if ((call != null) && (call.connections != null)) {
				// record all Addresses and Terminals
				int connSize = call.connections.length;
				for (int i = 0; i < connSize; i++) {
					ConnectionData cd = call.connections[i];
					if (this.mapConnection(cd, sub)) {
						// note the local connections, overriding any remote entry
						// remote connections will be reported by other sub-providers
						connections.put(cd.address, cd);
					} else {
						// only add to connections if no local yet found
						if (!connections.containsKey(cd.address))
							connections.put(cd.address, cd);
					}
				}
			}
		}
	
		// Merge the set of morphed sub-calls
		CallData mergedCall = null;
		int size = connections.size();
		if (size > 0) {
			mergedCall = new CallData(id, state, connections.values().toArray(new ConnectionData[0]));
		}
		
		// now return the set
		return mergedCall;
	}
	
	/**
	 * Get a set of CallData snapshots for all calls that are attached to an address.
	 * This will follow logical links to call legs in other sub-providers.
	 */
	@Override
	public CallData[] getCallsOnAddress(String number) {
		TelephonyProvider sub = this.getAddressSub(number);
		if (sub != null) {
			CallData[] cd = sub.getCallsOnAddress(number);
		
			// now map these calls to the sub-provider
			if (cd != null) {
				int cdSize = cd.length;
				for (int i = 0; i < cdSize; i++) {	// for each found call
					CallData call = cd[i];
						// lazily create the logical call
					MuxCallId callId = this.noteCall(call.id, sub);
						// trace the call to other providers and store back in our array
					cd[i] = merge(call, this.traceCalls(callId, call.connections, new HashSet<>()));
						// record the sub-parts
					this.mapCall(callId, call, sub);
				}
					// return the traced calls
				return cd;
			}
		}
	
		// We found no calls
		return null;
	}
	
	/**
	 * Get a set of CallData snapshots for all calls that are attached to a Terminal.
	 * This will follow logical links to call legs in other sub-providers.
	 */
	@Override
	public CallData[] getCallsOnTerminal(String name) {
		TelephonyProvider sub = getTerminalSub(name);
		if (sub != null) {
			CallData[] cd = sub.getCallsOnTerminal(name);
		
			// now map these calls to the sub-provider
			if (cd != null) {
				int cdSize = cd.length;
				for (int i = 0; i < cdSize; i++) {	// for each found call
					CallData call = cd[i];
						// lazily create the logical call
					MuxCallId callId = noteCall(call.id, sub);
						// trace the call to other providers and store back in our array
					cd[i] = merge(call, traceCalls(callId, call.connections, new HashSet<>()));
						// record the sub-parts
					this.mapCall(callId, call, sub);
				}
					// return the traced calls
				return cd;
			}
		}
	
		// We found no calls
		return null;
	}
	
	/**
	 * Create the lowest common denominator of capabilities and return it.
	 */
	@Override
	public Properties getCapabilities() {
		Properties merged = new Properties();
		
		// load the set of capabilities
		for (Iterator<?> subs = subToCaps.values().iterator(); subs.hasNext();) {
			Properties subProps = ((Properties)subs.next());
	
			if (subProps != null) {
				// test each capability
				for (Iterator<?> keys = subProps.keySet().iterator(); keys.hasNext();) {
					Object key = keys.next();
					Object val = null;
					if (key.equals(Capabilities.THROTTLE) || key.equals(Capabilities.MEDIA) ||
						key.equals(Capabilities.ALLOCATE_MEDIA) || key.equals(Capabilities.DYNAMIC_ADDRESSES) ||
						((key instanceof String) && (((String)key).endsWith(Capabilities.PD))))
						val = gcd(merged.get(key), subProps.get(key));
					else
						val = lcd(merged.get(key), subProps.get(key));
					merged.put(key, val);
				}
			}
		}
		return merged;
	}
	
	@Override
	public String getDialledDigits(CallId id, String address) {
		CallHolder ch = ((MuxCallId)id).getLeg(address);
		if (ch != null) {
			return ch.getTpi().getDialledDigits(ch.getCall(), address);
		}
		return null;
	}
	
	/**
	 * Find the sub-provider to forward the getPrivateData call to, and forward it.
	 */
	@Override
	public Object getPrivateData(CallId call, String address, String terminal) {
		TelephonyProvider tp = null;	// if single sub-provider identified
		CallId newCallId = null;
			// check if call ids the provider
		if (call != null) {
			if  (address != null) {		// Connection of TerminalConnection
				CallHolder ch = getSub(call, address);
				tp = ch.getTpi();
				newCallId = ch.getCall();
			} else {					// Broadcast to all multiplexed Calls
				Set<Object> set = new HashSet<>();
				for (Iterator<?> it = ((MuxCallId)call).getCallHolders(); it.hasNext();) {
					CallHolder ch = (CallHolder)it.next();
					Object o = ch.getTpi().getPrivateData(ch.getCall(), address, terminal);
					if (o != null)
						set.add(o);
				}
				return set.toArray();
			}
		} else if (address != null) {	// Call is null - just Address defined
			// check if the address will now do it
			tp = this.getAddressSub(address);
		} else if (terminal != null) {	// Call is null - just Terminal defined
			// check if only the terminal will id it.
			tp = this.getTerminalSub(terminal);
		} else {						// all null - Provider broadcast
			// all providers are required
			Set<Object> set = new HashSet<>();
			for (TelephonyProvider provider : getSubProviders()) {
				Object o = provider.getPrivateData(call, address, terminal);
				if (o != null) {
					set.add(o);
				}
			}
			return set.toArray();
		}
	
			// one provider found
		if (tp != null) {
			return tp.getPrivateData(newCallId, address, terminal);
		}
	
			// no providers found
		return null;
	}
	
	/**
	 * Get the sub-provider for a logical call that is handling a particular address.
	 * This allows a call being bridged across multiple sub-providers to find the correct sub-provider.
	 * 
	 * @return The subprovider that handles the call's address
	 * @param id A call id to find a subprovider for
	 * @paran address An address that identifies a call-leg handled by one of the sub-providers.
	 */
	private static CallHolder getSub(CallId id, String address) {
		return ((MuxCallId)id).getLeg(address);
	}
	
	/**
	 * Internal accessor.
	 * 
	 * @return The set of RawProviders I delegate to.
	 */
	private Set<TelephonyProvider> getSubProviders() {
		return subToCaps.keySet();
	}
	
	/**
	 * Return the static set of terminals I manage, using lazy instantiation.
	 * 
	 * @return The set of know addresses I manage
	 */
	@Override
	public TermData[] getTerminals() throws ResourceUnavailableException {
		Map<String, TelephonyProvider> termMap = termToSub;
		HashSet<TermData> tdSet = termData;
	
			// test if we need to flush the addresses in
		if (termFlag == MuxProvider.UNFLUSHED) {
			synchronized (tdSet) {
				if (termFlag == MuxProvider.UNFLUSHED) {	// double check
						// ask each sub-provider
					for (TelephonyProvider sub : getSubProviders()) {
						TermData[] subTerms = {};
						try {
							subTerms = sub.getTerminals();
						} catch (ResourceUnavailableException rue) {
							termFlag = MuxProvider.TOOBIG;	// mark as not all available
							tdSet.clear();			// might as well clear the TermData holder
							termData = null;
							break;
						}
						int size = subTerms.length;
						for (int i = 0; i < size; i++) {
							TermData td = subTerms[i];
							tdSet.add(td);
							termMap.put(td.getTerminal(), sub);
						}
					}
					if (termFlag == MuxProvider.UNFLUSHED)
						termFlag = FLUSHED;
				}
			}
		}
		// now test for too big
		if (termFlag == MuxProvider.TOOBIG) {
			throw new ResourceUnavailableException(ResourceUnavailableException.UNKNOWN,
				"Some Sub-TelephonyProviders cannot return all addresses");
		}
	
			// must now be set to FLUSHED
		return tdSet.toArray(new TermData[0]);
	}
	
	/**
	 * Return from the remote provider a set of terminal names for an address.
	 */
	@Override
	public TermData[] getTerminals(String address) throws InvalidArgumentException {
		TelephonyProvider sub = this.getAddressSub(address);
		TermData[] terms = null;
		if (sub != null) {
			terms = sub.getTerminals(address);
		} else {	// broadcast
			boolean found = false;
			for (Iterator<TelephonyProvider> it = getSubProviders().iterator(); it.hasNext() && !found;) {
				try {
					sub = it.next();
					terms = sub.getTerminals(address);
						// didn't throw exception -- success
					found = true;
					addToSub.put(address, sub);
				} catch (InvalidArgumentException iae) {
					// eat and move on to next provider
				}
			}
			if (!found)
				throw new InvalidArgumentException("No muxed subproviders know this Address: " + address);
		}
	
		// now map these to the sub-provider
		if (terms != null) {
			Map<String, TelephonyProvider> termMap = termToSub;
			int size = terms.length;
			for (int i = 0; i < size; i++)
				termMap.put(terms[i].getTerminal(), sub);
		}
	
		// now return the set
		return terms;
	}
	
	/**
	 * Get the sub-provider mapped to by a certain Terminal
	 * 
	 * @return The subprovider that handles the terminal
	 * @param address A terminal name to find a subprovider for
	 */
	private TelephonyProvider getTerminalSub(String terminal) {
		return termToSub.get(terminal);
	}
	
	/**
	 * Send a hold message for a terminal to a remote provider.
	 */
	@Override
	public void hold(CallId call, String address, String term) throws RawStateException,
			MethodNotSupportedException, PrivilegeViolationException, ResourceUnavailableException {
		TelephonyProvider sub = this.getAddressSub(address);
		if (sub != null) {
			sub.hold(call, address, term);
		}
	}
	
	/**
	 * Load my properties and use to look-up my set of sub-providers
	 * These properties could be used locally or sent to the server for the creation of a user-session.
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void initialize(Map props) throws ProviderUnavailableException {
		Map<String, Object> m = null;
		Object value = null;
		
		// See if I also need to load the properties file
		boolean replace = false;
		if (props != null) {
			value = props.get("replace");
			replace = Capabilities.resolve(value);
		}
		if (replace) {
			m = props;
		}
		else {
			Map properties = loadResources(MuxProvider.RESOURCE_NAME);
			m = properties;
			if (props != null) {
				m.putAll(props);
			}
		}
	
		// Now load each sub-provider
		Map<TelephonyProvider, Object> subCaps = subToCaps;
		for (String key : m.keySet()) {
			if (key.startsWith(MuxProvider.PROVIDER_PREFIX)) {
				String em = (String) m.get(key);
				String cn = (String) m.get(MuxProvider.CLASS_PREFIX+em);
				String propfile = (String) m.get(MuxProvider.PROPS_PREFIX+em);
	
				// load the sub-provider
				TelephonyProvider rp = null;
				try {
					rp = ProviderFactory.createProvider((CoreTpi)Class.forName(cn).newInstance());
				} catch (Exception ex) {
					throw new ProviderUnavailableException();
				}
				Map properties = loadResources(propfile);
				rp.initialize(properties);
	
				// add the provider's RawCapabilities
				subCaps.put(rp, rp.getCapabilities());
			}
		}
	}
	
	/**
	 * Delegate the call on to the appropriate sub-provider, if it registered for these calls.
	 */
	@Override
	public boolean isMediaTerminal(String terminal) {
		TelephonyProvider rp = getTerminalSub(terminal);
		if (((RawCapabilities) subToCaps.get(rp)).allMediaTerminals) {
			return true;	// The rawProvider guaranteed that all terminals are media terminals.
		}
		return rp.isMediaTerminal(terminal);
	}
	
	/**
	 * Tell the remote provider to join two calls
	 */
	@Override
	public CallId join(CallId call1, CallId call2, String address, String terminal)
			throws RawStateException, InvalidArgumentException, MethodNotSupportedException,
			PrivilegeViolationException, ResourceUnavailableException {
		MuxCallId mCall1 = (MuxCallId)call1;
		MuxCallId mCall2 = (MuxCallId)call2;
	
			// find the common callholders
		CallHolder ch1 = mCall1.getLeg(address);
		CallHolder ch2 = mCall2.getLeg(address);
	
			// check that we have the same sub-provider
		TelephonyProvider sub = ch1.getTpi();
		if (!sub.equals(ch2.getTpi())) {
			throw new InvalidArgumentException("Mux Join: No common TerminalConnection found");
		}
	
			// tell the sub-provider to join the parts
		sub.join(ch1.getCall(), ch2.getCall(), address, terminal);
	
			// update the address to CallHolder set in call1
		String[] adds = mCall2.getAddsForSubCall(ch2);
		int addSize = adds.length;
		for (int i = 0; i < addSize; i++) {
			mCall1.addLeg(adds[i], ch1);
		}
	
		// move the other calls from call23 to call1
		for (Iterator<?> it = mCall2.getCallHolders(); it.hasNext();) {
			CallHolder ch = (CallHolder)it.next();
			if (!ch.equals(ch2)) {
				// add to mCall1
				mCall1.addCall(ch);
				// update the address->CallHolder table in call 1
				adds = mCall2.getAddsForSubCall(ch);
				addSize = adds.length;
				for (int i = 0; i < addSize; i++) {
					mCall1.addLeg(adds[i], ch);
				}
	
				// update the provider
				lowToLogicalMap.put(ch, mCall1);
			}
		}
		
			// clear the old mux call
		mCall2.free();
	
			// return the call
		return call1;
	}
	
	/**
	 * Take the values pointed to by the existing and new and return the lowest common denominator in String
	 * format.
	 * 
	 * @param existing An existing value, a String of form "txxx" or "fxxx".
	 * @param update A new String representation of a boolean value.
	 */
	private static Object lcd(Object existing, Object update) {
		if ((existing instanceof String && ((String)existing).length() > 0 &&
				Character.toLowerCase(((String)existing).charAt(0)) != 't') ||
		    (existing instanceof Boolean && !((Boolean)existing).booleanValue()))
				// can stop now - already false
				return existing;
	
			// test if existing true and update exists -- replace with update
		if ((update instanceof String && ((String)update).length() > 0) || (update instanceof Boolean)) {
			return update;
		}
	
		// existing was true and update not valid
		return existing;
	}
	
	/**
	 * This method loads the Provider's values from a property file.
	 */
	private Properties loadResources(String resName) {
		// We must be able to load the properties file
		Properties props = new Properties();
		try (InputStream is = getClass().getResourceAsStream("/" + resName)) {
			props.load(is);
		}
		catch (IOException ioe) {
			// return empty Properties
		}
	
		return props;
	}
	
	/**
	 * Ensure that I have a logical CallId already for the given low-level call.
	 * This will look for the call as an existing call, or if it doesn't exist:
	 * <ol>
	 * <li>Check if any other calls connect to this call
	 * <li>Create a new logical call and trace it through any sub-providers for un-reported legs.
	 * </ol>
	 * 
	 * @param subCall Low-level CallId
	 * @param subTpi Low-level TelephonyProvider that holds the call.
	 */
	MuxCallId locateCall(CallId subCall, TelephonyProvider subTpi) {
		// Check for the logical CallId in the backward lookup table
		MuxCallId mci = this.findCall(subCall, subTpi);
		// test if the Logical CallId does not yet exist
		if (mci == null) {
			// first check if any other existing calls can be traced to this call
			CallData cd = subTpi.getCall(subCall);
			String[] remoteAddresses = cd.getRemoteAddresses();
			int raSize = remoteAddresses.length;
			if (raSize > 0) {
				for (int i = 0; (i < raSize) && (mci == null); i++) {
					for (MuxCallId testCall : calls) {
						if (testCall.contains(new CallHolder(subCall, subTpi))) {
							mci = testCall;
							break;
						}
					}
				}
			}
			if (mci == null) {
				// otherwise create the call
				mci = this.noteCall(subCall, subTpi);
				// now trace this call through to link up other legs
				this.traceCalls(mci, cd.connections, new HashSet<>());
			}
		}
		return mci;
	}
	
	/**
	 * Ensure that a physical sub-call is properly recorded.
	 */
	private void mapCall(MuxCallId call, CallData callData, TelephonyProvider sub) {
		// Create and add a new CallHolder if needed
		CallHolder tmpHolder = new CallHolder(callData.id, sub);
		if (!call.contains(tmpHolder)) {
			call.addCall(tmpHolder);
		}
	
		ConnectionData[] connData = callData.connections;
		// update the address and terminal mapping
		for (int i = 0, cdSize = connData.length; i < cdSize; i++) {
			this.mapConnection(connData[i], sub);
		}
	}
	
	/**
	 * Given a Connection, ensure it is mapped into a sub-provider
	 * 
	 * @return true if the connection is in the sub-provider's address space
	 */
	private boolean mapConnection(ConnectionData cd, TelephonyProvider sub) {
		// now map these to the sub-provider
		if (cd.isLocal) {
			Map<String, TelephonyProvider> termMap = termToSub;
			// record all Addresses and Terminals
			addToSub.put(cd.address, sub);
			// and the Connection's Terminals
			int tcSize = cd.terminalConnections.length;
			for (int j = 0; j < tcSize; j++) {
				TCData tcd = cd.terminalConnections[j];
				termMap.put(tcd.terminal.getTerminal(), sub);
			}
		}
		return cd.isLocal;
	}
	
	/**
	 * Merge two CallData collections into one that represents the merged call.
	 * 
	 * @param cd1 The first call
	 * @param cd2 The bridged call
	 * @return The merged CallData representing the virtual call
	 */
	private static CallData merge(CallData cd1, CallData cd2) {
		// null value test
		if (cd2 == null) {
			return cd1;
		}
		if (cd1 == null) {
			return cd2;
		}
		
		Set<ConnectionData> conns = new HashSet<>(); // the set of connections
		Set<String> localAddresses = new HashSet<>();		// set of local addresses so we can remove remote later
		int i;
	
		// Create a set of Connections
		ConnectionData[] cdSet = cd1.connections;
		ConnectionData cd;
		for (i = 0; i < cdSet.length; i++) {
			cd = cdSet[i];
			conns.add(cd);
			if (cd.isLocal) {
				localAddresses.add(cd.address);
			}
		}
		cdSet = cd2.connections;
		for (i = 0; i < cdSet.length; i++) {
			cd = cdSet[i];
			conns.add(cd);
			if (cd.isLocal) {
				localAddresses.add(cd.address);
			}
		}
	
		// prune remote connections that have a local twin
		for (Iterator<ConnectionData> it = conns.iterator(); it.hasNext();) {
			cd = it.next();
			if (!cd.isLocal && (localAddresses.contains(cd.address))) {
				it.remove();
			}
		}
	
		// Now return the merged set of CallData
		return new CallData(cd1.id, cd1.callState, conns.toArray(new ConnectionData[0]));
	}
	
	/**
	 * Map an Address name from a sub-provider.
	 * 
	 * @param id A name of a sub-provider's Address
	 * @param sub The sub-provider that handles this address
	 */
	void noteAddress(String addressName, TelephonyProvider sub) {
		addToSub.put(addressName, sub);
	}
	
	/**
	 * Ensure that I have a logical CallId already for the given low-level call.
	 * This doesn't follow links since that may or may not require the return of CallData information.
	 * 
	 * @param subCall Low-level CallId
	 * @param subTpi Low-level TelephonyProvider that holds the call.
	 */
	MuxCallId noteCall(CallId subCall, TelephonyProvider subTpi) {
		// Check for the logical CallId in the backward lookup table
		MuxCallId mci = this.findCall(subCall, subTpi);
		// test if the Logical CallId does not yet exist
		if (mci == null) {
			CallHolder ch = new CallHolder(subCall, subTpi);
			mci = new MuxCallId();
			mci.addCall(ch);
			this.addCall(ch, mci);
		}
		return mci;
	}
	
	/**
	 * Map a Terminal name from a sub-provider.
	 * 
	 * @param id A name of a sub-provider's Terminal
	 * @param sub The sub-provider that handles the terminal
	 */
	void noteTerminal(String terminalName, TelephonyProvider sub) {
		termToSub.put(terminalName, sub);
	}
	
	/**
	 * Delegate the call onto the appropriate sub-provider
	 */
	@Override
	public void play(String terminal, String[] streamIds, int offset, RTC[] rtcs, Dictionary optArgs) throws MediaResourceException {
		this.getTerminalSub(terminal).play(terminal, streamIds, offset, rtcs, optArgs);
	}
	
	/**
	 * Delegate off to the appropriate sub-provider
	 */
	@Override
	public void record(String terminal, String streamId, RTC[] rtcs, Dictionary optArgs) throws MediaResourceException {
		this.getTerminalSub(terminal).record(terminal, streamId, rtcs, optArgs);
	}
	
	/**
	 * Create a logical CallId to handle any sub-provider muxing and register it with
	 * the local lookup table.
	 * 
	 * @param id CallId
	 * @param sub TelephonyProvider
	 * @return MuxCallId
	 */
	private MuxCallId registerCall(CallId id, TelephonyProvider sub) {
		MuxCallId cid = new MuxCallId();
		cid.addCall(id, sub);
		calls.add(cid);
		return cid;
	}
	
	/**
	 * Tell the remote provider to release an address from a call.
	 */
	@Override
	public void release(String address, CallId call) throws PrivilegeViolationException,
		ResourceUnavailableException, MethodNotSupportedException, RawStateException {
		CallHolder ch = getSub(call, address);
		if (ch != null) {
			ch.getTpi().release(address, ch.getCall());
		}
	}
	
	/**
	 * Release any CallId's that I have reserved.
	 */
	@Override
	public void releaseCallId(CallId id) {
		MuxCallId mcid = (MuxCallId)id;		// This better cast...
		for (Iterator<?> chs = mcid.getCallHolders(); chs.hasNext();) {
			CallHolder call = (CallHolder)chs.next();
			call.getTpi().releaseCallId(call.getCall());
		}
		this.removeCall(mcid);
	}
	
	/**
	 * Remove a MuxCallId and its backwards lookup holder to my call set.
	 * 
	 * @param mci MuxCallId
	 */
	boolean removeCall(MuxCallId mci) {
		boolean removed = calls.remove(mci);
		if (removed) {	// logical call existed...
			for (Iterator<Entry<CallHolder, MuxCallId>> it = lowToLogicalMap.entrySet().iterator(); it.hasNext();) {
				Entry<CallHolder, MuxCallId> entry = it.next();
				if (entry.getValue().equals(mci)) {
					it.remove();
				}
			}
		}
		return removed;
	}
	
	/**
	 * Forward removeListener to remote provider.
	 */
	@Override
	public void removeListener(TelephonyListener rl) {
		for (TelephonyProvider rp : getSubProviders()) {
			rp.removeListener(new MuxListener(rl, this, rp));
		}
	}
	
	/**
	 * Delegate off to the appropriate subprovider
	 */
	@Override
	public void reportCallsOnAddress(String address, boolean flag) throws ResourceUnavailableException, InvalidArgumentException {
		TelephonyProvider rp = getAddressSub(address);
		if (rp != null) {
			if (((RawCapabilities) subToCaps.get(rp)).throttle) {
				rp.reportCallsOnAddress(address, flag);
			}
			return;
		}
		// poll each raw provider - should never do this since all addresses should have been recorded.
		for (TelephonyProvider provider : getSubProviders()) {
			try {
				provider.reportCallsOnAddress(address, flag);
				// no exception - store provider
				addToSub.put(address, provider);
			} catch (InvalidArgumentException iae) {
				// eat this one and try next
			}
		}
		// didn't find any to handle the address
		throw new InvalidArgumentException("No muxed providers handle address: " + address);
	}
	
	/**
	 * Delegate off to the appropriate subprovider
	 */
	@Override
	public void reportCallsOnTerminal(String terminal, boolean flag) throws ResourceUnavailableException, InvalidArgumentException {
		TelephonyProvider rp = this.getTerminalSub(terminal);
		if (rp != null) {
			if (((RawCapabilities) subToCaps.get(rp)).throttle) {
				rp.reportCallsOnTerminal(terminal, flag);
			}
			return;
		}
		// poll each raw provider  - should never do this since all terminals should have been recorded.
		for (TelephonyProvider provider : getSubProviders()) {
			try {
				provider.reportCallsOnAddress(terminal, flag);
				// no exception - store rp
				termToSub.put(terminal, provider);
			} catch (InvalidArgumentException iae) {
				// eat this one and try next
			}
		}
		// didn't find any to handle the address
		throw new InvalidArgumentException("No muxed providers handle terminal: " + terminal);
	}
	
	/**
	 * Reserve a call id on the remote server.
	 */
	@Override
	public CallId reserveCallId(String address) throws InvalidArgumentException {
		TelephonyProvider rp = this.getAddressSub(address);
		CallId id = null;
	
		if (rp != null) {
			id = rp.reserveCallId(address);
		} else {	// broadcast - we shouldn't see an Address that we haven't already recorded
			boolean found = false;
			for (Iterator<TelephonyProvider> it = getSubProviders().iterator(); it.hasNext() && !found;) {
				try {
					rp = it.next();
					id = rp.reserveCallId(address);
					found = true;
				} catch (InvalidArgumentException iae) {
					// eat and move on to next provider
				}
			}
			if (!found)
				throw new InvalidArgumentException("No muxed subproviders know this Address: " + address);
		}
	
		if (id != null) {
			// map the call id in our local table
			return this.registerCall(id, rp);
		}
		return null;
	}
	
	/**
	 * Delegate off to the appropriate subprovider
	 */
	@Override
	public RawSigDetectEvent retrieveSignals(String terminal, int num, Symbol[] patterns, RTC[] rtcs, Dictionary optArgs) throws MediaResourceException {
		return this.getTerminalSub(terminal).retrieveSignals(terminal, num, patterns, rtcs, optArgs);
	}
	
	/**
	 * Find the sub-provider to forward the sendPrivateData call to, and forward it.
	 */
	@Override
	public Object sendPrivateData(CallId call, String address, String terminal, Object data) {
		TelephonyProvider tp = null;	// used for unicast messages
		CallId newCallId = null;
			// check if call ids the provider
		if (call != null) {
			if  (address != null) {		// Connection of TerminalConnection
				CallHolder ch = getSub(call, address);
				tp = ch.getTpi();
				newCallId = ch.getCall();
			} else {					// Broadcast to all multiplexed Calls
				Set<Object> set = new HashSet<>();
				for (Iterator<?> it = ((MuxCallId)call).getCallHolders(); it.hasNext();) {
					CallHolder ch = (CallHolder)it.next();
					Object o = ch.getTpi().sendPrivateData(ch.getCall(), address, terminal, data);
					if (o != null)
						set.add(o);
				}
				return set.toArray();
			}
		} else if (address != null) {
			// check if the address will now do it
			tp = this.getAddressSub(address);
		} else if (terminal != null) {
			// check if only the terminal will id it.
			tp = this.getTerminalSub(terminal);
		} else {
			// all providers are required
			Set<Object> set = new HashSet<>();
			for (TelephonyProvider provider : getSubProviders()) {
				Object o = provider.sendPrivateData(null, null, null, data);
				if (o != null) {
					set.add(o);
				}
			}
			return set.toArray();
		}
	
			// one provider found
		if (tp != null) {
			return tp.sendPrivateData(newCallId, address, terminal, data);
		}
	
			// no providers found
		return null;
	}
	
	/**
	 * Delegate off to the appropriate subprovider
	 */
	@Override
	public void sendSignals(String terminal, Symbol[] syms, RTC[] rtcs, Dictionary optArgs) throws MediaResourceException {
		this.getTerminalSub(terminal).sendSignals(terminal, syms, rtcs, optArgs);
	}
	
	/**
	 * We assume that the providers are at both ends of the range
	 */
	@Override
	public void setLoadControl(java.lang.String startAddr, java.lang.String endAddr, double duration, double admissionRate, double interval, int[] treatment) throws javax.telephony.MethodNotSupportedException {
		if (startAddr != null) {
			this.getAddressSub(startAddr).setLoadControl(startAddr, endAddr, duration, admissionRate, interval, treatment);
		}
		if ((endAddr != null) && (!startAddr.equals(endAddr))) {
			this.getAddressSub(startAddr).setLoadControl(startAddr, endAddr, duration, admissionRate, interval, treatment);
		}
	}
	
	/**
	 * Find the sub-provider to forward the setPrivateData call to, and forward it.
	 */
	@Override
	public void setPrivateData(CallId call, String address, String terminal, Object data) {
		TelephonyProvider tp = null;	// used for unicast messages
		CallId newCallId = null;
		// check if call ids the provider
		if (call != null) {
			if  (address != null) {		// Connection of TerminalConnection
				CallHolder ch = getSub(call, address);
				tp = ch.getTpi();
				newCallId = ch.getCall();
			} else {					// Broadcast to all multiplexed Calls
				for (Iterator<?> it = ((MuxCallId)call).getCallHolders(); it.hasNext();) {
					CallHolder ch = (CallHolder)it.next();
					ch.getTpi().setPrivateData(ch.getCall(), address, terminal, data);
				}
			}
		} else if (address != null) {
			// check if the address will now do it
			tp = this.getAddressSub(address);
		} else if (terminal != null) {
			// check if only the terminal will id it.
			tp = this.getTerminalSub(terminal);
		} else {
			// all providers are required
			for (TelephonyProvider provider : getSubProviders()) {
				provider.setPrivateData(null, null, null, data);
			}
		}
	
			// one provider found
		if (tp != null) {
			tp.setPrivateData(newCallId, address, terminal, data);
		}
	}
	
	/**
	 * Tell the remote provider to shutdown.  It may choose to ignore me.
	 */
	@Override
	public void shutdown() {
		for (TelephonyProvider provider : getSubProviders()) {
			provider.shutdown();
		}
	}
	
	/**
	 * Pass on the stop media request to the correct sub-provider
	 */
	@Override
	public void stop(String terminal) {
		this.getTerminalSub(terminal).stop(terminal);
	}
	
	/**
	 * Delegate off to the appropriate sub-provider
	 */
	@Override
	public boolean stopReportingCall(CallId call) {
		boolean result = true;		// assume that they all will unless one declines
		for (Iterator<?> it = ((MuxCallId)call).getCallHolders(); it.hasNext();) {
			CallHolder ch = (CallHolder)it.next();
			result = result && ch.getTpi().stopReportingCall(ch.getCall());
		}
		return result;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("Raw Multiplexor Provider managing: ");
		for (TelephonyProvider rp : getSubProviders()) {
			sb.append(" ").append(rp.toString());
		}
		return sb.toString();
	}
	
	/**
	 * This method assists in knitting call legs together that exist in two sub-providers
	 * but that represent parts of one logical call.
	 * 
	 * @param muxCall The mux virtual call that holds all sub-call holders.
	 * @param from The address set the call leg comes from.  Used to help distinguish multiple calls at the destination.
	 * @param to The address where the call leg should be.
	 * @param state the Connection state of the call leg.  Used to help distinguish leg that both come from the same source.
	 * @param addressSet The set of currently found locally-resolved addresses.  Used to stop cyclical recursion.
	 * @return CallData
	 */
	private CallData traceCall(MuxCallId muxCall, String[] from, String to, int state, HashSet addressSet) {
		CallData found = null;
	
		int fromSize = from.length;
		// find the sub-provider and ask it for all calls on the address
		TelephonyProvider prov = this.getAddressSub(to);
		if (prov != null) {
			CallData[] subCalls = prov.getCallsOnAddress(to);
			int size = subCalls.length;
				// look at all sub-calls
			for (int i = 0; i < size; i++) {
				CallData cd = subCalls[i];
				int connSize = cd.connections.length;
				for (int j = 0; j < connSize; j++) {
					ConnectionData conn = cd.connections[j];
					for (int k = 0; k < fromSize; k++) {
						if ((conn.address.equals(from[k])) && (conn.connState == state)) {
							if (found == null) {
								found = cd;
								break;	// don't check any more connections
							}
							// two possible calls -- return null to indicate failure
							return null;
						}
					}
					if (found != null) {
						break;		// break out of connections
					}
				}
			}
	
			// now we need to check if we should shut off reporting for the other returned Calls
			boolean shutOff = false;
			for (int i = 0; (i < size) && (!shutOff); i++) {
				CallData cd = subCalls[i];
				CallId subCallId = cd.id;
				if ((!cd.equals(found)) && (this.findCall(subCallId, prov) == null)) {
					prov.stopReportingCall(subCallId);
					shutOff = true;
				}
			}
				// If we turned any call reporting off, we need to turn call reporting off for
				// future calls on this address
			if (shutOff) {
					// only catch non-Runtime exceptions -- we want others to blow things up
				Exception e = null;
				try {
					prov.reportCallsOnAddress(to, false);
					prov.getCall(found.id);				// to ensure this is still being reported
				} catch (InvalidArgumentException iae) {
					e = iae;
				} catch (ResourceUnavailableException rue) {
					e = rue;
				}
				if (e != null) {
					System.out.println("Internal logic error during multiplexor call tracing:");
					e.printStackTrace();
				}
			}
	
			if (found != null) {
				// Add the new local addresses to the addressSet
				ConnectionData[] connData = found.connections;
				int cdSize = connData.length;
				for (int i = 0; i < cdSize; i++) {
					if (connData[i].isLocal) {
						addressSet.add(connData[i].address);
					}
				}
	
				// Create and add a new CallHolder if needed
				if (muxCall.getLeg(to) == null) {
					muxCall.addCall(found.id, prov);
					// update the address and terminal mapping
					for (int i = 0; i < cdSize; i++) {
						this.mapConnection(connData[i], prov);
					}
				}
	
				// finally, recursively merge in any other bridged connections
				found = merge(found, traceCalls(muxCall, connData, addressSet));
			}
		}
	
		return found;
	}
	
	/**
	 * This method assists in knitting call legs together that exist in two sub-providers by merging all remote connections in the logical mux call.
	 * Only remote addresses not yet recorded in addressSet are followed.
	 * but that represent parts of one logical call.
	 * 
	 * @param muxCall The mux virtual call that holds all sub-call holders.
	 * @param addressSet The set of currently found locally-resolved addresses.  Used to stop cyclical recursion.
	 * @return CallData
	 */
	private CallData traceCalls(MuxCallId muxCall, ConnectionData[] conns, HashSet addressSet) {
		CallData found = null;
	
		int cdSize = conns.length;
		// finally, recursively merge in any other bridged connections
		for (int i = 0; i < cdSize; i++) {
			if (!conns[i].isLocal) {
				ConnectionData conn = conns[i];
				// ensure we don't get in a recursive loop
				if (!addressSet.contains(conn.address))
					found = merge(found, this.traceCall(muxCall, found.getLocalAddresses(),
							conn.address, conn.connState, addressSet));
			}
		}
	
		return found;
	}
	
	/**
	 * Pas on the triggerRTC method to the correct sub-provider.
	 */
	@Override
	public void triggerRTC(String terminal, Symbol action) {
		this.getTerminalSub(terminal).triggerRTC(terminal, action);
	}
	
	/**
	 * Tell the remote provider to unhold a terminal from a call
	 */
	@Override
	public void unHold(CallId call, String address, String term) throws RawStateException, MethodNotSupportedException,
		PrivilegeViolationException, ResourceUnavailableException {
		CallHolder ch = getSub(call, address);
		if (ch != null) {
			ch.getTpi().unHold(ch.getCall(), address, term);
		}
	}
}
