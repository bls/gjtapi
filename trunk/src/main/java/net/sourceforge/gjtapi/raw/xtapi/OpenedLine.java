package net.sourceforge.gjtapi.raw.xtapi;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * @author lak
 */
@Getter
@Setter
@RequiredArgsConstructor
@Builder
class OpenedLine {
	
	private final int lineHandle;
	private final int lineDeviceNo;
	private final int lineDeviceId;
	private final String lineDeviceName;
	private final String addressName;

	@Override
	public boolean equals(Object other) {
		return other instanceof OpenedLine && lineHandle == ((OpenedLine) other).lineHandle;
	}

	@Override
	public int hashCode() {
		return lineHandle;
	}
}