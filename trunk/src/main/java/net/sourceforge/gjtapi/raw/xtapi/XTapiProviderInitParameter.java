package net.sourceforge.gjtapi.raw.xtapi;

import lombok.Getter;

/**
 * @author haf
 */
public enum XTapiProviderInitParameter {
	
	FACTORY("factory"),
	INIT_TIMEOUT("init-timeout"),
	LOG_LEVEL("log-level");
	
	private @Getter final String key;
	
	private XTapiProviderInitParameter(String key) {
		this.key = key;
	}
	
	@Override
	public String toString() {
		return key;
	}
}
