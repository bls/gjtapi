/*
	Copyright (c) 2002 8x8 Inc. (www.8x8.com) 

	All rights reserved. 

	Permission is hereby granted, free of charge, to any person obtaining a 
	copy of this software and associated documentation files (the 
	"Software"), to deal in the Software without restriction, including 
	without limitation the rights to use, copy, modify, merge, publish, 
	distribute, and/or sell copies of the Software, and to permit persons 
	to whom the Software is furnished to do so, provided that the above 
	copyright notice(s) and this permission notice appear in all copies of 
	the Software and that both the above copyright notice(s) and this 
	permission notice appear in supporting documentation. 

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT 
	OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
	HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL 
	INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING 
	FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
	NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION 
	WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. 

	Except as contained in this notice, the name of a copyright holder 
	shall not be used in advertising or otherwise to promote the sale, use 
	or other dealings in this Software without prior written authorization 
	of the copyright holder.
*/

package net.sourceforge.gjtapi.raw.invert;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.telephony.Call;

import net.sourceforge.gjtapi.CallId;
import net.sourceforge.gjtapi.IdGenerator;
import net.sourceforge.gjtapi.SequentialIdGenerator;

/**
 * Map a CallId to a JTAPI Call.
 * Creation date: (2000-06-06 22:57:06)
 * 
 * @author Richard Deadman
 */
public class IdMapper {
	
	private final Map<CallId, Call> idToCallMap = new HashMap<>();
	private final Map<Call, CallId> callToIdMap = new HashMap<>();
	private final IdGenerator idGenerator = new SequentialIdGenerator();
	
	/**
	 * Free a CallId from both maps
	 * Creation date: (2000-02-17 23:51:15)
	 * 
	 * @author Richard Deadman
	 * @param id A proxy raw telephony call id.
	 * @return Call The real call that id was mapped to, or null
	 */
	public synchronized Call freeId(CallId id) {
		Call call = idToCallMap.remove(id);
		if (call != null) {
			callToIdMap.remove(call);
		}
		idGenerator.freeSerializableId(id);
		return call;
	}
	
	/**
	 * Look up a raw Call Id for a JTAPI Call object.
	 */
	public synchronized CallId getId(Call call) {
		Objects.requireNonNull(call);
		
		CallId ci = callToIdMap.get(call);
		if (ci == null) {
			ci = idGenerator.getSerializableId();
			callToIdMap.put(call, ci);
			idToCallMap.put(ci, call);
		}
		return ci;
	}
	
	/**
	 * Look up a JTAPI Call Id for a raw call id.
	 * Return null if no entry found
	 */
	public synchronized Call jtapiCall(CallId id) {
		return idToCallMap.get(id);
	}
	
	@Override
	public String toString() {
		return "CallId mapper with " + callToIdMap.size() + " entries.";
	}
}
