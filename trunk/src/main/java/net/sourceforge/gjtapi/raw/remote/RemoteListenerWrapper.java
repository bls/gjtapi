package net.sourceforge.gjtapi.raw.remote;

/*
	Copyright (c) 2002 8x8 Inc. (www.8x8.com) 

	All rights reserved. 

	Permission is hereby granted, free of charge, to any person obtaining a 
	copy of this software and associated documentation files (the 
	"Software"), to deal in the Software without restriction, including 
	without limitation the rights to use, copy, modify, merge, publish, 
	distribute, and/or sell copies of the Software, and to permit persons 
	to whom the Software is furnished to do so, provided that the above 
	copyright notice(s) and this permission notice appear in all copies of 
	the Software and that both the above copyright notice(s) and this 
	permission notice appear in supporting documentation. 

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT 
	OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
	HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL 
	INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING 
	FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
	NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION 
	WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. 

	Except as contained in this notice, the name of a copyright holder 
	shall not be used in advertising or otherwise to promote the sale, use 
	or other dealings in this Software without prior written authorization 
	of the copyright holder.
*/
import java.io.Serializable;
import java.rmi.RemoteException;
import java.time.Duration;
import java.time.LocalDateTime;

import javax.telephony.media.Symbol;

import lombok.extern.apachecommons.CommonsLog;
import net.sourceforge.gjtapi.CallId;
import net.sourceforge.gjtapi.TelephonyListener;
import net.sourceforge.gjtapi.media.SymbolHolder;

/**
 * This wraps a client's RemoteListener and makes it look like a RawListener. The RawProvider can
 * then deal with RawObservers as if they were local.
 * Creation date: (2000-02-17 13:36:23)
 * 
 * @author: Richard Deadman
 */
@CommonsLog
public class RemoteListenerWrapper implements TelephonyListener {
	
	private final RemoteListener delegate;
	private final CallMapper refMapper;
	private int unreachableCount = 0;
	private LocalDateTime firstUnreachable;
	private LocalDateTime latestUnreachable;
	
	/**
	 * Create a wrapper RawObserver that delegates to a RemoteObserver client.
	 * Creation date: (2000-02-17 13:36:49)
	 * 
	 * @param remoteListener the wrapped RemoteListener stub
	 * @param callMapper a map between RP call IDs and serializable call IDs.
	 */
	public RemoteListenerWrapper(RemoteListener remoteListener, CallMapper callMapper) {
		if (remoteListener == null || callMapper == null) {
			throw new IllegalArgumentException();
		}
		
		delegate = remoteListener;
		refMapper = callMapper;
	}
	
	/**
	 * Returns the number of exceptions since the last successful execution of a remote event
	 * 
	 * @return the number of consecutive remote exceptions since the last success
	 */
	public int getUnreachableCount() {
		return unreachableCount;
	}
	
	/**
	 * Returns the duration between the first and latest exception since the last successful
	 * execution of a remote event
	 * 
	 * @return the duration between the first and latest exception since the last success
	 */
	public Duration getUnreachableDuration() {
		if (firstUnreachable == null) {
			return Duration.ZERO;
		}
		return Duration.between(firstUnreachable, latestUnreachable);
	}
	
	private void fireRemoteEvent(RemoteEvent event) {
		try {
			event.fire();
			resetRemoteExceptionStatistic();
		}
		catch (RemoteException e) {
			log.error("Error pushing event back to client", e);
			updateRemoteExceptionStatistic();
		}
	}
	
	private void resetRemoteExceptionStatistic() {
		unreachableCount = 0;
		firstUnreachable = null;
		latestUnreachable = null;
	}
	
	private void updateRemoteExceptionStatistic() {
		unreachableCount++;
		latestUnreachable = LocalDateTime.now();
		if (firstUnreachable == null) {
			firstUnreachable = latestUnreachable;
		}
	}
	
	@Override
	public void addressPrivateData(String address, Serializable data, int cause) {
		fireRemoteEvent(() -> delegate.addressPrivateData(address, data, cause));
	}
	
	@Override
	public void callActive(CallId id, int cause) {
		fireRemoteEvent(() -> delegate.callActive(refMapper.swapId(id), cause));
	}
	
	@Override
	public void callInvalid(CallId id, int cause) {
		fireRemoteEvent(() -> delegate.callInvalid(refMapper.swapId(id), cause));
	}
	
	@Override
	public void callOverloadCeased(String address) {
		fireRemoteEvent(() -> delegate.callOverloadCeased(address));
	}
	
	@Override
	public void callOverloadEncountered(String address) {
		fireRemoteEvent(() -> delegate.callOverloadEncountered(address));
	}
	
	@Override
	public void callPrivateData(CallId call, Serializable data, int cause) {
		fireRemoteEvent(() -> delegate.callPrivateData(refMapper.swapId(call), data, cause));
	}
	
	@Override
	public void connectionAddressAnalyse(CallId id, String address, int cause) {
		fireRemoteEvent(() -> delegate.connectionAddressAnalyse(refMapper.swapId(id), address, cause));
	}
	
	@Override
	public void connectionAddressCollect(CallId id, String address, int cause) {
		fireRemoteEvent(() -> delegate.connectionAddressCollect(refMapper.swapId(id), address, cause));
	}
	
	@Override
	public void connectionAlerting(CallId id, String address, int cause) {
		fireRemoteEvent(() -> delegate.connectionAlerting(refMapper.swapId(id), address, cause));
	}
	
	@Override
	public void connectionAuthorizeCallAttempt(CallId id, String address, int cause) {
		fireRemoteEvent(() -> delegate.connectionAuthorizeCallAttempt(refMapper.swapId(id), address,
				cause));
	}
	
	@Override
	public void connectionCallDelivery(CallId id, String address, int cause) {
		fireRemoteEvent(() -> delegate.connectionCallDelivery(refMapper.swapId(id), address, cause));
	}
	
	@Override
	public void connectionConnected(CallId id, String address, int cause) {
		fireRemoteEvent(() -> delegate.connectionConnected(refMapper.swapId(id), address, cause));
	}
	
	@Override
	public void connectionDisconnected(CallId id, String address, int cause) {
		fireRemoteEvent(() -> delegate.connectionDisconnected(refMapper.swapId(id), address, cause));
	}
	
	@Override
	public void connectionFailed(CallId id, String address, int cause) {
		fireRemoteEvent(() -> delegate.connectionFailed(refMapper.swapId(id), address, cause));
	}
	
	@Override
	public void connectionInProgress(CallId id, String address, int cause) {
		fireRemoteEvent(() -> delegate.connectionInProgress(refMapper.swapId(id), address, cause));
	}
	
	@Override
	public void connectionSuspended(CallId id, String address, int cause) {
		fireRemoteEvent(() -> delegate.connectionSuspended(refMapper.swapId(id), address, cause));
	}
	
	@Override
	public void mediaPlayPause(String terminal, int index, int offset, Symbol trigger) {
		fireRemoteEvent(() -> delegate.mediaPlayPause(terminal, index, offset, new SymbolHolder(trigger)));
	}
	
	@Override
	public void mediaPlayResume(String terminal, Symbol trigger) {
		fireRemoteEvent(() -> delegate.mediaPlayResume(terminal, new SymbolHolder(trigger)));
	}
	
	@Override
	public void mediaRecorderPause(String terminal, int duration, Symbol trigger) {
		fireRemoteEvent(() -> delegate.mediaRecorderPause(terminal, duration, new SymbolHolder(trigger)));
	}
	
	@Override
	public void mediaRecorderResume(String terminal, Symbol trigger) {
		fireRemoteEvent(() -> delegate.mediaRecorderResume(terminal, new SymbolHolder(trigger)));
	}
	
	@Override
	public void mediaSignalDetectorDetected(String terminal, Symbol[] sigs) {
		fireRemoteEvent(() -> delegate.mediaSignalDetectorDetected(terminal, SymbolHolder.create(sigs)));
	}
	
	@Override
	public void mediaSignalDetectorOverflow(String terminal, Symbol[] sigs) {
		fireRemoteEvent(() -> delegate.mediaSignalDetectorOverflow(terminal, SymbolHolder.create(sigs)));
	}
	
	@Override
	public void mediaSignalDetectorPatternMatched(String terminal, Symbol[] sigs, int index) {
		fireRemoteEvent(() -> delegate.mediaSignalDetectorPatternMatched(terminal,
				SymbolHolder.create(sigs), index));
	}
	
	@Override
	public void providerPrivateData(Serializable data, int cause) {
		fireRemoteEvent(() -> delegate.providerPrivateData(data, cause));
	}
	
	@Override
	public void terminalConnectionCreated(CallId id, String address, String terminal, int cause) {
		fireRemoteEvent(() -> delegate.terminalConnectionCreated(refMapper.swapId(id), address,
				terminal, cause));
	}
	
	@Override
	public void terminalConnectionDropped(CallId id, String address, String terminal, int cause) {
		fireRemoteEvent(() -> delegate.terminalConnectionDropped(refMapper.swapId(id), address,
				terminal, cause));
	}
	
	@Override
	public void terminalConnectionHeld(CallId id, String address, String terminal, int cause) {
		fireRemoteEvent(() -> delegate.terminalConnectionHeld(refMapper.swapId(id), address,
				terminal, cause));
	}
	
	@Override
	public void terminalConnectionRinging(CallId id, String address, String terminal, int cause) {
		fireRemoteEvent(() -> delegate.terminalConnectionRinging(refMapper.swapId(id), address,
				terminal, cause));
	}
	
	@Override
	public void terminalConnectionTalking(CallId id, String address, String terminal, int cause) {
		fireRemoteEvent(() -> delegate.terminalConnectionTalking(refMapper.swapId(id), address,
				terminal, cause));
	}
	
	@Override
	public void terminalPrivateData(String terminal, Serializable data, int cause) {
		fireRemoteEvent(() -> delegate.terminalPrivateData(terminal, data, cause));
	}
	
	/**
	 * Checks if the two objects hold equal delegates.
	 * 
	 * @param object the Object to compare with
	 * @return true if these Objects are equal; false otherwise.
	 */
	@Override
	public boolean equals(Object object) {
		if (object instanceof RemoteListenerWrapper) {
			RemoteListenerWrapper otherWrapper = (RemoteListenerWrapper) object;
			return delegate.equals(otherWrapper.delegate);
		}
		return false;
	}
	
	/**
	 * Generates a hash code depending solely on the receiver.
	 * 
	 * @return an integer hash code for the receiver
	 */
	@Override
	public int hashCode() {
		return delegate.hashCode();
	}
	
	@Override
	public String toString() {
		return "RawObserver wrapper for RemoteObserver: " + delegate;
	}
	
	/**
	 * @author haf
	 */
	@FunctionalInterface
	private interface RemoteEvent {
		
		public void fire() throws RemoteException;
	}
}
