package net.sourceforge.gjtapi.raw.xtapi;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.telephony.InvalidArgumentException;

import net.sourceforge.gjtapi.TermData;

/**
 * @author lak
 */
class OpenedLineRegistry {
	// Map of address name to AddressInfo for XTAPI
	private Map<String, OpenedLine> addInfoMap = new HashMap<>();
	// Map of Terminal name to AddressInfo
	private Map<String, OpenedLine> termToAddr = new HashMap<>();
	// Map of line name (new Integer(rawHandle)) to AddressInfo
	private Map<Integer, OpenedLine> lineToAddr = new HashMap<>();
	
	private static final String ADDR_PREFIX = "addr_";

	public void add(OpenedLine line) {
		addInfoMap.put(ADDR_PREFIX + line.getLineDeviceNo(), line);
		termToAddr.put(line.getLineDeviceName(), line);
		lineToAddr.put(line.getLineHandle(), line);
	}
	
	public String[] getAddresses() {
		return addInfoMap
				.values()
				.stream()
				.map(OpenedLine::getAddressName)
				.toArray(String[]::new);
	}

	public TermData[] getTerminals() {
		return addInfoMap
				.values()
				.stream()
				.map(OpenedLine::getLineDeviceName)
				.map(TermData::new)
				.toArray(TermData[]::new);
	}

	public String[] getAddressesByTerminal(String terminal) throws InvalidArgumentException {
		if (terminal != null) {
			OpenedLine line = termToAddr.get(terminal);
			if (line != null) {
				return new String[] { line.getAddressName() };
			}
		}

		throw new InvalidArgumentException("No terminal found: " + terminal);
	}
	
	public TermData[] getTerminalsByAddress(String address) throws InvalidArgumentException {
		if (address != null) {
			OpenedLine line = getByAddress(address);
			return new TermData[] { new TermData(line.getLineDeviceName(), true) };
		}
		
		throw new InvalidArgumentException("No address found: " + address);
	}
	
	public OpenedLine getByAddress(String address) throws InvalidArgumentException {
		Optional<OpenedLine> result = addInfoMap.values()
				.stream()
				.filter(line -> address.equals(line.getAddressName()))
				.findAny();
		if (result.isPresent()) {
			return result.get();
		}
		
		throw new InvalidArgumentException("Address not known: " + address);
	}
	
	public Optional<OpenedLine> getByLineHandle(Integer id) {
		return Optional.ofNullable(lineToAddr.get(id));
	}
	
	public Optional<OpenedLine> getByDeviceNo(Integer deviceNo) {
		return Optional.ofNullable(addInfoMap.get(ADDR_PREFIX + deviceNo));
	}
	
	public Optional<OpenedLine> getByDeviceName(String terminal) {
		return Optional.ofNullable(termToAddr.get(terminal));
	}
	
	public boolean isRegisteredTerminal(String terminal) {
		return termToAddr.containsKey(terminal);
	}
}
