package net.sourceforge.gjtapi.raw.xtapi;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import lombok.Setter;
import net.sourceforge.gjtapi.TelephonyListener;

/**
 * @author haf
 */
final class EventDelayer {
	
	private @Setter TelephonyListener listener;
	private final Map<Integer, DelayedEvent> callHandleToEvent = new HashMap<>();
	private final DelayedEventFactory factory;
	
	EventDelayer(TelephonyListener telephonyListener, DelayedEventFactory factory) {
		this.listener = telephonyListener;
		this.factory = factory;
	}
	
	/**
	 * Stores call handle specific information needed for an event. If this was the last part of
	 * missing information for this call handle's event, that event is fired automatically.
	 * 
	 * @param callHandle the call handle to which the information belongs
	 * @param callInfo the information to be stored
	 */
	void addCallInformation(int callHandle, CallInformation callInfo) {
		addInformation(callHandle, event -> event.setCallInformation(callInfo));
	}
	
	/**
	 * Stores call handle specific information needed for an event. If this was the last part of
	 * missing information for this call handle's event, that event is fired automatically.
	 * 
	 * @param callHandle the call handle to which the information belongs
	 * @param callId the information to be stored
	 */
	void addCallId(int callHandle, XtapiCallId callId) {
		addInformation(callHandle, event -> event.setCallId(callId));
	}
	
	private void addInformation(int callHandle, Consumer<DelayedEvent> eventModifier) {
		DelayedEvent event = callHandleToEvent.get(callHandle);
		if (event != null) {
			eventModifier.accept(event);
			fireIfComplete(callHandle, event);
		}
		else {
			event = factory.create();
			eventModifier.accept(event);
			addEntry(callHandle, event);
		}
	}
	
	private DelayedEvent addEntry(int callHandle, DelayedEvent value) {
		return callHandleToEvent.put(callHandle, value);
	}
	
	private void fireIfComplete(int callHandle, DelayedEvent event) {
		if (event.isComplete()) {
			event.fire(listener);
			callHandleToEvent.remove(callHandle);
		}
	}
	
	/**
	 * Removes all currently stored information for the specified call handle.
	 * 
	 * @param callHandle the call handle for which all information is to be removed
	 */
	void remove(int callHandle) {
		callHandleToEvent.remove(callHandle);
	}
}
