package net.sourceforge.gjtapi.raw.remote;

/*
	Copyright (c) 2002 8x8 Inc. (www.8x8.com) 

	All rights reserved. 

	Permission is hereby granted, free of charge, to any person obtaining a 
	copy of this software and associated documentation files (the 
	"Software"), to deal in the Software without restriction, including 
	without limitation the rights to use, copy, modify, merge, publish, 
	distribute, and/or sell copies of the Software, and to permit persons 
	to whom the Software is furnished to do so, provided that the above 
	copyright notice(s) and this permission notice appear in all copies of 
	the Software and that both the above copyright notice(s) and this 
	permission notice appear in supporting documentation. 

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT 
	OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
	HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL 
	INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING 
	FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
	NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION 
	WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. 

	Except as contained in this notice, the name of a copyright holder 
	shall not be used in advertising or otherwise to promote the sale, use 
	or other dealings in this Software without prior written authorization 
	of the copyright holder.
*/
import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Objects;

import net.sourceforge.gjtapi.TelephonyListener;
import net.sourceforge.gjtapi.media.SymbolHolder;

/**
 * This is a wrapper class for a RawListener that lets the RawListener receive remote updates.
 * On the Framework side, this translates RemoteListener skeleton calls to be passed on to the real
 * RawListener.
 * Creation date: (2000-02-17 13:09:32)
 * 
 * @author: Richard Deadman
 */
public class RemoteListenerImpl extends UnicastRemoteObject implements RemoteListener {
	
	private static final long serialVersionUID = 137460616322975487L;
	
	private final TelephonyListener delegate;
	
	/**
	 * Create a Remote proxy for a RawObserver
	 * Creation date: (2000-02-17 13:16:33)
	 * @author: Richard Deadman
	 * @param obs The listener to forward events to
	 */
	public RemoteListenerImpl(TelephonyListener obs) throws RemoteException {
		this(obs, 0);
	}
	
	public RemoteListenerImpl(TelephonyListener obs, int port) throws RemoteException {
		super(port);
		this.delegate = Objects.requireNonNull(obs);
	}
	
	@Override
	public void addressPrivateData(String address, Serializable data, int cause) throws RemoteException {
		checkDelegate();
		delegate.addressPrivateData(address, data, cause);
	}
	
	/**
	 * Forward on the callActive event
	 */
	@Override
	public void callActive(SerializableCallId id, int cause) throws RemoteException {
		checkDelegate();
		delegate.callActive(id, cause);
	}
	
	/**
	 * Forward on the callInvalid event.
	 */
	@Override
	public void callInvalid(SerializableCallId id, int cause) throws RemoteException {
		checkDelegate();
		delegate.callInvalid(id, cause);
	}
	
	@Override
	public void callOverloadCeased(String address) throws RemoteException {
		checkDelegate();
		delegate.callOverloadCeased(address);
	}
	
	@Override
	public void callOverloadEncountered(String address) throws RemoteException {
		checkDelegate();
		delegate.callOverloadEncountered(address);
	}
	
	@Override
	public void callPrivateData(SerializableCallId call, Serializable data, int cause) throws RemoteException {
		checkDelegate();
		delegate.callPrivateData(call, data, cause);
	}
	
	@Override
	public void connectionAddressAnalyse(SerializableCallId id, String address, int cause) throws RemoteException {
		checkDelegate();
		delegate.connectionAddressAnalyse(id, address, cause);
	}
	
	@Override
	public void connectionAddressCollect(SerializableCallId id, String address, int cause) throws RemoteException {
		checkDelegate();
		delegate.connectionAddressCollect(id, address, cause);
	}
	/**
	 * Forward on the connectionAlerting event.
	 */
	@Override
	public void connectionAlerting(SerializableCallId id, String address, int cause) throws RemoteException {
		checkDelegate();
		delegate.connectionAlerting(id, address, cause);
	}
	
	@Override
	public void connectionAuthorizeCallAttempt(SerializableCallId id, String address, int cause) throws RemoteException {
		checkDelegate();
		delegate.connectionAuthorizeCallAttempt(id, address, cause);
	}
	
	@Override
	public void connectionCallDelivery(SerializableCallId id, String address, int cause) throws RemoteException {
		checkDelegate();
		delegate.connectionCallDelivery(id, address, cause);
	}
	
	/**
	 * Forward on the connectionConnected event.
	 */
	@Override
	public void connectionConnected(SerializableCallId id, String address, int cause) throws RemoteException {
		checkDelegate();
		delegate.connectionConnected(id, address, cause);
	}
	
	/**
	 * Forward on the connectionDisconnected event.
	 */
	@Override
	public void connectionDisconnected(SerializableCallId id, String address, int cause) throws RemoteException {
		checkDelegate();
		delegate.connectionDisconnected(id, address, cause);
	}
	
	/**
	 * Forward on the connectionFailed event.
	 */
	@Override
	public void connectionFailed(SerializableCallId id, String address, int cause) throws RemoteException {
		checkDelegate();
		delegate.connectionFailed(id, address, cause);
	}
	
	/**
	 * Forward on the connectionInProgress event.
	 */
	@Override
	public void connectionInProgress(SerializableCallId id, String address, int cause) throws RemoteException {
		checkDelegate();
		delegate.connectionInProgress(id, address, cause);
	}
	
	@Override
	public void connectionSuspended(SerializableCallId id, String address, int cause) throws RemoteException {
		checkDelegate();
		delegate.connectionSuspended(id, address, cause);
	}
	
	/**
	 * Forward on the mediaPlayPause event.
	 */
	@Override
	public void mediaPlayPause(String terminal, int index, int offset, SymbolHolder trigger) throws RemoteException {
		checkDelegate();
		delegate.mediaPlayPause(terminal, index, offset, trigger.getSymbol());
	}
	
	/**
	 * Forward on the mediaPlayResume event.
	 */
	@Override
	public void mediaPlayResume(String terminal, SymbolHolder trigger) throws RemoteException {
		checkDelegate();
		delegate.mediaPlayResume(terminal, trigger.getSymbol());
	}
	
	/**
	 * Forward on the mediaRecorderPause event.
	 */
	@Override
	public void mediaRecorderPause(String terminal, int duration, SymbolHolder trigger) throws RemoteException {
		checkDelegate();
		delegate.mediaRecorderPause(terminal, duration, trigger.getSymbol());
	}
	
	/**
	 * Forward on the mediaRecorderResume event.
	 */
	@Override
	public void mediaRecorderResume(String terminal, SymbolHolder trigger) throws RemoteException {
		checkDelegate();
		delegate.mediaRecorderResume(terminal, trigger.getSymbol());
	}
	
	/**
	 * Forward on the mediaSignalDetectorDetected event.
	 */
	@Override
	public void mediaSignalDetectorDetected(String terminal, SymbolHolder[] sigs) throws RemoteException {
		checkDelegate();
		delegate.mediaSignalDetectorDetected(terminal, SymbolHolder.decode(sigs));
	}
	
	/**
	 * Forward on the mediaSignalDetectorOverflow event.
	 */
	@Override
	public void mediaSignalDetectorOverflow(String terminal, SymbolHolder[] sigs) throws RemoteException {
		checkDelegate();
		delegate.mediaSignalDetectorOverflow(terminal, SymbolHolder.decode(sigs));
	}
	
	/**
	 * Forward on the mediaSignalDetectorPatternMatched event.
	 */
	@Override
	public void mediaSignalDetectorPatternMatched(String terminal, SymbolHolder[] sigs, int index) throws RemoteException {
		checkDelegate();
		delegate.mediaSignalDetectorPatternMatched(terminal, SymbolHolder.decode(sigs), index);
	}
	
	@Override
	public void providerPrivateData(Serializable data, int cause) throws RemoteException {
		checkDelegate();
		delegate.providerPrivateData(data, cause);
	}
	
	/**
	 * Forward on the terminalConnectionCreated event.
	 */
	@Override
	public void terminalConnectionCreated(SerializableCallId id, String address, String terminal, int cause) throws RemoteException {
		checkDelegate();
		delegate.terminalConnectionCreated(id, address, terminal, cause);
	}
	
	/**
	 * Forward on the terminalConnectionDropped event.
	 */
	@Override
	public void terminalConnectionDropped(SerializableCallId id, String address, String terminal, int cause) throws RemoteException {
		checkDelegate();
		delegate.terminalConnectionDropped(id, address, terminal, cause);
	}
	
	/**
	 * Forward on the terminalConnectionHeld event.
	 */
	@Override
	public void terminalConnectionHeld(SerializableCallId id, String address, String terminal, int cause) throws RemoteException {
		checkDelegate();
		delegate.terminalConnectionHeld(id, address, terminal, cause);
	}
	
	/**
	 * Forward on the terminalConnectionRinging event.
	 */
	@Override
	public void terminalConnectionRinging(SerializableCallId id, String address, String terminal, int cause) throws RemoteException {
		checkDelegate();
		delegate.terminalConnectionRinging(id, address, terminal, cause);
	}
	
	/**
	 * Forward on the terminalConnectionTalking event.
	 */
	@Override
	public void terminalConnectionTalking(SerializableCallId id, String address, String terminal, int cause) throws RemoteException {
		checkDelegate();
		delegate.terminalConnectionTalking(id, address, terminal, cause);
	}
	
	@Override
	public void terminalPrivateData(String terminal, Serializable data, int cause) throws RemoteException {
		checkDelegate();
		delegate.terminalPrivateData(terminal, data, cause);
	}
	
	private void checkDelegate() throws RemoteException {
		if (delegate == null) {
			throw new RemoteException("Unknown delegate");
		}
	}
	
	@Override
	public String toString() {
		return "Remote wrapper for: " + delegate;
	}
}
