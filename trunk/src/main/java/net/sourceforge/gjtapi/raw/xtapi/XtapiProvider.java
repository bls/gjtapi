package net.sourceforge.gjtapi.raw.xtapi;

import java.time.Duration;
import java.util.Collections;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;

import javax.telephony.Connection;
import javax.telephony.Event;
import javax.telephony.InvalidArgumentException;
import javax.telephony.InvalidStateException;
import javax.telephony.MethodNotSupportedException;
import javax.telephony.ProviderUnavailableException;
import javax.telephony.ResourceUnavailableException;
import javax.telephony.TerminalConnection;
import javax.telephony.media.MediaResourceException;
import javax.telephony.media.PlayerConstants;
import javax.telephony.media.RTC;
import javax.telephony.media.RecorderConstants;
import javax.telephony.media.SignalDetectorConstants;
import javax.telephony.media.Symbol;

import lombok.SneakyThrows;
import lombok.extern.apachecommons.CommonsLog;
import net.sourceforge.gjtapi.CallId;
import net.sourceforge.gjtapi.LineCallInfoState;
import net.sourceforge.gjtapi.LineError;
import net.sourceforge.gjtapi.RawSigDetectEvent;
import net.sourceforge.gjtapi.RawStateException;
import net.sourceforge.gjtapi.TelephonyListener;
import net.sourceforge.gjtapi.TermData;
import net.sourceforge.gjtapi.capabilities.Capabilities;
import net.sourceforge.gjtapi.media.SymbolConvertor;
import net.sourceforge.gjtapi.raw.MediaTpi;
import net.xtapi.serviceProvider.IXTapi;
import net.xtapi.serviceProvider.IXTapiCallBack;
import net.xtapi.serviceProvider.MsTapiFactory;
import net.xtapi.serviceProvider.XTapiFactory;
import net.xtapi.serviceProvider.XTapiLineInfo;

/*
*  GJTAPI - XTAPI Bridge
*  Copyright (C) 2002 Richard Deadman
* 
	All rights reserved. 
	
	Permission is hereby granted, free of charge, to any person obtaining a 
	copy of this software and associated documentation files (the 
	"Software"), to deal in the Software without restriction, including 
	without limitation the rights to use, copy, modify, merge, publish, 
	distribute, and/or sell copies of the Software, and to permit persons 
	to whom the Software is furnished to do so, provided that the above 
	copyright notice(s) and this permission notice appear in all copies of 
	the Software and that both the above copyright notice(s) and this 
	permission notice appear in supporting documentation. 
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT 
	OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
	HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL 
	INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING 
	FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
	NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION 
	WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. 
	
	Except as contained in this notice, the name of a copyright holder 
	shall not be used in advertising or otherwise to promote the sale, use 
	or other dealings in this Software without prior written authorization 
	of the copyright holder.
	
	---
	Licenced changed from GPL to the common GJTAPI licence since the
	Free Software Foundation acknowledges that the X11 licence is compatable
	with the GPL and may link to GPL (i.e. XTAPI) code. - 23May2005 Richard Deadman
 *
 * @author  Richard Deadman
 * @version .01
 */

/**
 * This is a bridge GJTAPI service provider that allows XTAPI service providers
 * to be plugged in underneath.
 */
@CommonsLog
public class XtapiProvider implements MediaTpi, IXTapiCallBack {
	
	private static final int DEFAULT_LOG_LEVEL = 4;
	private static final Duration DEFAULT_INIT_TIMEOUT = Duration.ofSeconds(15L);
	private static final TelephonyListener DUMMY_LISTENER = new NullTelephonyListener();
	
	// The XTAPI service provider
	private IXTapi realProvider = null;
	
	// The GJTAPI Listener I delegate callbacks to
	private TelephonyListener gjListener = DUMMY_LISTENER;
	
	// The number of lines my real provider manages
	private int numLines = 0;
	private OpenedLineRegistry linesRegistry = new OpenedLineRegistry();
	
	/**
	 * Map of terminals to CallHandles
	 * This is used by the media methods to map media terminal to calls
	 */
	private final Map<String, CallId> termToCalls = new HashMap<>();
	
	/**
	 * Map of line IDs to CallHandles
	 */
	private final Map<Integer, XtapiCallId> lineToCalls = new HashMap<>();
	
	/**
	 * map call handles to calls
	 */
	private final Map<Integer, CallId> callNumToCalls = new HashMap<>();
	
	private final EventDelayer lineOfferingDelayer =
			new EventDelayer(gjListener, new DelayedLineOfferingFactory());
	
	// The digit buckets to record digits for various terminals
	// This contains a Map of Terminal names to digit StringBuffers.
	private final Map<String, StringBuffer> termToBuckets = new HashMap<>();
	
	@Override
	@SneakyThrows({ResourceUnavailableException.class, InvalidStateException.class})
	public void initialize(Map<String, Object> props) {
		if (props == null) {
			props = Collections.emptyMap();
		}
		
		numLines = initProvider(props);
		initLineRegistry();
	}
	
	private int initProvider(Map<String, Object> props) {
		XTapiFactory factory = getFactory(props);
		realProvider = factory.create();
		int logLevel = getLogLevel(props);
		realProvider.XTDebug(logLevel);
		Duration initTimeout = getInitTimeout(props);
		return realProvider.XTinit(this, initTimeout);
	}
	
	private static int getLogLevel(Map<String, Object> props) {
		Object logLevelValue = props.get(XTapiProviderInitParameter.LOG_LEVEL.getKey());
		return (logLevelValue instanceof Integer) ? (int) logLevelValue : DEFAULT_LOG_LEVEL;
	}
	
	private static XTapiFactory getFactory(Map<String, Object> props) {
		Object factoryValue = props.get(XTapiProviderInitParameter.FACTORY.getKey());
		if (factoryValue == null) {
			return new MsTapiFactory();
		}
		else if (factoryValue instanceof XTapiFactory) {
			return (XTapiFactory) factoryValue;
		}
		throw new ProviderUnavailableException("The specified TAPI factory does not implement XTapiFactory");
	}
	
	private static Duration getInitTimeout(Map<String, Object> props) {
		Object value = props.get(XTapiProviderInitParameter.INIT_TIMEOUT.getKey());
		return (value instanceof Duration) ? (Duration) value : DEFAULT_INIT_TIMEOUT;
	}
	
	private void initLineRegistry() throws ResourceUnavailableException, InvalidStateException {
		linesRegistry = new OpenedLineRegistry();
		for (int lineNumber = 0; lineNumber < numLines; lineNumber++) {
			int lineHandle = realProvider.XTOpenLine(lineNumber);
			if (lineHandle >= 0) {
				registerLine(lineNumber, lineHandle);
			}
		}
	}
	
	private void registerLine(int lineNumber, int handle) throws ResourceUnavailableException, InvalidStateException {
		XTapiLineInfo lineInfo = realProvider.XTGetLineInfo(lineNumber);
		OpenedLine line = OpenedLine.builder()
				.lineHandle(handle)
				.lineDeviceNo(lineNumber)
				.lineDeviceId(lineInfo.getLinePermanentId())
				.lineDeviceName(lineInfo.getLineName())
				.addressName(lineInfo.getFirstAddress())
				.build();
		linesRegistry.add(line);
	}
	
	@Override
	public void shutdown() {
		try {
			realProvider.XTShutdown();
		}
		catch (InvalidStateException ise) {
			// ignore
		}
	}
	
	@Override
	public void addListener(TelephonyListener listener) {
		if (gjListener != DUMMY_LISTENER) {
			throw new IllegalStateException("The listener is already set");
		}
		
		setListener(listener);
	}
	
	@Override
	public void removeListener(TelephonyListener listener) {
		if (gjListener.equals(listener)) {
			setListener(DUMMY_LISTENER);
		}
	}
	
	private void setListener(TelephonyListener listener) {
		gjListener = listener;
		lineOfferingDelayer.setListener(listener);
	}
	
	/**
	 * Supports the default capabilities associated with the MediaTpi
	 */
	@Override
	public Properties getCapabilities() {
		Properties caps = new Properties();
		// mark differences from the default
		caps.put(Capabilities.HOLD, "f");
		caps.put(Capabilities.JOIN, "f");
		caps.put(Capabilities.THROTTLE, "f");
		caps.put(Capabilities.ALLOCATE_MEDIA, "f");
		
		return caps;
	}
	
	@Override
	public String[] getAddresses() {
		String[] addresses = linesRegistry.getAddresses();
		return addresses.length == 0 ? null : addresses;
	}
	
	/**
	 * @throws InvalidArgumentException 
	 */
	@Override
	public String[] getAddresses(String terminal) throws InvalidArgumentException {
		return linesRegistry.getAddressesByTerminal(terminal);
	}
	
	@Override
	public TermData[] getTerminals() {
		TermData[] terminals = linesRegistry.getTerminals();
		return terminals.length == 0 ? null : terminals;
	}
	
	/**
	 * @throws InvalidArgumentException 
	 */
	@Override
	public TermData[] getTerminals(String address) throws InvalidArgumentException {
		return linesRegistry.getTerminalsByAddress(address);
	}
	
	/**
	 * Get a CallId object for later connection. The address parameter is ignored.
	 */
	@Override
	public CallId reserveCallId(String address) {
		return new XtapiCallId();
	}
	
	@Override
	public CallId createCall(CallId id, String address, String term, String dest)
		throws ResourceUnavailableException, InvalidArgumentException, RawStateException {
		// translate the address into a line
		OpenedLine lineInfo = linesRegistry.getByAddress(address);
		// register the call with the line
		lineToCalls.put(Integer.valueOf(lineInfo.getLineDeviceNo()), (XtapiCallId) id);
		// Get the call handle
		int callNum = 0;
		try {
			callNum = realProvider.XTConnectCall(lineInfo.getLineDeviceNo(),
					dest,
					lineInfo.getLineHandle());
		}
		catch (InvalidStateException ise) {
			throw new RawStateException(id, ise.getState());
		}
		
		// update the call structure
		XtapiCallId xid = (XtapiCallId) id;
		xid.setCallNum(callNum);
		callNumToCalls.put(Integer.valueOf(callNum), xid);
		xid.setLocalAddress(address);
		xid.setRemoteAddress(dest);
		
		// register the call with the terminal
		termToCalls.put(term, id);
		
		// now note that the local leg is connected
		gjListener.connectionConnected(id, address, Event.CAUSE_NORMAL);
		gjListener.connectionAlerting(id, dest, Event.CAUSE_NORMAL);
		
		return id;
	}
	
	@Override
	public void answerCall(CallId call, String address, String terminal) throws ResourceUnavailableException, RawStateException {
		int returnCode;
		try {
			returnCode = realProvider.XTAnswerCall(((XtapiCallId) call).getCallNum());
		}
		catch (InvalidStateException e) {
			throw new RawStateException(call, e.getState());
		}
		if (returnCode < 0) {
			String errorString = getLineErrorString(returnCode);
			log.error(String.format("XTAnswerCall returned with an error: %s", errorString));
			throw new RawStateException(call, TerminalConnection.UNKNOWN);
		}
		log.debug(String.format("XTAnswerCall returned: 0x%s", Integer.toHexString(returnCode)));
	}
	
	private static String getLineErrorString(int errorCode) {
		Optional<LineError> error = LineError.fromBitValue(errorCode);
		return error.isPresent() ? error.get().toString() : "unknown error";
	}
	
	/**
	 * The GJTAPI Framework is done with the Call and is releasing the CallId.
	 */
	@Override
	public void releaseCallId(CallId callId) {
		int returnCode = 0;
		try {
			returnCode = realProvider.XTDropCall(((XtapiCallId) callId).getCallNum());
		}
		catch (InvalidStateException | ResourceUnavailableException e) {
			// ignore
		}
		if (returnCode < 0) {
			String errorString = getLineErrorString(returnCode);
			log.warn(String.format("XTDropCall returned with an error: %s", errorString));
		}
		else {
			log.debug(String.format("XTDropCall returned: 0x%s", Integer.toHexString(returnCode)));
		}
	}
	
	@Override
	public void release(String address, CallId callId) throws ResourceUnavailableException, RawStateException {
		// check if this is a local address
		int returnCode = 0;
		try {
			OpenedLine openedLine = linesRegistry.getByAddress(address);
			if (openedLine == null) {
				return;
			}
			
			returnCode = realProvider.XTDropCall(((XtapiCallId) callId).getCallNum());
		}
		catch (InvalidStateException ise) {
			throw new RawStateException(callId, ise.getState());
		}
		catch (InvalidArgumentException iae) {
			// is not a local address
		}
		if (returnCode < 0) {
			String errorString = getLineErrorString(returnCode);
			log.error(String.format("XTDropCall returned with an error: %s", errorString));
			throw new RawStateException(callId, Connection.UNKNOWN);
		}
		log.debug(String.format("XTAnswerCall returned: 0x%s", Integer.toHexString(returnCode)));
	}
	
	// Media calls
	/**
	 * Allocate media resources.
	 * This is a NO-OP, since XTAPI doesn't need this.
	 */
	@Override
	public boolean allocateMedia(String terminal, int type, Dictionary resourceArgs) {
		return true;
	}

	/**
	 * Free media resources.
	 */
	@Override
	public boolean freeMedia(String terminal, int type) {
		// clear out the digit bucket for the terminal.
		retrieveSignals(terminal);
		return true;
	}

	/**
	 * All known terminals support media
	 */
	@Override
	public boolean isMediaTerminal(String terminal) {
		return linesRegistry.isRegisteredTerminal(terminal);
	}

	@Override
	public void play(String terminal, String[] streamIds, int offset, RTC[] rtcs, Dictionary optArgs) throws MediaResourceException {
		// look up the id for the terminal
		int id = getLineId(terminal);
		int size = streamIds.length;
		
		try {
			// play each id on the found line
			for (int i = 0; i < size; i++) {
				realProvider.XTPlaySound(streamIds[i], id);
			}
		}
		catch (InvalidStateException | MethodNotSupportedException | ResourceUnavailableException e) {
			throw new MediaResourceException(e.toString());
		}
	}
	
	@Override
	public void record(String terminal, String streamId, RTC[] rtcs, Dictionary optArgs) throws MediaResourceException {
		try {
			// look up the id for the terminal
			int id = getLineId(terminal);
			realProvider.XTRecordSound(streamId, id);
		}
		catch (InvalidStateException | MethodNotSupportedException | ResourceUnavailableException e) {
			throw new MediaResourceException(e.toString());
		}
	}
	
	@Override
	public RawSigDetectEvent retrieveSignals(String terminal, int num, Symbol[] patterns,
			RTC[] rtcs, Dictionary optArgs) throws MediaResourceException {
		// look up the id for the terminal
		int id = getCallId(terminal);
		try {
			realProvider.XTMonitorDigits(id, true);
		}
		catch (InvalidStateException | MethodNotSupportedException | ResourceUnavailableException e) {
			throw new MediaResourceException(e.toString());
		}
		// wait for signals to come in
		//---------------------------------------------------------------------------
		// Get the local p_Duration (if any).
		//---------------------------------------------------------------------------
		Object timeoutVal = null;
		if (optArgs != null) {
			timeoutVal = optArgs.get(SignalDetectorConstants.p_Duration);
		}
		
		boolean timed = (timeoutVal != null);
		int timeout = (timed ? ((Integer) timeoutVal).intValue() : 0);
		int timeSoFar = 0;
		
		StringBuffer sb = this.getSignalBuffer(terminal);
		synchronized (sb) {
			if (sb.length() >= num) {
				return RawSigDetectEvent.maxDetected(terminal, SymbolConvertor.convert(sb.toString()));
			}
			
			// should add support for timeout as well
			while ((sb.length() < num) && ((!timed) || (timeSoFar < timeout))) {
				try {
					sb.wait(100); // wait until another signal comes in
				}
				catch (InterruptedException ie) {
					// keep going...
				}
				timeSoFar += 100;
			}
			if (sb.length() >= num) {
				return RawSigDetectEvent.maxDetected(terminal, SymbolConvertor.convert(sb.toString()));
			}
			
			return RawSigDetectEvent.timeout(terminal, SymbolConvertor.convert(sb.toString()));
		}
	}
	
	/**
	 * This should wait until the signals have all been sent.
	 */
	@Override
	public void sendSignals(String terminal, Symbol[] syms, RTC[] rtcs, Dictionary optArgs) throws MediaResourceException {
		// look up the id for the terminal
		int id = getCallId(terminal);
		try {
			realProvider.XTSendDigits(id, SymbolConvertor.convert(syms));
		}
		catch (InvalidStateException | MethodNotSupportedException | ResourceUnavailableException e) {
			throw new MediaResourceException(e.toString());
		}
	}

	@Override
	public void stop(String terminal) {
		// look up the id for the terminal
		int id = this.getLineId(terminal);
		this.realProvider.XTStopPlaying(id);
		this.realProvider.XTStopRecording(id);
		
		int callId = this.getCallId(terminal);
		try {
			this.realProvider.XTMonitorDigits(callId, false);
		} catch (InvalidStateException ise) {
			// ignore
		} catch (MethodNotSupportedException mnse) {
			// ignore
		} catch (ResourceUnavailableException rue) {
			// ignore
		}
	}

	/**
	 * RTCs are not supported by XTAPI, but we do support stopping
	 */
	@Override
	public void triggerRTC(String terminal, Symbol action) {
		// look up the id for the terminal
		int id = getCallId(terminal);
		
		if (action.equals(PlayerConstants.rtca_Stop))
			realProvider.XTStopPlaying(id);
			
		if (action.equals(RecorderConstants.rtca_Stop))
			realProvider.XTStopRecording(id);
			
		if (action.equals(SignalDetectorConstants.rtca_Stop)) {
			int callId = getCallId(terminal);
			try {
				realProvider.XTMonitorDigits(callId, false);
			}
			catch (InvalidStateException | MethodNotSupportedException | ResourceUnavailableException e) {
				// If InvalidStateException, we tried to stop it... Otherwise, ignore
			}
		}
	}

	// Callback interface
	/**
	 * Receive the XTAPI service provider callback events and transform them
	 * into JTAPI TelephonyListener events.
	 * The delegation method also stores transformation information that is needed
	 * for other delegation methods, such as media action termination.
	 * @see IXTapiCallBack#callback(int, int, int, int, int, int)
	 */
	@Override
	public void callback(int dwDevice, int dwMessage, int dwInstance, int dwParam1, int dwParam2, int dwParam3) {
		try {
			switch (dwMessage) {
				case LINE_ADDRESSSTATE:
					break;
				case LINE_CALLINFO:
					handleLineCallInfo(dwDevice, dwInstance, dwParam1); // dwParam2 and dwParam3 have no meaning in this case
					break;
				case LINE_CALLSTATE:
					handleLineCallState(dwDevice, dwInstance, dwParam1, dwParam2, dwParam3);
					break;
				case LINE_CLOSE:
					break;
				case LINE_DEVSPECIFIC:
					break;
				case LINE_DEVSPECIFICFEATURE:
					break;
				case LINE_GATHERDIGITS:
					break;
				case LINE_GENERATE:
					break;
				case LINE_MONITORDIGITS:
					XtapiCallId cid = (XtapiCallId) callNumToCalls.get(Integer.valueOf(dwDevice));
					String address = null;
					if (cid != null) {
						address = cid.getLocalAddress();
					}
					OpenedLine openedLine = null;
					if (address != null) {
						openedLine = linesRegistry.getByAddress(address);
					}
					if (openedLine != null) {
						String terminal = openedLine.getLineDeviceName();
						char[] detectedChars = { (char) dwParam1 };
						Symbol[] syms = SymbolConvertor.convert(new String(detectedChars));
						gjListener.mediaSignalDetectorDetected(terminal, syms);
						
						// now drop the digits into the digit bucket
						storeSignals(terminal, detectedChars);
					}
					break;
				case LINE_LINEDEVSTATE:
					handleLineDevState(dwDevice, dwInstance, dwParam1, dwParam2, dwParam3);
					break;
				case LINE_MONITORMEDIA:
					break;
				case LINE_MONITORTONE:
					break;
				case LINE_REPLY:
					//m_reply = dwParam2;
					//m_device = dwInstance;
					/*
					 * TODO:
					 * - Check LINE_REPLY Data for success
					 * - Switch on current state, maybe connecting or disconnecting.
					 */
					/*
					 * evt = new XCallActiveEv(m_Call,CallActiveEv.ID,
					 * Ev.CAUSE_NEW_CALL,0,false);
					 * evlist[0] = evt;
					 * publishEvent(evlist);
					 */
					break;
				case LINE_REQUEST:
					break;
				case PHONE_BUTTON:
					break;
				case PHONE_CLOSE:
					break;
				case PHONE_DEVSPECIFIC:
					break;
				case PHONE_REPLY:
					break;
				case PHONE_STATE:
					break;
				case LINE_CREATE:
					break;
				case PHONE_CREATE:
					break;
				default:
					log.warn("UNKNOWN TAPI EVENT: " + dwMessage);
					break;
			}
		}
		catch(Exception e) {
			log.error("Exception " + e.toString() + " in callback()", e);
		}
	}
	
	private void handleLineCallInfo(int callHandle, int lineKey, int infoState) {
		log.debug(String.format("LINE_CALLINFO, callHandle=0x%X, lineKey=%d, infoState=%d",
				callHandle, lineKey, infoState));
		
		Set<LineCallInfoState> set = LineCallInfoState.fromBitField(infoState);
		if (set.contains(LineCallInfoState.CALLED_ID) || set.contains(LineCallInfoState.CALLER_ID)) {
			try {
				storeCallInformation(callHandle);
			}
			catch (Exception e) {
				log.error("Exception while handling a line call info state event", e);
			}
		}
	}
	
	private void storeCallInformation(int callHandle) throws MethodNotSupportedException, ResourceUnavailableException, InvalidStateException {
		String[] info = realProvider.XTGetCallInfo(callHandle);
		if (info != null) {
			CallInformation callInfo = new CallInformation(info);
			lineOfferingDelayer.addCallInformation(callHandle, callInfo);
			
			log.debug(String.format("XTGetCallInfo returned %s", callInfo));
		}
	}
	
	private void handleLineCallState(int callHandle,int dwInstance, int callState, int dwParam2, int dwParam3) {
		log.debug(String.format("LINE_CALLSTATE, callHandle=0x%X, lineKey=%d, callState=%s, dwParam2=%d, dwParam3=%d",
				callHandle, dwInstance, IXTapiCallBack.callStateToString(callState), dwParam2, dwParam3));
		
		Integer lineKey = Integer.valueOf(dwInstance);
		switch (callState) {
			case LINECALLSTATE_IDLE:
				invalidateCall(lineKey);
				break;
			case LINECALLSTATE_OFFERING:
				try {
					// create a call
					linesRegistry.getByDeviceNo(lineKey).ifPresent(line -> {
						XtapiCallId callId = registerCall(callHandle, line);
						// get the calling connection and notify of new connection
						gjListener.connectionAlerting(callId, line.getAddressName(),
								Event.CAUSE_NORMAL);
						gjListener.terminalConnectionRinging(callId, line.getAddressName(),
								line.getLineDeviceName(), Event.CAUSE_NORMAL);
						
						lineOfferingDelayer.addCallId(callHandle, callId);
					});
				}
				catch (Exception e) {
					log.error("Exception in LINECALLSTATE_OFFERING: " + e.toString(), e);
				}
				break;
			case LINECALLSTATE_ACCEPTED:
				break;
			case LINECALLSTATE_DIALTONE:
				break;
			case LINECALLSTATE_DIALING:
				break;
			case LINECALLSTATE_RINGBACK:
				// event.add(new XEv(ConnAlertingEv.ID,Ev.CAUSE_NEW_CALL, 0,false));
				break;
			case LINECALLSTATE_BUSY:
				break;
			case LINECALLSTATE_SPECIALINFO:
				break;
			case LINECALLSTATE_CONNECTED:
				try {
					XtapiCallId callId = lineToCalls.get(lineKey);
					if (callId != null) {
						linesRegistry.getByDeviceNo(lineKey).ifPresent(line -> {
							gjListener.connectionConnected(callId, line.getAddressName(),
									Event.CAUSE_NORMAL);
							gjListener.terminalConnectionTalking(callId, line.getAddressName(),
									line.getLineDeviceName(), Event.CAUSE_NORMAL);
						});
						// now note that the remote connection is connected
						String remoteAddress = callId.getRemoteAddress();
						if (remoteAddress != null) {
							gjListener.connectionConnected(callId, remoteAddress, Event.CAUSE_NORMAL);
						}
					}
				}
				catch (Exception e) {
					log.error("LINECALLSTATE_CONNECTED exception: " + e.toString(), e);
				}
				break;
			case LINECALLSTATE_PROCEEDING:
				// Outgoing call invokes two JTAPI Connection events.
				try {
					XtapiCallId callId = lineToCalls.get(lineKey);
					if (callId != null) {
						linesRegistry.getByDeviceNo(lineKey).ifPresent(line -> {
							gjListener.connectionInProgress(callId, line.getAddressName(),
									Event.CAUSE_NORMAL);
							gjListener.connectionAlerting(callId, line.getAddressName(),
									Event.CAUSE_NORMAL);
						});
					}
				}
				catch (Exception e) {
					log.error("LINECALLSTATE_PROCEEDING exception: " + e.toString(), e);
				}
				break;
			case LINECALLSTATE_ONHOLD:
				break;
			case LINECALLSTATE_CONFERENCED:
				break;
			case LINECALLSTATE_ONHOLDPENDCONF:
				break;
			case LINECALLSTATE_ONHOLDPENDTRANSFER:
				break;
			case LINECALLSTATE_DISCONNECTED:
				try {
					XtapiCallId callId = lineToCalls.get(lineKey);
					if (callId != null) {
						disconnect(lineKey, callId);
					}
					// remove potentially lingering call information
					lineOfferingDelayer.remove(callHandle);
				}
				catch (Exception e) {
					log.error("LINECALLSTATE_IDLE exception: " + e.toString(), e);
				}
				break;
			case LINECALLSTATE_UNKNOWN:
				break;
			default:
				break;
		}
	}
	
	private XtapiCallId registerCall(int callHandle, OpenedLine line) {
		XtapiCallId callId = new XtapiCallId();
		callId.setCallNum(callHandle);
		callNumToCalls.put(Integer.valueOf(callHandle), callId);
		lineToCalls.put(Integer.valueOf(line.getLineDeviceNo()), callId); 
		termToCalls.put(line.getLineDeviceName(), callId);
		return callId;
	}
	
	/**
	 * At the end of this the call is invalid and all connections have the state
	 * {@link Connection#DISCONNECTED}
	 * 
	 * @param lineKey the identifier of the line on which the call should be invalidated
	 */
	private void invalidateCall(Integer lineKey) {
		try {
			XtapiCallId callId = lineToCalls.get(lineKey);
			if (callId != null) {
				linesRegistry.getByDeviceNo(lineKey)
						.ifPresent(line -> disconnectLocalConnection(callId, line));
				disconnectRemoteConnection(callId);
			}
		}
		catch (Exception e) {
			log.error("LINECALLSTATE_IDLE exception: " + e.toString(), e);
		}
	}
	
	private void disconnect(Integer lineKey, XtapiCallId callId) {
		dropLocalConnection(lineKey, callId);
		disconnectRemoteConnection(callId);
		
		lineToCalls.remove(lineKey);
		callNumToCalls.remove(callId.getCallNum());
	}
	
	private void dropLocalConnection(Integer lineKey, XtapiCallId callId) {
		linesRegistry.getByDeviceNo(lineKey).ifPresent(openedLine -> {
			gjListener.terminalConnectionDropped(callId, openedLine.getAddressName(),
					openedLine.getLineDeviceName(), Event.CAUSE_NORMAL);
			disconnectLocalConnection(callId, openedLine);
			termToCalls.remove(openedLine.getLineDeviceName());
		});
	}
	
	private void disconnectLocalConnection(XtapiCallId callId, OpenedLine line) {
		gjListener.connectionDisconnected(callId, line.getAddressName(), Event.CAUSE_NORMAL);
	}
	
	private void disconnectRemoteConnection(XtapiCallId callId) {
		String remoteAddress = callId.getRemoteAddress();
		if (remoteAddress != null) {
			gjListener.connectionDisconnected(callId, remoteAddress, Event.CAUSE_NORMAL);
		}
	}
	
	private void handleLineDevState(int dwDevice, int dwInstance, int dwParam1, int dwParam2, int dwParam3) {
		switch(dwParam1) {
			case LINEDEVSTATE_OTHER:
				break;
			case LINEDEVSTATE_RINGING:
				// this may be for subsequent rings, in which case we don't need it.
				// dwParam3 contains the ring count.
				XtapiCallId xCallId = lineToCalls.get(Integer.valueOf(dwInstance));
				if (xCallId != null) {
					linesRegistry.getByDeviceNo(dwInstance).ifPresent(line -> {
						gjListener.connectionAlerting(xCallId, line.getAddressName(),
								Event.CAUSE_NEW_CALL);
						gjListener.terminalConnectionRinging(xCallId, line.getAddressName(),
								line.getLineDeviceName(), Event.CAUSE_NORMAL);
					});
				}
				break;
			case LINEDEVSTATE_CONNECTED:
				break;
			case LINEDEVSTATE_DISCONNECTED:
				break;
			case LINEDEVSTATE_MSGWAITON:
				break;
			case LINEDEVSTATE_MSGWAITOFF:
				break;
			case LINEDEVSTATE_INSERVICE:
				break;
			case LINEDEVSTATE_OUTOFSERVICE:
				break;
			case LINEDEVSTATE_MAINTENANCE:
				break;
			case LINEDEVSTATE_OPEN:
				break;
			case LINEDEVSTATE_CLOSE:
				break;
			case LINEDEVSTATE_NUMCALLS:
				break;
			case LINEDEVSTATE_NUMCOMPLETIONS:
				break;
			case LINEDEVSTATE_TERMINALS:
				break;
			case LINEDEVSTATE_ROAMMODE:
				break;
			case LINEDEVSTATE_BATTERY:
				break;
			case LINEDEVSTATE_SIGNAL:
				break;
			case LINEDEVSTATE_DEVSPECIFIC:
				break;
			case LINEDEVSTATE_REINIT:
				break;
			case LINEDEVSTATE_LOCK:
				break;
			case LINEDEVSTATE_CAPSCHANGE:
				break;
			case LINEDEVSTATE_CONFIGCHANGE:
				break;
			case LINEDEVSTATE_TRANSLATECHANGE:
				break;
			case LINEDEVSTATE_COMPLCANCEL:
				break;
			case LINEDEVSTATE_REMOVED:
				break;
			default:
				break; 
		}
	}
	// end of interface implementation
	
	/**
	 * Helper function for getting a call id from a terminal name
	 */
	private int getCallId(String terminal) {
		return ((XtapiCallId)this.termToCalls.get(terminal)).getCallNum();
	}
	
	/**
	 * Helper function for getting a line id from a terminal name
	 */
	private int getLineId(String terminal) {
		return linesRegistry.getByDeviceName(terminal)
				.orElseThrow(() -> new IllegalArgumentException("Terminal not found: " + terminal))
				.getLineHandle();
	}
	
	@Override
	public String toString() {
		return "GJTAPI bridge to XTAPI service provider: " + realProvider.toString();
	}
	
	/**
	 * Stores signals for a terminal and notifies any waiting
	 * threads of the arrival of the digits.
	 * @param terminalName
	 * @param digits
	 */
	private void storeSignals(String terminalName, char[] digits) {
		StringBuffer sb = this.getSignalBuffer(terminalName);
		synchronized (sb) {
			sb.append(digits);
			
			// now we notify anyone waiting for digits
			sb.notify();
		}
	}

	/**
	 * Retrieves the latest uncollected signals on a
	 * terminal and clears the terminal signal bucket.
	 * @param terminalName
	 * @return
	 */
	private String retrieveSignals(String terminalName) {
		StringBuffer sb = this.getSignalBuffer(terminalName);
		synchronized (sb) {
			String signals = sb.toString();
			sb.delete(0, sb.length());
			return signals;
		}
	}
	
	/**
	 * Get a StringBuffer for a terminal, used to store
	 * the latest unretrieved signals on that terminal.
	 * @param terminalName
	 * @return A StringBuffer holding signals for the terminal.
	 */
	private StringBuffer getSignalBuffer(String terminalName) {
		Map<String, StringBuffer> bucketSet = this.termToBuckets;
		StringBuffer signalBuffer = bucketSet.get(terminalName);
		if (signalBuffer == null) {
			synchronized (bucketSet) {
				// try again to avoid double creation error
				signalBuffer = bucketSet.get(terminalName);
				if (signalBuffer == null) {
					signalBuffer = new StringBuffer();
					bucketSet.put(terminalName, signalBuffer);
				}
			}
		}
		return signalBuffer;
	}
}
