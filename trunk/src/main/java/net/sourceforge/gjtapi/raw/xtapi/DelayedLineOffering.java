package net.sourceforge.gjtapi.raw.xtapi;

import javax.telephony.Event;

import lombok.Setter;
import net.sourceforge.gjtapi.TelephonyListener;

/**
 * @author haf
 */
class DelayedLineOffering implements DelayedEvent {
	
	private @Setter XtapiCallId callId;
	private @Setter CallInformation callInformation;
	private boolean isClosed = false;
	
	private void checkState() {
		if (!isComplete()) {
			throw new IllegalStateException("Missing information to fire a line offering event");
		}
		
		if (isClosed) {
			throw new IllegalStateException("Trying to fire an already fired line offering event");
		}
	}
	
	@Override
	public boolean isComplete() {
		return callId != null && callInformation != null;
	}
	
	@Override
	public void fire(TelephonyListener listener) {
		checkState();
		isClosed = true;
		
		callId.setRemoteAddress(callInformation.getNumber());
		if (listener != null) {
			fireConnectionEvents(listener);
		}
	}
	
	private void fireConnectionEvents(TelephonyListener listener) {
		if (callInformation.getNumber() != null) {
			createConnectedRemoteConnection(listener);
		}
		if (callInformation.getTerminalName() != null) {
			createActiveRemoteTerminalConnection(listener);
		}
	}
	
	private void createConnectedRemoteConnection(TelephonyListener listener) {
		listener.connectionConnected(callId, callInformation.getNumber(), Event.CAUSE_NORMAL);
	}
	
	private void createActiveRemoteTerminalConnection(TelephonyListener listener) {
		listener.terminalConnectionTalking(callId, callInformation.getNumber(),
				callInformation.getTerminalName(), Event.CAUSE_NORMAL);
	}
}
