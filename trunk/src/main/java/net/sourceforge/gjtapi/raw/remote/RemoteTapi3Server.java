package net.sourceforge.gjtapi.raw.remote;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.AlreadyBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Properties;

import net.sourceforge.gjtapi.raw.CoreTpi;
import net.sourceforge.gjtapi.raw.ProviderFactory;
import net.sourceforge.gjtapi.raw.xtapi.XtapiProvider;

public class RemoteTapi3Server {
	public static void main(String[] args) throws AlreadyBoundException, IOException {
		Registry reg = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
		CoreTpi provider = new XtapiProvider();
		provider.initialize(null);
		RemoteProviderImpl remoteProvider = new RemoteProviderImpl(ProviderFactory.createProvider(provider));
		reg.bind("Xtapi", remoteProvider);
	}
}
