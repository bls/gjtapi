package net.sourceforge.gjtapi.raw.xtapi;

/**
 * @author haf
 */
interface DelayedEventFactory {
	
	public DelayedEvent create();
}
