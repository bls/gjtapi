package net.sourceforge.gjtapi.raw.xtapi;

import lombok.Data;

/**
 * @author haf
 */
@Data
final class CallInformation {
	
	private final String terminalName;
	private final String number;
	
	CallInformation(String[] information) {
		this(information[0], information[1]);
	}
	
	CallInformation(String terminalName, String number) {
		this.terminalName = terminalName;
		this.number = number;
	}
}
