package net.sourceforge.gjtapi.raw.remote;

/*
	Copyright (c) 2002 8x8 Inc. (www.8x8.com) 

	All rights reserved. 

	Permission is hereby granted, free of charge, to any person obtaining a 
	copy of this software and associated documentation files (the 
	"Software"), to deal in the Software without restriction, including 
	without limitation the rights to use, copy, modify, merge, publish, 
	distribute, and/or sell copies of the Software, and to permit persons 
	to whom the Software is furnished to do so, provided that the above 
	copyright notice(s) and this permission notice appear in all copies of 
	the Software and that both the above copyright notice(s) and this 
	permission notice appear in supporting documentation. 

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT 
	OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
	HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL 
	INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING 
	FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
	NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION 
	WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. 

	Except as contained in this notice, the name of a copyright holder 
	shall not be used in advertising or otherwise to promote the sale, use 
	or other dealings in this Software without prior written authorization 
	of the copyright holder.
*/
import java.io.NotSerializableException;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.server.Unreferenced;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import javax.telephony.InvalidArgumentException;
import javax.telephony.InvalidPartyException;
import javax.telephony.MethodNotSupportedException;
import javax.telephony.PrivilegeViolationException;
import javax.telephony.ProviderUnavailableException;
import javax.telephony.ResourceUnavailableException;
import javax.telephony.media.MediaResourceException;
import javax.telephony.media.RTC;
import javax.telephony.media.Symbol;

import lombok.extern.apachecommons.CommonsLog;
import net.sourceforge.gjtapi.CallData;
import net.sourceforge.gjtapi.CallId;
import net.sourceforge.gjtapi.RawSigDetectEvent;
import net.sourceforge.gjtapi.RawStateException;
import net.sourceforge.gjtapi.TelephonyProvider;
import net.sourceforge.gjtapi.TermData;
import net.sourceforge.gjtapi.media.RTCHolder;
import net.sourceforge.gjtapi.media.SymbolHolder;
import net.sourceforge.gjtapi.raw.RawListenerMux;
import net.sourceforge.gjtapi.raw.RawListenerPool;

/**
 * This is a server-side implementation for a RemoteProvider.  It finishes the job off of
 * delegating requests from the client to a real RawProvider.
 * Creation date: (2000-02-17 13:07:36)
 * 
 * @author: Richard Deadman
 */
@CommonsLog
public class RemoteProviderImpl extends UnicastRemoteObject implements Unreferenced, RemoteProvider {
	
	static final long serialVersionUID = 4108831180888441168L;
	
	private TelephonyProvider delegate;
	private final CallMapper refMapper = new CallMapper();
	private final RawListenerMux callbackMux = new RawListenerMux(20, 10);
	
	/**
	 * Create a new instance of the remote service, wrapping a RawProvider.
	 * Creation date: (2000-02-17 13:32:03)
	 * 
	 * @author: Richard Deadman
	 * @param rp net.sourceforge.gjtapi.RawProvider
	 * @exception RawException The provider was null.
	 * @exception RemoteException Error creating remote object.
	 */
	public RemoteProviderImpl(TelephonyProvider rp) throws RemoteException {
		this(rp, 0);
	}
	
	public RemoteProviderImpl(TelephonyProvider rp, int port) throws RemoteException {
		super(port);
		
		if (rp == null) {
			throw new NullPointerException();
		}
		
		delegate = rp;
		// register my listener (wrapped in a event dispatch pool)
		rp.addListener(new RawListenerPool(callbackMux));
	}
	
	/**
	 * Delegate the add observer command off to the real provider.
	 */
	@Override
	public void addListener(RemoteListener rl)  {
		callbackMux.addListener(new RemoteListenerWrapper(rl, refMapper));
		log.info("client listener added: " + rl);
	}
	
	/**
	 * Forward the request on to the local RawProvider
	 */
	@Override
	public boolean allocateMedia(String terminal, int type, Dictionary params) {
		return getDelegate().allocateMedia(terminal, type, fromSerializable(params));
	}
	
	/**
	 * Delegate off to remote provider
	 */
	@Override
	public void answerCall(SerializableCallId call, String address, String terminal) throws PrivilegeViolationException, ResourceUnavailableException, MethodNotSupportedException, RawStateException {
		getDelegate().answerCall(getProvCall(call), address, terminal);
	}
	
	@Override
	public boolean attachMedia(SerializableCallId call, String address, boolean onFlag) throws java.rmi.RemoteException {
		return getDelegate().attachMedia(getProvCall(call), address, onFlag);
	}
	
	@Override
	public void beep(SerializableCallId call) throws java.rmi.RemoteException {
		getDelegate().beep(getProvCall(call));
	}
	
	/**
	 * delegate on createCall
	 */
	@Override
	public SerializableCallId createCall(SerializableCallId id, String address, String term, String dest) throws ResourceUnavailableException, PrivilegeViolationException, InvalidPartyException, InvalidArgumentException, MethodNotSupportedException, RawStateException {
		TelephonyProvider rp = getDelegate();
		return refMapper.swapId(rp.createCall(getProvCall(id), address, term, dest));
	}
	
	/**
	 * Clean up
	 * Creation date: (2000-04-26 16:09:25)
	 * 
	 * @author: Richard Deadman
	 */
	@Override
	public void finalize() {
		unreferenced();
	}
	
	/**
	 * Forward the request on to the local RawProvider
	 */
	@Override
	public boolean freeMedia(String terminal, int type) {
		return getDelegate().freeMedia(terminal, type);
	}
	
	/**
	 * Replace each serializable SymbolHolder key or value with its held Symbol.
	 * Creation date: (2000-03-13 9:34:39)
	 * 
	 * @author: Richard Deadman
	 * @param dict A dictionary of parameters and values that control the usage of the resource.  This should move to a Map later.
	 * @return A clone that now holds non-serializable Symbols again.
	 */
	private static Dictionary fromSerializable(Dictionary dict) {
		if (dict == null) {
			return null;
		}
		Hashtable table = new Hashtable();
		Enumeration keys = dict.keys();
		while (keys.hasMoreElements()) {
			Object k = keys.nextElement();
			Object v = dict.get(k);
			if (k instanceof SymbolHolder) {
				k = ((SymbolHolder) k).getSymbol();
			}
			if (v instanceof SymbolHolder) {
				v = ((SymbolHolder) v).getSymbol();
			}
			table.put(k, v);
		}
		return table;
	}
	
	/**
	 * Delegate remote getAddresses to real provider.
	 */
	@Override
	public String[] getAddresses() throws RemoteException, ResourceUnavailableException {
		TelephonyProvider rp = getDelegate();
		
		if (rp != null) {
			return rp.getAddresses();
		}
		
		throw new RemoteException();
	}
	
	/**
	 * Delegate remote getTerminals method to real provider.
	 */
	@Override
	public String[] getAddresses(String terminal) throws RemoteException, InvalidArgumentException {
		TelephonyProvider rp = getDelegate();
		
		if (rp != null) {
			return rp.getAddresses(terminal);
		}
		
		throw new RemoteException();
	}
	
	@Override
	public int getAddressType(String name) throws java.rmi.RemoteException {
		return getDelegate().getAddressType(name);
	}
	
	/**
	 * Delegate off to real TelephonyProvider.
	 */
	@Override
	public CallData getCall(CallId id) throws RemoteException  {
		TelephonyProvider rp = getDelegate();
		
		if (rp != null) {
			CallData cd = rp.getCall(id);
			return new CallData(refMapper.swapId(cd.id), cd.callState, cd.connections);
		}
		
		throw new RemoteException();
	}
	
	@Override
	public CallData[] getCallsOnAddress(String number) throws RemoteException {
		TelephonyProvider rp = getDelegate();
		
		if (rp != null) {
			return toSerialCallData(rp.getCallsOnAddress(number));
		}
		
		throw new RemoteException();
	}
	
	/**
	 * Delegate on to real TelephonyProvider.
	 */
	@Override
	public CallData[] getCallsOnTerminal(String name) throws RemoteException  {
		TelephonyProvider rp = getDelegate();
		
		if (rp != null) {
			return toSerialCallData(rp.getCallsOnTerminal(name));
		}
		
		throw new RemoteException();
	}
	
	/**
	 * Helper method to turn an array of CallData objects into a Serialized version.
	 * 
	 * @param cds The CallData objects to make serializable
	 * @return CallData[] A clone with the internal CallIds replaced with their SerializableCallIds.
	 */
	private CallData[] toSerialCallData(CallData[] cds) {
		if (cds == null) {
			return null;
		}
		int len = cds.length;
		CallData[] newCds = new CallData[len];
		for (int i = 0; i < len; i++) {
			CallData cd = cds[i];
			newCds[i] = new CallData(refMapper.swapId(cd.id), cd.callState, cd.connections);
		}
		return newCds;
	}
	
	@Override
	public Properties getCapabilities() throws RemoteException {
		TelephonyProvider rp = getDelegate();
		
		if (rp != null) {
			return rp.getCapabilities();
		}
		
		throw new RemoteException();
	}
	
	/**
	 * Get the delegate that actually performs the TelephonyProvider operations.
	 * Creation date: (2000-02-17 13:24:02)
	 * 
	 * @author: Richard Deadman
	 * @return A TelephonyProvider
	 */
	protected TelephonyProvider getDelegate() {
		return delegate;
	}
	
	/**
	 * Return the dialed digits for a connection identified by a CallId and address.
	 */
	@Override
	public String getDialledDigits(SerializableCallId id, String address) throws java.rmi.RemoteException {
		return getDelegate().getDialledDigits(getProvCall(id), address);
	}
	
	/**
	 * Forward privateData access call to real TPI Provider.
	 */
	@Override
	public Serializable getPrivateData(CallId call, String address, String terminal) throws NotSerializableException, RemoteException {
		TelephonyProvider rp = getDelegate();
		
		if (rp != null) {
			Object o = rp.getPrivateData(call, address, terminal);
			if (!(o instanceof Serializable)) {
				throw new NotSerializableException();
			}
			return (Serializable) o;
		}
		
		throw new RemoteException();
	}
	
	/**
	 * Simple utility function for looking up a provider id
	 * Creation date: (2000-02-18 0:16:44)
	 * 
	 * @author: Richard Deadman
	 * @param id The transmission SerializableCallId proxy reference
	 * @return The provider id
	 */
	private CallId getProvCall(SerializableCallId id) {
		return refMapper.providerId(id);
	}
	
	/**
	 * Delegate on to real TelephonyProvider.
	 */
	@Override
	public TermData[] getTerminals() throws RemoteException, ResourceUnavailableException  {
		TelephonyProvider rp = getDelegate();
		
		if (rp != null) {
			return rp.getTerminals();
		}
		
		throw new RemoteException();
	}
	
	/**
	 * Delegate remote getTerminals method to real provider.
	 */
	@Override
	public TermData[] getTerminals(String address) throws RemoteException, InvalidArgumentException {
		TelephonyProvider rp = getDelegate();
		
		if (rp != null) {
			return rp.getTerminals(address);
		}
		
		throw new RemoteException();
	}
	
	/**
	 * Delegate remote hold message to real provider.
	 */
	@Override
	public void hold(SerializableCallId call, String address, String term) throws RawStateException, MethodNotSupportedException, PrivilegeViolationException, ResourceUnavailableException {
		getDelegate().hold(getProvCall(call), address, term);
	}
	
	/**
	 * delegate remote initialize message to real provider.
	 */
	@Override
	public void initialize(Map props) throws ProviderUnavailableException {
		getDelegate().initialize(props);
	}
	
	/**
	 * Forward the request on to the local RawProvider
	 */
	@Override
	public boolean isMediaTerminal(String terminal) {
		return getDelegate().isMediaTerminal(terminal);
	}
	
	/**
	 * Delegate remote join message to real provider.
	 */
	@Override
	public CallId join(SerializableCallId call1, SerializableCallId call2, String address, String terminal) throws RawStateException, InvalidArgumentException, MethodNotSupportedException, PrivilegeViolationException, ResourceUnavailableException {
		
		return refMapper.swapId(getDelegate().join(getProvCall(call1), getProvCall(call2), address,
				terminal));
	}
	
	/**
	 * Forward the request on to the local RawProvider
	 */
	@Override
	public void play(String terminal, String[] streamIds, int offset, RTCHolder[] holders, Dictionary optArgs) throws MediaResourceException {
		RTC[] rtcs = new RTC[holders.length];
		for (int i = 0; i < holders.length; i++) {
			rtcs[i] = holders[i].getRTC();
		}
		getDelegate().play(terminal, streamIds, offset, rtcs, fromSerializable(optArgs));
	}
	
	/**
	 * Forward the request on to the local RawProvider
	 */
	@Override
	public void record(String terminal, String streamId, RTCHolder[] holders, Dictionary optArgs) throws MediaResourceException {
		RTC[] rtcs = new RTC[holders.length];
		for (int i = 0; i < holders.length; i++) {
			rtcs[i] = holders[i].getRTC();
		}
		getDelegate().record(terminal, streamId, rtcs, fromSerializable(optArgs));
	}
	
	/**
	 * Delegate remote release message to real provider.
	 */
	@Override
	public void release(String address, SerializableCallId call) throws PrivilegeViolationException, ResourceUnavailableException, MethodNotSupportedException, RawStateException {
		getDelegate().release(address, getProvCall(call));
		refMapper.freeId(call);
	}
	
	@Override
	public void releaseCallId(SerializableCallId id) throws RemoteException {
		getDelegate().releaseCallId(getProvCall(id));
		refMapper.freeId(id);
	}
	
	/**
	 * Delegate removeObserver message to real provider
	 */
	@Override
	public void removeListener(RemoteListener rl) {
		callbackMux.removeListener(new RemoteListenerWrapper(rl, refMapper));
		log.info("client listener removed: " + rl);
	}
	
	/**
	 * Forward the request on to the local RawProvider
	 * <P>Since we don't track which Frameworks monitor a call, only allow turning this on.  Note
	 * that multiple Generic Framework clients may have registered for reporting of this call.
	 */
	@Override
	public void reportCallsOnAddress(String address, boolean flag) throws InvalidArgumentException, ResourceUnavailableException {
		if (flag) {
			getDelegate().reportCallsOnAddress(address, flag);
		}
	}
	
	/**
	 * Forward the request on to the local RawProvider
	 * <P>Since we don't track which Frameworks monitor a call, only allow turning this on.  Note
	 * that multiple Generic Framework clients may have registered for reporting of this call.
	 */
	@Override
	public void reportCallsOnTerminal(String terminal, boolean flag) throws InvalidArgumentException, ResourceUnavailableException {
		if (flag) {
			getDelegate().reportCallsOnTerminal(terminal, flag);
		}
	}
	
	/**
	 * Delegate remote reserveCallId message to real provider.
	 */
	@Override
	public SerializableCallId reserveCallId(String address) throws RemoteException, InvalidArgumentException {
		TelephonyProvider rp = getDelegate();
		
		if (rp != null) {
			return refMapper.swapId(rp.reserveCallId(address));
		}
		
		throw new RemoteException();
	}
	
	/**
	 * Forward the request on to the local RawProvider
	 */
	@Override
	public RawSigDetectEvent retrieveSignals(String terminal, int num, SymbolHolder[] patHolders, RTCHolder[] rtcHolders, Dictionary optArgs) throws MediaResourceException {
		RTC[] rtcs = new RTC[rtcHolders.length];
		for (int i = 0; i < rtcHolders.length; i++) {
			rtcs[i] = rtcHolders[i].getRTC();
		}
		Symbol[] patterns = new Symbol[patHolders.length];
		for (int i = 0; i < patHolders.length; i++) {
			patterns[i] = patHolders[i].getSymbol();
		}
	
		return getDelegate().retrieveSignals(terminal, num, patterns, rtcs, fromSerializable(optArgs));
	}
	
	/**
	 * Forward sendPrivateData method to real TPI Provider.
	 */
	@Override
	public Serializable sendPrivateData(CallId call, String address, String terminal, Serializable data) throws NotSerializableException, RemoteException {
		TelephonyProvider rp = getDelegate();
		
		if (rp != null) {
			Object o = rp.sendPrivateData(call, address, terminal, data);
			if (!(o instanceof Serializable)) {
				throw new NotSerializableException();
			}
			
			return (Serializable) o;
		}
		
		throw new RemoteException();
	}
	
	/**
	 * Forward the request on to the local RawProvider
	 */
	@Override
	public void sendSignals(String terminal, SymbolHolder[] symHolders, RTCHolder[] rtcHolders, Dictionary optArgs) throws MediaResourceException {
		RTC[] rtcs = new RTC[rtcHolders.length];
		for (int i = 0; i < rtcHolders.length; i++) {
			rtcs[i] = rtcHolders[i].getRTC();
		}
		Symbol[] syms = new Symbol[symHolders.length];
		for (int i = 0; i < symHolders.length; i++) {
			syms[i] = symHolders[i].getSymbol();
		}
	
		getDelegate().sendSignals(terminal,syms, rtcs, fromSerializable(optArgs));
	}
	
	/**
	 * setLoadControl method comment.
	 */
	@Override
	public void setLoadControl(String startAddr, String endAddr, double duration, double admissionRate, double interval, int[] treatment) throws RemoteException, MethodNotSupportedException {
		getDelegate().setLoadControl(startAddr, endAddr, duration, admissionRate, interval, treatment);
	}
	
	/**
	 * Forward setPrivateData method to real TPI Provider.
	 */
	@Override
	public void setPrivateData(CallId call, String address, String terminal, Serializable data) throws RemoteException {
		TelephonyProvider rp = getDelegate();
		
		if (rp != null) {
			rp.setPrivateData(call, address, terminal, data);
		}
		else {
			throw new RemoteException();
		}
	}
	
	/**
	 * Eat remote shutdown message -- since more than one client may be connected.
	 * The provider should handle shutdown from finalize or Unreferenced.
	 */
	@Override
	public void shutdown() {
	}
	
	/**
	 * Receive a remote stop media request
	 */
	@Override
	public void stop(String terminal) {
		getDelegate().stop(terminal);
	}
	
	/**
	 * Eat request to the local TelephonyProvider
	 * <P>Since we don't track which Frameworks monitor a call, we should eat this message, even though that will mean
	 * any call once tracked will always be reported on.  Note
	 * that multiple Generic Framework clients may have registered for reporting of this call.
	 */
	@Override
	public boolean stopReportingCall(SerializableCallId call)  {
		/*TelephonyProvider rp = this.getDelegate();
		
		if (rp != null)
			return rp.stopReportingCall(call);
		else
			throw new RemoteException();
		*/
		return true;
	}
	
	@Override
	public String toString() {
		return "RemoteProvider implementation wrapping a RawProvider: " + getDelegate();
	}
	
	/**
	 * Receive and forward a triggerRTC remote command.
	 */
	@Override
	public void triggerRTC(String terminal, SymbolHolder action) {
		getDelegate().triggerRTC(terminal, action.getSymbol());
	}
	
	/**
	 * Delegate remote unHold message to real provider
	 */
	@Override
	public void unHold(SerializableCallId call, String address, String term) throws RawStateException, MethodNotSupportedException, PrivilegeViolationException, ResourceUnavailableException {
		getDelegate().unHold(getProvCall(call), address, term);
	}
	
	/**
	 * Clean up when the clients are all removed.
	 */
	@Override
	public void unreferenced() {
		TelephonyProvider rp = getDelegate();
		
		if (rp != null) {
			rp.removeListener(callbackMux);
			rp.shutdown();
		}
	}
}
