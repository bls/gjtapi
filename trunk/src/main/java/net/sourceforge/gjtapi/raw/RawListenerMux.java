package net.sourceforge.gjtapi.raw;

/*
	Copyright (c) 2002 8x8 Inc. (www.8x8.com) 

	All rights reserved. 

	Permission is hereby granted, free of charge, to any person obtaining a 
	copy of this software and associated documentation files (the 
	"Software"), to deal in the Software without restriction, including 
	without limitation the rights to use, copy, modify, merge, publish, 
	distribute, and/or sell copies of the Software, and to permit persons 
	to whom the Software is furnished to do so, provided that the above 
	copyright notice(s) and this permission notice appear in all copies of 
	the Software and that both the above copyright notice(s) and this 
	permission notice appear in supporting documentation. 

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT 
	OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
	HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL 
	INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING 
	FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
	NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION 
	WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. 

	Except as contained in this notice, the name of a copyright holder 
	shall not be used in advertising or otherwise to promote the sale, use 
	or other dealings in this Software without prior written authorization 
	of the copyright holder.
*/
import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.function.Consumer;

import javax.telephony.media.Symbol;

import lombok.extern.apachecommons.CommonsLog;
import net.sourceforge.gjtapi.CallId;
import net.sourceforge.gjtapi.TelephonyListener;
import net.sourceforge.gjtapi.raw.remote.RemoteListenerWrapper;
/**
 * This is a simple RawListener broadcaster that allows multiple remote RawListeners to appear as one
 * to the RawProvider.  This simplifies RawProvider construction since they now only have to worry about
 * holding onto one RawListener, not a set.
 * Creation date: (2000-04-26 15:43:02)
 * 
 * @author: Richard Deadman
 */
@CommonsLog
public class RawListenerMux implements TelephonyListener {
	
	private final Set<RemoteListenerWrapper> listeners = new HashSet<>();
	/**
	 * Amount of allowed remote exceptions of a listener before it will be removed automatically
	 */
	private int maxRemoteExceptions;
	/**
	 * Amount of seconds a remote listener must be unreachable before it is removed automatically
	 */
	private long maxRemoteUnreachableSeconds;
	
	public RawListenerMux(int allowedExceptions, long allowedUnreachableSeconds) {
		this.maxRemoteExceptions = allowedExceptions;
		this.maxRemoteUnreachableSeconds = allowedUnreachableSeconds;
	}
	
	/**
	 * Add to the set of Raw Listeners I broadcast to.
	 * Creation date: (2000-04-26 16:01:55)
	 * 
	 * @author: Richard Deadman
	 * @param l A new TelphonyListener to receive callbacks
	 */
	public void addListener(RemoteListenerWrapper l) {
		listeners.add(l);
	}
	
	/**
	 * Removes a Raw Listener from the set to which to broadcast.
	 * Creation date: (2000-04-26 16:01:55)
	 * 
	 * @author: Richard Deadman
	 * @param l A new RawListener to remove
	 */
	public void removeListener(RemoteListenerWrapper l) {
		listeners.remove(l);
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void addressPrivateData(String address, Serializable data, int cause) {
		actOrRemoveOnListeners(listener -> listener.addressPrivateData(address, data, cause));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void callActive(CallId id, int cause) {
		actOrRemoveOnListeners(listener -> listener.callActive(id, cause));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void callInvalid(CallId id, int cause) {
		actOrRemoveOnListeners(listener -> listener.callInvalid(id, cause));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void callOverloadCeased(String address) {
		actOrRemoveOnListeners(listener -> listener.callOverloadCeased(address));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void callOverloadEncountered(String address) {
		actOrRemoveOnListeners(listener -> listener.callOverloadEncountered(address));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void callPrivateData(CallId call, Serializable data, int cause) {
		actOrRemoveOnListeners(listener -> listener.callPrivateData(call, data, cause));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void connectionAddressAnalyse(CallId id, String address, int cause) {
		actOrRemoveOnListeners(listener -> listener.connectionAddressAnalyse(id, address, cause));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void connectionAddressCollect(CallId id, String address, int cause) {
		actOrRemoveOnListeners(listener -> listener.connectionAddressCollect(id, address, cause));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void connectionAlerting(CallId id, String address, int cause) {
		actOrRemoveOnListeners(listener -> listener.connectionAlerting(id, address, cause));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void connectionAuthorizeCallAttempt(CallId id, String address, int cause) {
		actOrRemoveOnListeners(listener -> listener.connectionAuthorizeCallAttempt(id, address, cause));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void connectionCallDelivery(CallId id, String address, int cause) {
		actOrRemoveOnListeners(listener -> listener.connectionCallDelivery(id, address, cause));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void connectionConnected(CallId id, String address, int cause) {
		actOrRemoveOnListeners(listener -> listener.connectionConnected(id, address, cause));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void connectionDisconnected(CallId id, String address, int cause) {
		actOrRemoveOnListeners(listener -> listener.connectionDisconnected(id, address, cause));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void connectionFailed(CallId id, String address, int cause) {
		actOrRemoveOnListeners(listener -> listener.connectionFailed(id, address, cause));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void connectionInProgress(CallId id, String address, int cause) {
		actOrRemoveOnListeners(listener -> listener.connectionInProgress(id, address, cause));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void connectionSuspended(CallId id, String address, int cause) {
		actOrRemoveOnListeners(listener -> listener.connectionSuspended(id, address, cause));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void mediaPlayPause(String terminal, int index, int offset, Symbol trigger) {
		actOrRemoveOnListeners(listener -> listener.mediaPlayPause(terminal, index, offset, trigger));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void mediaPlayResume(String terminal, Symbol trigger) {
		actOrRemoveOnListeners(listener -> listener.mediaPlayResume(terminal, trigger));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void mediaRecorderPause(String terminal, int duration, Symbol trigger) {
		actOrRemoveOnListeners(listener -> listener.mediaRecorderPause(terminal, duration, trigger));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void mediaRecorderResume(String terminal, Symbol trigger) {
		actOrRemoveOnListeners(listener -> listener.mediaRecorderResume(terminal, trigger));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void mediaSignalDetectorDetected(String terminal, Symbol[] sigs) {
		actOrRemoveOnListeners(listener -> listener.mediaSignalDetectorDetected(terminal, sigs));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void mediaSignalDetectorOverflow(String terminal, Symbol[] sigs) {
		actOrRemoveOnListeners(listener -> listener.mediaSignalDetectorOverflow(terminal, sigs));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void mediaSignalDetectorPatternMatched(String terminal, Symbol[] sigs, int index) {
		actOrRemoveOnListeners(listener -> listener.mediaSignalDetectorPatternMatched(terminal, sigs, index));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void providerPrivateData(Serializable data, int cause) {
		actOrRemoveOnListeners(listener -> listener.providerPrivateData(data, cause));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void terminalConnectionCreated(CallId id, String address, String terminal, int cause) {
		actOrRemoveOnListeners(listener -> listener.terminalConnectionCreated(id, address, terminal, cause));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void terminalConnectionDropped(CallId id, String address, String terminal, int cause) {
		actOrRemoveOnListeners(listener -> listener.terminalConnectionDropped(id, address, terminal, cause));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void terminalConnectionHeld(CallId id, String address, String terminal, int cause) {
		actOrRemoveOnListeners(listener -> listener.terminalConnectionHeld(id, address, terminal, cause));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void terminalConnectionRinging(CallId id, String address, String terminal, int cause) {
		actOrRemoveOnListeners(listener -> listener.terminalConnectionRinging(id, address, terminal, cause));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void terminalConnectionTalking(CallId id, String address, String terminal, int cause) {
		actOrRemoveOnListeners(listener -> listener.terminalConnectionTalking(id, address, terminal, cause));
	}
	
	/**
	 * Broadcast off to all listeners.
	 */
	@Override
	public void terminalPrivateData(String terminal, Serializable data, int cause) {
		actOrRemoveOnListeners(listener -> listener.terminalPrivateData(terminal, data, cause));
	}
	
	private void actOrRemoveOnListeners(Consumer<RemoteListenerWrapper> action) {
		Iterator<RemoteListenerWrapper> it = listeners.iterator();
		while (it.hasNext()) {
			RemoteListenerWrapper listener = it.next();
			action.accept(listener);
			if (isUnreachable(listener)) {
				log.warn(String.format("Removing the unreachable remote listener '%s'", listener));
				it.remove();
			}
		}
	}
	
	private boolean isUnreachable(RemoteListenerWrapper listener) {
		return  listener.getUnreachableCount() > maxRemoteExceptions
				&& listener.getUnreachableDuration().getSeconds() > maxRemoteUnreachableSeconds;
	}
}
