package net.sourceforge.gjtapi.raw.xtapi;

import net.sourceforge.gjtapi.TelephonyListener;

/**
 * @author haf
 */
interface DelayedEvent {
	
	public void fire(TelephonyListener listener);
	public boolean isComplete();
	public void setCallId(XtapiCallId callId);
	public void setCallInformation(CallInformation callInfo);
}
