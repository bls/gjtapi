package net.sourceforge.gjtapi.raw.xtapi;

import java.io.Serializable;

import javax.telephony.media.Symbol;

import net.sourceforge.gjtapi.CallId;
import net.sourceforge.gjtapi.TelephonyListener;

/**
 * @author haf
 */
public class NullTelephonyListener implements TelephonyListener {
	
	@Override
	public void addressPrivateData(String address, Serializable data, int cause) {
	}
	
	@Override
	public void callActive(CallId id, int cause) {
	}
	
	@Override
	public void callInvalid(CallId id, int cause) {
	}
	
	@Override
	public void callOverloadCeased(String address) {
	}
	
	@Override
	public void callOverloadEncountered(String address) {
	}
	
	@Override
	public void callPrivateData(CallId call, Serializable data, int cause) {
	}
	
	@Override
	public void connectionAddressAnalyse(CallId id, String address, int cause) {
	}
	
	@Override
	public void connectionAddressCollect(CallId id, String address, int cause) {
	}
	
	@Override
	public void connectionAlerting(CallId id, String address, int cause) {
	}
	
	@Override
	public void connectionAuthorizeCallAttempt(CallId id, String address, int cause) {
	}
	
	@Override
	public void connectionCallDelivery(CallId id, String address, int cause) {
	}
	
	@Override
	public void connectionConnected(CallId id, String address, int cause) {
	}
	
	@Override
	public void connectionDisconnected(CallId id, String address, int cause) {
	}
	
	@Override
	public void connectionFailed(CallId id, String address, int cause) {
	}
	
	@Override
	public void connectionInProgress(CallId id, String address, int cause) {
	}
	
	@Override
	public void connectionSuspended(CallId id, String address, int cause) {
	}
	
	@Override
	public void mediaPlayPause(String terminal, int index, int offset, Symbol trigger) {
	}
	
	@Override
	public void mediaPlayResume(String terminal, Symbol trigger) {
	}
	
	@Override
	public void mediaRecorderPause(String terminal, int duration, Symbol trigger) {
	}
	
	@Override
	public void mediaRecorderResume(String terminal, Symbol trigger) {
	}
	
	@Override
	public void mediaSignalDetectorDetected(String terminal, Symbol[] sigs) {
	}
	
	@Override
	public void mediaSignalDetectorOverflow(String terminal, Symbol[] sigs) {
	}
	
	@Override
	public void mediaSignalDetectorPatternMatched(String terminal, Symbol[] sigs, int index) {
	}
	
	@Override
	public void providerPrivateData(Serializable data, int cause) {
	}
	
	@Override
	public void terminalConnectionCreated(CallId id, String address, String terminal, int cause) {
	}
	
	@Override
	public void terminalConnectionDropped(CallId id, String address, String terminal, int cause) {
	}
	
	@Override
	public void terminalConnectionHeld(CallId id, String address, String terminal, int cause) {
	}
	
	@Override
	public void terminalConnectionRinging(CallId id, String address, String terminal, int cause) {
	}
	
	@Override
	public void terminalConnectionTalking(CallId id, String address, String terminal, int cause) {
	}
	
	@Override
	public void terminalPrivateData(String terminal, Serializable data, int cause) {
	}
}
