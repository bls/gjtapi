package net.sourceforge.gjtapi.raw.xtapi;

/**
 * @author haf
 */
class DelayedLineOfferingFactory implements DelayedEventFactory {
	
	@Override
	public DelayedEvent create() {
		return new DelayedLineOffering();
	}
}
