/*
	Copyright (c) 2002 8x8 Inc. (www.8x8.com) 

	All rights reserved. 

	Permission is hereby granted, free of charge, to any person obtaining a 
	copy of this software and associated documentation files (the 
	"Software"), to deal in the Software without restriction, including 
	without limitation the rights to use, copy, modify, merge, publish, 
	distribute, and/or sell copies of the Software, and to permit persons 
	to whom the Software is furnished to do so, provided that the above 
	copyright notice(s) and this permission notice appear in all copies of 
	the Software and that both the above copyright notice(s) and this 
	permission notice appear in supporting documentation. 

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT 
	OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
	HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL 
	INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING 
	FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
	NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION 
	WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. 

	Except as contained in this notice, the name of a copyright holder 
	shall not be used in advertising or otherwise to promote the sale, use 
	or other dealings in this Software without prior written authorization 
	of the copyright holder.
*/

package net.sourceforge.gjtapi.raw.remote;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import net.sourceforge.gjtapi.CallId;
import net.sourceforge.gjtapi.IdGenerator;
import net.sourceforge.gjtapi.SequentialIdGenerator;

/**
 * Map a provider's CallId to a serializable CallId.
 * Creation date: (2000-02-17 22:57:06)
 * 
 * @author Richard Deadman
 */
public class CallMapper {
	
	private final Map<CallId, SerializableCallId> idToSerializableIdMap = new HashMap<>();
	private final Map<SerializableCallId, CallId> serializableIdToIdMap = new HashMap<>();
	private final IdGenerator idGenerator = new SequentialIdGenerator();
	
	/**
	 * Translate a CallId to a remote integer handle, storing the relationship as well.
	 */
	public int callToInt(CallId call) {
		return swapId(call).getId();
	}
	
	/**
	 * Free a SerializableCallId from both maps
	 * Creation date: (2000-02-17 23:51:15)
	 * 
	 * @author Richard Deadman
	 * @param id A proxy call id.
	 * @return CallId The real id that id was mapped to, or null
	 */
	public synchronized CallId freeId(SerializableCallId id) {
		CallId callId = serializableIdToIdMap.remove(id);
		if (callId != null) {
			idToSerializableIdMap.remove(callId);
		}
		idGenerator.freeSerializableId(id);
		return callId;
	}
	
	/**
	 * Translate a remote integer handle back to the original CallId.
	 */
	public CallId intToCall(int callRef) {
		return providerId(idGenerator.newSerializableId(callRef));
	}
	
	/**
	 * Look up a Provider's CallId for a SerializableCallId.
	 * Return null if no entry found
	 */
	public synchronized CallId providerId(SerializableCallId id) {
		return serializableIdToIdMap.get(id);
	}
	
	/**
	 * Look up a SerializableCallId for a Provider's CallId
	 */
	public synchronized SerializableCallId swapId(CallId id) {
		Objects.requireNonNull(id);
		
		SerializableCallId serializableId = idToSerializableIdMap.get(id);
		if (serializableId == null) {
			serializableId = idGenerator.getSerializableId();
			idToSerializableIdMap.put(id, serializableId);
			serializableIdToIdMap.put(serializableId, id);
		}
		return serializableId;
	}
	
	@Override
	public String toString() {
		return "CallId mapper with " + idToSerializableIdMap.size() + " entries.";
	}
}
