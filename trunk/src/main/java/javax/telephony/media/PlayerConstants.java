/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

/**
 * Defines the Symbols used by the Player Resource.
 */
public 
interface PlayerConstants extends ResourceConstants {
    /**
     * Player selection attributes in ConfigSpec.
     */

    /**
     * Attribute to request support for various coders.
     * Value is a Symbol or Symbol[] that identifies the required coder(s).
     * <p>
     * The Symbols that identify coders are in 
     * {@link CoderConstants}
     */
    Symbol a_Coder		= ESymbol.Player_Coder;

    /**
     * Attribute to request capability to Jump forward and backward in a MSC.
     * <p>
     * Value is a boolean.
     */
    Symbol a_Jump		= ESymbol.Player_Jump;

    /**
     * Attribute to request capability to Pause and Resume Play of MSC. 
     * Value is a boolean.
     */
    Symbol a_Pause		= ESymbol.Player_Pause;

    /**
     * Attribute to request capability to adjust Speed during Play of a MSC. 
     * <p>
     * Value is an IntRange or a boolean.
     */
    Symbol a_Speed		= ESymbol.Player_Speed;

    /**
     * Returned when <code>(p_IfBusy == v_Fail)</code>.
     */
    Symbol e_Busy 		= ESymbol.Error_Busy;

    /**
     * Attribute to request capability to adjust Volume during Play of a MSC. 
     * <p>
     * Value is an IntRange or a boolean.
     */
    Symbol a_Volume		= ESymbol.Player_Volume;

    /**
     * Parameters of a Player, read/write unless indicated as Read-Only.
     */

    /**
     * Value is an array of Symbols
     * identifying the coders actually supported by this Player.
     * <p> Valid values: {@link CoderConstants}
     * <br> Default value is implementation dependent.
     * <br> Read-Only.
     *
     * @see #a_Coder
     */
    Symbol p_CoderTypes		= ESymbol.Player_CoderTypes;

    /**
     * Indicates the action to take if Player is busy
     * when <code>play()</code> is invoked.
     * <p>Valid values: <code>v_Queue, v_Stop, v_Fail</code>. 
     * <br>Default value is {@link #v_Queue}.
     */
    Symbol p_IfBusy 		= ESymbol.Player_IfBusy;

    /** value for p_IfBusy: wait for previous requests to complete. 
     * @see #p_IfBusy
     */
    Symbol v_Queue 		= ESymbol.Player_Queue;

    /** value for p_IfBusy: stop any previous play. 
     * @see #p_IfBusy
     */
    Symbol v_Stop 		= ESymbol.Player_Stop;

    /** value for p_IfBusy: signal an error, throw a MediaResourceException.
     * @see #p_IfBusy
     */
    Symbol v_Fail 		= ESymbol.Player_Fail;

    /** 
     * Integer indicating maximum duration of this or subsequent 
     * <code>play()</code> in milleseconds. 
     * <p>
     * Value is an Integer (0 - 1,000,000) milliseconds, or v_Forever. 
     */
    Symbol p_MaxDuration	= ESymbol.Player_MaxDuration;

    /**
     * Boolean indicating that this or subsequent <code>play()</code>
     * should be started in the Paused state.
     */
    Symbol p_StartPaused	= ESymbol.Player_StartPaused;

    /**
     * An array of Player event Symbols, indicating which events 
     * are generated and delivered to the application.
     * If a given eventID is not enabled, then no processing
     * is expended to generate or distribute that kind of event.
     * <p>
     * Valid values: <code>ev_Pause, ev_Resume, ev_Speed, ev_Volume</code>
     * <br> Default value = <i>no events</i>.
     * <p>
     * For a PlayerListener to receive events, those events must
     * be enabled by setting this parameter.  
     * This parameter can be set in a {@link ConfigSpec} or by
     * {@link MediaService#setParameters setParameters}.
     * <p>
     * <b>Note:</b>
     * This parameter controls the generation of events.
     * The events cannot be delivered unless a {@link PlayerListener}
     * is added using 
     * {@link MediaService#addMediaListener addMediaListener}. 
     *
     * @see #ev_Pause
     * @see #ev_Resume
     * @see #ev_Speed
     * @see #ev_Volume
     */
    // * @see #ev_Marker

    Symbol p_EnabledEvents	= ESymbol.Player_EnabledEvents;

    /**
     * Integer number of MSCs to jump forward or backward in the MSC list,
     * for either a rtca_JumpForward or rtca_JumpBackward.
     * <p> Valid values: any positive Integer.
     * <br>Default value = 1.
     * 
     * @see #rtca_JumpForwardMSCs
     * @see #rtca_JumpBackwardMSCs
     */
    Symbol p_JumpMSCIncrement	= ESymbol.Player_JumpTVMIncrement;

    /**
     * Integer number of milliseconds by which the current
     * MSC offset is changed by rtca_JumpForward or rtca_JumpBackward.
     * <p>Valid values: any positive Integer. 
     * <br>Default value = 1.
     * 
     * @see #rtca_JumpForward
     * @see #rtca_JumpBackward
     */
    Symbol p_JumpTime		= ESymbol.Player_JumpTime;

    /**
     * Determines the amount the playback speed is changed
     * by RTC actions <code>rtca_SpeedUp</code> or <code>rtca_SpeedDown</code>.
     * Value is an Integer in the range [0..100] expressing
     * percent from normal.
     * <p>Valid values: Integer in the range [0..100] 
     * <br>Default value = 0.
     * <p>
     * <b>Note:</b>
     * Whenever p_SpeedChange is altered, the player speed is reset to normal.
     *
     * @see #rtca_SpeedUp
     * @see #rtca_SpeedDown
     */
    Symbol p_SpeedChange	= ESymbol.Player_SpeedChange;

    /**
     * Determines the amount the volume parameter is changed
     * by the RTC actions rtca_VolumeUp and rtca_VolumeDown.
     * The value is an Integer in the range [0..30] expressing dB of change.
     * <p> Valid values: an Integer in the range [0..30].
     * <br>Default value is 0.
     * <p>
     * <b>Note:</b>
     * Whenever p_VolumeChange is altered, the player volume is reset to normal.
     *
     * @see #rtca_VolumeUp
     * @see #rtca_VolumeDown
     */
    Symbol p_VolumeChange	= ESymbol.Player_VolumeChange;

    /**
     * These symbols are recognized RTC Actions for Player.
     * They can be used when creating RTC objects,
     * and as the argument to <code>MediaService.TriggerRTCAction</code>.
     */

    /**
     * Stop the current Play operation. 
     */
    Symbol rtca_Stop		= ESymbol.Player_Stop;
    
    /**
     * Resume the current Play operation if paused.
     */
    Symbol rtca_Resume		= ESymbol.Player_Resume;
    
    /**
     * Pause the current Play operation, maintaining
     * the current position in the MSClist. (Media Stream)
     */
    Symbol rtca_Pause		= ESymbol.Player_Pause;
    
    /**
     * Jump forward the amount specified by p_JumpTime.
     */
    Symbol rtca_JumpForward	= ESymbol.Player_JumpForward;
    
    /**
     * Jump backward the amount specified by p_JumpTime.
     */
    Symbol rtca_JumpBackward	= ESymbol.Player_JumpBackward;

    /**
     * Jump to the start of the curent MSC
     */
    Symbol rtca_JumpStartMSC	= ESymbol.Player_JumpStartTVM;
    
    /**
     * Jump to the end of the curent MSC.
     */
    Symbol rtca_JumpEndMSC	= ESymbol.Player_JumpEndTVM;

    /**
     * Jump forward the number of MSCs indicated by p_JumpMSCIncrement.
     */
    Symbol rtca_JumpForwardMSCs	= ESymbol.Player_JumpForwardTVMs;
    
    /**
     * Jump backward the number of MSCs indicated by p_JumpMSCIncrement.
     */
    Symbol rtca_JumpBackwardMSCs = ESymbol.Player_JumpBackwardTVMs;

    /**
     * Jump to the First MSC in the MSC List.
     */
    Symbol rtca_JumpStartMSCList	= ESymbol.Player_JumpStartTVMList;
    
    /**
     * Jump to the Last MSC in the MSC List.
     */
    Symbol rtca_JumpEndMSCList	= ESymbol.Player_JumpEndTVMList;

    /**
     * Increase speed by value of parameter p_SpeedChange.
     */
    Symbol rtca_SpeedUp		= ESymbol.Player_SpeedUp;
    
    /**
     * Decrease speed by value of parameter p_SpeedChange.
     */
    Symbol rtca_SpeedDown	= ESymbol.Player_SpeedDown;
    
    /**
     * Set speed to normal.
     */
    Symbol rtca_NormalSpeed	= ESymbol.Player_NormalSpeed;
    
    /**
     * Toggle speed between normal and previous adjusted value.
     */
    Symbol rtca_ToggleSpeed	= ESymbol.Player_ToggleSpeed;
    
    /**
     * Increase volume by value of parameter p_VolumeChange.
     */
    Symbol rtca_VolumeUp	= ESymbol.Player_VolumeUp;
    
    /**
     * Decrease volume by value of parameter p_VolumeChange.
     */
    Symbol rtca_VolumeDown	= ESymbol.Player_VolumeDown;
    
    /**
     * Set volume to normal.
     */
    Symbol rtca_NormalVolume	= ESymbol.Player_NormalVolume;
    
    /**
     * Toggle volume between normal and previous adjusted value.
     */
    Symbol rtca_ToggleVolume	= ESymbol.Player_ToggleVolume;
    
    
    /**
     * These symbols are recognized RTC Conditions for Player.
     * They can be used when creating RTC objects.
     * (They may also show up as events.)
     */

    /**
     * Trigger when a Play is started.
     */
    Symbol rtcc_PlayStarted	= ESymbol.Player_PlayStarted;

    /**
     * Trigger when a Play is completed.
     */
    Symbol rtcc_PlayComplete	= ESymbol.Player_Play;



    /*************************************************************/

    /**
     * Player events Constants.
     * These are the EventID Symbols.
     */

    /**
     * Completion event for the <code>play</code> method.
     * <p>
     * Possible qualifiers are:<br><code>
     * q_RTC, q_Stop, q_Duration, q_Standard, q_EndOfData
     * </code>
     * @see PlayerEvent#getIndex()
     * @see PlayerEvent#getOffset() 
     * @see PlayerEvent#getRTCTrigger()
     */
    Symbol ev_Play			= ESymbol.Player_Play;

    /**
     * Play has been paused by RTC.
     * @see PlayerEvent#getIndex()
     * @see PlayerEvent#getOffset() 
     * @see PlayerEvent#getRTCTrigger()
     */
    Symbol ev_Pause			= ESymbol.Player_Pause;

    /**
     * Play has been resumed by RTC.
     * @see PlayerEvent#getIndex()
     * @see PlayerEvent#getOffset() 
     * @see PlayerEvent#getRTCTrigger()
     */
    Symbol ev_Resume			= ESymbol.Player_Resume;

    /**
     * Playback speed has been changed due to RTC.
     * @see PlayerEvent#getChangeType()
     * @see PlayerEvent#getRTCTrigger()
     */
    Symbol ev_Speed			= ESymbol.Player_Speed;

    /**
     * Playback volume has been changed due to RTC.
     * @see PlayerEvent#getChangeType()
     * @see PlayerEvent#getRTCTrigger()
     */
    Symbol ev_Volume			= ESymbol.Player_Volume;

    /**
     * Indicates a TTS Marker has been reached.
     */
    //Symbol ev_Marker			= ESymbol.Player_Marker;
    
    /**
     * Completion event for LoadDictionary (TTS)
     */
    //Symbol ev_LoadDictionary 		= ESymbol.Player_LoadDictionary;
    /**
     * Completion event for UnLoadDictionary (TTS)
     */
    //Symbol ev_UnloadDictionary	 = ESymbol.Player_UnLoadDictionary;
    /**
     * Completion event for ActivateDictionary (TTS)
     */
    //Symbol ev_ActivateDictionary	= ESymbol.Player_ActivateDictionary;
    
    /**
     * Indicate Play complete because all the streams played to completion.
     */
    Symbol q_EndOfData			= ESymbol.Player_EOD;
    
    /**
     * Play ended because the maximum play duration time has been reached.
     */
    Symbol q_Duration 			= ESymbol.Any_Duration;
}
