/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

/**
 * Defines the callback methods for non-transactional Player Events. 
 * <p>
 */
public 
interface PlayerListener extends ResourceListener {
    /** 
     * Indicates that an RTC has caused a speed change.
     * @see PlayerEvent#getChangeType()
     */
    public void onSpeedChange(PlayerEvent ev);
    /** 
     * Indicates that an RTC has caused a volume change.
     * @see PlayerEvent#getChangeType()
     */
    public void onVolumeChange(PlayerEvent ev);
	
    // public void onMarker(PlayerEvent ev);
	
    /** 
     * Indicates that an RTC has caused play to pause.
     */
    public void onPause(PlayerEvent ev);
	
    /** 
     * Indicates that an RTC has caused play to resume.
     */
    public void onResume(PlayerEvent ev);
}
