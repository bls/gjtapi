/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

/**
 * Defines the callback methods for non-transactional SignalDetector Events. 
 * <p>
 */
public 
interface SignalDetectorListener extends ResourceListener {
    /**
     * Invoked when the SignalDetector detects a signal.
     * <p>
     * <code>event.getEventID()</code> is <code>ev_SignalDetected</code>.
     * <p>
     * This method is invoked only if 
     * {@link SignalDetectorConstants#p_EnabledEvents p_EnabledEvents}
     * contains the Symbol <code>ev_SignalDetected</code>.
     * @see SignalDetectorConstants#p_EnabledEvents p_EnabledEvents
     */
    public void onSignalDetected(SignalDetectorEvent event);

    /**
     * Invoked when the SignalDetector matches a pattern.
     * <p>
     * <code>event.getEventID()</code> is <code>ev_Pattern[<b>i</b>]</code>.
     * <p>
     * This method is invoked only if 
     * {@link SignalDetectorConstants#p_EnabledEvents p_EnabledEvents}
     * contains one of the Symbols <code>ev_Pattern[<b>i</b>]</code>.
     * @see SignalDetectorConstants#p_EnabledEvents p_EnabledEvents
     */
    public void onPatternMatched(SignalDetectorEvent event);

    /**
     * Invoked when the SignalDetector signal buffer overflows.
     * <p>
     * <code>event.getEventID()</code> is <code>ev_Overflow</code>.
     * <p>
     * This method is invoked only if 
     * {@link SignalDetectorConstants#p_EnabledEvents p_EnabledEvents}
     * contains the Symbol <code>ev_Overflow</code>.
     */
    public void onOverflow(SignalDetectorEvent event);

}
