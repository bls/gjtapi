/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

/*
 * In the best of all object language worlds, this could be
 * a simple interface that indicate that <i>some</i> object class
 * is a java.lang.Exception that includes the method getResourceEvent().
 * However, the java compiler does not work that way; the only
 * way to be an Exception is to derive from the Exception class.
 * Hence, we are constrained here to define an actual class.
 */

/**
 * Thrown when a Resource method fails for various reasons.
 * MediaResourceException contains an embedded ResourceEvent
 * that explains the details of the problem.
 * <p>
 * A MediaResourceException generally corresponds to a Resource error
 * or other error detected by the implementation that prevents
 * the normal completion of a request.
 */
public class MediaResourceException extends MediaException {
    /**
     * Constructs a <code>MediaResourceException</code> 
     * with no specified detail message. 
     */
    public MediaResourceException() {super();}

    /**
     * Constructs a <code>MediaResourceException</code> 
     * with the specified detail message. 
     *
     * @param   s   the detail message.
     */
    public MediaResourceException(String s) {super(s);}

    /**
     * @serial the wrapped ResourceEvent
     */
    private ResourceEvent resourceEvent = null;
    
    /**
     * Construct a <code>MediaResourceException</code>
     * with the specified ResourceEvent and no detail message.
     *
     * Wraps the given ResourceEvent in a MediaResourceException.
     * This exception is thrown if the ResourceEvent indicates
     * an error or other exceptional status.
     * 
     * @param ev a ResourceEvent 
     */
    public MediaResourceException(ResourceEvent ev) {
	super();
	resourceEvent = ev;
    }
    
    /**
     * Construct a <code>MediaResourceException</code>
     * with the specified detail message and ResourceEvent.
     *
     * Wraps the given ResourceEvent in a MediaResourceException.
     * This exception is thrown if the ResourceEvent indicates
     * an error or other exceptional status.
     * 
     * @param s  the detail message
     * @param ev a ResourceEvent 
     */
    public MediaResourceException(String s, ResourceEvent ev) {
	super(s);
	resourceEvent = ev;
    }

    /** 
     * Extract the underlying ResourceEvent from this MediaResourceException.
     * The application can use ResourceEvent accessors like 
     * {@link ResourceEvent#getError()} or
     * {@link ResourceEvent#getQualifier()}
     * to get more information.
     * <p>
     * The returned <code>ResourceEvent</code> may have
     * <code>getQualifier() == null</code> indicating an error.
     * <p>
     * <b>Note:</b>
     * It is safe to use <code>getResourceEvent().getError()</code>
     * because <code>getResourceEvent() != null</code>
     * and if there is no error, then <code>getError() == e_OK</code>.
     * 
     * @return the encapsulated ResourceEvent
     */
    public ResourceEvent getResourceEvent() {return resourceEvent;}
}
