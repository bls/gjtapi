/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

/*
 * Logically this is a MediaResourceException that is a RuntimeException.
 * <p>
 * Due to the Exception <i>class</i> heirarchy, this can not
 * be more closely related to MediaResourceException.
 * <p>
 * <b>Implementors note:</b>
 * If the Connection becomes disconnected <i>after</i> the method has started,
 * then the resource method completes with
 * <br><code>ResourceEvent().getQualifier() == q_RTC</code> and
 * <br><code>ResourceEvent().getRTCTrigger() == rtcc_Disconnected</code>.
 * This should be detected, the error field set to e_Disconnected,
 * the event wrapped in an exception, 
 * and thrown to the invoking (synchronous) method.
 * <br>So:
 * <br><code>getResourceEvent().getError() = e_Disconnected</code> and
 * <br><code>getResourceEvent().getQualifier() == q_RTC</code> and
 * <br><code>getResourceEvent().getRTCTrigger() == rtcc_Disconnected</code>.
 * <p>
 * Is the [revised or original] event sent to Listeners?
 */

/**
 * Thrown by any resource method 
 * if the operation stops because the associated Terminal
 * is in the <code>Connection.DISCONNECTED</code> state.
 * <p>
 * Resource operations are stopped or disallowed if the Connection to the 
 * Terminal associated with the MediaService is <code>DISCONNECTED</code>.
 * <p>
 * If the Connection is disconnected,
 * then the resource method throws a DisconnectedException with
 * <br><code>getResourceEvent().getError() == 
 * {@link ResourceConstants#e_Disconnected e_Disconnected}</code>.
 * <p>
 * If the Connection becomes disconnected <i>after</i> the method has started,
 * then the ResourceEvent may contain additional information.
 */
public class
DisconnectedException extends MediaRuntimeException {
    /**
     * wraps a ResourceEvent
     * @serial
     */
    private ResourceEvent resourceEvent = null;

    /** 
     * Construct a <code>DisconnectedException</code>
     * with the ResourceEvent and no detail message.
     * @param ev a ResourceEvent that indicates <code>DISCONNECTED</code>.
     */
    public DisconnectedException(ResourceEvent ev) {
	super();
	resourceEvent = ev;
    }
    
    /** 
     * Construct a <code>DisconnectedException</code>
     * with the specified detail message and ResourceEvent.
     * @param str the detail message.
     * @param ev a ResourceEvent that indicates <code>DISCONNECTED</code>.
     */
    public DisconnectedException(String str, ResourceEvent ev) {
	super(str);
	resourceEvent = ev;
    }

    /** 
     * Extract the underlying ResourceEvent from this DisconnectedException.
     * Application uses standard ResourceEvent accessors on the event.
     * <p>
     * If an <b>ongoing</b> media operation was terminated, 
     * this ResourceEvent contains
     * getQualifier() = {@link ResourceEvent#q_Disconnected q_Disconnected}
     * If a <b>new</b> media operation can not be started, 
     * this ResourceEvent contains
     * getError() = <code>e_Disconnected</code>.
     *
     * @return a ResourceEvent that indicates <code>DISCONNECTED</code>.
     */
    public ResourceEvent getResourceEvent() {return resourceEvent;}
}
