/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;
import javax.telephony.*;	// get core objects and Exceptions
import java.util.Dictionary;
import java.util.Vector;

/**
 * An implementation of interface MediaService that does nothing.
 * <p>
 * A no-op implementation that compiles without the provider/async packages.
 * The reference source code for BasicMediaService extends this class.
 * A vendor that chooses to use the provider/async packages as the
 * basis for development would extend the ProtoMediaService instead.
 */

//package
class NullMediaService implements MediaService {
    /** a null MediaGroup implementation. */
    class NullMediaGroup /* implements MediaGroup */ {
	NullEvent doSomething() {return new NullEvent(false);}
    }

    /** a null MediaProvider implementation. */
    class NullMediaProvider implements MediaProvider {
	NullEvent bind() {return new NullEvent(true);}
	NullEvent release() {return new NullEvent(false);}
    } 

    /** a Null Event implementation. */
    class NullEvent {
	NullMediaGroup group;
	NullEvent(boolean bind){group = bind ? new NullMediaGroup() : null;}
	NullMediaGroup getGroup() {return group;}
	boolean isDone() {return true;}
	ConfigSpec getConfigSpec() {return null;}
	Dictionary getDictionary() {return null;}
	String getTerminalName() {return null;}
	Terminal getTerminal() {return null;}
	Connection getConnection() {return null;}
	NullEvent throwIfRuntimeException() {return this;}	
	NullEvent throwIfBindException() throws MediaBindException 
	    {return this;}
	NullEvent throwIfConfigException() throws MediaConfigException
	    {return this;}
	NullEvent throwIfCallException() throws MediaCallException
	    {return this;}
    }

    /** The Provider object to work with */
    private NullMediaProvider _provider = null;
    
    /** 
      * The MediaGroup bound to this MediaService or null. 
      */
    private NullMediaGroup _group = null;
    
    /**
     * Verifies whether service is bound to a group, or a bind is in progress.
     * Methods setting _event are synchronized. 
     */
    private NullEvent _event = null;

    /**
     * Returns the bound MediaGroup.
     * If no MediaGroup is bound, throw the RuntimeException NotBoundException.
     * <p>
     * This is "package" scoped so that it can be seen/used by BasicMediaService.
     * Applications that subclass BasicMediaService should generally
     * <b>not</b> use this directly.
     *
     * @exception NotBoundException if not currently bound to a MediaGroup
     * @return the MediaGroup to which this MediaService is bound.
     */
    // in package, so only BasicMediaGroup can find it!
    NullMediaGroup checkGroup() throws NotBoundException {
	if (_group == null) throw new NotBoundException();
	return _group;
    }
    
    /**
     * @return true iff this MediaService is bound to a MediaGroup.
     */
    public boolean isBound() {
	return (_group != null);
    }


    /* This implementation assumes that the Jtapi Provider
     * is a MediaProvider, possibly an unfounded assumption.
     * However, it serves to convince the compiler that the
     * indicated exceptions may be thrown.
     */ 
    MediaProvider findMediaProvider(String peerName, String providerString) 
    throws ClassNotFoundException, 
        InstantiationException,
        IllegalAccessException,
        ProviderUnavailableException {
	if(peerName == null)
	    /* use the installation default */
	    return new NullMediaProvider();
	return ((MediaProvider)
		((JtapiPeer)Class.forName(peerName).newInstance())
		.getProvider(providerString));
    }

    private void initMediaService(MediaProvider provider) {
	this._provider = (NullMediaProvider)provider;
	// mark this group as unbound, no bind in progress:
	// also tests _provider for null, pings _provider.
	this._event = _provider.release();
    }

    /*
     * Create an unbound MediaService, using services associated
     * with the given MediaProvider object.
     * The MediaProvider object may be a JTAPI Provider,
     * or may be some other object obtained by other vendor-specific means.
     *
     * @param provider a MediaProvider instance
     */
    public NullMediaService(MediaProvider provider) {
	initMediaService(provider);
    }

    /*
     * Create an unbound MediaService, using services identified 
     * as the installation default.
     */
    public NullMediaService() {
	try {
	    initMediaService(findMediaProvider(null, null));
	} catch (Exception ex) {
	    throw new RuntimeException("MediaProvider not found: " + ex);
	}
    }

    /**
     * Create an unbound MediaService, using services identified
     * by the two String arguments.
     * 
     * @param peerName the name of a Class that implements JtapiPeer.
     * @param providerString a "login" string for that JtapiPeer.
     *
     * @throws ClassNotFoundException if JtapiPeer class is not found
     * @throws IllegalAccessException if JtapiPeer class is not accessible
     * @throws InstantiationException if JtapiPeer class does not instantiate
     * @throws ProviderUnavailableException if Provider is not available
     * @throws ClassCastException if Provider instance is not a MediaProvider
     */
    public NullMediaService(String peerName, String providerString) 
    throws ClassNotFoundException, 
        InstantiationException, // hope to remove in Jtapi-2.0
        IllegalAccessException,	// hope to remove in Jtapi-2.0
        ProviderUnavailableException {
	    initMediaService(findMediaProvider(peerName, providerString));
    }

    /*
     * Return the installation specific String that identifies
     * the Terminal to which this MediaService is bound.
     *
     * @return a String that uniquely identifies the bound Terminal.
     * @throws NotBoundException if not currently bound to a MediaGroup
     * @see #getTerminal
     */
    public String getTerminalName() {
	return checkGroup().doSomething().getTerminalName();
    }

    /* 
     * Get the Jtapi Terminal associated with the MediaService.
     * The Terminal may be used to access the associated JTAPI objects.
     * <p>
     * If this MediaService is not associated with a 
     * JTAPI call control Provider, this method may return null.
     */
    public Terminal getTerminal() {
	// get a getTerminal event, and then getTerminal()
	return checkGroup().doSomething().getTerminal();
    }
    
    /* 
     * Bind this MediaService to a particular Terminal.
     * <p>
     * @param configSpec declares configuration requirments for the MediaGroup
     * @param terminalName a String that names a media capable Terminal
     * @throws MediaBindException one of AlreadyBoundException, BindCancelledException.
     * @throws MediaConfigException if MediaGroup can not be configured as requested
     */
    public void bindToTerminalName(ConfigSpec configSpec, 
					 String terminalName)
	throws MediaBindException, MediaConfigException {
	if(_group != null) throw new AlreadyBoundException();
	_event = _provider.bind();
	_event.throwIfBindException().throwIfConfigException().throwIfRuntimeException();
	_group = _event.getGroup();
	return ;
    }
    
    /* 
     * Bind this MediaService to a particular Terminal.
     * <p>
     *
     * @param configSpec declares configuration requirments for the MediaGroup.
     * @param terminal a JTAPI Terminal object.
     *
     * @exception MediaBindException one of AlreadyBoundException, BindCancelledException.
     * @exception MediaConfigException if MediaGroup can not be configured as requested.
     */
    synchronized
    public void bindToTerminal(ConfigSpec configSpec,
				     Terminal terminal)
	throws MediaBindException, MediaConfigException {
	if(_group != null) throw new AlreadyBoundException();
	_event = _provider.bind();
	_event.throwIfBindException().throwIfConfigException().throwIfRuntimeException();
	_group = _event.getGroup();
	return ;
    }
    
    /*
     * This MediaService is ready for a MediaGroup.
     * Waits for a Call to be delivered to this service name.
     * <p>
     * On return, this MediaService is bound to a MediaGroup,
     * and that MediaGroup is configured to configSpec.
     * <p>
     * if cancelBindRequest() terminates this method,
     * this method unblocks, throwing a BindCancelledException.
     * <p>
     * @param configSpec configure MediaGroup to configspec before binding.
     * @param serviceName name under which this MediaService is to be registered.
     * @exception MediaBindException one of AlreadyBoundException, BindCancelledException.
     */
    // Does not throw MediaConfigException: that exception goes to releaseToService().
    
    /* The logical equivalent of S.100:RequestGroup() */
    synchronized
    public void bindToServiceName(ConfigSpec configSpec,
					String serviceName)
	throws MediaBindException {
	if(_group != null) throw new AlreadyBoundException();
	_event = _provider.bind();
	_event.throwIfBindException().throwIfRuntimeException();
	_group = _event.getGroup();
	return ;
    }
    
    /* 
     * Bind this MediaService to a MediaGroup connected to an existing Call.
     * This will typically create a new Connection to the Call.
     * <p>
     * @param configSpec declares configuration requirments for the MediaGroup.
     * @param call a JTAPI Call object
     *
     * @exception MediaBindException one of AlreadyBoundException, BindCancelledException.
     * @exception MediaConfigException if MediaGroup can not be configured as requested.
     * @exception MediaCallException encapsulates any call processing exceptions
     * generated while trying to establish the new Call and Connections.
     * @return a Connection or null.
     */
    synchronized
    public Connection bindToCall(ConfigSpec configSpec,
				 Call call)
	throws MediaBindException, MediaConfigException, MediaCallException {
	if(_group != null) throw new AlreadyBoundException();
	_event = _provider.bind();
	_event.throwIfBindException().throwIfConfigException();
	_group = _event.getGroup();
	return _event.getConnection();
	}
    
    /*
     * bind to an new outbound call.
     * @exception MediaBindException one of AlreadyBoundException, BindCancelledException.
     * @exception MediaConfigException if MediaGroup can not be configured as requested.
     * @exception MediaCallException encapsulates any call processing exceptions
     * generated while trying to establish the new Call and Connections.
     */
    synchronized
    public void bindAndConnect(ConfigSpec configSpec, 
				     String origAddr, 
				     String dialDigits)
	throws MediaBindException, MediaConfigException, MediaCallException {
	if(_group != null) throw new AlreadyBoundException();
	_event = _provider.bind();
	_event.throwIfBindException().throwIfConfigException().throwIfCallException();
	_event.throwIfRuntimeException();
	_group = _event.getGroup();
	return ;
    }
    
    /*
     * Revoke previous bindToXXXX() request on this MediaService.
     * <br>
     * This is called from a thread that is NOT blocked in a bind() method, 
     * and will unblock the bindToXXXX() request.
     * <p>
     * This method is "one-way" and Listener-safe. The app/Listener will
     * receive either a bind completion event or a BindCancelledException as the
     * result of the outstanding bindToXXXX(). 
     */
    public void cancelBindRequest() {
	if (_event.isDone()) return; // no bind in progress
	if (_group != null) return;  // no bind in progress
	//_provider.asyncCancelBindRequest(this);
    }
    
    /*
     * Release the MediaGroup with an indication that the 
     * Connection (and perhaps the Call) should be,
     * or perhaps already has been, dropped.
     * <p>
     * In general, this method will provoke framework specific processing
     * to handle the end of the call. 
     * <p>
     * @exception NotBoundException if not currently bound to a MediaGroup
     * <p>
     */
    synchronized
    public void release() {
	_provider.release().throwIfRuntimeException();
	_group = null;
    }
    
    
    /*
     * Release for further processing.
     * <p>
     * @param disposition A string that identifies the next MediaService. 
     * @param timeout int milliseconds to wait for new service to become ready.
     *
     * @exception NotBoundException if not currently bound to a MediaGroup
     * @exception MediaBindException one of NoServiceAssignedException or NoServiceReadyException
     * @exception NoServiceAssignedException if disposition is not recognised 
     * or is not mapped to any serviceName.
     * @exception NoServiceReadyException if disposition is mapped to a serviceName,
     * but none of the MediaServices registered to serviceName 
     * are ready and do not become ready within <i>timeout</i> millisecs.
     * @exception MediaConfigException if the MediaGroup could not be configured
     * for the recipient service.
     */
    synchronized
    public void releaseToService(String disposition, int timeout)
	throws MediaBindException, MediaConfigException {
	    NullEvent event = _provider.release();
	    event.throwIfRuntimeException();
	    event.throwIfBindException();
	    _group = null;
	}

    /* Now, the AsyncMediaGroup methods */

    /*
     * Configure current MediaGroup according to ConfigSpec.
     * <p>
     * Post-Condition: (foreach R in configSpec {this.isInstanceOf(R)})
     * @param configSpec the requested configuration 
     * @exception NotBoundException if not currently bound to a MediaGroup
     * @exception MediaConfigException if resources can not be configured as requested
     */
    public void configure(ConfigSpec configSpec) 
	throws MediaConfigException {
	    checkGroup().doSomething().throwIfConfigException();
	}
    
    /*
     * Get current configuration of the MediaGroup.
     * <p>
     * Note this does not generally return a copy of the ConfigSpec 
     * used to create/configure  this MediaGroup; the returned ConfigSpec
     * describes the full and actual configuration of resources.
     *
     * @return a ConfigSpec describing the current configuration.
     * @exception NotBoundException if not currently bound to a MediaGroup
     * <p>
     */
    public ConfigSpec getConfiguration() throws NotBoundException {
	return checkGroup().doSomething().getConfigSpec();
    }

    /*
     * Stop all media/resource operations currently in progress.
     * Unblocks all synchronous resource methods (all AsyncResourceEvent complete).
     * <p>
     * This is a non-blocking, one-way invocation.
     *
     * @exception NotBoundException if not currently bound to a MediaGroup
     */
    public void stop() throws NotBoundException {
	checkGroup().doSomething();
    }
    
    /*
     * Trigger a RTC action.
     * <p>
     * This method allows the application to synthesize the triggering
     * of RTC actions. The RTC Condition will be rtcc_Application.
     * <p>
     * This is a non-blocking, one-way invocation.
     *
     * @param a Symbol for a recognized RTC action: rtca_<i>Action</i>
     * @exception NotBoundException if not currently bound to a MediaGroup
     */
    public void triggerRTC(Symbol rtca) throws NotBoundException {
	checkGroup().doSomething();
    }
    
    
    public void setUserValues(Dictionary newDict) throws NotBoundException {
	checkGroup().doSomething().throwIfRuntimeException();
    }
    
    public Dictionary getUserValues(Symbol[] keys) throws NotBoundException {
	return checkGroup().doSomething().getDictionary();
    }
    

    /*
     * Set the entire UserDictionary to a new collection of key-value pairs.
     * <p>
     * Note: 
     * setUserDictionary(null) will clear all key-value pairs from the Dictionary.
     * <p>
     * @param newDict A Dictionary whose contents is copied into the MediaGroup.
     * @exception NotBoundException if not currently bound to a MediaGroup
     */
    public void setUserDictionary(Dictionary newDict) throws NotBoundException {
	checkGroup().doSomething().throwIfRuntimeException();
    }

    /*
     * Creates and returns a new Dictionary object that contains a snapshot of
     * the MediaGroup Dictionary.
     * <p>
     * @return A Dictionary of application-shared information.
     * @exception NotBoundException if not currently bound to a MediaGroup
     */
    public Dictionary getUserDictionary() throws NotBoundException {
	return checkGroup().doSomething().getDictionary();
    }
    
    /*
     * get a collection of Parameters from the Group/Resources.
     *
     * @param keys is a Dictionary indicating the paramters of interest.
     * The values in this Dictionary are placed with the values of the
     * indicated parameter. If a requested parameter is not supported 
     * by the MediaService implementation, that key is removed from the Dictionary.
     * @return Dictionary of values bound to the given keys
     */
    public Dictionary getParameters(Symbol[] keys) { 
	return checkGroup().doSomething().getDictionary();
    }
    
    /* 
     * get <b>all</b> parameters from the MediaService.
     * @return Dictionary of values bound to the given keys     
     */
    public Dictionary getParamters() throws NotBoundException {
	return checkGroup().doSomething().getDictionary();
    }
    
    
    /*
     * Set the value of various parameters to the given values.
     */
  public void setParameters(Dictionary params) 
      throws NotBoundException {
	  checkGroup().doSomething().throwIfRuntimeException();
      }

    /** Vector of ResourceListener */
    protected static Vector theListeners = new Vector();

    /** 
     * Add a MediaListener to this MediaService.
     * <p>
     * Events generated by this MediaService are dispatched to the
     * given listener if it implements the appropriate Listener interface.
     * @@param listener an object that implements 
     * one or more of the ResourceListener interfaces and/or 
     * the MediaServiceListener interface.
     */
    public void addMediaListener(MediaListener listener) {
	 theListeners.add(listener);
    }

    /** remove this MediaListener. */
    public void removeMediaListener(MediaListener listener) {
	 theListeners.remove(listener);
    }
}
