/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

/**
 * Thrown by <code>bindToCall()</code> or <code>bindAndConnect()</code> if 
 * the operation cannot be completed due to a call processing failure.
 * The nature of the call processing problem should be apparent from
 * the detail message string in the MediaCallException.
 */
public 
class MediaCallException extends MediaException {
    /**
     * Constructs a <code>MediaCallException</code> 
     * with no specified detail message. 
     */
    public MediaCallException() {super();}

    /**
     * Constructs a <code>MediaCallException</code> 
     * with the specified detail message. 
     *
     * @param   s   the detail message.
     */
    public MediaCallException(String s) {super(s);}
}
