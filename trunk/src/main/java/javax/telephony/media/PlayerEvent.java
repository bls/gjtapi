/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

/**
 * Defines the methods for Player Events.
 */
public 
interface PlayerEvent extends ResourceEvent, PlayerConstants {

    /**
     * Return index in to MSC List, indicating which MSC was stopped.
     *
     * @return the int index into the MSC List, 
     * indicating which MSC was stopped, paused, etc.
     * 
     */
    public int getIndex();
    
    /**
     * Return index into a MSC where play stopped.
     * Typically this is milliseconds into an audio MSC.
     *
     * @return the int index into a MSC phrase where play stopped, paused, etc.
     */
    public int getOffset();

    /**
     * Return Symbol that identifies the type of Speed or Volume adjustment.
     *
     * @return one of the Symbols:
     * <code>rtca_SpeedUp, rtca_SpeedDown, rtca_ToggleSpeed, rtca_NormalSpeed
     * rtca_VolumeUp, rtca_VolumeDown, rtca_ToggleVolume, rtca_NormalVolume</code>
     *
     * @see PlayerConstants#rtca_SpeedDown
     */
    public Symbol getChangeType();
}
