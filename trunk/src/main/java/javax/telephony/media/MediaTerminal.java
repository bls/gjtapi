/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;
import javax.telephony.Terminal;

/** 
 * Interface to mark Terminals with streams that can be connected 
 * to a MediaGroup.
 * These Terminals are suitable for use with 
 * {@link MediaService#bindToTerminal(ConfigSpec, Terminal)}.
 */
public 
interface MediaTerminal extends Terminal {
    /*
     * TO DO: embed this as showable text using DHTML!
     *
     * Design note:<br>
     * <p>
     * A MediaTerminal is a Terminal with a S.100 CCR or TAPI-3 
     * media streams and some method to connect to a MediaGroup.
     * [or in TAPI-3 some way for the MSP to connect them to T3 "Terminals"]
     * <p>
     * That is: there is some implementation like a S.100 Server
     * (or a TAPI-3 MSP) that can process a ConfigSpec and deliver
     * an object that implements the requested Resource interfaces.
     * In S.100/S.400 that means delivering a MediaGroup (of Resources).
     * In TAPI-3 that would mean delivering a collection of T3 "Terminals"
     * <p>
     * the methods that distinguish a MediaTerminal from a Terminal
     * are accessible only to the implementation of the MediaService
     * bind methods, MediaService.configure(), and MediaService.release().
     * <p>
     * Those methods constitute part of the JTAPI Media SPI 
     * the javax.telephony.media.provider package.
     */
}
