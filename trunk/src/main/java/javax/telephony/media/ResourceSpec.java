/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

import java.util.Dictionary;

/**
 * Describes the attributes and other information for configuring Resources.
 * A media resource is described by three items:<ul>
 * <li>the name or interface Class of a Resource, </li>
 * <li>an Array of selection <i>attributes</i>, and</li>
 * <li>a Dictionary of the initial parameter settings.</li></ul>
 * <p>
 * <h4>Allocation Operators:</h4>
 * Alternatively, a ResourceSpec may be a collection of nested
 * ResourceSpec objects related by an allocation operator.
 * The allocation operators are: And, Alt, Or, Future, Add.
 * Each operator is represented by an inner subclass of ResourceSpec.
 * <p>
 * <h4>Selection Attributes:</h4>
 * Each resource implementation is registered with a collection
 * of Attributes that describe the features or capabilities of 
 * that implementation. 
 * Selection attributes specify the features or capabilities
 * that an application expects or relies on.
 * During configuration, only Resource implementations that have the 
 * requested Attributes are allocated and connected into the group.
 * <p>
 * For example, a Player that is capable of speed changing and that
 * implements the speed changing API, RTCs, etc. would register the
 * attribute: (a_Speed = TRUE). An application that wants to use
 * the speed change features during <code>play()</code> should request
 * a Player with Attribute: (a_Speed = TRUE) by including this binding
 * in the selectionAttributes Dictionary when constructing the ResourceSpec.
 * That is:
 * <br><new>new ResourceSpec(Player.class, {(a_Speed=TRUE)}, ...);</code>
 * <p>
 * The Attributes of a given resouce implementation are fixed 
 * ('final' as one would say in Java). If a resource is configured
 * with (a_Speed=TRUE), then a_Speed will always report TRUE.
 * <p>
 * <h4>Attributes versus Parameters</H4>
 * In contrast, the amount of speed change to be invoked when the
 * speed is changed is a variable controlled by the application.
 * Application controllable settings are called <b>parameters</b>.
 * Applications can set the parameters of a resource using 
 * {@link MediaService#setParameters(Dictionary) MediaService.setParameters()}
 * or pre-set them during configuration by including them in the
 * initialParameters of a ResourceSpec.
 * <p>
 * <b>Note:</b>
 * This specification does not constrain how or whether the structured
 * sub-components of a ResourceSpec are shared or copied between uses.
 * Application developers are advised that there may be side-effects
 * if an Array or Dictionary used in a ResourceSpec is subsequently 
 * modified.  For example, it would be poor practice to modify
 * the ResourceSpec[] that was used in the constructor to Alt().
 * Modifications should be made only to <i>copies</i> of such Arrays 
 * or Dictionaries.
 */
public class ResourceSpec {
    /* This private variable stores the resource name. */
    private Class resourceClass;
    
    /* Private field to store the selection attributes */
    private Dictionary selectionAttributes;
    /* Private field to store the initial parameters. */
    private Dictionary initialParameters; 
    
    
    /**
     * Constructor using the resource class, selection attributes
     * and initial parameters.
     * <p>
     * The resouceClass must be a valid Resource <code>Class</code> object. 
     * If the other arguments are <code>null</code>, 
     * they are treated the same as empty collections.
     *
     * @param resourceClass the Class object for the requested Resource interface. 
     * @param selectionAttributes a Dictionary containing required values
     * for Attributes of the Resource implementation.
     * @param initialParameters a Dictionary of initial parameter settings
     * for this resource.
     */
    public ResourceSpec(Class resourceClass,
			Dictionary selectionAttributes, 
			Dictionary initialParameters) {
	if(!Resource.class.isAssignableFrom(resourceClass))
	    throw new ClassCastException(resourceClass+" is not a Resource Class");
	this.resourceClass = resourceClass;
	this.selectionAttributes = selectionAttributes; 
	this.initialParameters = initialParameters;
    }
    
    /**
     * Constructor using the resource name, selection attributes
     * and initial parameters.
     * This form of the constructor defers .class lookup until runtime.
     * <p>
     * The resouceName must identify a valid Resource class. 
     * The selectionAttributes and initialParameters may be <code>null</code>, 
     * which is treated as an empty Dictionary.
     *
     * @param resourceName the FQCN of the requested Resource interface. 
     * @param selectionAttributes a Dictionary containing required values
     * for Attributes of the Resource implementation.
     * @param initialParameters a Dictionary of initial parameter settings
     * for this resource.
     *
     * @throws ClassNotFoundException if class is not found
     */
    public ResourceSpec(String resourceName,
			Dictionary selectionAttributes, 
			Dictionary initialParameters)
	throws ClassNotFoundException {
	    this(Class.forName(resourceName),
		 selectionAttributes, 
		 initialParameters);
	}
    
    /**
     * Returns the Class of the media resource associated with this object.
     * <p>
     * @return The Class object representing the requested Resource interface.
     */
    public Class getResourceClass() {
	return resourceClass;
    }
    
    /**
     * Get the Dictionary of additional selection attributes.
     * @return a Dictionary containing required values
     * for Attributes of the Resource implementation.
     */
    public Dictionary getSelectionAttributes() {
	return selectionAttributes;
    }
    

    /**
     * Get the Dictionary of initial parameters.
     * @return a Dictionary of initial parameter settings
     * for this resource.
     */
    public Dictionary getInitialParameters() {
	return initialParameters;
    }
    
    /** 
     * Represents the ResourceSpec operator nodes: And, Alt, Or, Add, Future.
     * Non-Operator ResourceSpecs are the "leaf-nodes".
     * <p>
     * Note that these ResourceSpec nodes have <code>null</code>
     * for Attributes and Parameters.
     * <p>
     * Operator implements Resource, so that the first argument
     * to the ResourceSpec constructor is a Resource Class...
     */
    public static abstract class Operator extends ResourceSpec
	implements Resource {
	/* implements Resource so we can use super constructor.
	 * If this causes trouble, take it out: change constructor 
	 * to check for resourceClass isa Resource or Operator.
	 */
	protected ResourceSpec[] resSpecs;
	public Operator(ResourceSpec[] resSpecs) {
	    super(Operator.class, null, null);
	    this.resSpecs = resSpecs;
	}

	/** get the array of ResourceSpecs. */
	public ResourceSpec[] getResourceSpecs() {
	    return resSpecs;
	}
    }

    /**
     * All ResourceSpecs must be satisfied, simultaneously.
     */
    public static class And extends Operator {
	/** Constructor for the And operator.
	 * @param resSpecs an Array of ResourceSpec.
	 */
	public And(ResourceSpec[] resSpecs) {super(resSpecs);}
    }

    /**
     * All ResourceSpecs must be satisfied, 
     * but only needs to statisfy one at a time.
     * That is, the implementation is allowed to switch between 
     * each Resource dynamically; for example, a Recorder may
     * be disabled while a Player is playing.
     */
    public static
    class Alt extends Operator {
	/** Constructor for the Alt operator.
	 * @param resSpecs an Array of ResourceSpec.
	 */
	public Alt(ResourceSpec[] resSpecs) {super(resSpecs);}
    }

    /**
     * The given ResourceSpec[] is searched in order for a ResourceSpec
     * that is satisfied.  This construct allows an application to
     * specify a series of fallback options if the best alternative
     * is not available. If none of the ResourceSpecs are available
     * allocation of this ResourceSpec fails.
     */
    public static
    class Or extends Operator {
	/** Constructor for the Or operator.
	 * @param resSpecs an Array of ResourceSpec.
	 */
	public Or(ResourceSpec[] resSpecs) {super(resSpecs);}
    }

    /**
     * Add each Resource, unless a compatible Resource is already configured.
     * In common usage only one ResourceSpec is supplied;
     * if multiple ResourceSpecs are supplied,
     * they are treated as for <code>And</code>.
     */
    public static
    class Add extends Operator {
	/** Constructor for the Add operator.
	 * @param resSpecs an Array of ResourceSpec.
	 */
	public Add(ResourceSpec[] resSpecs) {super(resSpecs);}
    }

    /**
     * The given ResourceSpec[] specifies Resources that will
     * be added in the future.  Operationally, the configuration
     * implementation must determine that Resources for each 
     * ResourceSpec are installed and connectable to this group.
     * However, the Resource are not actually allocated and connected
     * at this time.
     * <p>
     * This acts as a hint to the allocation process about
     * how and where to implement the other Resources.
     * <p>
     * Typically, one or more of these <i>future</i> Resources may
     * be allocated and configured into the group by a later configure()
     * request, possibly using a ResourceSpec.Add object.
     */
    public static
    class Future extends Operator {
	/** Constructor for the Future operator.
	 * @param resSpecs an Array of ResourceSpec.
	 */
	public Future(ResourceSpec[] resSpecs) {super(resSpecs);}
    }

    /* data section */

    /**
     * A Player with default parameters and attributes.
     */
    public static final ResourceSpec basicPlayer =  
	new ResourceSpec(Player.class, null, null);

    /**
     * A Recorder with default parameters and attributes.
     */
    public static final ResourceSpec basicRecorder =
	new ResourceSpec(Recorder.class, null, null);

    /**
     * A SignalDetector with default parameters and attributes.
     */
    public static final ResourceSpec basicSignalDetector =
	new ResourceSpec(SignalDetector.class, null, null);

    /**
     * A SignalGenerator with default parameters and attributes.
     */
    public static final ResourceSpec basicSignalGenerator =
	new ResourceSpec(SignalGenerator.class, null, null);

    //not in spec yet:
    //public static final 
    //ResourceSpec basicASR      = new ResourceSpec(ASR.class, null, null);


    /** ResourceSpec[] used to define Alt({Player, Recorder}). */
    private static final 
	ResourceSpec[] aryPlayerRecorder = {basicPlayer, basicRecorder};

    /**
     * A ResourceSpec that provides alternating access to Player and Recorder.
     */
    public static final 
	ResourceSpec basicAltPlayerRecorder = new Alt(aryPlayerRecorder);

    /**
     * This ResourceSpec requests no actual resource.
     * As such, it is always satisfiable, by any server.
     * This may be used to ensure that a {@link ResourceSpec.Or} always succeeds.
     */
    public static final 
	ResourceSpec none = new And(null);


}
