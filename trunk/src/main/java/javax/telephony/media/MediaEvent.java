/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential,
 * not for public distribution.
 */

package javax.telephony.media;

import javax.telephony.*;		// for javadoc to find Connection

/**
 * An event from a MediaService.
 * Characterized by the MediaService which is the source of the event,
 * and an EventID representing the event identity.
 * <p>
 * <b>Note:</b>
 * Classes that implement this interface shall follow the standard
 * Java Listener design pattern by extending java.util.EventObject.
 */
public
interface MediaEvent {
    /**
     * Get the MediaService associated with the current event.
     * <p>
     * @return the MediaService which is the source of this event.
     */
    MediaService getMediaService();

    /**
     * Get the Symbol that identifies this event.
     * <P>
     * For completion events, this identifies the operation that has completed.
     * For synchronous code this is mostly redundant, but it may be useful
     * for asynchronous Listeners. Further detail about how/why the operation
     * completed is available using {@link ResourceEvent#getQualifier()}.
     * <p>
     * The Symbols returned from by this method will have names of the form:
     * <code>ev_Operation</code>.
     * 
     * @return the Symbol that identifies this event
     *
     * @see ResourceEvent
     */
    public Symbol getEventID();

    /**
     * Indicates that a Connection to this MediaService has been disconnected.
     * A MediaEvent with this EventID is sent to 
     * <code>{@link MediaServiceListener#onDisconnected}</code> 
     * if the Connection to this MediaService transitions to the state
     * <code>{@link Connection#DISCONNECTED Connection.DISCONNECTED}</code>.
     * If <code>{@link ConfigSpecConstants#a_StopOnDisconnect} = TRUE</code>
     * then all current resource transactions are terminated with 
     * qualifier {@link ResourceEvent#q_Disconnected q_Disconnected}.
     */
    public static final Symbol ev_Disconnected	= ESymbol.CCR_Idle;

    /** 
     * Indicates that <i>all</i> Connections to this MediaService 
     * have been disconnected. 
     * A MediaEvent with this EventID is sent to 
     * <code>{@link MediaServiceListener#onDisconnected}</code> 
     * when the last Connection to this MediaService transitions to the state
     * <code>{@link Connection#DISCONNECTED Connection.DISCONNECTED}</code>.
     * All current resource transactions are terminated with 
     * qualifier {@link ResourceEvent#q_Disconnected q_Disconnected}.
     */
    public static final Symbol ev_TerminalIdle	= ESymbol.SCR_TerminalIdle;
}
