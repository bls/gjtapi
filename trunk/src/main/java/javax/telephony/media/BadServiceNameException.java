/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;
import javax.telephony.media.Symbol;

/**
 * Thrown when the disposition or serviceName String is ill-formed.
 * This Exception indicates the String cannot be (or be mapped to) 
 * a legal service name.
 * <p>
 * Conceptually, this is like getting a <i>reorder</i> tone when dialing
 * a number that is not in the dial plan.
 */
public 
class BadServiceNameException extends NoServiceAssignedException {
    /**
     * Constructs a <code>BadServiceNameException</code> 
     * with no specified detail message. 
     */
    public BadServiceNameException() {super();}

    /**
     * Constructs a <code>BadServiceNameException</code> 
     * with the specified detail message. 
     *
     * @param   s   the detail message.
     */
    public BadServiceNameException(String s) {super(s);}
}
