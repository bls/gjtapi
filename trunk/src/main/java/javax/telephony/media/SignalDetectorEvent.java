/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

/**
 * Defines the methods for SignalDetector Events.
 */
public
interface SignalDetectorEvent extends ResourceEvent, SignalDetectorConstants {
    /**
     * Get SignalBuffer as a String.
     * Each DTMF signal has a single char equivalent, one of:
     * <code> 0,1,2,3,4,5,6,7,8,9,*,#,A,B,C,D</code>.
     * If the Signal Buffer contains non-DTMF signals
     * that do not have defined char equivalents, 
     * then a '?' is inserted in that place.
     * <p>
     * <b>Note:</b>
     * When getEventID() = <code>ev_flushBuffer</code>
     * and getQualifier() = <code>q_RTC</code>, 
     * some implemetations may not return the flushed signals.
     * In that case, this method returns <code>null</code>.
     *
     * @return a String of DTMF Signal chars.
     */
    String getSignalString();

    /**
     * Get array of signal names.
     * Each signal has a Symbol "name".
     * <P>
     * This method is useful for examining Signals that  are not
     * represented by single character names. In particular, 
     * extensions to the SignalDetector may use the Symbol namespace
     * to define new signals.
     * <P>
     * <b>Note:</b>
     * When getEventID() = <code>ev_flushBuffer</code>
     * and getQualifier() = <code>q_RTC</code>, 
     * some implemetations may not return the flushed signals.
     * In that case, this method returns <code>null</code>.
     *
     * @return an Array of Symbols, one for each Signal in the event.
     */
    Symbol[] getSignalBuffer();

    /**
     * When eventID is one of <code>ev_Pattern[<b>i</b>]</code>, 
     * return the int <code><b>i</b></code>.
     * If eventID is not <code>ev_Pattern[i]</code>, 
     * then return minus one (<code>-1</code>).
     * <p>
     * @return an int indicating which pattern was matched.
     */
    int getPatternIndex();

    /* <b>Implementation note:</b>
     * For i < 32 this could be a trivial subtraction operation,
     * because the first 32 ev_Pattern symbols are linear.
     * However, if the vendor has extended the number of
     * available patterns, this method must recognize that
     * and do the right thing.  This could be
     * implemented with a Hashtable, or a linear search,
     * or the vendor may use a piecewise linear approach.
     * <p>
     * Better yet, we build the SignalDetectorEvent to include
     * the index directly and this is just the accessor.
     */
}
