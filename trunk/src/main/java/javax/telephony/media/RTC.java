/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */

/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

/**
 * An RTC (Run Time Control) object associates a trigger condition
 * with some action.
 * When the Resource identified by the trigger condition emits an event
 * that corresponds to that condition, the action command is sent
 * to the Resource whose operation is being controlled by the RTC.
 * <p>
 * The RTC trigger Symbols are a subset of the event Symbols.  
 * The events available as RTC triggers are defined 
 * by the resource that generates them.
 * <ul>
 * <li>The name of an RTC <i>trigger</i> has the form:
 * <code>rtcc_&ltEventName&gt</code>.
 * </li>
 * <li>
 * The name of an RTC <i>action</i> has the form:
 * <code>rtca_&ltCommandName&gt</code>.
 * </li></ul>
 * <p>
 * For example:<pre>
 * static RTC speedUp = new RTC(SignalDetector.rtcc_Pattern[2],
 *                                      Player.rtca_SpeedUp);
 *        RTC[] rtcs = {speedUp};
 *        playEvent = play(..., rtcs, ...);</pre>
 * In this case, when the signal detector resource 
 * matches Pattern 2, the Player resource recieves the 
 * <code>Player.rtca_speedUp</code> command.
 * <p>
 * RTC objects typically appear in media transaction commands
 * as elements of the <code>RTC[] rtc</code> argument.
 */
public class RTC {
    // This is basically a read-only struct of two fields:
    protected Symbol trigger;
    protected Symbol action;

    /**
     * Create an RTC object linking the <code>trigger</code>
     * condition to the <code>action</code> command.
     * <p>
     * The Symbols used to define an RTC <i>trigger</i> are a subset
     * of the EventID Symbols.  The applicable RTC triggers are defined 
     * by the resource that generates the event.
     * <ul>
     * <li>The name of an RTC <i>trigger</i> has the form:
     * <code>rtcc_&ltEventName&gt</code>.
     * </li>
     * <li> The name of an RTC <i>action</i> has the form:
     * <code>rtca_&ltCommandName&gt</code>.
     * </li></ul>
     */
    public RTC(Symbol trigger, Symbol action) {
	this.trigger = trigger;
	this.action = action;
    }
    
    /** Return the Symbol that defines the trigger condition for this RTC.
     * @return the Symbol that defines the trigger condition for this RTC.
     */
    public Symbol getTrigger() {return trigger;}

    /** Return the Symbol that defines action for this RTC.
     * @return the Symbol that defines action for this RTC.
     */
    public Symbol getAction() {return action;}

    /**
     * The common RTC to stop a prompt when a DTMF is detected.
     */
    public final static RTC 
	SigDet_StopPlay = new RTC(SignalDetector.rtcc_SignalDetected,
				  Player.rtca_Stop);
    
    /**
     * The common RTC to stop a recording when a DTMF is detected.
     */
    public final static RTC 
	SigDet_StopRecord = new RTC(SignalDetector.rtcc_SignalDetected,
				    Recorder.rtca_Stop);

    /*
     * The RTC that implements <code>a_StopOnDisconnect</code>.
     * This RTC is enabled by default.
     * It is disabled by requesting 
     * <code>({@link ConfigSpecConstants#a_StopOnDisconnect} == FALSE)</code>.
     */
    //public final static RTC
    //Disconnect_Stop = new RTC(ESymbol.CCR_Idle, ESymbol.Group_Stop);

    /*
     * The commonly used RTC to stop a prompt when ASR recognises voice energy.
     */
    //public final static RTC 
    //ASR_StopPlay = new RTC(ESymbol.ASR_BargeIn, ESymbol.Player_Stop);

}
