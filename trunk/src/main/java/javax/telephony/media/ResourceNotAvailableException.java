/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

/**
 * Thrown when configuration fails due to resource contention. 
 * One or more requested resources could not be obtained
 * because all instances are busy (or out-of-service).
 */
public
class ResourceNotAvailableException extends MediaConfigException {
    /**
     * Constructs a <code>ResourceNotAvailableException</code> 
     * with no specified detail message. 
     */
    public ResourceNotAvailableException() {super();}

    /**
     * Constructs a <code>ResourceNotAvailableException</code> 
     * with the specified detail message. 
     *
     * @param   s   the detail message.
     */
    public ResourceNotAvailableException(String s) {super(s);}
}
