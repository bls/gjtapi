/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media.impl;
import javax.telephony.media.*;
import java.util.Hashtable;


/** 
 * A Parser for S100-R2 compatible ECTF defined Symbols with FORMAT==0.
 * <p>
 * The internal numeric value of Format0 is partitioned into fields:<br>
 * Format (2 LSBs), and for format = 00:<br>
 * Reserved (next  2 LSBs), Object (next 12 LSBs), Item (top 16 bits)<br>
 * <p>
 * For ECTF defined Symbols, the Vendor (ECTF) is <i>implicity</i> <b>zero</b>.
 */
// package
public
class Format0 implements Stringifier.Parser {
    /** IIIIIIIIIIIIIIIIOOOOOOOOOOOOXXFF */
    private static final int FORMAT = 00;
    private static final int maskFormat	= 0x0000000f; // format bits [==0]
    private static final int maskVendor	= 0x0000000f; // format & vendor [==0]
    private static final int maskObject	= 0x0000ffff; // object bits 
    private static final int maskItem	= 0xffff0000; // just the Item bits
    
    private static final int venShift 	= 0; 	      // Vendor = Format = 0:
    private static final int objShift 	= 4;
    private static final int itemShift	= 16;
	
    public boolean isFormat(int value) {return (value & maskFormat) == FORMAT;}
    public int getVendor(int value)    {return (value & maskVendor);}
    public int getVendorOnly(int value){return (value & maskVendor)>>venShift;}
    public int getObject(int value)    {return (value & maskObject);}
    public int getObjectOnly(int value){return (value & maskObject)>>objShift;}
    public int getItem(int value)      {return (value & maskItem)>>itemShift;}

    /** Used exclusively for Vendor = ECTF (ven == 0). */
    public static Symbol f0symbol(int obj, int item) {
	return Symbol.getSymbol((item<<itemShift)+(obj<<objShift)+FORMAT);
    }
    /** use Format0 to Stringify Symbols with value:(format==0) */
    static {Stringifier.addParser(new Format0());}


    /** Setup print string translations for "Object" of ECTF 
     * String is assigned to the combination of ObjectVendorFormat
     * as returned by Stringifier.getObject();
     * [value is computed with vendor and format = 0]
     */
    private static void setObjectName(int obj, String name) {
	Stringifier.setObjectName(obj<<objShift, name+"_ECTF");
    }

    static {
	setObjectName(R2Symbol.Any_		, "Any");
	setObjectName(R2Symbol.ASR_		, "ASR");
	setObjectName(R2Symbol.CCR_		, "CCR");
	setObjectName(R2Symbol.CPR_		, "CPR");
	setObjectName(R2Symbol.Container_	, "Container");
	setObjectName(R2Symbol.Error_		, "Error");
	setObjectName(R2Symbol.FaxReceiver_	, "FaxReceiver");
	setObjectName(R2Symbol.FaxSender_	, "FaxSender");
	setObjectName(R2Symbol.Faxll_		, "Faxll");
	setObjectName(R2Symbol.Fax_		, "Fax");
	setObjectName(R2Symbol.Group_		, "Group");
	setObjectName(R2Symbol.KVS_		, "KVS");
	setObjectName(R2Symbol.Language_	, "Language");
	setObjectName(R2Symbol.Macro_		, "Macro");
	setObjectName(R2Symbol.Message_		, "Message");
	setObjectName(R2Symbol.Player_		, "Player");
	setObjectName(R2Symbol.Recorder_	, "Recorder");
	setObjectName(R2Symbol.SCR_		, "SCR");
	setObjectName(R2Symbol.Session_		, "Session");
	setObjectName(R2Symbol.SD_		, "SD");
	setObjectName(R2Symbol.SG_		, "SG");
	setObjectName(R2Symbol.Admin_		, "Admin");
	setObjectName(R2Symbol.SessionManager_	, "SessionManager");
	setObjectName(R2Symbol.Symbol_		, "Symbol");
	setObjectName(R2Symbol.Application_	, "Application");
	setObjectName(R2Symbol.Service_		, "AppService");
    }
}
