/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media.impl;
import javax.telephony.media.*;
import java.util.Hashtable;

/**
 * A Parser for S100-R2 compatible, Vendor defined Symbols with FORMAT==1.
 * <p>
 * The internal numeric value of Format1 is partitioned into fields:<br>
 * Format (2 LSBs), and for format = 01: <br>
 * Vendor   (next 10 LSBs), Object (next 10 LSBs), ItemName (top 10 bits)<br>
 * <p>
 * Server and Resource vendors can use this class, and their [ECTF] defined
 * vendor ID to create unique Symbols. Print strings for whole symbols
 * or Object/Vendor fields may be registered to provide readable print strings.
 */
// package
public
class Format1 implements Stringifier.Parser {
    /** IIIIIIIIIIOOOOOOOOOOVVVVVVVVVVFF */
    private static final int FORMAT = 01;
    private static final int maskFormat	= 0x00000003;	// format bits [==1]
    private static final int maskVendor	= 0x00000fff;	// format & vendor
    private static final int maskObject	= 0x003fffff;	// object bits 
    private static final int maskItem	= 0xffC00000;	// just the Item bits

    private static final int venShift = 2;
    private static final int objShift = 12;
    private static final int itemShift = 22;
    
    public boolean isFormat(int value) {return (value & maskFormat) == FORMAT;}
    public int getVendor(int value)    {return (value & maskVendor);}
    public int getVendorOnly(int value){return (value & maskVendor)>>venShift;}
    public int getObject(int value)    {return (value & maskObject);}
    public int getObjectOnly(int value){return (value & maskObject)>>objShift;}
    public int getItem(int value)      {return (value & maskItem)>>itemShift;}

    /** create Symbol for given vendor, object and itemName fields. */
    public static Symbol f1symbol(int ven, int obj, int item) {
	return Symbol.getSymbol((item<<itemShift)+(obj<<objShift)+(ven<<venShift)+FORMAT);
    }
    /** use Format1 to Stringify Symbols with value:(FORMAT==1) */
    static {Stringifier.addParser(new Format1());}
}
