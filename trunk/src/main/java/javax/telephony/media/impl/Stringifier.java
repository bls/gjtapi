/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */

/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media.impl;
import javax.telephony.media.*;
import java.util.Hashtable;

public
class Stringifier {
    /** object with this interface help us parse the
     * various symbol formats.*/
    public static
    interface Parser {
	/** return true if this Parser can parse this symbol value. */
	boolean isFormat(int value);
	
	/**
	 * Isolate the bit field containing Vendor & Format.
	 * @return the int value of the Vendor field.
	 */
	int getVendor(int value);

	/**
	 * Return the official ECTF vendor value.
	 */
	int getVendorOnly(int value);
	
	/**
	 * Isolate the bit field containing Object, Vendor, Format bits.
	 * @return the int value of the Object, Vendor, and Format fields
	 */
	int getObject(int value);
	
	/**
	 * @return the int field containing only the Object bits.
	 */
	int getObjectOnly(int value);
	
	/**
	 * the bit field containing the Item, right-shifted as necessary.
	 * @return the int value of the Item segment of this Symbol.
	 */
	int getItem(int value);
    }

    public
    static Parser[] parsers = new Parser[0];

    public static void addParser(Parser parser) {
	synchronized (parsers) {
	    int len = parsers.length;
	    Parser[] parsers1 = new Parser[len+1];
	    System.arraycopy(parsers, 0, parsers1, 0, len);
	    parsers1[len] = parser;
	    parsers = parsers1;
	}
    }
    public static Parser getParser(int value) {
	for(int i = parsers.length-1; i>=0; i--) 
	    if (parsers[i].isFormat(value)) return parsers[i];
	return null;
    }
    
    /**
     * Decode and print as many of the Symbol components as
     * are registered.
     * Unregistered segments are printed in hex.
     */
    public static String toString(Symbol symbol) {
	// see if whole name is registered for this value
	String symstr = (String)symbolName.get(symbol);
	if (symstr != null) return symstr;
	// whole value is not registered, try parsing it:
	
	int value = symbol.hashCode();
	// look for a Symbol parser for this format:
	Parser parser = getParser(value);
	if (parser == null)	// if no Parser, then give up.
	    // everything in hex, don't bother with the underbars:
	    return "Symbol(0x" + Integer.toString(value, 16)+")";
	

	// This Symbol not registered, item will have to be in hex.
 	String itemstr = "0x" + Integer.toString(parser.getItem(value), 16);

	// if Obj_VENDOR is registered use that:
	int objectValue = parser.getObject(value);
	symstr = getObjectName(objectValue);
	if (symstr != null) return symstr + "_" + itemstr;

	// Object name not registered, use hex for Object segment.
	String objstr = "0x"+Integer.toString(parser.getObjectOnly(value),16);

	// Check for known vendor:
	int vendorValue = parser.getVendorOnly(value);
	String vendor = getVendorName(vendorValue);
	if (vendor != null) return objstr+"_"+vendor+"_"+itemstr;
	
	// everything in hex, but parsed into fields:
	vendor = "0x"+Integer.toString(parser.getVendorOnly(value),16);
	return objstr+"_"+vendor+"_"+itemstr;

    }
    /** Hashtable maps Symbol to its Name */
    static Hashtable symbolName = new Hashtable();
    public static void setSymbolName(Symbol symbol, String name) {
	symbolName.put(symbol, name);
    }

    /** map from ObjectVendor to name */
    protected static Hashtable venobjName = new Hashtable();

    /** allow vendors to label their ObjectVendorFormat combination */
    public static void setObjectName(int obj, String name) {
        venobjName.put(new Integer(obj), name);
    }
    /** @return the String associated with a given ObjectVendorFormat */
    protected static String getObjectName(int objectValue) {
	return (String)venobjName.get(new Integer(objectValue));
    }

    /* There should also be a "Symbol.parseName(String)"
     * which tries to reconstitute a Symbol based on a toString() result.
     * That is, recode from hex if necessary.
     */

    /** Register a print String associated with the given vendor ID. 
     * Vendor IDs and the associated names are assigned by ECTF.
     * @see "http://www.ectf.org/ectf/tech/vendint.htm"
     */
    public static void registerVendor(int vendor, String name) {
	vendorString.put(new Integer(vendor), name);
    }
    /** Return Vendor's name String, if it is registered. 
     * @see registerVendor
     */
    protected static String getVendorName(int vendorValue) {
	return (String)vendorString.get(new Integer(vendorValue));
    }
    /** Contains String names for each vendorID. 
     * Could be an Array, since the VendorID is compact small int
     */
    private static Hashtable vendorString = new Hashtable();

    /**
     * We put these into a table with the String versions, 
     * so one can translate Symbol values back to reasonable names.
     * <p>
     * This table downloaded 10/1/98 from
     * linkto: http://www.ectf.org/ectf/tech/vendrgty.htm
     */
    static {
	registerVendor(0, "ECTF");
	registerVendor(1, "DLGC");
	registerVendor(2, "AMTEVA");
	registerVendor(3, "NMS");
	registerVendor(4, "BRT");
	registerVendor(5, "AMTEL");
	registerVendor(6, "NORTEL");
	registerVendor(7, "PERI");
	registerVendor(8, "DTAG");
	registerVendor(9, "ROCKWELL");
	registerVendor(10, "VOICETEK");
	registerVendor(11, "PIKA");
	registerVendor(12, "CSELT");
	registerVendor(13, "ACULAB");
	registerVendor(14, "DASA");
	registerVendor(15, "MCI");
	registerVendor(16, "PTI");
	registerVendor(17, "SPCO");
	registerVendor(18, "ATT");
	registerVendor(19, "WILHELM");
	registerVendor(20, "ELANTTS");
	registerVendor(21, "LHSP");
	registerVendor(22, "VICORP");
	registerVendor(23, "DTB");
	registerVendor(24, "PSP");
	registerVendor(25, "TSI");
	registerVendor(26, "PHYLON");
	registerVendor(27, "COMMETREX");
	registerVendor(28, "ACB");
	registerVendor(29, "VIPR");
	registerVendor(30, "ESP");
	registerVendor(31, "ATS_ISOS");
	registerVendor(32, "ND");
	registerVendor(33, "NP1");
	registerVendor(34, "AVTEC");
	registerVendor(35, "LUCENT");
	registerVendor(36, "PAS");	
    }
}
