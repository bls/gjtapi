/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

/**
 * Thrown by {@link MediaService#configure MediaService.configure} if
 * another configuration (or release) request is already active.
 * <p>
 * Thrown by {@link MediaService#bindToTerminalName bindToTerminalName} 
 * or {@link MediaService#bindToTerminal bindToTerminal} 
 * if the requested Terminal is already bound.
 */
public
class ConfigFailedBusyException extends MediaConfigException {
    /**
     * Constructs a <code>ConfigFailedBusyException</code> 
     * with no specified detail message. 
     */
    public ConfigFailedBusyException() {super();}

    /**
     * Constructs a <code>ConfigFailedBusyException</code> 
     * with the specified detail message. 
     *
     * @param   s   the detail message.
     */
    public ConfigFailedBusyException(String s) {super(s);}
}
