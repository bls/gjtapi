/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;
/**
 * Represents a closed interval delimited by upper and lower ints.
 * <p>
 * Some Resources have attributes and/or parameters that use
 * this range type.
 */
public class IntRange {
    /** Field containing the lower bound. */
    public int lower; 
    /** Field containing the upper bound. */
    public int upper; 
    /**
     * Construct an IntRange, a closed interval
     * delimited by the [lower, upper] bounds.
     * @param lower the int at the lower bound of the range.
     * @param upper the int at the upper bound of the range.
     */
    public IntRange(int lower, int upper) {
	this.lower=lower;
	this.upper=upper;
    }
    public String toString() {
	return "IntRange["+lower+","+upper+"]";
    }
}
