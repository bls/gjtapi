/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

/**
 * Thrown when one of the 
 * <a href="MediaService.html#bindMethods">MediaService bind methods</a>
 * is interrupted/revoked by 
 * {@link MediaService#cancelBindRequest cancelBindRequest}.
 */
public class BindCancelledException extends MediaBindException {
    /**
     * Constructs a <code>BindCancelledException</code> 
     * with no specified detail message. 
     */
    public BindCancelledException() {super();}

    /**
     * Constructs a <code>BindCancelledException</code> 
     * with the specified detail message. 
     *
     * @param   s   the detail message.
     */
    public BindCancelledException(String s) {super(s);}
}
