/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

import java.util.Dictionary;

/**
 * Defines a method for sending signals out to the telephony network.
 */
public 
interface SignalGenerator extends Resource, SignalGeneratorConstants {
    /**
     * Transmit a series of signals out the Terminal to the network. 
     * <p>
     * Each signal is defined by a Symbol in the Symbol[].
     * 
     * @param signals an array of Symbols that defines a signal.
     * @param rtc an array of RTC objects that controls this transactions.
     * @param optargs a Dictionary of optional arguments 
     * @return a SignalGeneratorEvent
     * @exception MediaResourceException if this request fails. 
     */
    SignalGeneratorEvent sendSignals(Symbol[] signals, RTC[] rtc, 
				     Dictionary optargs)
	throws MediaResourceException;
	
    /**
     * Transmit a series of signals out the Terminal to the network. 
     * <p>
     * Each signal is defined by a char in the String.
     *
     * @param signals an array of Symbols that defines a signal.
     * @param rtc an array of RTC objects that controls this transactions.
     * @param optargs a Dictionary of optional arguments 
     * @return a SignalGeneratorEvent.
     * @exception MediaResourceException if this request fails. 
     */
    SignalGeneratorEvent sendSignals(String signals, RTC[] rtc,
				     Dictionary optargs)
	throws MediaResourceException;
}
