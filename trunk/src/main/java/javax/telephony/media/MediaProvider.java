/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;
/**
 * A marker interface indicating that this object (typically a JTAPI Provider)
 * can be used directly as an argument to construct a MediaService object.
 * <p>
 * A MediaProvider implementation will provide the media services
 * (for Resource allocation and configuration), call processing services
 * for bind and release, and may provide other services such as OA&M.
 * <p>
 * A MediaService must be associated with some implementation
 * that provides the requiste call and media services.
 * The simplest way to find the media services implementation
 * is to use the default obtained by using the no argument constructor.
 * <p>
 * The most general way for an application to specify the 
 * media service implementation to be used is by supplying a 
 * JtapiPeer class name and a peer-specific <i>providerString</i> 
 * to a MediaService constructor.
 * <p>
 * Sometimes, the media services implementation is obtained by
 * an alternative, vendor specific technique. For example, a vendor
 * may specify that its JTAPI Provider object supports media services.
 * In these cases, it should be declared that the object
 * <code>implements MediaProvider</code>, so the object 
 * may be cast to a MediaProvider and used in the constructor
 * for a MediaService.
 * <p>
 * @see BasicMediaService#BasicMediaService(MediaProvider)
 */
public 
interface MediaProvider {}
/**
 * Note to reviewers:
 * The relationship between a MediaService object and the MediaProvider
 * are currently vendor specific.  If/when a MediaProvider SPI interface
 * is published, there may be standard methods that are required of a
 * MediaProvider when used by a standard implementation of a MediaService.
 */
