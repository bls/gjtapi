/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

/**
 * The simple, synchronous interface to Resource Events.
 * ResourceEvent objects are returned from Resource methods
 * and non-tranactional ResourceEvents are delivered to a 
 * ResourceListener.
 * <p>
 * When a media resource method (for a Player, Recorder, etc.) completes 
 * successfully, it returns a ResourceEvent to the invoking program.
 * If the method does not complete normally (ie, if the post-conditions
 * are not satisfied), then the method throws a
 * {@link MediaResourceException}
 * that contains a ResourceEvent (accessible using getResourceEvent()).
 * In either case, the ResourceEvent can be queried for additional 
 * information.
 *
 * <h4>ResourceEvent properties:</h4>
 *
 * Each ResourceEvent has read-only properties: 
 * EventID, an Error or Qualifier, and possibly an RTCTrigger.
 * ResourceEvent defines <i>get</i> methods for each.
 * <p>
 * If a media resource generates events that have additional fields,
 * then the Event interface for that resource extends ResourceEvent
 * by defining methods to access the additional information.
 * 
 * <h4>Names for Symbol constants used in ResourceEvent objects:</h4>
 * 
 * The constants (Symbols and ints) defined in {@link ResourceConstants}
 * are inherited by ResourceEvent.
 * Other Resource interfaces may define additional constants.
 * <p>
 * <table border="1" cellpadding="3">
 * <tr><td>The Symbols returned by</td>
 * <td>have names of the form:</td></tr>
 * <tr><td><code>getEventID()</code></td>
 * 		     <td><code>&nbsp;&nbsp;ev_OperationName</code></td></tr>
 * <tr><td><code>getQualifier()</code></td>
 * 		     <td><code>&nbsp;&nbsp;&nbsp;q_ReasonName</code></td></tr>
 * <tr><td><code>getRTCTrigger()</code></td>
 * 		     <td><code>rtcc_ConditionName</code></td></tr>
 * <tr><td><code>getError()</code></td>
 * 		     <td><code>&nbsp;&nbsp;&nbsp;e_ErrorName</code></td></tr>
 * </table>
 */
public interface ResourceEvent extends MediaEvent, ResourceConstants {
    /**
     * Get the Qualifier of this event.
     * Additional information about how/why a transaction terminated.
     * <p>
     * Standard return values are one of:
     * <br><code>q_Standard, q_Duration, q_RTC, q_Stop</code><br>
     * which indicate the reason or mode of completion.
     * Other qualifiers may be documented in the resource event classes.
     * <p>
     * If the associated method or transaction did not succeed,
     * this methods returns null; use {@link #getError()} instead. 
     * <p>
     * Because many qualifiers are orthogonal to the EventID, 
     * using a contained qualifier is simpler
     * than using a subclassing scheme.
     *
     * @return one of the qualifier Symbols (q_Something)
     */
    public Symbol getQualifier();
	
    /**
     * Get the RTC Trigger that caused this transaction completion.
     * This is non-null iff <code>getQualifier == ResourceEvent.q_RTC</code>
     * <p>
     * The Symbol returned has a name of the form <code>rtcc_Something</code>.
     * 
     * @return a Symbol identifying an RTC trigger condition.
     */
    public Symbol getRTCTrigger();

    /**
     * Identify the reason or cause of an error or failure.
     * <p>
     * If this ResourceEvent <b>is</b> associated with an Exception,
     * then <code>getError()</code> returns a Symbol that identifies 
     * the problem.
     * <br>If this ResourceEvent <b>is not</b> associated with an Exception,
     * then <code>getError()</code> returns the Symbol <code>e_OK</code>.
     * <p>
     * A list of error Symbols is defined in interface {@link ErrSym}.
     * @return one of the error Symbols. 
     */
    public Symbol getError();    
}
