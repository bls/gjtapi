/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

import javax.telephony.*;	// get core objects and Exceptions
import java.util.Dictionary;

/* Generated: Sat May 15 08:24:05 PDT 1999 */
/* 
   
*/
/**
 * Implements MediaService and the basic Resource interfaces.
 * Resource methods are delegated to the bound MediaGroup.
 * <p>
 * This class understands about binding and configuration,
 * and knows how to delegate resource methods to whatever
 * object or objects are bound to this one. And conversely
 * understand how to register Listeners for each resource,
 * and dispatch events from any of the resources 
 * to all of the ResourceListeners.
 * <p>
 * The Resource methods in BasicMediaService are documented
 * in the associated Resource interface definition.
 * <P>
 * <b>Note:</b>
 * This class is documented as extending 
 * DispatchMediaService. However,
 * actual implementations may extend some other vendor specific class.
 * In all cases, this class (or one of its super-classes)
 * implements MediaService and the defined Resource interfaces.
 */
public class BasicMediaService extends DispatchMediaService
    implements MediaService, 
	       Player, 
	       Recorder, 
	       SignalDetector, 
	       SignalGenerator
{
    /**
     * Create an unbound MediaService,
     * using the installation default MediaProvider.
     */
    public BasicMediaService() {super();}
    
    /**
     * Create an unbound MediaService, using the given MediaProvider object.
     * The MediaProvider object may be a JTAPI core Provider,
     * or may be obtained by other vendor-specific means.
     *
     * @param provider a MediaProvider instance
     */
    public BasicMediaService(MediaProvider arg1) {
	super(arg1);}
    
    /**
     * Create an unbound MediaService, using a MediaProvider
     * identified by the two String arguments.
     * <p>
     * The format of a <code>providerString</code> 
     * as used to obtain a MediaProvider from the JtapiPeer
     * is vendor specific.
     * <p>
     * The format of a <code>providerString</code> 
     * as used to obtain a Jtapi [core/Call] Provider 
     * is documented in 
     * {@link JtapiPeer#getProvider(String) JtapiPeer.getProvider(String)}.
     *
     * @param peerName the name of a Class that implements JtapiPeer.
     * @param providerString a "login" string for that JtapiPeer.
     *
     * @throws ClassNotFoundException if JtapiPeer class is not found
     * @throws IllegalAccessException if JtapiPeer class is not accessible
     * @throws InstantiationException if JtapiPeer class does not instantiate
     * @throws ProviderUnavailableException if Provider is not available
     * @throws ClassCastException if Provider instance is not a MediaProvider
     */
    public BasicMediaService(String peerName, String providerString) throws 
	ClassNotFoundException, 
    	InstantiationException, 
    	IllegalAccessException, 
    	ProviderUnavailableException {
	    super(peerName, providerString);}

    /* Here are the Resource transaction methods */
    /*
     * Javadoc should supply a pointer to actual documentation.
     * someday rewrite codegen using doclet API to get parent docs.
     */


    /* @see Player#play(String, int, RTC[], Dictionary) */ 
    public PlayerEvent play(String string0, int int1, RTC[] rtc2, Dictionary dictionary3)
	throws MediaResourceException 
    {
	return ((Player)checkGroup()).
	    play(string0, int1, rtc2, dictionary3);
    }

    /* @see Player#play(String[], int, RTC[], Dictionary) */ 
    public PlayerEvent play(String[] string0, int int1, RTC[] rtc2, Dictionary dictionary3)
	throws MediaResourceException 
    {
	return ((Player)checkGroup()).
	    play(string0, int1, rtc2, dictionary3);
    }

    /* @see Recorder#record(String, RTC[], Dictionary) */ 
    public RecorderEvent record(String string0, RTC[] rtc1, Dictionary dictionary2)
	throws MediaResourceException 
    {
	return ((Recorder)checkGroup()).
	    record(string0, rtc1, dictionary2);
    }

    /* @see SignalDetector#flushBuffer() */ 
    public SignalDetectorEvent flushBuffer()
	throws MediaResourceException 
    {
	return ((SignalDetector)checkGroup()).
	    flushBuffer();
    }

    /* @see SignalDetector#retrieveSignals(int, Symbol[], RTC[], Dictionary) */ 
    public SignalDetectorEvent retrieveSignals(int int0, Symbol[] symbol1, RTC[] rtc2, Dictionary dictionary3)
	throws MediaResourceException 
    {
	return ((SignalDetector)checkGroup()).
	    retrieveSignals(int0, symbol1, rtc2, dictionary3);
    }

    /* @see SignalGenerator#sendSignals(String, RTC[], Dictionary) */ 
    public SignalGeneratorEvent sendSignals(String string0, RTC[] rtc1, Dictionary dictionary2)
	throws MediaResourceException 
    {
	return ((SignalGenerator)checkGroup()).
	    sendSignals(string0, rtc1, dictionary2);
    }

    /* @see SignalGenerator#sendSignals(Symbol[], RTC[], Dictionary) */ 
    public SignalGeneratorEvent sendSignals(Symbol[] symbol0, RTC[] rtc1, Dictionary dictionary2)
	throws MediaResourceException 
    {
	return ((SignalGenerator)checkGroup()).
	    sendSignals(symbol0, rtc1, dictionary2);
    }

}

