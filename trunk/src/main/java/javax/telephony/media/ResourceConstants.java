/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

/**
 * Defines constants used by all Resources.
 * These are inherited by every <i>Resource</i> interface,
 * so they are accessible from any Resource class or instance.
 */
public interface ResourceConstants {
    /** Special int value (-1) used to indicate infinite timeout. */
    int		FOREVER 		= -1;
	
    /** Integer value for FOREVER, when used in a value in a Dictionary */
    Integer	v_Forever 		= new Integer(FOREVER);
	
    /**
     * Symbol returned from {@link ResourceEvent#getError()}
     * when an operation fails because the associated Terminal
     * is in the <code>Connection.DISCONNECTED</code> state.
     */
    Symbol e_Disconnected		= ESymbol.Error_Disconnected;

    /**
     * Symbol returned from {@link ResourceEvent#getError()} 
     * if there is no error.
     */
    Symbol e_OK				= ESymbol.Error_OK;

    /**
     * Symbol returned from {@link ResourceEvent#getQualifier()}
     * if an operation completed because the requested duration
     * was reached.
     */
    Symbol q_Duration 			= ESymbol.Any_Duration;

    /**
     * Qualifier: normal, default completion.
     */
    Symbol q_Standard 			= ESymbol.Any_Standard;
    
    /**
     * Qualifier: Completion caused by a Stop.
     * Normal response from operations stopped by MediaService.stop() method.
     */
    Symbol q_Stop 			= ESymbol.Any_Stop;
    
    /**
     * Qualifier: Completion caused by a Run-Time Control.
     */
    Symbol q_RTC 			= ESymbol.Group_RTC;

    /**
     * Indicates that the operation was terminated because 
     * the Terninal's Connection to the current Call is in the
     * <code>Connection.DISCONNECTED</code> state.
     * <p>
     * <b>Note:</b>
     * Some implementations may use <code>q_RTC</code> 
     * and <code>rtcc_Disconnected</code>. 
     */
    Symbol q_Disconnected		= ESymbol.CCR_Idle;

    /**
     * RTC Trigger: Operation terminated because the Connection to the
     * associated Terminal is in the <code>Connection.DISCONNECTED</code> state.
     * This is delivered in a ResourceEvent via DisconnectedException.
     * @see javax.telephony.Connection#DISCONNECTED Connection.DISCONNECTED
     */
    Symbol rtcc_Disconnected		= ESymbol.CCR_Idle;

    /** 
     * The Symbol returned from {@link ResourceEvent#getRTCTrigger()}
     * representing the RTC Condition for ResourceEvents triggered by 
     * {@link MediaService#triggerRTC(Symbol)}.
     */
    Symbol rtcc_TriggerRTC 		= ESymbol.Group_RTCTrigger;

}
