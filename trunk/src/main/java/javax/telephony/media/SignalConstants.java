/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

/**
 * SignalConstants supplies the names of the DTMF signals.
 * <p>
 * These Symbols are used by SignalGenerator and SignalDetector.
 */
public 
interface SignalConstants {

    /** Symbol for DTMF 0 */
    Symbol v_DTMF0		= ESymbol.SG_DTMF0;
    /** Symbol for DTMF 1 */
    Symbol v_DTMF1		= ESymbol.SG_DTMF1;
    /** Symbol for DTMF 2 */
    Symbol v_DTMF2		= ESymbol.SG_DTMF2;
    /** Symbol for DTMF 3 */
    Symbol v_DTMF3		= ESymbol.SG_DTMF3;
    /** Symbol for DTMF 4 */
    Symbol v_DTMF4		= ESymbol.SG_DTMF4;
    /** Symbol for DTMF 5 */
    Symbol v_DTMF5		= ESymbol.SG_DTMF5;
    /** Symbol for DTMF 6 */
    Symbol v_DTMF6		= ESymbol.SG_DTMF6;
    /** Symbol for DTMF 7 */
    Symbol v_DTMF7		= ESymbol.SG_DTMF7;
    /** Symbol for DTMF 8 */
    Symbol v_DTMF8		= ESymbol.SG_DTMF8;
    /** Symbol for DTMF 9 */
    Symbol v_DTMF9		= ESymbol.SG_DTMF9;
    /** Symbol for DTMF A */
    Symbol v_DTMFA		= ESymbol.SG_DTMFA;
    /** Symbol for DTMF B */
    Symbol v_DTMFB		= ESymbol.SG_DTMFB;
    /** Symbol for DTMF C */
    Symbol v_DTMFC		= ESymbol.SG_DTMFC;
    /** Symbol for DTMF D */
    Symbol v_DTMFD		= ESymbol.SG_DTMFD;
    
    /** Symbol for DTMF * */
    Symbol v_DTMFStar		= ESymbol.SG_DTMFStar;
    /** Symbol for DTMF # */
    Symbol v_DTMFHash		= ESymbol.SG_DTMFHash;
    
    /** Symbol for CNG */
    Symbol v_CNG		= ESymbol.SG_CNG;
    /** Symbol for CED */
    Symbol v_CED		= ESymbol.SG_CED;

    /**
     * Specifies the chars to use when non standard Signals
     * are converted to a String or vice versa.
     * The associated value is a Object[] even numbered elements
     * are a Symbol, odd numbered elements are the associated Character.
     * <p>
     * For example, The standard values could be represented like:
     * <pre>
     * Object[] v_StandardDTMFChars = { 
     *     v_DTMF0, new Character('0'),
     *     v_DTMF1, new Character('1'),
     *     v_DTMF2, new Character('2'),
     *     v_DTMF3, new Character('3'),
     *     // ...
     *     };
     * </pre>
     */
    Symbol p_SymbolChar		= ESymbol.SG_SymbolChar;

}
