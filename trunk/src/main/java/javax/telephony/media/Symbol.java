/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;
import javax.telephony.media.impl.*;
import java.lang.reflect.Field;

/**
 * Symbols are opaque, immutable, nominal tokens. 
 * They are used in applications only for assignment
 * and equality/identity testing. 
 * <p>
 * Symbol is a Java class to encapsulate/implement the ECTF defined Symbols.
 * Symbols are namespace and value space controlled ints
 * that allow extensibilty by vendors, interoperability
 * across languages, and other benefits.
 * <p>
 * Symbols are defined as "public final static" members of various
 * JTAPI Media interfaces. 
 * Symbol constants are used to identify Events, Event Qualifiers, 
 * MediaService and Resource Parameters, RTC triggers Conditions,
 * RTC Actions, and Keys or Values in Dictionaries (or Maps).
 * <p>
 * <H4>Symbol name convention:</H4>
 * A particular Symbol (and its associated value) 
 * may be used in multiple contexts.
 * Each expected usage of the Symbol is given a
 * distinct name, composed by a diffentiating prefix
 * that indicates the usage, 
 * and an <i>item name</i> that identifies the actual value.
 * <p>
 * The convention for Symbol name prefixes is as follows:
 * <table border="1" cellpadding="3">
 * <tr><td>With prefix:</td><td>the Symbol is used as:</td>
 * <td>for example:</td></tr>
 * <tr><td align=right><code>  a_</code></td><td>Attribute</td>
 * <td><code>&nbsp;&nbsp;&nbsp;a_LocalState</code></td></tr>
 * <tr><td align=right><code>  e_</code></td><td>Error</td>
 * <td><code>&nbsp;&nbsp;&nbsp;e_Disconnected</code></td></tr>
 * <tr><td align=right><code> ev_</code></td><td>Event</td>
 * <td><code>&nbsp;&nbsp;ev_SignalDetected</code></td></tr>
 * <tr><td align=right><code>  p_</code></td><td>Parameter</td>
 * <td><code>&nbsp;&nbsp;&nbsp;p_EnabledEvents</code></td></tr>
 * <tr><td align=right><code>  q_</code></td><td>Qualifier</td>
 * <td><code>&nbsp;&nbsp;&nbsp;q_Disconnected</code></td></tr>
 * <tr><td align=right><code>rtca_</code></td><td>RTC Action</td>
 * <td><code>rtca_Stop</code></td></tr>
 * <tr><td align=right><code>rtcc_<code></td><td>RTC Condition</td>
 * <td><code>rtcc_SignalDetected</code></td></tr>
 * <tr><td align=right><code>  v_</code></td><td>Dictionary value</td>
 * <td><code>&nbsp;&nbsp;&nbsp;v_Stop</code></td></tr>
 * </table>
 * <p>
 * Each (non-error) Symbol typically appears as a final static field
 * in each interface or class to which it is relevant. 
 * This is often accomplished by defining the Symbol in a "Constants"
 * interface that is inherited by the classes or interfaces
 * in which the Symbol is relevant.
 * For example, each <i>Resource</i> class has a <i>Resource</i>Constants
 * interface that is inherited by the <i>Resource</i> interface and 
 * the associated <i>Resource</i>Event interface.
 * <p>
 * <b>Note:</b>
 * Symbols used to identify errors, 
 * (that is, the return values from {@link ResourceEvent#getError getError()}
 * are used solely for errors and do not appear in other contexts.
 * Because error Symbols are numerous and may appear in many
 * contexts,
 * the error Symbols are defined in interface {@link ErrSym}
 * and are not always renamed or inherited in other interfaces.
 * Therefore, error Symbols are generally accessed 
 * as <code>ErrSym.e_ItemName</code>.
 * <p>
 * <b>Note:</b>
 * Pre-defined Symbol constants with the same internal value 
 * generally refer to the same Object, but this cannot be relied upon.
 * To compare two Symbols for numerical/logical equivalence, 
 * the {@link #equals equals} method must be used.
 * <p>
 * <b>Note:</b>
 * The other methods on Symbol are useful when debugging,
 * and may be used by JTAPI Media implementors,
 * but are not usually needed (or useful) during production.
 * Symbols are defined by name in final static fields,
 * by the classes that assign some meaning to a Symbol.
 * Applications do not need to create Symbols at runtime.
 */

public 
class Symbol { 
    /*
     * <b>Implementation note:</b>
     * A Symbol wraps its internal int value exactly the way
     * an Integer wraps an int value. The runtime/performance impact 
     * is the same. 
     * Also, Symbols derive their hashcode from the internal int value.
     * <p>
     * There are two internal formats defined for Symbols. 
     * <p>
     * Provider/implementors may need to define new Symbols.
     * The protected static method Symbol.getSymbol(int value) 
     * should be used to create the Symbol for the given value.
     * <p>
     * The internal numeric value of a Symbol is partitioned into fields:
     * Format (2 LSBs), and for format = 00:<br>
     * Reserved (next  2 LSBs), Object (next 12 LSBs), ItemName (top 16 bits)
     * and for format = 01: <br>
     * Vendor   (next 10 LSBs), Object (next 10 LSBs), ItemName (top 10 bits)
     * <p>
     * There are protected methods for parsing a Symbol into its
     * internal fields: format, vendor, object, and item.
     * <p>
     * This is part of S.200 or S.300 level definitions.
     * It is currently published as "package" not "public".
     */
    /** The standard constructor. */
    /*package*/ Symbol(int value) {this.value = value;} 

    /** unused no arg constructor. */
    protected Symbol() {this(0);}
     
    /**
     * Returns a <code>Symbol</code> object 
     * with the given <code>int</code> value. 
     *
     * @param value the int value represented by the <code>Symbol</code>.
     */
    public static Symbol getSymbol(int value) { return new Symbol(value); }

    /**
     * The unique identifing value of this Symbol.
     * this value is final, once it is set in the constructor.
     * <p>
     * int=32 bits, could be long=64 bits in next release. 
     */
    protected int value = 0;

    /** return the internal identifing value, as an int.
     * this value may be of interest to provider implementors
     * and debuggers, but generally should not be used by 
     * applications.
     * @return the internal value.
     * @see Symbol.equals
     */
    /*package*/ int intValue() { return value; }

    /* Note: if the motivated provider developer wants to extract
     * the intValue() of a Symbol, the method hashCode() may be used.
     * However, we do not want to publish "intValue()" per se,
     * because apps should not be peeking inside of a Symbol.
     * <p>
     * Symbol is basically an Integer but restricted to NOMINAL 
     * not SCALAR operations: equals and hashCode, but not an "int".
     */
    
    /**
     * Compares the internal value of two Symbols.
     * <p>
     * The result is <code>TRUE</code> if and only if the argument 
     * is an instance of Symbol and
     * contains the same internal value as this Symbol. 
     * <p>
     * <b>Note:</b>
     * The "<code>==</code>" operator just compares
     * the identity (object handle) of two Symbols. 
     * <p>
     * @param other a Symbol to compare to this Symbol.
     * @return TRUE iff the Symbols have the same value.
     */
    public boolean equals(Object other) {
 	return ((other instanceof Symbol)
		&& (this.value == ((Symbol)other).value));
    }

    /**
     * Returns a hashcode for this Symbol.
     * <p>
     * @return a hashcode value for this Symbol.
     */
    public int hashCode() {
	return value;
    }


    /** 
     * Return a String representation of this Symbol.
     * <p>
     * Attempt to parse the symbol value to identify the 
     * Vendor, Object and ItemName.
     * @return a String representing this Symbol.
     */
    public String toString() {
	return Stringifier.toString(this);
    }

    /** 
     * Returns a <code>Symbol</code> with the given name.
     * <p>
     * Returns a Symbol with the given name, if the name is recognized.
     * <p>
     * The default implementation works for Symbols that are
     * <tt>public static Symbol</tt> fields of public classes.
     * The name should be formatted as: (<b>class.FieldName</b>)
     * The package name "javax.telephony.media" is prepended if necessary.
     * <p>
     * For example: 
     * <br><code>getSymbol("ESymbol.Any_NULL");</code>
     * <br><code>getSymbol("PlayerEvent.q_RTC");</code>
     * <br><code>getSymbol("ErrSym.e_OK");</code>
     * <br><code>getSymbol("com.vendor.package.VendorSyms.Item_Name");</code>
     * <p>
     * A JTAPI Media vendor may extend this implementation; 
     * for example, to prepend other package names.
     * 
     * @param stringName a String that names a Symbol
     * @return the named Symbol, or null if stringName is not registered.
     */
    public static Symbol getSymbol(String stringName) {
	int dot = stringName.lastIndexOf('.');
	if (dot < 1) return null;
	String clsName = stringName.substring(0,dot);
	String itmName = stringName.substring(dot+1);
	try {
	    // itmName must be a "public static Symbol" of clsName
	    return (Symbol)Class.forName(clsName).getField(itmName).get(null);
	} catch (Exception ex) {}
	clsName = "javax.telephony.media." + clsName;
	try {
	    // itmName must be a "public static Symbol" of clsName
	    return (Symbol)Class.forName(clsName).getField(itmName).get(null);
	} catch (Exception ex) {}
	return null;
    }
    
    /**
     * Assign a name to a Symbol.
     * A Symbol may be given multiple names.
     * <code>Symbol.toString()</code> uses the last assigned name.
     *
     * @param symbol the Symbol to be named 
     * @param stringName the name to be given to that Symbol
     */
    public void setName(String stringName) {
	Stringifier.setSymbolName(this, stringName);
    }

    /**
     * Store Symbol name information to someplace.
     * @param pathName a String that indicates where to store the symbol table.
     */
    public static void saveSymbolNames(String pathName) {
	// dump nameSymbol = Symbol.value
	//
	// There should be a standard for this in the next release.
	// basically dump records of the form: 
	// Object_VENDOR_ItemName = 0xFFFFFF;
	//
	// or if using the Java class approach, files in the format of: 
	//    impl/R2Symbols.java PLUS impl/R2SymTable.java
	//    [and then invoke the java compiler!]
    }

    /**
     * Load Symbol name information from someplace.
     * 
     * @param pathName a String that indicates which symbol table to load.
     * <p>
     * <b>Note:</b>
     * The format of the <tt>pathName</tt> String is not yet standardized.<br>
     * The implementation supplied in the reference code accepts strings
     * like: <tt>"class:javax.telephony.media.impl.R2SymbolNames"</tt>.
     * The indicated class file is loaded, which runs its <tt>static</tt>
     * block and invokes <tt>Symbol.setName(String)</tt> for each Symbol.
     */ 
    public static void loadSymbolNames(String pathName) {
	// load into nameSymbol (and symbolName)
	//
	// There should be a standard for this in the next release.
	// read file in the format dumped, 
	// and for each nameSymbol and intValue:
	// ((getSymbol(intValue)).setName(nameSymbol));
	//
	// There may be better formats later, but for now one can use:
	// loadSymbolNames("class:javax.telephony.media.impl.R2SymbolNames");
	// Which loads that class file, and runs its static{} block,
	// which includes a Symbol.setName(String) for each R2Symbol.
	if (pathName.startsWith("class:")) {
	    try {Class.forName(pathName.substring(6));}
	    catch (ClassNotFoundException ex) {
		throw new RuntimeException("Could not load symbols from: "+pathName);
	    }
	}
    }
}    
