/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

import javax.telephony.*;	// get core objects and Exceptions
import java.util.Dictionary;
import java.util.Iterator;
import java.util.Vector;

/* Generated: Sat May 15 08:23:43 PDT 1999 */

/**
 * Implement event dispatching adapters for each MediaListener interface.
 */
//package
class DispatchMediaService extends NullMediaService
    implements MediaServiceListener,
	       PlayerListener,
	       RecorderListener,
	       SignalDetectorListener
{
    public DispatchMediaService() {super();}
    public DispatchMediaService(MediaProvider p) {super(p);}
    public DispatchMediaService(String s1, String s2)
        throws ClassNotFoundException, 
	       InstantiationException, // hope to remove in Jtapi-2.0
	       IllegalAccessException,	// hope to remove in Jtapi-2.0
	       ProviderUnavailableException 
    {super(s1, s2);}

    /* @see MediaServiceListener#onDisconnected(MediaEvent) */ 
    public void onDisconnected(MediaEvent event) {
	MediaListener listener; 
	Iterator iter = theListeners.iterator();
	while(iter.hasNext()) {
	    listener = (MediaListener)iter.next();
	    if(listener instanceof MediaServiceListener) {
		try {
		    ((MediaServiceListener)listener).onDisconnected(event);
		} catch (Exception ex) {}
	    }
	}
    }

    /* @see PlayerListener#onPause(PlayerEvent) */ 
    public void onPause(PlayerEvent event) {
	MediaListener listener; 
	Iterator iter = theListeners.iterator();
	while(iter.hasNext()) {
	    listener = (MediaListener)iter.next();
	    if(listener instanceof PlayerListener) {
		try {
		    ((PlayerListener)listener).onPause(event);
		} catch (Exception ex) {}
	    }
	}
    }

    /* @see PlayerListener#onResume(PlayerEvent) */ 
    public void onResume(PlayerEvent event) {
	MediaListener listener; 
	Iterator iter = theListeners.iterator();
	while(iter.hasNext()) {
	    listener = (MediaListener)iter.next();
	    if(listener instanceof PlayerListener) {
		try {
		    ((PlayerListener)listener).onResume(event);
		} catch (Exception ex) {}
	    }
	}
    }

    /* @see PlayerListener#onSpeedChange(PlayerEvent) */ 
    public void onSpeedChange(PlayerEvent event) {
	MediaListener listener; 
	Iterator iter = theListeners.iterator();
	while(iter.hasNext()) {
	    listener = (MediaListener)iter.next();
	    if(listener instanceof PlayerListener) {
		try {
		    ((PlayerListener)listener).onSpeedChange(event);
		} catch (Exception ex) {}
	    }
	}
    }

    /* @see PlayerListener#onVolumeChange(PlayerEvent) */ 
    public void onVolumeChange(PlayerEvent event) {
	MediaListener listener; 
	Iterator iter = theListeners.iterator();
	while(iter.hasNext()) {
	    listener = (MediaListener)iter.next();
	    if(listener instanceof PlayerListener) {
		try {
		    ((PlayerListener)listener).onVolumeChange(event);
		} catch (Exception ex) {}
	    }
	}
    }

    /* @see RecorderListener#onPause(RecorderEvent) */ 
    public void onPause(RecorderEvent event) {
	MediaListener listener; 
	Iterator iter = theListeners.iterator();
	while(iter.hasNext()) {
	    listener = (MediaListener)iter.next();
	    if(listener instanceof RecorderListener) {
		try {
		    ((RecorderListener)listener).onPause(event);
		} catch (Exception ex) {}
	    }
	}
    }

    /* @see RecorderListener#onResume(RecorderEvent) */ 
    public void onResume(RecorderEvent event) {
	MediaListener listener; 
	Iterator iter = theListeners.iterator();
	while(iter.hasNext()) {
	    listener = (MediaListener)iter.next();
	    if(listener instanceof RecorderListener) {
		try {
		    ((RecorderListener)listener).onResume(event);
		} catch (Exception ex) {}
	    }
	}
    }

    /* @see SignalDetectorListener#onOverflow(SignalDetectorEvent) */ 
    public void onOverflow(SignalDetectorEvent event) {
	MediaListener listener; 
	Iterator iter = theListeners.iterator();
	while(iter.hasNext()) {
	    listener = (MediaListener)iter.next();
	    if(listener instanceof SignalDetectorListener) {
		try {
		    ((SignalDetectorListener)listener).onOverflow(event);
		} catch (Exception ex) {}
	    }
	}
    }

    /* @see SignalDetectorListener#onPatternMatched(SignalDetectorEvent) */ 
    public void onPatternMatched(SignalDetectorEvent event) {
	MediaListener listener; 
	Iterator iter = theListeners.iterator();
	while(iter.hasNext()) {
	    listener = (MediaListener)iter.next();
	    if(listener instanceof SignalDetectorListener) {
		try {
		    ((SignalDetectorListener)listener).onPatternMatched(event);
		} catch (Exception ex) {}
	    }
	}
    }

    /* @see SignalDetectorListener#onSignalDetected(SignalDetectorEvent) */ 
    public void onSignalDetected(SignalDetectorEvent event) {
	MediaListener listener; 
	Iterator iter = theListeners.iterator();
	while(iter.hasNext()) {
	    listener = (MediaListener)iter.next();
	    if(listener instanceof SignalDetectorListener) {
		try {
		    ((SignalDetectorListener)listener).onSignalDetected(event);
		} catch (Exception ex) {}
	    }
	}
    }

}
