/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

/**
 * Thrown if a resource method is invoked on a MediaService
 * when the corresponding resource is not configured into the MediaService.
 * <p>
 * This typically indicates that the {@link ConfigSpec} used to 
 * {@link MediaService#configure} the MediaService is incorrect; 
 * or the application has otherwise failed
 * to achieve the correct configuration.
 */
public
class NoResourceException extends MediaRuntimeException {
    /**
     * Constructs a <code>NoResourceException</code> 
     * with no specified detail message. 
     */
    public NoResourceException() {super();}

    /**
     * Constructs a <code>NoResourceException</code> 
     * with the specified detail message. 
     *
     * @param   s   the detail message.
     */
    public NoResourceException(String s) {super(s);}
}

