/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

/**
 * Thrown when a resource operation cannot be completed 
 * because the resource is already busy.
 * <p>
 * For example, this is thrown
 * by Player.play() when the Player is busy and (Player.p_IfBusy = v_Fail).
 * 
 */
public
class ResourceBusyException extends MediaResourceException {
    /**
     * Constructs a <code>ResourceBusyException</code> 
     * with no specified detail message. 
     */
    public ResourceBusyException() {super();}

    /**
     * Constructs a <code>ResourceBusyException</code> 
     * with the specified detail message. 
     *
     * @param   s   the detail message.
     */
    public ResourceBusyException(String s) {super(s);}
}
