/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

/** 
 * The class of things that go wrong during Configuration.
 */
public abstract
class MediaConfigException extends MediaServiceException {
    /**
     * Constructs a <code>MediaConfigException</code> 
     * with no specified detail message. 
     */
    public MediaConfigException() {super();}

    /**
     * Constructs a <code>MediaConfigException</code> 
     * with the specified detail message. 
     *
     * @param   s   the detail message.
     */
    public MediaConfigException(String s) {super(s);}

    /**
     * Implementor will fill this in if there is a badResourceSpec.
     * @serial
     */
    protected ResourceSpec badResourceSpec = null;

    /**
     * Return the ResourceSpec that is bad or unknown or unavailable.
     * @return the ResourceSpec that is bad or unknown or unavailable.
     */
    public ResourceSpec getResourceSpec() {
	return badResourceSpec;
    }
}
