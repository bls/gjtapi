/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential,
 * not for public distribution.
 */

package javax.telephony.media;

/*
 * <pre>
 *
 *        CException                          CRuntimeException
 *            |                                   |
 *           CMediaException*                 CMediaRuntimeException
 *            |					  |
 *	      |					  |
 *	      |->CMediaCallException		  |->CDisconnectedException
 *            |             + getCallException	  |->CNoResourceException
 *	      |         	    	     	  |->CNotBoundException
 *	      |         	    	     	  +->CNotOwnerException
 *            |->CMediaResourceException
 *	      |             + getResourceEvent
 *	      |
 *	      +->CMediaServiceException*
 *	          |
 *		  |->CMediaBindException*
 *		  |   |
 *		  |   |->CAlreadyBoundException
 *		  |   |->CBindCancelledException
 *		  |   |->CNoServiceAssignedException
 *		  |   |   -->CBasServiceNameException
 *		  |   +->CNoServiceReadyException
 *		  |
 *		  +->CMediaConfigException*
 *                    |	      +getResourceSpec
 *		      |->CBadConfigSpecException
 *                    |   -->CBadResourceSpecException
 *		      |->CConfigFailedBusyException
 *		      |->CResourceBusyException
 *		      +->CResourceNotSupportedException
 *
 * </pre>
 */
/**
 * MediaException is the parent for [non-Runtime] Exceptions
 * in JTAPI Media.
 */
public abstract class MediaException extends Exception {
    /**
     * Constructs a <code>MediaException</code> 
     * with no specified detail message. 
     */
    public MediaException() {super();}

    /**
     * Constructs a <code>MediaException</code> 
     * with the specified detail message. 
     *
     * @param   s   the detail message.
     */
    public MediaException(String s) {super(s);}
}
