/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;


/**
 * Define the Symbol constants for Coders.
 * <p>
 * These are inherited into the Player and Recorder interfaces.
 * These are used as value for Player.a_Coder or Recorder.a_Coder.
 */
public 
interface CoderConstants {
    /* S.100-R2: */
    //Symbol Class			= ESymbol.Container_Class;
    Symbol v_ADPCM_24k	 		= ESymbol.Container_24kADPCM;
    Symbol v_ADPCM_32k	 		= ESymbol.Container_32kADPCM;
    Symbol v_ADPCM_44k	 		= ESymbol.Container_44kADPCM;
    Symbol v_ALawPCM_48k		= ESymbol.Container_48kALawPCM;
    Symbol v_ALawPCM_64k		= ESymbol.Container_64kALawPCM;
    Symbol v_ALawPCM_88k 		= ESymbol.Container_88kALawPCM;
    Symbol v_MuLawPCM_48k 		= ESymbol.Container_48kMuLawPCM;
    Symbol v_MuLawPCM_64k		= ESymbol.Container_64kMuLawPCM;
    Symbol v_MuLawPCM_88k		= ESymbol.Container_88kMuLawPCM;
    Symbol v_Linear8Bit_48k		= ESymbol.Container_48k8BitLinear;
    Symbol v_Linear8Bit_64k		= ESymbol.Container_64k8BitLinear;
    Symbol v_Linear8Bit_88k		= ESymbol.Container_88k8BitLinear;

    /* S.100-R1:
    //Symbol Class			= ESymbol.Coder_Class;
    Symbol ADPCM_11K			= ESymbol.Coder_11KADPCM;
    Symbol ADPCM_24K			= ESymbol.Coder_24KADPCM;
    Symbol ADPCM_32K			= ESymbol.Coder_32KADPCM;
    Symbol MulawPCM_11K			= ESymbol.Coder_11KMulawPCM;
    Symbol MuLawPCM_48K			= ESymbol.Coder_48KMuLawPCM;
    Symbol MulawPCM_64K			= ESymbol.Coder_64KMulawPCM;
    Symbol ALawPCM_11K			= ESymbol.Coder_11KALawPCM;
    Symbol ALawPCM_48K			= ESymbol.Coder_48KALawPCM;
    Symbol ALawPCM_64K			= ESymbol.Coder_64KALawPCM;
    Symbol Linear8Bit_11K		= ESymbol.Coder_11K8BitLinear;
    Symbol Linear8Bit_24K		= ESymbol.Coder_24K8BitLinear;
    Symbol Linear8Bit_32K		= ESymbol.Coder_32K8BitLinear;
    */

    /* These are for TTS (and there are more)
    Symbol ADSI				= ESymbol.Language_ADSI;
    Symbol VoiceView			= ESymbol.Language_VoiceView;
    Symbol EnglishText			= ESymbol.Language_EnglishText;
    Symbol GermanText			= ESymbol.Language_GermanText;
    Symbol SpanishText			= ESymbol.Language_SpanishText;
    Symbol FrenchText			= ESymbol.Language_FrenchText;
    Symbol DutchText			= ESymbol.Language_DutchText;
    Symbol KoreanText			= ESymbol.Language_KoreanText;
    Symbol AsciiText			= ESymbol.Language_AsciiText;
    Symbol TDD				= ESymbol.Language_TDD;
    */
}
