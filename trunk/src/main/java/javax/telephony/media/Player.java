/*
 * Copyright (c) 1999 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
/**
 * ECTF working group internal, sensitive and confidential, 
 * not for public distribution.
 */

package javax.telephony.media;

import java.util.Dictionary;

/**
 * Defines methods for playing a media stream into a Terminal.
 * A Player extracts data from a media stream container (MSC),
 * transcodes the data as necessary for the telephony network,
 * and streams the resultant data out to that network.
 * <p>
 * The <code>play()</code> method accepts one or an array of Strings
 * which identify the media streams to be played.  Each media stream
 * may be encoded by any coder type supported by this player resource.
 * <p>
 * In addition to the <code>play</code> method, a number of RTC actions
 * affect the operation of the player.  These can be invoked through a
 * Run Time Control {@link RTC}, or directly through the MediaService
 * method {@link MediaService#triggerRTC triggerRTC()}.
 * <p>
 * A Player may optionally support additional processing features.
 * These features define parameters and runtime controls that affect
 * operation of an ongoing play operation.
 * The presence of these features is indicated by a <code>true</code>
 * value for the following attributes:
 * <table border="1" cellpadding="3">
 * <tr><td>Attribute:</td><td>indicates the ability to:</td></tr>
 * <tr><td>a_Pause</td><td>pause and resume in place</td></tr>
 * <tr><td>a_Speed</td><td>change the playback speed</td></tr>
 * <tr><td>a_Volume</td><td>increase and decrease the volume</td></tr>
 * <tr><td>a_Jump</td><td>jump forward and backward while playing</td></tr>
 * </table>
 * Each of these is explained in the <a href="#Attributes">Attributes</a> section.
 * <p>
 * Coding types for data objects are defined in {@link CoderConstants}.
 * <p>
 * The MSC to be played is identified by its String name. 
 * The String name is an identifier that may be interpreted 
 * as a path or URL (URI) for the MSC.
 * <p>
 * <b>Note:</b>
 * The format of the String that identifies 
 * an MSC is not defined by this release of specification.  
 * The vendor-specific implementation of the MediaService 
 * (for example, the vendor of the server or resource)
 * shall define the supported String formats.
 * <p>
 * For example, an implementation may support URLs or File pathnames;
 * in which case an application could use Strings of the form: 
 * <code>"file://node/dir/name"</code> or <code>"dir/dir/name"</code>
 * <p>
 * If the MSC identifier is a file pathname, it is interpreted in the
 * logical filename-space of the <b>server</b>.  
 * Applications and their prompts need to
 * be installed with correct pathnames.
 * <p>
 * <h4>Operation:</h4>
 *
 * <h5>Player States:</h5>
 * The following is the state diagram for a player.
 * <br>
 * <IMG SRC="doc-files/PlayerStates.gif" ALT="States: IDLE<=>ACTIVE<=>PAUSED">
 * <p>
 * The Player has three states: Idle, Active, and Paused.
 * <p>
 * In the Idle state, the Player is performing no coding or
 * transmit operations. The state transitions to the Active
 * or Paused state when the play() method starts. 
 * The state of the Player after play() starts is determined
 * by the Boolean value of the parameter <code>p_StartPaused</code>.
 * The state transitions to Active if <code>p_StartPaused</code> is false.
 * The state transitions to Paused if <code>p_StartPaused</code> is true.
 * <p>
 * In the Paused state, the player is not transmitting the media stream.
 * It retains its place in the MSC being played, and resumes from
 * the same place when the RTC action, <code>rtca_Resume</code> is received. 
 * <p>
 * In the Active state, the Player is busy, actively transmitting the 
 * media stream from a MSC to its output media stream (the Terminal). 
 * The Player continues in this state 
 * until it reaches the end of the MSC list
 * or until
 * RTC actions tell it to pause or stop. 
 * In the Active state, the Player may receive RTC commands to change 
 * the speed or volume of the play operation.
 * It may also receive commands to jump forward or backward in the MSC.
 * <p>
 * <a name="IfBusy"><!-- --></a>
 * If a new play() is attempted while the Player is Active, the result
 * is determined by the value of the <code>p_IfBusy</code> parameter.
 * The alternative values for <code>p_IfBusy</code> are:<ul>
 * <li><code>v_Queue</code>:
 * 	This play is queued (default). It is played 
 * 	when the previous plays have completed.</li>
 * <li><code>v_Stop</code>:
 *	The current play and any queued plays are stopped,
 *	(with event qualifier <code>q_Stop</code>).
 *	This play begins immediately.</li>
 * <li><code>v_Fail</code>:
 *	This play fails, with <code>(getError() == e_Busy)</code></li>
 * </ul>
 * <p>
 * <a name="Attributes"><!-- --></a>
 * <h4>Player Attributes</h4>
 * 
 * <h5>Attribute a_Pause</h5>
 *
 * If the attribute <code>Player.a_Pause</code> is true, 
 * then the Player supports the pause and resume operations.
 * <ul>
 * <li><code>rtca_Pause</code> sets the player state to Paused
 * and {@link PlayerListener#onPause onPause} is invoked if <code>ev_Pause</code>
 * is enabled in <code>p_EnabledEvents</code>.</li>
 * <li><code>rtca_Resume</code> sets the player state to Active,
 * continues to play the stream from it current offset,
 * and {@link PlayerListener#onResume onResume} is invoked if <code>ev_Resume</code>
 * is enabled in <code>p_EnabledEvents</code>.</li>
 * </ul>
 * If the value of attribute <code>a_Pause</code> is false, then 
 * then the effects of the RTC actions, parameters, and methods
 * discussed in this section are not defined.
 * 
 * <h5>Attribute a_Speed</h5>
 * 
 * A MSC has a "normal" speed at which its data is to be played.
 * The RTC actions discussed in this section may change the
 * speed up or down. The playback speed achieved by these actions is
 * called the adjusted speed.
 * <p>
 * The units by which speed may be adjusted are percentage change from
 * the normal speed.
 * If the attribute <code>Player.a_Speed</code> is true, 
 * then the player supports the following features:<ul>
 * <li>
 * The <code>rtca_SpeedUp</code> and <code>rtca_SpeedDown</code>
 * RTC actions cause the playback speed
 * to be adjusted one quantum up or down respectively.
 * </li><li>
 * If the current playback speed is not at the default normal value,
 * the <code>rtca_ToggleSpeed</code> RTC action changes the speed to
 * the normal value.
 * If the current playback speed is at the default normal value, the
 * <code>rtca_ToggleSpeed</code> RTC action changes the speed to the
 * last adjusted value.
 * </li><li>
 * If a RTC action would result in setting the speed beyond
 * the maximum or minimum allowed values, the command/action is ignored.
 * If a RTC action would result in no change to the playback
 * speed (e.g., changing speed to normal when the speed is already normal,
 * toggling the speed when no adjustment had been made to the speed),
 * the action is ignored.
 * </li><li>
 * The event <code>ev_Speed</code> is delivered to 
 * PlayerListener.onSpeedChange. This event
 * may be enabled or disabled by the <code>p_EnabledEvents</code> parameter.
 * </ul>
 * If the value of attribute <code>a_Speed</code> is false, then 
 * then the effects of the RTC actions, parameters, and methods
 * discussed in this section are not defined.
 * 
 * <h5>Attribute a_Volume</h5>
 *
 * A MSC has a "normal" default volume at which its data is to
 * be played. The RTC actions discussed in this section may
 * change the volume up or down. The playback volume achieved by these
 * actions is called the adjusted volume.
 * Local regulatory restrictions may limit the levels to which volume may
 * be adjusted. It is the responsibility for the client application to
 * ensure that it does not adjust the volume so as to exceed those levels. 
 * The units by which volume may be adjusted are measured in dB from the
 * normal volume. 
 * <p>
 * If the value of attribute <code>a_Volume</code> is <code>true</code>
 * then the Player supports the following features:<ul>
 * <li>
 * The <code>rtca_VolumeUp</code> and <code>rtca_VolumeDown</code> 
 * RTC actions cause the playback volume to be adjusted one quantum
 * up or down respectively.
 * The size of the quantum is governed by the parameter 
 * <code>p_VolumeChange</code>, measured in dB from the normal volume.
 * </li><li>
 * If the current playback volume is not at the default normal value, the
 * <code>rtca_ToggleVolume</code> RTC action changes the volume to the 
 * normal value.
 * If the current playback volume is at the default normal value, the
 * <code>rtca_ToggleVolume</code> RTC action changes the volume to the
 * last adjusted value.
 * </li><li>
 * The event <code>ev_Volume</code> is delivered to 
 * PlayerListener.onVolumeChange. This event
 * may be enabled or disabled by the <code>p_EnabledEvents</code> parameter.
 * </li></ul>
 * 
 * If an RTC action would result in no change to the playback
 * volume (e.g., changing volume to normal when the volume is already normal,
 * toggling the volume when no adjustment had been made to the volume),
 * then the command/action is ignored.
 * Likewise, when the volume is already at its maximum or minimum value,
 * RTC actions that attempt to set it beyond the limit are ignored.
 * <p>
 * If the value of attribute <code>a_Volume</code> is false, 
 * then the effects of the RTC actions, parameters, and methods
 * discussed in this section are not defined.
 *
 * <h5> Attribute a_Jump</h5>
 * A Player has a "current location" within a MSC when in the
 * Active or Paused state. The location and the granularity of 
 * changing that location depends on the coder type; being determined
 * by the sample rate and whether the coder is "stateful".
 * For simple Pulse Code Modulation (PCM) coders the location
 * is reported and changed in millisecond units (rounded to the sample rate).
 * <p>
 * If a player resource supports <code>a_Jump</code> attribute, then
 * the following features are supported:<ul>
 * <li>
 * The rtca_JumpForward and rtca_JumpBackward RTC actions change
 * the current location one quantum forward or backward respectively.
 * The quantum is set by the parameter p_JumpTime.
 * </li><li>
 * The rtca_JumpStartMSC and rtca_JumpEndMSC RTC actions change the current
 * location to the start or the end of the current MSC.
 * </li><li>
 * The rtca_JumpForwardMSCs and rtca_JumpBackwardMSCs RTC actions change
 * the current location to the beginning of the MSC that is n MSCs ahead
 * or behind the current MSC in the play list respectively. The value n is
 * set by the parameter p_JumpMSCIncrement.
 * </li><li>
 * The rtca_JumpStartMSCList and rtca_JumpEndMSCList RTC actions change
 * the current location to the beginning or end respectively of the list
 * of MSCs being played.
 * </li></ul>
 * <p>
 * A Jump method or RTC action that goes past the end of the current MSC
 * continues into the next MSC in the MSC list if the
 * coders for the MSCs support the same time increment. If the next
 * MSC in sequence has a different coder type, the current location
 * is set to the beginning of that MSC.
 * <p>
 * If the value of attribute <code>a_Jump</code> is false, 
 * then the effects of the RTC actions, parameters, and methods
 * discussed in this section are not defined.
 */
public 
interface Player extends Resource, PlayerConstants {
    /**
     * Play a sequence of MSCs (Media Stream Containers) identified
     * by the streamIDs. This sequence of MSCs is called the "MSC list".
     * <p>
     * This method returns when the MSC list is completely played, or
     * when the playing has been stopped. 
     * (Player Pause does not cause this method to return.)  
     * Play returns a PlayerEvent.
     * The condition that caused the play to stop is included in the
     * completion event. RTC actions can alter the course of the play
     * without the intervention of the application. 
     * Optional arguments can alter the characteristics of the play.
     * <p>
     * Exceptions are thrown if pre- or post- conditions are not satisfied
     * <p><b>Pre-conditions:</b><ol>
     * <li>a Player resource is configured</li>
     * <li>all MSCs are playable</li>
     * </ol>
     * <p><b>Post-conditions:</b> <ol>
     * <li>As indicated by <code>getQualifier()</code>:<ul>
     *     <li><code>q_EndOfData</code>: All MSCs named in this MSC list 
     *	       have been played.</li>
     *     <li><code>q_Stop</code>: play was stopped by the 
     *         <code>stop()</code> method 
     *         or because (p_IfBusy=v_Stop)</li>
     *     <li><code>q_RTC</code>: play was stopped by 
     *         <code>Player.rtca_Stop</code></li>
     *    </ol>
     * </ol>
     * <p>
     *
     * @param streamIDs a String[] naming the MSC list to be played.
     * @param offset number of milliseconds into the MSC list at 
     * which play is to start, offset may span several items in streamIDs.
     * @param rtc Array of RTC that effect this play.
     * @param optargs a Dictionary of optional arguments.
     * @return a PlayerEvent when the operation is complete (or queued).
     * @throws MediaResourceException if requested operation fails.
     */
    PlayerEvent play(String[] streamIDs,
		     int offset, 
		     RTC[] rtc , 
		     Dictionary optargs)
	throws MediaResourceException;

    /**
     * Play a single MSC (Media Stream Container) named by streamID.
     * Equivalent to <code>play()</code> with a String[] of length one,
     * containing the given streamID.
     *
     * @param streamID a String naming the MSC to be played.
     * @param offset int number milliseconds into the MSC 
     * at which play is to start.
     * @param rtc Array of RTC that effect this play.
     * @param optargs a Dictionary of optional arguments.
     * @return a PlayerEvent when the operation is complete (or queued).
     * @throws MediaResourceException if requested operation fails.
     */
    PlayerEvent play(String streamID,
		     int offset, 
		     RTC[] rtc , 
		     Dictionary optargs) 
	throws MediaResourceException;
}
