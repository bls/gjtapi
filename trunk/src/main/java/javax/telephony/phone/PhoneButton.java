/*
#pragma ident "%W%      %E%     SMI"

 * Copyright (c) 1996 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * and its documentation for NON-COMMERCIAL purposes and without
 * fee is hereby granted provided that this copyright notice
 * appears in all copies. Please refer to the file "copyright.html"
 * for further important copyright and licensing information.
 *
 * SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */

package javax.telephony.phone;
import  javax.telephony.*;
/**
 * Phone button interface.
 *
 */
public interface PhoneButton extends Component {

  /**
   * Returns the button information.
   * <p>
   * @return The string button information.
   */
  public String getInfo();


  /**
   * Sets button information.
   * <p>
   * @param buttonInfo The button information.
   */
  public void setInfo(String buttonInfo);


  /**
   * Returns the associated lamp information.
   * <p>
   * @return The associated lamp object.
   */
  public PhoneLamp getAssociatedPhoneLamp();


  /**
   * Press the button.
   * <p>
   */
  public void buttonPress();
}
